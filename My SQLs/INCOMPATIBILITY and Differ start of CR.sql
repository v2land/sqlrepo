
--###########################################################################################
--#  Start Differ Stop date or requests   (INVESTIGATIOn REQUESTED_DATE differ from STARTED)  
--#########################################################################################
select 
f.concurrent_program_id "conc pr ID", f.concurrent_program_name,f.user_concurrent_program_name
,a.request_id, a.requested_by, u.user_name
,To_Char(a.request_date,'DD-MON-YY HH24:MI:SS') submitted
,To_Char(a.REQUESTED_START_DATE,'DD-MON-YY HH24:MI:SS') REQUESTED_START_DATE
,To_Char(a.ACTUAL_START_DATE,'DD-MON-YY HH24:MI:SS') started
,To_Char(a.ACTUAL_COMPLETION_DATE,'DD-MON-YY HH24:MI:SS') completed
,Extract(day from (NUMTODSINTERVAL((a.ACTUAL_START_DATE - a.REQUESTED_START_DATE)*24*60,'minute')))  "Delayed Days"
,To_Char(TRUNC(sysdate) + NUMTODSINTERVAL((a.ACTUAL_START_DATE - a.REQUESTED_START_DATE)*1440,'minute'),'HH24:MI:SS') "Delayed time"
,a.phase_code, a.status_code, a.HOLD_FLAG, a.argument_text 
,a.RESUBMIT_INTERVAL, a.RESUBMIT_INTERVAL_TYPE_CODE, a.RESUBMIT_INTERVAL_UNIT_CODE
from    
APPLSYS.FND_CONCURRENT_REQUESTS a,
fnd_concurrent_programs_vl f,
fnd_user u 
where 
f.CONCURRENT_PROGRAM_ID=a.CONCURRENT_PROGRAM_ID
and u.user_id = a.requested_by
--and a.status_code = 'C' and a.phase_code = 'C'
--and a.ACTUAL_COMPLETION_DATE> SYSDATE-30
and a.REQUEST_DATE>SYSDATE-30
and a.ACTUAL_START_DATE > (a.REQUESTED_START_DATE + INTERVAL '60' MINUTE)
and a.REQUESTED_START_DATE > a.request_date  --REMOVE BUG of REQUESTED_START_DATE -1 day
--and request_id in ('21654900','21654549','21647375','21646020','21645974')
--and f.concurrent_program_name LIKE 'XXAOL0240_AUTO%'     ---Request Set Archiving AUTO Documents Interface
--and u.user_name = 'XXAPPSADM'
--and a.REQUESTED_START_DATE < a.REQUEST_DATE  -- !!! CHECHK The BUG SR		Severity 4		SR 3-18426826121 : The requested_start_date in the fnd_concurrent_requests is some time one day late
--and f.user_concurrent_program_name like 'Archiving%'
 --AND   a.requested_start_date > TO_DATE('16-NOV-18 05:00:00','DD-MON-YY HH24:MI:SS')
 AND   a.actual_start_date > TO_DATE('29-NOV-18 05:00:00','DD-MON-YY HH24:MI:SS')
-- AND   a.actual_start_date < TO_DATE('30-NOV-18 10:32:11','DD-MON-YY HH24:MI:SS')
 
order by REQUEST_DATE desc;

/*
240448	XXAOL0240_AUTO	Request Set Archiving AUTO Documents Interface	21710127	1429	XXAPPSADM	09-NOV-18 05:13:02	10-NOV-18 05:00:00			P	Q	Y	20005, 2038	1	START	DAYS
*/
--##############################################
--------       INCOMPATIBLES

--Query to find the Concurrent Requests which are incompatible with Request
--input:    Concurrent program name
SELECT distinct fcpt.user_concurrent_program_name Input_Program,
            (SELECT fcpt1.user_concurrent_program_name Incompatible_Programs
                FROM fnd_concurrent_programs_tl   fcpt1
             WHERE fcpt1.concurrent_program_id in (fcps.to_run_concurrent_program_id)
                  AND rownum = 1) Incompatible_Programs
   FROM fnd_concurrent_program_serial fcps
            ,fnd_concurrent_programs_tl       fcpt       
WHERE fcps.running_concurrent_program_id = fcpt.concurrent_program_id
     AND fcpt.user_concurrent_program_name like '%'||:Program_Name||'%';  --XXAOL0240_MANUAL	Request Set --###########################################################################################
      --and fcpt.concurrent_program_id in ('240449') ;
     /*
Request Set Archiving AUTO Documents Interface	Jeu demandes AGR: FACTURATION VALIDATION QUOT. (CRP)
Request Set Archiving AUTO Documents Interface	Request Set AR - Print new invoices (CRP)
Request Set Archiving AUTO Documents Interface	Request Set Archiving AUTO Documents Interface
Request Set Archiving AUTO Documents Interface	Request Set Archiving CLAIMS Documents Interface
Request Set Archiving AUTO Documents Interface	OUT: EDI 882 Sommaire Factures
Request Set Archiving AUTO Documents Interface	Request Set Archiving INVOICE Documents Interface
Request Set Archiving AUTO Documents Interface	Request Set Archiving MANUAL Documents Interface
*/
SELECT * FROM fnd_concurrent_program_serial fcps order by 2 desc;
SELECT * from fnd_concurrent_programs_tl fcpt;  
WHERE fcps.running_concurrent_program_id = fcpt.concurrent_program_id
      AND fcpt.language = USERENV('Lang')
     AND fcpt.user_concurrent_program_name like '%'||:Program_Name||'%';  --Request Set Archiving AUTO Documents Interface   #######################################################################
    --and fcpt.concurrent_program_id in ('240454');
    --and fcps.to_run_concurrent_program_id ='';


SELECT *
   FROM fnd_concurrent_program_serial fcps  ,fnd_concurrent_programs_tl fcpt  
WHERE fcps.running_concurrent_program_id = fcpt.concurrent_program_id
      AND fcpt.language = USERENV('Lang')
     AND fcpt.user_concurrent_program_name like '%'||:Program_Name||'%';  --Request Set Archiving AUTO Documents Interface   #######################################################################
    --and fcpt.concurrent_program_id in ('240454');
    --and fcps.to_run_concurrent_program_id ='';

-- SIMPLE Request set:
SELECT  fcpt.CONCURRENT_PROGRAM_ID,fcpt.user_concurrent_program_name Input_Program,
            fcps.to_run_concurrent_program_id,
                        (SELECT fcpt1.user_concurrent_program_name Incompatible_Programs
                FROM fnd_concurrent_programs_tl   fcpt1
             WHERE fcpt1.concurrent_program_id in (fcps.to_run_concurrent_program_id)
                  AND rownum = 1) Incompatible_Programs
   FROM fnd_concurrent_program_serial fcps
            ,fnd_concurrent_programs_tl       fcpt  
WHERE fcps.running_concurrent_program_id = fcpt.concurrent_program_id
      AND fcpt.language = USERENV('Lang')
     AND fcpt.user_concurrent_program_name like '%'||:Program_Name||'%'  --XXAOL0240_MANUAL	Request Set --###########################################################################################
    --and fcpt.concurrent_program_id in ('240454')
    ;
    
    

-- DRILLED DOWN
SELECT  fcpt.CONCURRENT_PROGRAM_ID,
        DECODE (running_type,
                'P', 'Program',
                'S', 'Request set',
                'UNKNOWN'
                ) "Type",
        fcpt.user_concurrent_program_name Input_Program,
            fcps.to_run_concurrent_program_id,
            DECODE (to_run_type,
                    'P', 'Program',
                    'S', 'Request set',
                    'UNKNOWN'
                    ) incompatible_type,
                        (SELECT fcpt1.user_concurrent_program_name Incompatible_Programs
                FROM fnd_concurrent_programs_tl   fcpt1
             WHERE fcpt1.concurrent_program_id in (fcps.to_run_concurrent_program_id)
                  AND rownum = 1) Incompatible_Programs
   FROM fnd_concurrent_program_serial fcps
            ,fnd_concurrent_programs_tl       fcpt  
WHERE fcps.running_concurrent_program_id = fcpt.concurrent_program_id
      AND fcpt.language = USERENV('Lang')
     AND fcpt.user_concurrent_program_name like '%'||:Program_Name||'%'  --Request Set Archiving AUTO Documents Interface --##############################################
    --and fcpt.concurrent_program_id in ('240449') ;
--AND Sub request sets incompatibility
UNION

SELECT fcpt.CONCURRENT_PROGRAM_ID,
        DECODE (running_type,
                'P', 'Program',
                'S', 'Request set',
                'UNKNOWN'
                ) "Type",
        fcpt.user_concurrent_program_name Input_Program,
            fcps.to_run_concurrent_program_id,            
            DECODE (to_run_type,
                    'P', 'Program',
                    'S', 'Request set',
                    'UNKNOWN'
                    ) incompatible_type,
            (SELECT fcpt1.user_concurrent_program_name Incompatible_Programs
                FROM fnd_concurrent_programs_tl   fcpt1
             WHERE fcpt1.concurrent_program_id in (fcps.to_run_concurrent_program_id)
                  AND rownum = 1) Incompatible_Programs
   FROM fnd_concurrent_program_serial fcps
            ,fnd_concurrent_programs_tl   fcpt  
WHERE  fcps.running_concurrent_program_id = fcpt.concurrent_program_id
    AND fcpt.language = USERENV('Lang')
    and fcpt.concurrent_program_id in 
                (SELECT  fcps.to_run_concurrent_program_id
                       FROM fnd_concurrent_program_serial fcps
                                ,fnd_concurrent_programs_tl       fcpt  
                    WHERE fcps.running_concurrent_program_id = fcpt.concurrent_program_id
                         AND fcpt.user_concurrent_program_name like '%'||:Program_Name||'%' 
                        AND fcpt.language = USERENV('Lang')
                
                ) 
order by 1,3;

     
/*
240448	Request Set Archiving AUTO Documents Interface	240408	OUT: EDI 882 Sommaire Factures
240448	Request Set Archiving AUTO Documents Interface	240448	Request Set Archiving AUTO Documents Interface
240448	Request Set Archiving AUTO Documents Interface	240449	Request Set Archiving CLAIMS Documents Interface
240448	Request Set Archiving AUTO Documents Interface	240450	Request Set Archiving INVOICE Documents Interface
240448	Request Set Archiving AUTO Documents Interface	240451	Request Set Archiving MANUAL Documents Interface
240448	Request Set Archiving AUTO Documents Interface	240456	Jeu demandes AGR: FACTURATION VALIDATION QUOT. (CRP)
240448	Request Set Archiving AUTO Documents Interface	240458	Request Set AR - Print new invoices (CRP)
*/
--###########################################################################################
-- ###############           ALL INCOMPATIBILITIES    ###################################--
--###########################################################################################
--How to check  Incompatibilities in concurrent requests

SELECT a2.application_name, 
DECODE (running_type,
'P', 'Program',
'S', 'Request set',
'UNKNOWN'
) "Type",
a1.user_concurrent_program_name,
a1.CONCURRENT_PROGRAM_ID CONC_PR_ID,
b1.CONCURRENT_PROGRAM_ID Incomp_CONC_PR_ID,
b1.user_concurrent_program_name "Incompatible_Prog",
DECODE (to_run_type,
'P', 'Program',
'S', 'Request set',
'UNKNOWN'
) incompatible_type,
b2.application_name "Incompatible App"
FROM apps.fnd_concurrent_program_serial cps,
apps.fnd_concurrent_programs_tl a1,
apps.fnd_concurrent_programs_tl b1,
apps.fnd_application_tl a2,
apps.fnd_application_tl b2
WHERE a1.application_id = cps.running_application_id
AND a1.concurrent_program_id = cps.running_concurrent_program_id
AND a2.application_id = cps.running_application_id
AND b1.application_id = cps.to_run_application_id
AND b1.concurrent_program_id = cps.to_run_concurrent_program_id
AND b2.application_id = cps.to_run_application_id
AND a1.language = 'US'
AND a2.language = 'US'
AND b1.language = 'US'
AND b2.language = 'US'
and a1.user_concurrent_program_name like '%Archiving AUTO Documents Interface%'
order by 4,5;


SELECT request_id, status_code, phase_code
from fnd_concurrent_requests
WHERE request_id = '21710127';


--Query to find the Concurrent Requests which are incompatible with Request set and all child Programs of SET
--input:    Concurrent program name
SELECT distinct fcpt.user_concurrent_program_name Input_Program,
            (SELECT fcpt1.user_concurrent_program_name Incompatible_Programs
                FROM fnd_concurrent_programs_tl   fcpt1
                 WHERE fcpt1.concurrent_program_id in (fcps.to_run_concurrent_program_id)
                  AND rownum = 1) Incompatible_Programs
   FROM fnd_concurrent_program_serial fcps
            ,fnd_concurrent_programs_tl       fcpt       
WHERE fcps.running_concurrent_program_id = fcpt.concurrent_program_id
     --AND fcpt.user_concurrent_program_name like '%'||:Program_Name||'%';  --XXAOL0240_MANUAL	Request Set --###########################################################################################
    and
    (fcpt.concurrent_program_id in (select CONCURRENT_PROGRAM_ID from fnd_concurrent_requests where request_id = :Request_Id)
        or fcpt.concurrent_program_id in (
                                        select sepr.CONCURRENT_PROGRAM_ID Child_CP_ID
                                        from
                                        fnd_concurrent_requests re,
                                        fnd_concurrent_programs pr,
                                        fnd_request_sets se,
                                        fnd_request_set_programs sepr
                                        where
                                        request_id = :Request_Id   --21962768 top parent Request_ID 
                                        and re.CONCURRENT_PROGRAM_ID=pr.CONCURRENT_PROGRAM_ID
                                        and pr.CONCURRENT_PROGRAM_ID=se.CONCURRENT_PROGRAM_ID
                                        and se.REQUEST_SET_ID = sepr.REQUEST_SET_ID
                                        )
     )
;



----------------- connect_by syntax -----------------------------
SELECT cat.CONCURRENT_PROGRAM_ID, items.CONCURRENT_PROGRAM_ID
  FROM fnd_concurrent_programs_tl cat JOIN fnd_concurrent_programs_tl items ON cat.CONCURRENT_PROGRAM_ID = items.CONCURRENT_PROGRAM_ID
 START WITH cat.CONCURRENT_PROGRAM_ID = 1  -- ID of the starting category
 CONNECT BY PRIOR item.to_run_concurrent_program_id = cat.running_concurrent_program_id;
 
 
 SELECT to_run_concurrent_program_id , running_concurrent_program_id
FROM fnd_concurrent_program_serial 
  START WITH to_run_concurrent_program_id = 240448 
  CONNECT BY PRIOR to_run_concurrent_program_id = running_concurrent_program_id;

 SELECT 
       t1.running_concurrent_program_id parent_id,
       t1.to_run_concurrent_program_id id, 
       --t1.name,
       t2.to_run_concurrent_program_id AS parent_id
       --t2.name AS parent_name,
        FROM fnd_concurrent_program_serial t1 LEFT JOIN fnd_concurrent_program_serial t2 ON t1.running_concurrent_program_id = t2.to_run_concurrent_program_id 
START WITH t1.to_run_concurrent_program_id = '240448' 
CONNECT BY PRIOR t1.to_run_concurrent_program_id = t1.running_concurrent_program_id;

--################################
SELECT *
   FROM fnd_concurrent_program_serial fcps  ,fnd_concurrent_programs_tl fcpt  
WHERE fcps.running_concurrent_program_id = fcpt.concurrent_program_id
      AND fcpt.language = USERENV('Lang')
     AND fcpt.user_concurrent_program_name like '%'||:Program_Name||'%';  --Request Set Archiving AUTO Documents Interface
     
----------Details of Incompatibility Dependency of Concurrent programs:
--  using connect by prior

SELECT *
FROM   (SELECT /*+ index (fcr1 fnd_concurrent_requests_n3) */ fcr1.request_id
        FROM   apps.fnd_concurrent_requests fcr1
        WHERE  1 = 1
        START WITH fcr1.request_id = 21998750
        CONNECT BY PRIOR fcr1.request_id = fcr1.parent_request_id) x,
       apps.fnd_concurrent_requests fcr,
       apps.fnd_concurrent_programs fcp,
       apps.fnd_concurrent_programs_tl fcptl
WHERE  fcr.request_id = x.request_id
       AND fcr.concurrent_program_id = fcp.concurrent_program_id
       AND fcr.program_application_id = fcp.application_id
       AND fcp.application_id = fcptl.application_id
       AND fcp.concurrent_program_id = fcptl.concurrent_program_id
       AND fcptl.LANGUAGE = 'US'

ORDER  BY fcr.actual_start_date;

--21962768	21962768	19-NOV-18	21971	18-NOV-18	1429	P	Q	21962768	50	19-NOV-18	N	Y	N	N	N	N	N	B	S	900	20005	240448	20005	51833	2	0	Y	N	16602885	AMERICAN	CANADA	noprint	PORTRAIT	N			21941034	16589305		Archiving AUTO Documents Interface		1	DAYS	START			N														20005, 2038	20005	2038																								-1	19-NOV-18		M					TEXT	0	2385519				0	N				N		N	36					0					-1		20965370		.,									N	BINARY	ORA$BASE	20005	240448	XXAOL0240_AUTO	08-NOV-18	1471	20-DEC-17	7318	0	0	117	I	4	N	B	Y	Y	Y	N	N				Y	N	PORTRAIT																					2282	TEXT	N	Y	N						N							N				Y						SET1	INSERTED	20005	240448	US	Request Set Archiving AUTO Documents Interface	20-DEC-17	7318	08-NOV-18	1471	0	Archiving AUTO Documents Interface	US	SET1	INSERTED



--#####################################################################
-- Request Set execution programs,
--  with connect_by syntax

SELECT /*+ index (fcr1 fnd_concurrent_requests_n3) */ fcr1.request_id
        FROM   apps.fnd_concurrent_requests fcr1
        WHERE  1 = 1
        START WITH fcr1.request_id = 21962768-- 21962768 --21984686
        CONNECT BY PRIOR fcr1.request_id = fcr1.parent_request_id;

--######################################################################
----------Details Drilldown of a Concurrent Request Set that IS Running or WAS Executed:
--  using connect by prior


SELECT /*+ ORDERED USE_NL(x fcr fcp fcptl)*/
                                fcr.request_id
                                "Request ID",
                                             fcptl.user_concurrent_program_name
                                "Program Name"
                                ,
                                fcr.phase_code,
                                fcr.status_code,
                                --     to_char(fcr.request_date,'DD-MON-YYYY HH24:MI:SS') "Submitted", 
                                --     (fcr.actual_start_date - fcr.request_date)*1440 "Delay",
                                To_char(fcr.actual_start_date,
                                'DD-MON-YYYY HH24:MI:SS')      "Start Time",
                                To_char(fcr.actual_completion_date,
                                'DD-MON-YYYY HH24:MI:SS') "End Time",
                                --( fcr.actual_completion_date - fcr.actual_start_date ) * 1440 "Elapsed",
                                TO_CHAR ( TRUNC(sysdate) + NUMTODSINTERVAL ((fcr.actual_completion_date - fcr.actual_start_date)*24*60,'minute'),'HH24:MI:SS') "Process_TIME",
                                fcr.oracle_process_id
                                "Trace ID"
FROM   (SELECT /*+ index (fcr1 fnd_concurrent_requests_n3) */ fcr1.request_id
        FROM   apps.fnd_concurrent_requests fcr1
        WHERE  1 = 1
        START WITH fcr1.request_id = 21962768
        CONNECT BY PRIOR fcr1.request_id = fcr1.parent_request_id) x,
       apps.fnd_concurrent_requests fcr,
       apps.fnd_concurrent_programs fcp,
       apps.fnd_concurrent_programs_tl fcptl
WHERE  fcr.request_id = x.request_id
       AND fcr.concurrent_program_id = fcp.concurrent_program_id
       AND fcr.program_application_id = fcp.application_id
       AND fcp.application_id = fcptl.application_id
       AND fcp.concurrent_program_id = fcptl.concurrent_program_id
       AND fcptl.LANGUAGE = 'US'
       and fcptl.user_concurrent_program_name != 'Request Set Stage'  --

ORDER  BY fcr.actual_start_date;




--# -- Qu'est-ce que a roull� entre  08-NOV-18 05:45:00	08-NOV-18 10:39:16   (INVESTIGATIOn REQUESTED_DATE differ from STARTED)
--#########################################################################################
select 
f.concurrent_program_id "conc pr ID", f.concurrent_program_name,f.user_concurrent_program_name
,a.request_id, a.requested_by, u.user_name
,To_Char(a.request_date,'DD-MON-YY HH24:MI:SS') submitted
,To_Char(a.REQUESTED_START_DATE,'DD-MON-YY HH24:MI:SS') REQUESTED_START_DATE
,To_Char(a.ACTUAL_START_DATE,'DD-MON-YY HH24:MI:SS') started
,To_Char(a.ACTUAL_COMPLETION_DATE,'DD-MON-YY HH24:MI:SS') completed
,a.phase_code, a.status_code, a.HOLD_FLAG, a.argument_text, 
a.RESUBMIT_INTERVAL, a.RESUBMIT_INTERVAL_TYPE_CODE, a.RESUBMIT_INTERVAL_UNIT_CODE
from    
APPLSYS.FND_CONCURRENT_REQUESTS a,
fnd_concurrent_programs_vl f,
fnd_user u 
where 
f.CONCURRENT_PROGRAM_ID=a.CONCURRENT_PROGRAM_ID
and u.user_id = a.requested_by
--and a.status_code = 'C' and a.phase_code = 'C'
--and a.ACTUAL_COMPLETION_DATE> SYSDATE-30
and a.REQUEST_DATE>SYSDATE-1
--and request_id in ('21654900','21654549','21647375','21646020','21645974')
--and f.concurrent_program_name LIKE 'XXAOL0240%'
--and u.user_name = 'XXAPPSADM'
--and a.REQUESTED_START_DATE > to_date('08-NOV-18 05:45:00','DD-MON-YY HH24:MI:SS')
--and a.ACTUAL_START_DATE < to_date('08-NOV-18 10:39:16','DD-MON-YY HH24:MI:SS')
and f.user_concurrent_program_name in
(SELECT  (SELECT fcpt1.user_concurrent_program_name Incompatible_Programs
                FROM fnd_concurrent_programs_tl   fcpt1
             WHERE fcpt1.concurrent_program_id in (fcps.to_run_concurrent_program_id)
                  AND rownum = 1) Incompatible_Programs
  FROM fnd_concurrent_program_serial fcps
      ,fnd_concurrent_programs_tl       fcpt       
  WHERE fcps.running_concurrent_program_id = fcpt.concurrent_program_id
    -- AND fcpt.user_concurrent_program_name like '%'||:Program_Name||'%'  --XXAOL0240_MANUAL	Request Set Archiving MANUAL Documents Interface --Archiving e-Commerce invoice file
        and fcpt.CONCURRENT_PROGRAM_ID in
        ( 
        SELECT (SELECT fcpt1.CONCURRENT_PROGRAM_ID Incompatible_Programs_ID
                FROM fnd_concurrent_programs_tl   fcpt1
             WHERE fcpt1.concurrent_program_id in (fcps.to_run_concurrent_program_id)
                  AND rownum = 1) Incompatible_Programs_ID
        FROM fnd_concurrent_program_serial fcps
            ,fnd_concurrent_programs_tl       fcpt       
        WHERE fcps.running_concurrent_program_id = fcpt.concurrent_program_id
              AND fcpt.user_concurrent_program_name like '%'||:Program_Name||'%'  --XXAOL0240_MANUAL	Request Set --###########################################################################################
         )   
     )
order by 4 desc; --MANUAL Documents Interface
     

--###########################################################################################
--# -- Qu'est-ce que a roull� entre  08-NOV-18 05:45:00	08-NOV-18 10:39:16   (INVESTIGATIOn REQUESTED_DATE differ from STARTED)
--#########################################################################################
SELECT
    f.concurrent_program_id "conc pr ID",
    f.concurrent_program_name,
    f.user_concurrent_program_name,
    a.request_id,
    a.requested_by,
    u.user_name,
    TO_CHAR(a.request_date,'DD-MON-YY HH24:MI:SS') submitted,
    TO_CHAR(a.requested_start_date,'DD-MON-YY HH24:MI:SS') requested_start_date,
    TO_CHAR(a.actual_start_date,'DD-MON-YY HH24:MI:SS') started,
    TO_CHAR(a.actual_completion_date,'DD-MON-YY HH24:MI:SS') completed,
    a.phase_code,
    a.status_code,
    a.hold_flag,
    a.argument_text,
    a.resubmit_interval,
    a.resubmit_interval_type_code,
    a.resubmit_interval_unit_code
FROM
    applsys.fnd_concurrent_requests a,
    fnd_concurrent_programs_vl f,
    fnd_user u
WHERE
    f.concurrent_program_id = a.concurrent_program_id
    AND   u.user_id = a.requested_by
--and a.status_code = 'C' and a.phase_code = 'C'
--and a.ACTUAL_COMPLETION_DATE> SYSDATE-30
    AND   a.request_date > SYSDATE - 1
--and request_id in ('21654900','21654549','21647375','21646020','21645974')
and f.concurrent_program_name LIKE 'XXAOL%'
--and u.user_name = 'XXAPPSADM'
 --   AND   a.requested_start_date > TO_DATE('08-NOV-18 05:00:00','DD-MON-YY HH24:MI:SS')
   -- AND   a.actual_start_date < TO_DATE('08-NOV-18 10:39:16','DD-MON-YY HH24:MI:SS')
    AND   f.user_concurrent_program_name IN (
        SELECT
            (
                SELECT
                    fcpt1.user_concurrent_program_name incompatible_programs
                FROM
                    fnd_concurrent_programs_tl fcpt1
                WHERE
                    fcpt1.concurrent_program_id IN (
                        fcps.to_run_concurrent_program_id
                    )
                    AND   ROWNUM = 1
            ) incompatible_programs
        FROM
            fnd_concurrent_program_serial fcps,
            fnd_concurrent_programs_tl fcpt
        WHERE
            fcps.running_concurrent_program_id = fcpt.concurrent_program_id
            AND   fcpt.user_concurrent_program_name LIKE '%'
            ||:program_name
            || '%'  --XXAOL0240_MANUAL	Request Set Archiving MANUAL Documents Interface
    )
ORDER BY
    8;


-- Check Locked Session and Concurrent Request in EBS R12
select
b.process " Process id ",
b.osuser,
b.machine,
b.sid,
b.serial#,
b.schemaname,
b.module,
b.action,
trunc(b.SECONDS_IN_WAIT/60/60,3) " Waiting Time in H ",
blocking_session,
'*** KILL ONLY IF BLOCKING_SESSION IS NULL, otherwise trace that blocking session till has null *** -- alter system kill session '''||b.sid||','||b.SERIAL#||''' immediate;' " SQL kill session statement" ,
'*** KILL ONLY IF BLOCKING_SESSION IS NULL, otherwise trace that blocking session till has null --- kill -9 '||b.process " OS kill process command",
b.PADDR,
p.SPID,
b.PROCESS
from v$session b , v$process p
where b.PADDR=p.ADDR and b.sid in (select BLOCKING_SESSION from v$session s where  BLOCKING_SESSION is not null);
