--R12: AP: Performance Issue With Invoice Workbench (APXINWKB) After Database Upgrade (Doc ID 2063670.1)  : 

--Solution

--1) Open the Payables Invoice workbench and search for invoice number.
--2) Run below query and make sure the query as below.
            select * from v$sql where sql_id='22uqxsfcjj7zf';
 --invoice number = DBC38240 
 select invoice_id, invoice_num, org_id, i.* from ap_invoices_all i where invoice_num = '76820546496'; --'DBC38240';
    --INVOICE_ID; INVOICE_NUM; ORG_ID
    --50000;	76820546496; 	123 
    
            SELECT COUNT (1) FROM AP_INVOICE_DISTRIBUTIONS_ALL AID WHERE AID.INVOICE_ID = :b1 AND AID.ORG_ID = :b2 AND AID.POSTED_FLAG = 'Y' AND ROWNUM = 1;
            --0
--3) Run below query and note down the values for executing Step 4.
            SELECT address, hash_value, PLAN_HASH_VALUE FROM v$sqlarea WHERE sql_id = '22uqxsfcjj7zf';
            --07000100D4794450	2568527854
--4) Execute below from database sysdba user. (sqlplus / as sysdba)
       exec DBMS_SHARED_POOL.PURGE ('07000100D4794450, 2568527854', 'c');
          --  exec sys.dbms_shared_pool.purge(' &address, & hash_value','c');
          --  exec sys.dbms_shared_pool.purge('C0000004FEC8A028, 2568527854','c');
          
--5) Run below query and make sure there is no data returned.
            SELECT sql_text, address, hash_value, executions FROM v$sqlarea WHERE sql_id = '22uqxsfcjj7zf';
            	-- Check againt it should be clean :
  select ADDRESS, HASH_VALUE, PLAN_HASH_VALUE, MODULE, ELAPSED_TIME, DISK_READS, USER_IO_WAIT_TIME, SQL_TEXT, sql_profile  from v$sql where sql_id = '22uqxsfcjj7zf' order by ELAPSED_TIME desc;

--6) Execute the SQL Command
            alter system set "_fix_control"='14127824:OFF';
--7) Open the Invoice workbench and search for invoice number.
           --Make sure performance issue is resolved.
--8) Run below query and note down the values for step 9.
            SELECT sql_id, plan_hash_value FROM v$sql WHERE sql_id = '22uqxsfcjj7zf';
--9) Then load the query using the SQL_ID and good PLAN_HASH_VALUE
    -- Execute below steps at SQL Prompt.
            var res number ;
            exec :res := dbms_spm.load_plans_from_cursor_cache(sql_id => '&sql_id',
            plan_hash_value => '&plan_hash_value' );
---10) Execute the SQL Command :
            alter system set "_fix_control"='14127824:ON';
--11) Open the Invoice workbench and search for invoice number.
            -- Make sure performance issue is resolved.