DECLARE

i number;

--dev6
--avant
-- 387231 notif
-- 186788 count 
-- apres 
-- 0 count
-- 200443 notif
 CURSOR c1 is
     SELECT *-- wi.item_type, wi.item_key, wpa.instance_label, wi.parent_item_type,WIAS.ACTIVITY_STATUS
    FROM APPLSYS.WF_ITEMS wi,
      APPLSYS.WF_ITEM_ACTIVITY_STATUSES wias,
      APPLSYS.Wf_Process_Activities wpa
    WHERE wi.item_type       = wias.item_type
    AND wi.item_key          = wias.item_key
    AND wias.process_activity = wpa.instance_id
    AND wi.item_type         = 'WFERROR'
    AND wi.parent_item_type     in ('POAPPRV')
--    AND WIAS.ACTIVITY_STATUS = 'ERROR'
--    AND wias.ASSIGNED_USER = 'OLMANNA'
    and wi.ITEM_KEY = 'WF2203992'
    ;
    
/* OMERROR
SELECT wi.item_type, wi.item_key, wpa.instance_label, wi.parent_item_type,WIAS.ACTIVITY_STATUS
    FROM APPLSYS.WF_ITEMS wi,
      APPLSYS.WF_ITEM_ACTIVITY_STATUSES wias,
      APPLSYS.Wf_Process_Activities wpa
    WHERE wi.item_type       = wias.item_type
    AND wi.item_key          = wias.item_key
    AND wias.process_activity = wpa.instance_id
    AND wi.item_type         = 'OMERROR'
    AND wi.parent_item_type     in ('OEOL', 'OEOH')
    AND WIAS.ACTIVITY_STATUS = 'NOTIFIED'
    ;
*/
select *
from wf_notifications
--where item_key = 'WF1679130';
where notification_id = 4175820;

/*
SELECT RoutingRulesEO.rule_id,
  RoutingRulesEO.message_type,
  RoutingRulesEO.message_name,
  RoutingRulesEO.begin_date,
  RoutingRulesEO.end_date,
  RoutingRulesEO.action,
  RoutingRulesEO.action_argument,
  ItemTypesEO.display_name AS TYPE_DISPLAY,
  MessagesEO.display_name  AS MSG_DISPLAY,
  MessagesEO.subject,
  LookupsEO.meaning AS ACTION_DISPLAY,
  ItemTypesEO.name,
  MessagesEO.TYPE,
  MessagesEO.name AS NAME1,
  LookupsEO.lookup_type,
  LookupsEO.lookup_code
FROM apps.wf_routing_rules RoutingRulesEO,
  apps.wf_item_types_vl ItemTypesEO,
  apps.wf_messages_vl MessagesEO,
  apps.wf_lookups LookupsEO
WHERE RoutingRulesEO.message_type = ItemTypesEO.name (+)
AND RoutingRulesEO.message_type   = MessagesEO.TYPE (+)
AND RoutingRulesEO.message_name   = MessagesEO.name (+)
AND RoutingRulesEO.action         = LookupsEO.lookup_code
AND LookupsEO.lookup_type         = 'WFSTD_ROUTING_ACTIONS'
AND RoutingRulesEO.end_date      IS NULL
ORDER BY type_display,
  msg_display,
  begin_date
*/
--update wf_routing_rules
--set end_date = sysdate
--where end_date is null;


BEGIN

-- OMERROR	WF1761292	NOTIFY	OEOH
   i := 0;
  for v in c1
  loop
    wf_engine.completeactivityinternalname(itemtype => v.item_type, itemkey => v.item_key, activity => v.instance_label, result => 'RETRY', raise_engine_exception => FALSE);

      -- Commit after processing 100,000 records
      i:= i+1;

      if i = 100000 then
        commit;
        dbms_output.put_line('committed '|| i||' items');
        i := 0;
      end if;


  end loop;

  commit;

END;
/