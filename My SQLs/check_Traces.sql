--########      PERFORMANCE MONITORING TOOLS and CHECK UP:     ########--

------    Query to find Trace Enabled Programs in Oracle EBS  ----

-----              ON CONCURRENT PROGRAMS                     ----
SELECT
    a.concurrent_program_name "Program Name",
    substr(a.user_concurrent_program_name,1,40) "User Program Name",
    substr(b.user_name,1,15) "Last Updated By",
    substr(b.description,1,25) description, a.LAST_UPDATE_DATE, b.*
FROM
    apps.fnd_concurrent_programs_vl a,
    applsys.fnd_user b
WHERE
    a.enable_trace = 'Y'
    AND   a.last_updated_by = b.user_id;
    
    
SELECT
    req.request_id
    ,req.logfile_node_name node
    ,req.oracle_Process_id
    ,req.enable_trace
    ,dest.VALUE||'/'||LOWER(dbnm.VALUE)||'_ora_'||oracle_process_id||'.trc' trace_filename
    ,prog.user_concurrent_program_name
    ,execname.execution_file_name
    ,execname.subroutine_name
    ,phase_code
    ,status_code
    ,ses.SID
    ,ses.serial#
    ,ses.module
    ,ses.machine
    FROM
    fnd_concurrent_requests req
    ,v$session ses
    ,v$process proc
    ,v$parameter dest
    ,v$parameter dbnm
    ,fnd_concurrent_programs_vl prog
    ,fnd_executables execname
    WHERE 1=1
    AND req.request_id = &request --Request ID
    AND req.oracle_process_id=proc.spid(+)
    AND proc.addr = ses.paddr(+)
    AND dest.NAME='user_dump_dest'
    AND dbnm.NAME='db_name'
    AND req.concurrent_program_id = prog.concurrent_program_id
    AND req.program_application_id = prog.application_id
    AND prog.application_id = execname.application_id
    AND prog.executable_id=execname.executable_id;
    
    
-- Check Profiles: 
/*
FND: Debug Log Enabled = YES 
FND: Debug Log Filename for Middle-Tier = NULL 
FND: Debug Log Level = STATEMENT 
FND: Debug Log Mode = Synchronus 
FND: Debug Log Module = % 


*/
select p.profile_option_name SHORT_NAME,
n.user_profile_option_name NAME,
decode(v.level_id,
10001, 'Site',
10002, 'Application',
10003, 'Responsibility',
10004, 'User',
10005, 'Server',
10006, 'Org',
10007, decode(to_char(v.level_value2), '-1', 'Responsibility',
decode(to_char(v.level_value), '-1', 'Server',
'Server+Resp')),
'UnDef') LEVEL_SET,
decode(to_char(v.level_id),
'10001', '',
'10002', app.application_short_name,
'10003', rsp.responsibility_key,
'10004', usr.user_name,
'10005', svr.node_name,
'10006', org.name,
'10007', decode(to_char(v.level_value2), '-1', rsp.responsibility_key,
decode(to_char(v.level_value), '-1',
(select node_name from fnd_nodes
where node_id = v.level_value2),
(select node_name from fnd_nodes
where node_id = v.level_value2)||'-'||rsp.responsibility_key)),
'UnDef') "CONTEXT",
v.profile_option_value VALUE,
p.ZD_EDITION_NAME
from fnd_profile_options p,
fnd_profile_option_values v,
fnd_profile_options_tl n,
fnd_user usr,
fnd_application app,
fnd_responsibility rsp,
fnd_nodes svr,
hr_operating_units org
where p.profile_option_id = v.profile_option_id (+)
and p.profile_option_name = n.profile_option_name
and upper(p.profile_option_name) in ( select profile_option_name
from fnd_profile_options_tl 
where 
upper(user_profile_option_name) like upper('%FND: Debug Log%')) --External ADF Application URL
--upper(profile_option_name) in  ('FND_EXTERNAL_ADF_URL','MSC_ASCP_WEBLOGIC_URL')) -- like upper('%&Profile_SHORT_NAME%'))--External ADF Application URL
and usr.user_id (+) = v.level_value
and rsp.application_id (+) = v.level_value_application_id
and rsp.responsibility_id (+) = v.level_value
and app.application_id (+) = v.level_value
and svr.node_id (+) = v.level_value
and org.organization_id (+) = v.level_value
order by short_name, user_profile_option_name, level_id, level_set;