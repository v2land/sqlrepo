--How to update XML Publisher Temp directory
select value from apps.XDO_CONFIG_VALUES WHERE  property_code = 'SYSTEM_TEMP_DIR';

-- VALUE
--------------------------------------------------------------------------------
--/app/xxxPROD/inst/appltmp


update apps.XDO_CONFIG_VALUES set value='/UEBSDEV9_Apps/fs_ne/EBSapps/comn/temp' WHERE  property_code = 'SYSTEM_TEMP_DIR';

-- 1 row updated.
 commit;

-- Commit complete.

select value from apps.XDO_CONFIG_VALUES WHERE  property_code = 'SYSTEM_TEMP_DIR';

 --VALUE
--------------------------------------------------------------------------------
--/app/XXXDEV/inst/appltmp

