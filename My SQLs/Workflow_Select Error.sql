----APINVHDN ERROR STUCK investigation Get the Details of the list of all Errored Workflow Activities for a given item type/ item key
--And Compare with existing Payed Invoices (Hold Count 0)
With TMP AS ( Select distinct i.item_type,iav.number_value invoice_id, i.item_key,ias.activity_status 
,ias.activity_result_code
           FROM wf_item_activity_statuses ias,
                wf_process_activities     pa,
                wf_activities_vl          ac,
                wf_activities_vl          ap,
                wf_items                  i,
                wf_item_attribute_values iav
          WHERE ias.item_type = 'APINVHDN'
            --AND ias.item_key = '541980'
            AND ias.process_activity = pa.instance_id
            AND pa.activity_name = ac.name
            AND pa.activity_item_type = ac.item_type
            AND pa.process_name = ap.name
            AND pa.process_item_type = ap.item_type
            AND pa.process_version = ap.version
            AND i.item_type = 'APINVHDN'
            AND i.item_key = ias.item_key
            AND i.begin_date >= ac.begin_date
            AND i.begin_date < nvl(ac.end_date,  i.begin_date + 1)
            and ias.activity_status = 'ERROR'
         and iav.ITEM_TYPE = 'APINVHDN'-- 'APINVHDN'-- POAPPRV  item_key = '1216705-468541'
         and iav.ITEM_KEY = ias.ITEM_KEY
         and iav.name||'' = 'INVOICE_ID'
         order by invoice_id)
Select  item_type,item_key,ai.invoice_num, ai.INVOICE_DATE, ai.INVOICE_AMOUNT, ai.INVOICE_CURRENCY_CODE,
        (Select sup.VENDOR_NAME from ap_suppliers sup Where sup.VENDOR_ID = ai.vendor_id) vendor,
        ai.DESCRIPTION,
        (Select count(*) from ap_holds_all ha Where ha.RELEASE_LOOKUP_CODE is null and ha.INVOICE_ID = ai.INVOICE_ID) hold_count,
        (Select listagg (po.SEGMENT1, ' , ') within group (order by po.segment1) from ap_invoice_lines_all ail, po_headers_all po where ail.invoice_id = ai.invoice_id and po.po_header_id = ail.PO_HEADER_ID ) po_order_num
,activity_status 
,activity_result_code
  from tmp,
       ap_invoices_all ai
  Where ai.INVOICE_ID = tmp.invoice_id
    and ai.CANCELLED_DATE is null
  order by invoice_num;
  --591
  -- item-key: 552265 retry
--550271
-->"Retry Errored Workflow Activities" Concurrent Request : 
--31
--ex: APINVHDN 503123

-------------------------------
--Get the error messages of the list of all Errored Workflow Activities for a given item type/ item key
SELECT i.item_type,i.item_key,
       i.begin_date,
        i.end_date,
       ias.execution_time,
       ac.display_name          activity,
       ias.activity_result_code RESULT,
       ias.error_name           error_name,
       ias.error_message        error_message,
       ias.error_stack          error_stack
  FROM wf_item_activity_statuses ias,
       wf_process_activities     pa,
       wf_activities_vl          ac,
       wf_activities_vl          ap,
       wf_items                  i
 WHERE ias.item_type = 'APINVHDN'--'POAPPRV'
  --AND ias.item_key = '550271'
   AND ias.activity_status = 'ERROR'
   AND ias.process_activity = pa.instance_id
   AND pa.activity_name = ac.name
   AND pa.activity_item_type = ac.item_type
   AND pa.process_name = ap.name
   AND pa.process_item_type = ap.item_type
   AND pa.process_version = ap.version
   AND i.item_type = 'APINVHDN'-- 'APINVHDN'
   AND i.item_key = ias.item_key
   AND i.begin_date >= ac.begin_date
   AND i.begin_date < nvl(ac.end_date,
                          i.begin_date + 1)
    --and  i.end_date is NULL
 ORDER BY ias.BEGIN_DATE
;
-- Investigation ORA-20215: Un approbateur non valide a �t� d�tect� lors du traitement ame_engine.fetchRuntimeGroup:XXAGR_AP_HOLDS_RESOLUTION_APPROVER_GROUP. 
-- Veuillez supprimer l'approbateur non valide. Syst�me d'origine de l'approbateur non valide : person_id et ID syst�me d'origine : 2539.
--check  Status - Active and End_date of Person id 
select * from per_people_f where person_id in ('1777','2539');

select ppf.employee_number, ppf.full_name, fu.user_name, wr.status, fu.END_DATE, fu.last_logon_date, orig_system_id, fu.EMAIL_ADDRESS
from per_people_f ppf,
fnd_user fu,
wf_roles wr
where ppf.person_id =fu.employee_id (+)
and ppf.person_id =wr.orig_system_id (+)
--and ppf.current_employee_flag='Y'
and ppf.person_id in ('1777','2539')
;

--select * from wf_roles where orig_system_id = '2539';--email_address='Alberto.Fontova@XXXX.com';
--select * from wf_per_role_roles where orig_system_id=2539;
select * from fnd_user where email_address='Scott.Stephens@XXXXX.com';--
select * from wf_roles where orig_system_id=64001;

--======================================
--17-10-16	192	Avis de blocage qt� rec de BC rapproch�	#EXCEPTION	WFNTF_ROLE	3205: 'JESASSEV' is not a valid role or user name. 	"
---Wf_Notification.Send(JESASSEV, APINVHDN, POMATCHED_QTYREC_HLD_NOTIF_MSG, 25-NOV-17, WF_ENGINE.CB)
--Wf_Engine_Util.Notification_Send(APINVHDN, 570010, 344030, APINVHDN:POMATCHED_QTYREC_HLD_NOTIF_MSG)
--Wf_Engine_Util.Notification(APINVHDN, 570010, 344030, RUN)"

--17-11-14	1463	Obtenir l'approbateur	#EXCEPTION	-20215	ORA-20215: An invalid approver has been encountered in ame_engine.fetchRuntimeGroup:XXAGR_AP_HOLDS_RESOLUTION_APPROVER_GROUP. Please remove the invalid approver. Invalid approver originating system person_id and originating system ID 1777.	"
--AME_APPROVER_TYPE_PKG.getWfRolesName(1094854)
--AME_ENGINE.fetchRuntimeGroup(1094855)
--AME_ENGINE.getRuntimeGroupMembers(1094856)
--AME_AG_CHAIN_HANDLER.handler(1094857)
--AME_ENGINE.processActionType(1094858)
--AME_ENGINE.processRules(1094859)
---AME_ENGINE.updateTransactionState(1094860)
--AME_ENGINE.getNextApprovers(1094861)
--AME_API2.getNextApprovers4(1094862)
--AME_API.getNextApprover(1094863)
--APINVHDN.GET_APPROVER(APINVHDN, 593147, 344012, RUN)
--Wf_Engine_Util.Function_Call(XXSQLAP0090_AP_WORKFLOW_PKG.get_approver, APINVHDN, 593147, 344012, RUN)"

--prompt **** Find the Activity Statuses for all workflow activities of a given item type and item key
SELECT execution_time,
       to_char(ias.begin_date,
               'DD-MON-RR HH24:MI:SS') begin_date,
       ap.display_name || '/' || ac.display_name activity,
       ias.activity_status status,
       ias.activity_result_code RESULT,
       ias.assigned_user ass_user
  FROM wf_item_activity_statuses ias,
       wf_process_activities     pa,
       wf_activities_vl          ac,
       wf_activities_vl          ap,
       wf_items                  i
 WHERE ias.item_type = 'APINVHDN'
   AND ias.item_key = '550304'
   AND ias.process_activity = pa.instance_id
   AND pa.activity_name = ac.name
   AND pa.activity_item_type = ac.item_type
   AND pa.process_name = ap.name
   AND pa.process_item_type = ap.item_type
   AND pa.process_version = ap.version
   AND i.item_type = 'APINVHDN'
   AND i.item_key = ias.item_key
   AND i.begin_date >= ac.begin_date
   AND i.begin_date < nvl(ac.end_date,
                          i.begin_date + 1)
UNION ALL
SELECT execution_time,
       to_char(ias.begin_date,
               'DD-MON-RR HH24:MI:SS') begin_date,
       ap.display_name || '/' || ac.display_name activity,
       ias.activity_status status,
       ias.activity_result_code RESULT,
       ias.assigned_user ass_user
  FROM wf_item_activity_statuses_h ias,
       wf_process_activities       pa,
       wf_activities_vl            ac,
       wf_activities_vl            ap,
       wf_items                    i
 WHERE ias.item_type = 'APINVHDN'
   AND ias.item_key = '550304'
   AND ias.process_activity = pa.instance_id
   AND pa.activity_name = ac.name
   AND pa.activity_item_type = ac.item_type
   AND pa.process_name = ap.name
   AND pa.process_item_type = ap.item_type
   AND pa.process_version = ap.version
   AND i.item_type = 'APINVHDN'
   AND i.item_key = ias.item_key
   AND i.begin_date >= ac.begin_date
   AND i.begin_date < nvl(ac.end_date,
                          i.begin_date + 1)
 ORDER BY 2,
          1
;
--===--=============================













-- Details of Mailer Notification of one specific item_key and item_type
 select  wn.notification_id nid, 
        wn.context, 
        wn.group_id, 
        wn.status, 
        wn.mail_status, 
        wn.message_type, 
        wn.message_name, 
        wn.access_key, 
        wn.priority, 
        wn.begin_date, 
        wn.end_date, 
        wn.due_date, 
        wn.callback, 
        wn.recipient_role, 
        wn.responder, 
        wn.original_recipient, 
        wn.from_user, 
        wn.to_user, 
        wn.subject 
from    wf_notifications wn, wf_item_activity_statuses wias 
where  wn.group_id = wias.notification_id 
and  wias.item_type = 'POAPPRV'
--and  wias.item_key = '1216705-468541'
and wn.MAIL_STATUS not in ('SENT','MAIL')
and wn.STATUS='OPEN'
;

  select item_type, item_key, root_activity, owner_role, begin_date from wf_items where item_type='POAPPRV' and item_key in (
select item_key from wf_notifications
where message_name='AME_PO_HAS_BEEN_APPROVED' and status='OPEN')
order by begin_date;

--69
--Ex: POAPPRV	1201577-418874	PO_AME_APPRV_TOP	CAHAMEL	17-09-10

 
 
 
--  ========APINVHDN=========
 select  wn.notification_id nid, 
        wn.context, 
        wn.group_id, 
        wn.status, 
        wn.mail_status, 
        wn.message_type, 
        wn.message_name, 
        wn.access_key, 
        wn.priority, 
        wn.begin_date, 
        wn.end_date, 
        wn.due_date, 
        wn.callback, 
        wn.recipient_role, 
        wn.responder, 
        wn.original_recipient, 
        wn.from_user, 
        wn.to_user, 
        wn.subject 
from    wf_notifications wn, wf_item_activity_statuses wias 
where  wn.group_id = wias.notification_id 
and  wias.item_type = 'APINVHDN'
and  wias.item_key = '550271'
;
  select item_type, item_key, root_activity, owner_role, begin_date from wf_items where item_type='APINVHDN' and item_key in (
select item_key from wf_notifications
where message_name='POMATCHED_QTYREC_HLD_NOTIF_MSG' and status='OPEN')
order by begin_date;

--69
--Ex: POAPPRV	1201577-418874	PO_AME_APPRV_TOP	CAHAMEL	17-09-10




