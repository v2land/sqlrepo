SELECT
    sql_handle,
    plan_name,
    TRIM(substr(g.plan_table_output,instr(g.plan_table_output,':') + 1) ) plan_hash_value,
    LAST_EXECUTED,CREATED, enabled,accepted,fixed,
    sql_text
FROM
    ( SELECT
            t.*,
            c.sql_handle,
            c.plan_name,
            To_Char(LAST_EXECUTED,'DD-MON-YY HH24:MI:SS') LAST_EXECUTED,
            To_Char(CREATED,'DD-MON-YY HH24:MI:SS') CREATED, enabled,accepted,fixed,
            c.sql_text
        FROM
            dba_sql_plan_baselines c,
            TABLE ( dbms_xplan.display_sql_plan_baseline(c.sql_handle,c.plan_name) ) t
/*where c.sql_handle = '&sql_handle'*/
    ) g
WHERE    plan_table_output LIKE 'Plan hash value%';


select * from TABLE(dbms_xplan.display_awr('194dm4z2yp21c'));

set serveroutput on
begin
 for c in (select address,hash_value,users_executing,sql_text from v$sqlarea where sql_id='&sql_id') 
 loop 
  dbms_output.put_line(c.users_executing||' users executing '||c.sql_text);
  sys.dbms_shared_pool.purge(c.address||','||c.hash_value,'...'); 
  dbms_output.put_line('flushed.');
 end loop;
end;
/


-- Show the Plan Hash Values for a Given SQLID Over a Given Period
--
 
SELECT DISTINCT sql_id, plan_hash_value
FROM dba_hist_sqlstat q,
    (
    SELECT /*+ NO_MERGE */ MIN(snap_id) min_snap, MAX(snap_id) max_snap
    FROM dba_hist_snapshot ss
    WHERE ss.begin_interval_time BETWEEN (SYSDATE - &No_Days) AND SYSDATE
    ) s
WHERE q.snap_id BETWEEN s.min_snap AND s.max_snap
  AND q.sql_id IN ( '194dm4z2yp21c')
/


SELECT SNAP_ID, BEGIN_INTERVAL_TIME, END_INTERVAL_TIME FROM dba_hist_snapshot ORDER BY END_INTERVAL_TIME DESC;