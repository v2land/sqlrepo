-- Locate Concurrent Program using specific Tbale:

SELECT USER_concurrent_program_name  FROM fnd_concurrent_programs_tl 
where 
concurrent_program_id in(SELECT concurrent_program_id  
                        FROM fnd_concurrent_programs 
                        where executable_id in(SELECT executable_id --Check Executables
                                                FROM fnd_executables 
                                                where UPPER(substr(execution_file_name,1,instr(execution_file_name,'.')-1)) in  --like ('%XXOZF0240%')
                                                                                (with TAB_REF as   -- Check Packages                            
                                                                                    (SELECT name, referenced_name ref_table
                                                                                    FROM dba_dependencies
                                                                                    WHERE referenced_TYPE = 'TABLE' and UPPER(referenced_name) like UPPER('XXONT1100_REASSGN_DIST_ORD_STG%'))
                                                                                select dd.name from   DBA_dependencies dd, TAB_REF tr where 
                                                                                type like 'PACKAGE%'
                                                                                and tr.name=dd.referenced_name)
                                              )
                        )
AND language = userenv('LANG');

select distinct referenced_name 
from   user_dependencies
where  referenced_type = 'TABLE'
and    type like 'PACKAGE%';


-- Locate all tables used by a specific Concurrent Program:

