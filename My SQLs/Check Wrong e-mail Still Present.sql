-- check fnd_concurrent_request
select 
   request_id,
   REGEXP_REPLACE(argument1,  '\w+([[:alnum:]+[:punct:][:alnum:]+])+@([[:alnum:][:punct:]]\w+)+', 'EBSDEVC'||'_APPSTEST@agropur.com') "clone_argument1", argument1,
   REGEXP_REPLACE(argument2,  '\w+([[:alnum:]+[:punct:][:alnum:]+])+@([[:alnum:][:punct:]]\w+)+', 'EBSDEVC'||'_APPSTEST@agropur.com') "clone_argument2", argument2,
   REGEXP_REPLACE(argument3,  '\w+([[:alnum:]+[:punct:][:alnum:]+])+@([[:alnum:][:punct:]]\w+)+', 'EBSDEVC'||'_APPSTEST@agropur.com') "clone_argument3", argument3,
   REGEXP_REPLACE(argument4,  '\w+([[:alnum:]+[:punct:][:alnum:]+])+@([[:alnum:][:punct:]]\w+)+', 'EBSDEVC'||'_APPSTEST@agropur.com') "clone_argument4", argument4,
   REGEXP_REPLACE(argument5,  '\w+([[:alnum:]+[:punct:][:alnum:]+])+@([[:alnum:][:punct:]]\w+)+', 'EBSDEVC'||'_APPSTEST@agropur.com') "clone_argument5", argument5,
   REGEXP_REPLACE(argument6,  '\w+([[:alnum:]+[:punct:][:alnum:]+])+@([[:alnum:][:punct:]]\w+)+', 'EBSDEVC'||'_APPSTEST@agropur.com') "clone_argument6", argument6,
   REGEXP_REPLACE(argument7,  '\w+([[:alnum:]+[:punct:][:alnum:]+])+@([[:alnum:][:punct:]]\w+)+', 'EBSDEVC'||'_APPSTEST@agropur.com') "clone_argument7", argument7,
   REGEXP_REPLACE(argument8,  '\w+([[:alnum:]+[:punct:][:alnum:]+])+@([[:alnum:][:punct:]]\w+)+', 'EBSDEVC'||'_APPSTEST@agropur.com') "clone_argument8", argument8,
   REGEXP_REPLACE(argument9,  '\w+([[:alnum:]+[:punct:][:alnum:]+])+@([[:alnum:][:punct:]]\w+)+', 'EBSDEVC'||'_APPSTEST@agropur.com') "clone_argument9", argument9,
   REGEXP_REPLACE(argument10, '\w+([[:alnum:]+[:punct:][:alnum:]+])+@([[:alnum:][:punct:]]\w+)+', 'EBSDEVC'||'_APPSTEST@agropur.com') "clone_argument10",argument10,
   REGEXP_REPLACE(argument11, '\w+([[:alnum:]+[:punct:][:alnum:]+])+@([[:alnum:][:punct:]]\w+)+', 'EBSDEVC'||'_APPSTEST@agropur.com') "clone_argument11",argument11,
   REGEXP_REPLACE(argument12, '\w+([[:alnum:]+[:punct:][:alnum:]+])+@([[:alnum:][:punct:]]\w+)+', 'EBSDEVC'||'_APPSTEST@agropur.com') "clone_argument12",argument12,
   REGEXP_REPLACE(argument13, '\w+([[:alnum:]+[:punct:][:alnum:]+])+@([[:alnum:][:punct:]]\w+)+', 'EBSDEVC'||'_APPSTEST@agropur.com') "clone_argument13",argument13,
   REGEXP_REPLACE(argument14, '\w+([[:alnum:]+[:punct:][:alnum:]+])+@([[:alnum:][:punct:]]\w+)+', 'EBSDEVC'||'_APPSTEST@agropur.com') "clone_argument14",argument14,
   REGEXP_REPLACE(argument15, '\w+([[:alnum:]+[:punct:][:alnum:]+])+@([[:alnum:][:punct:]]\w+)+', 'EBSDEVC'||'_APPSTEST@agropur.com') "clone_argument15",argument15,
   REGEXP_REPLACE(argument16, '\w+([[:alnum:]+[:punct:][:alnum:]+])+@([[:alnum:][:punct:]]\w+)+', 'EBSDEVC'||'_APPSTEST@agropur.com') "clone_argument16",argument16,
   REGEXP_REPLACE(argument17, '\w+([[:alnum:]+[:punct:][:alnum:]+])+@([[:alnum:][:punct:]]\w+)+', 'EBSDEVC'||'_APPSTEST@agropur.com') "clone_argument17",argument17,
   REGEXP_REPLACE(argument18, '\w+([[:alnum:]+[:punct:][:alnum:]+])+@([[:alnum:][:punct:]]\w+)+', 'EBSDEVC'||'_APPSTEST@agropur.com') "clone_argument18",argument18,
   REGEXP_REPLACE(argument19, '\w+([[:alnum:]+[:punct:][:alnum:]+])+@([[:alnum:][:punct:]]\w+)+', 'EBSDEVC'||'_APPSTEST@agropur.com') "clone_argument19",argument19,
   REGEXP_REPLACE(argument20, '\w+([[:alnum:]+[:punct:][:alnum:]+])+@([[:alnum:][:punct:]]\w+)+', 'EBSDEVC'||'_APPSTEST@agropur.com') "clone_argument20",argument20,
   REGEXP_REPLACE(argument21, '\w+([[:alnum:]+[:punct:][:alnum:]+])+@([[:alnum:][:punct:]]\w+)+', 'EBSDEVC'||'_APPSTEST@agropur.com') "clone_argument21",argument21,
   REGEXP_REPLACE(argument22, '\w+([[:alnum:]+[:punct:][:alnum:]+])+@([[:alnum:][:punct:]]\w+)+', 'EBSDEVC'||'_APPSTEST@agropur.com') "clone_argument22",argument22,
   REGEXP_REPLACE(argument23, '\w+([[:alnum:]+[:punct:][:alnum:]+])+@([[:alnum:][:punct:]]\w+)+', 'EBSDEVC'||'_APPSTEST@agropur.com') "clone_argument23",argument23,
   REGEXP_REPLACE(argument24, '\w+([[:alnum:]+[:punct:][:alnum:]+])+@([[:alnum:][:punct:]]\w+)+', 'EBSDEVC'||'_APPSTEST@agropur.com') "clone_argument24",argument24,
   REGEXP_REPLACE(argument25, '\w+([[:alnum:]+[:punct:][:alnum:]+])+@([[:alnum:][:punct:]]\w+)+', 'EBSDEVC'||'_APPSTEST@agropur.com') "clone_argument25",argument25,
   concurrent_program_id
from fnd_concurrent_requests
where  REGEXP_LIKE(
          argument1 || 
          argument2 || 
          argument3 || 
          argument4 || 
          argument5 || 
          argument6 || 
          argument7 || 
          argument8 || 
          argument9 || 
          argument10 || 
          argument11 || 
          argument12 || 
          argument13 || 
          argument14 || 
          argument15 || 
          argument16 || 
          argument17 || 
          argument18 || 
          argument19 || 
          argument20 ||
          argument21 || 
          argument22 || 
          argument23 || 
          argument24 || 
          argument25 , 
          '@')
and  not REGEXP_LIKE(
          argument1 || 
          argument2 || 
          argument3 || 
          argument4 || 
          argument5 || 
          argument6 || 
          argument7 || 
          argument8 || 
          argument9 || 
          argument10 || 
          argument11 || 
          argument12 || 
          argument13 || 
          argument14 || 
          argument15 || 
          argument16 || 
          argument17 || 
          argument18 || 
          argument19 || 
          argument20 ||
          argument21 || 
          argument22 || 
          argument23 || 
          argument24 || 
          argument25 , 
          'EBSDEVC'||'_APPSTEST@agropur.com');


--======fnd_run_request============
select 
   request_id,
 --  REGEXP_REPLACE(argument1,  '^[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[A-Za-z]{2,4}$', 'EBSTEST'||'_APPSTEST@agropur.com') "clone_argument1", argument1,
REGEXP_REPLACE(argument1, '\w+([[:alnum:]+[:punct:][:alnum:]+])+@([[:alnum:][:punct:]]\w+)+', 'EBSTEST'||'_APPSTEST@agropur.com') "clone_argument1",argument1,
REGEXP_REPLACE(argument2, '\w+([[:alnum:]+[:punct:][:alnum:]+])+@([[:alnum:][:punct:]]\w+)+', 'EBSTEST'||'_APPSTEST@agropur.com') "clone_argument2",argument2,
REGEXP_REPLACE(argument3, '\w+([[:alnum:]+[:punct:][:alnum:]+])+@([[:alnum:][:punct:]]\w+)+', 'EBSTEST'||'_APPSTEST@agropur.com') "clone_argument3",argument3,
REGEXP_REPLACE(argument4, '\w+([[:alnum:]+[:punct:][:alnum:]+])+@([[:alnum:][:punct:]]\w+)+', 'EBSTEST'||'_APPSTEST@agropur.com') "clone_argument4",argument4,
REGEXP_REPLACE(argument5, '\w+([[:alnum:]+[:punct:][:alnum:]+])+@([[:alnum:][:punct:]]\w+)+', 'EBSTEST'||'_APPSTEST@agropur.com') "clone_argument5",argument5,
REGEXP_REPLACE(argument6, '\w+([[:alnum:]+[:punct:][:alnum:]+])+@([[:alnum:][:punct:]]\w+)+', 'EBSTEST'||'_APPSTEST@agropur.com') "clone_argument6",argument6,
REGEXP_REPLACE(argument7, '\w+([[:alnum:]+[:punct:][:alnum:]+])+@([[:alnum:][:punct:]]\w+)+', 'EBSTEST'||'_APPSTEST@agropur.com') "clone_argument7",argument7,
REGEXP_REPLACE(argument8, '\w+([[:alnum:]+[:punct:][:alnum:]+])+@([[:alnum:][:punct:]]\w+)+', 'EBSTEST'||'_APPSTEST@agropur.com') "clone_argument8",argument8,
REGEXP_REPLACE(argument9, '\w+([[:alnum:]+[:punct:][:alnum:]+])+@([[:alnum:][:punct:]]\w+)+', 'EBSTEST'||'_APPSTEST@agropur.com') "clone_argument9",argument9,
REGEXP_REPLACE(argument10, '\w+([[:alnum:]+[:punct:][:alnum:]+])+@([[:alnum:][:punct:]]\w+)+', 'EBSTEST'||'_APPSTEST@agropur.com') "clone_argument10",argument10,
REGEXP_REPLACE(argument11, '\w+([[:alnum:]+[:punct:][:alnum:]+])+@([[:alnum:][:punct:]]\w+)+', 'EBSTEST'||'_APPSTEST@agropur.com') "clone_argument11",argument11,
REGEXP_REPLACE(argument12, '\w+([[:alnum:]+[:punct:][:alnum:]+])+@([[:alnum:][:punct:]]\w+)+', 'EBSTEST'||'_APPSTEST@agropur.com') "clone_argument12",argument12,
REGEXP_REPLACE(argument13, '\w+([[:alnum:]+[:punct:][:alnum:]+])+@([[:alnum:][:punct:]]\w+)+', 'EBSTEST'||'_APPSTEST@agropur.com') "clone_argument13",argument13,
REGEXP_REPLACE(argument14, '\w+([[:alnum:]+[:punct:][:alnum:]+])+@([[:alnum:][:punct:]]\w+)+', 'EBSTEST'||'_APPSTEST@agropur.com') "clone_argument14",argument14,
REGEXP_REPLACE(argument15, '\w+([[:alnum:]+[:punct:][:alnum:]+])+@([[:alnum:][:punct:]]\w+)+', 'EBSTEST'||'_APPSTEST@agropur.com') "clone_argument15",argument15,
REGEXP_REPLACE(argument16, '\w+([[:alnum:]+[:punct:][:alnum:]+])+@([[:alnum:][:punct:]]\w+)+', 'EBSTEST'||'_APPSTEST@agropur.com') "clone_argument16",argument16,
REGEXP_REPLACE(argument17, '\w+([[:alnum:]+[:punct:][:alnum:]+])+@([[:alnum:][:punct:]]\w+)+', 'EBSTEST'||'_APPSTEST@agropur.com') "clone_argument17",argument17,
REGEXP_REPLACE(argument18, '\w+([[:alnum:]+[:punct:][:alnum:]+])+@([[:alnum:][:punct:]]\w+)+', 'EBSTEST'||'_APPSTEST@agropur.com') "clone_argument18",argument18,
REGEXP_REPLACE(argument19, '\w+([[:alnum:]+[:punct:][:alnum:]+])+@([[:alnum:][:punct:]]\w+)+', 'EBSTEST'||'_APPSTEST@agropur.com') "clone_argument19",argument19,
REGEXP_REPLACE(argument20, '\w+([[:alnum:]+[:punct:][:alnum:]+])+@([[:alnum:][:punct:]]\w+)+', 'EBSTEST'||'_APPSTEST@agropur.com') "clone_argument20",argument20,
REGEXP_REPLACE(argument21, '\w+([[:alnum:]+[:punct:][:alnum:]+])+@([[:alnum:][:punct:]]\w+)+', 'EBSTEST'||'_APPSTEST@agropur.com') "clone_argument21",argument21,
REGEXP_REPLACE(argument22, '\w+([[:alnum:]+[:punct:][:alnum:]+])+@([[:alnum:][:punct:]]\w+)+', 'EBSTEST'||'_APPSTEST@agropur.com') "clone_argument22",argument22,
REGEXP_REPLACE(argument23, '\w+([[:alnum:]+[:punct:][:alnum:]+])+@([[:alnum:][:punct:]]\w+)+', 'EBSTEST'||'_APPSTEST@agropur.com') "clone_argument23",argument23,
REGEXP_REPLACE(argument24, '\w+([[:alnum:]+[:punct:][:alnum:]+])+@([[:alnum:][:punct:]]\w+)+', 'EBSTEST'||'_APPSTEST@agropur.com') "clone_argument24",argument24,
REGEXP_REPLACE(argument25, '\w+([[:alnum:]+[:punct:][:alnum:]+])+@([[:alnum:][:punct:]]\w+)+', 'EBSTEST'||'_APPSTEST@agropur.com') "clone_argument25",argument25,
REGEXP_REPLACE(argument26, '\w+([[:alnum:]+[:punct:][:alnum:]+])+@([[:alnum:][:punct:]]\w+)+', 'EBSTEST'||'_APPSTEST@agropur.com') "clone_argument26",argument26,
REGEXP_REPLACE(argument27, '\w+([[:alnum:]+[:punct:][:alnum:]+])+@([[:alnum:][:punct:]]\w+)+', 'EBSTEST'||'_APPSTEST@agropur.com') "clone_argument27",argument27,
REGEXP_REPLACE(argument28, '\w+([[:alnum:]+[:punct:][:alnum:]+])+@([[:alnum:][:punct:]]\w+)+', 'EBSTEST'||'_APPSTEST@agropur.com') "clone_argument28",argument28,
REGEXP_REPLACE(argument29, '\w+([[:alnum:]+[:punct:][:alnum:]+])+@([[:alnum:][:punct:]]\w+)+', 'EBSTEST'||'_APPSTEST@agropur.com') "clone_argument29",argument29,
REGEXP_REPLACE(argument30, '\w+([[:alnum:]+[:punct:][:alnum:]+])+@([[:alnum:][:punct:]]\w+)+', 'EBSTEST'||'_APPSTEST@agropur.com') "clone_argument30",argument30,
REGEXP_REPLACE(argument31, '\w+([[:alnum:]+[:punct:][:alnum:]+])+@([[:alnum:][:punct:]]\w+)+', 'EBSTEST'||'_APPSTEST@agropur.com') "clone_argument31",argument31,
REGEXP_REPLACE(argument32, '\w+([[:alnum:]+[:punct:][:alnum:]+])+@([[:alnum:][:punct:]]\w+)+', 'EBSTEST'||'_APPSTEST@agropur.com') "clone_argument32",argument32,
REGEXP_REPLACE(argument33, '\w+([[:alnum:]+[:punct:][:alnum:]+])+@([[:alnum:][:punct:]]\w+)+', 'EBSTEST'||'_APPSTEST@agropur.com') "clone_argument33",argument33,
REGEXP_REPLACE(argument34, '\w+([[:alnum:]+[:punct:][:alnum:]+])+@([[:alnum:][:punct:]]\w+)+', 'EBSTEST'||'_APPSTEST@agropur.com') "clone_argument34",argument34,
REGEXP_REPLACE(argument35, '\w+([[:alnum:]+[:punct:][:alnum:]+])+@([[:alnum:][:punct:]]\w+)+', 'EBSTEST'||'_APPSTEST@agropur.com') "clone_argument35",argument35,
REGEXP_REPLACE(argument36, '\w+([[:alnum:]+[:punct:][:alnum:]+])+@([[:alnum:][:punct:]]\w+)+', 'EBSTEST'||'_APPSTEST@agropur.com') "clone_argument36",argument36,
REGEXP_REPLACE(argument37, '\w+([[:alnum:]+[:punct:][:alnum:]+])+@([[:alnum:][:punct:]]\w+)+', 'EBSTEST'||'_APPSTEST@agropur.com') "clone_argument37",argument37,
REGEXP_REPLACE(argument38, '\w+([[:alnum:]+[:punct:][:alnum:]+])+@([[:alnum:][:punct:]]\w+)+', 'EBSTEST'||'_APPSTEST@agropur.com') "clone_argument38",argument38,
REGEXP_REPLACE(argument39, '\w+([[:alnum:]+[:punct:][:alnum:]+])+@([[:alnum:][:punct:]]\w+)+', 'EBSTEST'||'_APPSTEST@agropur.com') "clone_argument39",argument39,
REGEXP_REPLACE(argument40, '\w+([[:alnum:]+[:punct:][:alnum:]+])+@([[:alnum:][:punct:]]\w+)+', 'EBSTEST'||'_APPSTEST@agropur.com') "clone_argument40",argument40,
REGEXP_REPLACE(argument41, '\w+([[:alnum:]+[:punct:][:alnum:]+])+@([[:alnum:][:punct:]]\w+)+', 'EBSTEST'||'_APPSTEST@agropur.com') "clone_argument41",argument41,
REGEXP_REPLACE(argument42, '\w+([[:alnum:]+[:punct:][:alnum:]+])+@([[:alnum:][:punct:]]\w+)+', 'EBSTEST'||'_APPSTEST@agropur.com') "clone_argument42",argument42,
REGEXP_REPLACE(argument43, '\w+([[:alnum:]+[:punct:][:alnum:]+])+@([[:alnum:][:punct:]]\w+)+', 'EBSTEST'||'_APPSTEST@agropur.com') "clone_argument43",argument43,
REGEXP_REPLACE(argument44, '\w+([[:alnum:]+[:punct:][:alnum:]+])+@([[:alnum:][:punct:]]\w+)+', 'EBSTEST'||'_APPSTEST@agropur.com') "clone_argument44",argument44,
REGEXP_REPLACE(argument45, '\w+([[:alnum:]+[:punct:][:alnum:]+])+@([[:alnum:][:punct:]]\w+)+', 'EBSTEST'||'_APPSTEST@agropur.com') "clone_argument45",argument45,
REGEXP_REPLACE(argument46, '\w+([[:alnum:]+[:punct:][:alnum:]+])+@([[:alnum:][:punct:]]\w+)+', 'EBSTEST'||'_APPSTEST@agropur.com') "clone_argument46",argument46,
REGEXP_REPLACE(argument47, '\w+([[:alnum:]+[:punct:][:alnum:]+])+@([[:alnum:][:punct:]]\w+)+', 'EBSTEST'||'_APPSTEST@agropur.com') "clone_argument47",argument47,
REGEXP_REPLACE(argument48, '\w+([[:alnum:]+[:punct:][:alnum:]+])+@([[:alnum:][:punct:]]\w+)+', 'EBSTEST'||'_APPSTEST@agropur.com') "clone_argument48",argument48,
REGEXP_REPLACE(argument49, '\w+([[:alnum:]+[:punct:][:alnum:]+])+@([[:alnum:][:punct:]]\w+)+', 'EBSTEST'||'_APPSTEST@agropur.com') "clone_argument49",argument49,
REGEXP_REPLACE(argument50, '\w+([[:alnum:]+[:punct:][:alnum:]+])+@([[:alnum:][:punct:]]\w+)+', 'EBSTEST'||'_APPSTEST@agropur.com') "clone_argument50",argument50,
REGEXP_REPLACE(argument51, '\w+([[:alnum:]+[:punct:][:alnum:]+])+@([[:alnum:][:punct:]]\w+)+', 'EBSTEST'||'_APPSTEST@agropur.com') "clone_argument51",argument51,
REGEXP_REPLACE(argument52, '\w+([[:alnum:]+[:punct:][:alnum:]+])+@([[:alnum:][:punct:]]\w+)+', 'EBSTEST'||'_APPSTEST@agropur.com') "clone_argument52",argument52,
REGEXP_REPLACE(argument53, '\w+([[:alnum:]+[:punct:][:alnum:]+])+@([[:alnum:][:punct:]]\w+)+', 'EBSTEST'||'_APPSTEST@agropur.com') "clone_argument53",argument53,
REGEXP_REPLACE(argument54, '\w+([[:alnum:]+[:punct:][:alnum:]+])+@([[:alnum:][:punct:]]\w+)+', 'EBSTEST'||'_APPSTEST@agropur.com') "clone_argument54",argument54,
REGEXP_REPLACE(argument55, '\w+([[:alnum:]+[:punct:][:alnum:]+])+@([[:alnum:][:punct:]]\w+)+', 'EBSTEST'||'_APPSTEST@agropur.com') "clone_argument55",argument55,
REGEXP_REPLACE(argument56, '\w+([[:alnum:]+[:punct:][:alnum:]+])+@([[:alnum:][:punct:]]\w+)+', 'EBSTEST'||'_APPSTEST@agropur.com') "clone_argument56",argument56,
REGEXP_REPLACE(argument57, '\w+([[:alnum:]+[:punct:][:alnum:]+])+@([[:alnum:][:punct:]]\w+)+', 'EBSTEST'||'_APPSTEST@agropur.com') "clone_argument57",argument57,
REGEXP_REPLACE(argument58, '\w+([[:alnum:]+[:punct:][:alnum:]+])+@([[:alnum:][:punct:]]\w+)+', 'EBSTEST'||'_APPSTEST@agropur.com') "clone_argument58",argument58,
REGEXP_REPLACE(argument59, '\w+([[:alnum:]+[:punct:][:alnum:]+])+@([[:alnum:][:punct:]]\w+)+', 'EBSTEST'||'_APPSTEST@agropur.com') "clone_argument59",argument59,
REGEXP_REPLACE(argument60, '\w+([[:alnum:]+[:punct:][:alnum:]+])+@([[:alnum:][:punct:]]\w+)+', 'EBSTEST'||'_APPSTEST@agropur.com') "clone_argument60",argument60,
REGEXP_REPLACE(argument61, '\w+([[:alnum:]+[:punct:][:alnum:]+])+@([[:alnum:][:punct:]]\w+)+', 'EBSTEST'||'_APPSTEST@agropur.com') "clone_argument61",argument61,
REGEXP_REPLACE(argument62, '\w+([[:alnum:]+[:punct:][:alnum:]+])+@([[:alnum:][:punct:]]\w+)+', 'EBSTEST'||'_APPSTEST@agropur.com') "clone_argument62",argument62,
REGEXP_REPLACE(argument63, '\w+([[:alnum:]+[:punct:][:alnum:]+])+@([[:alnum:][:punct:]]\w+)+', 'EBSTEST'||'_APPSTEST@agropur.com') "clone_argument63",argument63,
REGEXP_REPLACE(argument64, '\w+([[:alnum:]+[:punct:][:alnum:]+])+@([[:alnum:][:punct:]]\w+)+', 'EBSTEST'||'_APPSTEST@agropur.com') "clone_argument64",argument64,
REGEXP_REPLACE(argument65, '\w+([[:alnum:]+[:punct:][:alnum:]+])+@([[:alnum:][:punct:]]\w+)+', 'EBSTEST'||'_APPSTEST@agropur.com') "clone_argument65",argument65,
REGEXP_REPLACE(argument66, '\w+([[:alnum:]+[:punct:][:alnum:]+])+@([[:alnum:][:punct:]]\w+)+', 'EBSTEST'||'_APPSTEST@agropur.com') "clone_argument66",argument66,
REGEXP_REPLACE(argument67, '\w+([[:alnum:]+[:punct:][:alnum:]+])+@([[:alnum:][:punct:]]\w+)+', 'EBSTEST'||'_APPSTEST@agropur.com') "clone_argument67",argument67,
REGEXP_REPLACE(argument68, '\w+([[:alnum:]+[:punct:][:alnum:]+])+@([[:alnum:][:punct:]]\w+)+', 'EBSTEST'||'_APPSTEST@agropur.com') "clone_argument68",argument68,
REGEXP_REPLACE(argument69, '\w+([[:alnum:]+[:punct:][:alnum:]+])+@([[:alnum:][:punct:]]\w+)+', 'EBSTEST'||'_APPSTEST@agropur.com') "clone_argument69",argument69,
REGEXP_REPLACE(argument70, '\w+([[:alnum:]+[:punct:][:alnum:]+])+@([[:alnum:][:punct:]]\w+)+', 'EBSTEST'||'_APPSTEST@agropur.com') "clone_argument70",argument70,
REGEXP_REPLACE(argument71, '\w+([[:alnum:]+[:punct:][:alnum:]+])+@([[:alnum:][:punct:]]\w+)+', 'EBSTEST'||'_APPSTEST@agropur.com') "clone_argument71",argument71,
REGEXP_REPLACE(argument72, '\w+([[:alnum:]+[:punct:][:alnum:]+])+@([[:alnum:][:punct:]]\w+)+', 'EBSTEST'||'_APPSTEST@agropur.com') "clone_argument72",argument72,
REGEXP_REPLACE(argument73, '\w+([[:alnum:]+[:punct:][:alnum:]+])+@([[:alnum:][:punct:]]\w+)+', 'EBSTEST'||'_APPSTEST@agropur.com') "clone_argument73",argument73,
REGEXP_REPLACE(argument74, '\w+([[:alnum:]+[:punct:][:alnum:]+])+@([[:alnum:][:punct:]]\w+)+', 'EBSTEST'||'_APPSTEST@agropur.com') "clone_argument74",argument74,
REGEXP_REPLACE(argument75, '\w+([[:alnum:]+[:punct:][:alnum:]+])+@([[:alnum:][:punct:]]\w+)+', 'EBSTEST'||'_APPSTEST@agropur.com') "clone_argument75",argument75,
REGEXP_REPLACE(argument76, '\w+([[:alnum:]+[:punct:][:alnum:]+])+@([[:alnum:][:punct:]]\w+)+', 'EBSTEST'||'_APPSTEST@agropur.com') "clone_argument76",argument76,
REGEXP_REPLACE(argument77, '\w+([[:alnum:]+[:punct:][:alnum:]+])+@([[:alnum:][:punct:]]\w+)+', 'EBSTEST'||'_APPSTEST@agropur.com') "clone_argument77",argument77,
REGEXP_REPLACE(argument78, '\w+([[:alnum:]+[:punct:][:alnum:]+])+@([[:alnum:][:punct:]]\w+)+', 'EBSTEST'||'_APPSTEST@agropur.com') "clone_argument78",argument78,
REGEXP_REPLACE(argument79, '\w+([[:alnum:]+[:punct:][:alnum:]+])+@([[:alnum:][:punct:]]\w+)+', 'EBSTEST'||'_APPSTEST@agropur.com') "clone_argument79",argument79,
REGEXP_REPLACE(argument80, '\w+([[:alnum:]+[:punct:][:alnum:]+])+@([[:alnum:][:punct:]]\w+)+', 'EBSTEST'||'_APPSTEST@agropur.com') "clone_argument80",argument80,
REGEXP_REPLACE(argument81, '\w+([[:alnum:]+[:punct:][:alnum:]+])+@([[:alnum:][:punct:]]\w+)+', 'EBSTEST'||'_APPSTEST@agropur.com') "clone_argument81",argument81,
REGEXP_REPLACE(argument82, '\w+([[:alnum:]+[:punct:][:alnum:]+])+@([[:alnum:][:punct:]]\w+)+', 'EBSTEST'||'_APPSTEST@agropur.com') "clone_argument82",argument82,
REGEXP_REPLACE(argument83, '\w+([[:alnum:]+[:punct:][:alnum:]+])+@([[:alnum:][:punct:]]\w+)+', 'EBSTEST'||'_APPSTEST@agropur.com') "clone_argument83",argument83,
REGEXP_REPLACE(argument84, '\w+([[:alnum:]+[:punct:][:alnum:]+])+@([[:alnum:][:punct:]]\w+)+', 'EBSTEST'||'_APPSTEST@agropur.com') "clone_argument84",argument84,
REGEXP_REPLACE(argument85, '\w+([[:alnum:]+[:punct:][:alnum:]+])+@([[:alnum:][:punct:]]\w+)+', 'EBSTEST'||'_APPSTEST@agropur.com') "clone_argument85",argument85,
REGEXP_REPLACE(argument86, '\w+([[:alnum:]+[:punct:][:alnum:]+])+@([[:alnum:][:punct:]]\w+)+', 'EBSTEST'||'_APPSTEST@agropur.com') "clone_argument86",argument86,
REGEXP_REPLACE(argument87, '\w+([[:alnum:]+[:punct:][:alnum:]+])+@([[:alnum:][:punct:]]\w+)+', 'EBSTEST'||'_APPSTEST@agropur.com') "clone_argument87",argument87,
REGEXP_REPLACE(argument88, '\w+([[:alnum:]+[:punct:][:alnum:]+])+@([[:alnum:][:punct:]]\w+)+', 'EBSTEST'||'_APPSTEST@agropur.com') "clone_argument88",argument88,
REGEXP_REPLACE(argument89, '\w+([[:alnum:]+[:punct:][:alnum:]+])+@([[:alnum:][:punct:]]\w+)+', 'EBSTEST'||'_APPSTEST@agropur.com') "clone_argument89",argument89,
REGEXP_REPLACE(argument90, '\w+([[:alnum:]+[:punct:][:alnum:]+])+@([[:alnum:][:punct:]]\w+)+', 'EBSTEST'||'_APPSTEST@agropur.com') "clone_argument90",argument90,
REGEXP_REPLACE(argument91, '\w+([[:alnum:]+[:punct:][:alnum:]+])+@([[:alnum:][:punct:]]\w+)+', 'EBSTEST'||'_APPSTEST@agropur.com') "clone_argument91",argument91,
REGEXP_REPLACE(argument92, '\w+([[:alnum:]+[:punct:][:alnum:]+])+@([[:alnum:][:punct:]]\w+)+', 'EBSTEST'||'_APPSTEST@agropur.com') "clone_argument92",argument92,
REGEXP_REPLACE(argument93, '\w+([[:alnum:]+[:punct:][:alnum:]+])+@([[:alnum:][:punct:]]\w+)+', 'EBSTEST'||'_APPSTEST@agropur.com') "clone_argument93",argument93,
REGEXP_REPLACE(argument94, '\w+([[:alnum:]+[:punct:][:alnum:]+])+@([[:alnum:][:punct:]]\w+)+', 'EBSTEST'||'_APPSTEST@agropur.com') "clone_argument94",argument94,
REGEXP_REPLACE(argument95, '\w+([[:alnum:]+[:punct:][:alnum:]+])+@([[:alnum:][:punct:]]\w+)+', 'EBSTEST'||'_APPSTEST@agropur.com') "clone_argument95",argument95,
REGEXP_REPLACE(argument96, '\w+([[:alnum:]+[:punct:][:alnum:]+])+@([[:alnum:][:punct:]]\w+)+', 'EBSTEST'||'_APPSTEST@agropur.com') "clone_argument96",argument96,
REGEXP_REPLACE(argument97, '\w+([[:alnum:]+[:punct:][:alnum:]+])+@([[:alnum:][:punct:]]\w+)+', 'EBSTEST'||'_APPSTEST@agropur.com') "clone_argument97",argument97,
REGEXP_REPLACE(argument98, '\w+([[:alnum:]+[:punct:][:alnum:]+])+@([[:alnum:][:punct:]]\w+)+', 'EBSTEST'||'_APPSTEST@agropur.com') "clone_argument98",argument98,
REGEXP_REPLACE(argument99, '\w+([[:alnum:]+[:punct:][:alnum:]+])+@([[:alnum:][:punct:]]\w+)+', 'EBSTEST'||'_APPSTEST@agropur.com') "clone_argument99",argument99,
   concurrent_program_id
from fnd_run_requests
where  REGEXP_LIKE(
          argument1 ||
argument2 ||
argument3 ||
argument4 ||
argument5 ||
argument6 ||
argument7 ||
argument8 ||
argument9 ||
argument10 ||
argument11 ||
argument12 ||
argument13 ||
argument14 ||
argument15 ||
argument16 ||
argument17 ||
argument18 ||
argument19 ||
argument20 ||
argument21 ||
argument22 ||
argument23 ||
argument24 ||
argument25 ||
argument26 ||
argument27 ||
argument28 ||
argument29 ||
argument30 ||
argument31 ||
argument32 ||
argument33 ||
argument34 ||
argument35 ||
argument36 ||
argument37 ||
argument38 ||
argument39 ||
argument40 ||
argument41 ||
argument42 ||
argument43 ||
argument44 ||
argument45 ||
argument46 ||
argument47 ||
argument48 ||
argument49 ||
argument50 ||
argument51 ||
argument52 ||
argument53 ||
argument54 ||
argument55 ||
argument56 ||
argument57 ||
argument58 ||
argument59 ||
argument60 ||
argument61 ||
argument62 ||
argument63 ||
argument64 ||
argument65 ||
argument66 ||
argument67 ||
argument68 ||
argument69 ||
argument70 ||
argument71 ||
argument72 ||
argument73 ||
argument74 ||
argument75 ||
argument76 ||
argument77 ||
argument78 ||
argument79 ||
argument80 ||
argument81 ||
argument82 ||
argument83 ||
argument84 ||
argument85 ||
argument86 ||
argument87 ||
argument88 ||
argument89 ||
argument90 ||
argument91 ||
argument92 ||
argument93 ||
argument94 ||
argument95 ||
argument96 ||
argument97 ||
argument98 ||
argument99,
          '@')
and not REGEXP_LIKE(
          argument1 ||
argument2 ||
argument3 ||
argument4 ||
argument5 ||
argument6 ||
argument7 ||
argument8 ||
argument9 ||
argument10 ||
argument11 ||
argument12 ||
argument13 ||
argument14 ||
argument15 ||
argument16 ||
argument17 ||
argument18 ||
argument19 ||
argument20 ||
argument21 ||
argument22 ||
argument23 ||
argument24 ||
argument25 ||
argument26 ||
argument27 ||
argument28 ||
argument29 ||
argument30 ||
argument31 ||
argument32 ||
argument33 ||
argument34 ||
argument35 ||
argument36 ||
argument37 ||
argument38 ||
argument39 ||
argument40 ||
argument41 ||
argument42 ||
argument43 ||
argument44 ||
argument45 ||
argument46 ||
argument47 ||
argument48 ||
argument49 ||
argument50 ||
argument51 ||
argument52 ||
argument53 ||
argument54 ||
argument55 ||
argument56 ||
argument57 ||
argument58 ||
argument59 ||
argument60 ||
argument61 ||
argument62 ||
argument63 ||
argument64 ||
argument65 ||
argument66 ||
argument67 ||
argument68 ||
argument69 ||
argument70 ||
argument71 ||
argument72 ||
argument73 ||
argument74 ||
argument75 ||
argument76 ||
argument77 ||
argument78 ||
argument79 ||
argument80 ||
argument81 ||
argument82 ||
argument83 ||
argument84 ||
argument85 ||
argument86 ||
argument87 ||
argument88 ||
argument89 ||
argument90 ||
argument91 ||
argument92 ||
argument93 ||
argument94 ||
argument95 ||
argument96 ||
argument97 ||
argument98 ||
argument99,
          'EBSDEVC_APPSTEST@agropur.com');
          

---- Replace PRINTER profile preferences to noprint
SELECT
    ffvs.FLEX_VALUE_SET_NAME,
    ffv.flex_value_set_id,
    ffv.flex_value,
    ffv.flex_value_id,
    --flex_value_meaning,
    ffv.attribute1,
    ffv.attribute2,
    ffv.attribute3,
    ffv.attribute4,
    ffv.attribute5,
    ffv.attribute6,
    ffv.attribute7,
    ffv.attribute8,
    ffv.attribute9,
    ffv.attribute10,
    ffv.attribute11,
    ffv.attribute12,
    ffv.attribute13,
    ffv.attribute14,
    ffv.attribute15
FROM
    fnd_flex_values ffv,
    fnd_flex_value_sets ffvs
where ffv.flex_value_set_id = ffvs.flex_value_set_id
and REGEXP_INSTR(
          attribute1 || 
          attribute2 || 
          attribute3 || 
          attribute4 || 
          attribute5 || 
          attribute6 || 
          attribute7 || 
          attribute8 || 
          attribute9 || 
          attribute10 || 
          attribute11 || 
          attribute12 || 
          attribute13 || 
          attribute14 || 
          attribute15  , 
          '\w+([[:alnum:]+[:punct:][:alnum:]+])+@([[:alnum:][:punct:]]\w+)+') > 0 
and not REGEXP_LIKE(
          attribute1 || 
          attribute2 || 
          attribute3 || 
          attribute4 || 
          attribute5 || 
          attribute6 || 
          attribute7 || 
          attribute8 || 
          attribute9 || 
          attribute10 || 
          attribute11 || 
          attribute12 || 
          attribute13 || 
          attribute14 || 
          attribute15  , 
          'EBSTEST_APPSTEST@agropur.com');










SELECT r.request_id,
DECODE (r.description,
NULL, pt.user_concurrent_program_name,
r.description || ' ( ' || pt.user_concurrent_program_name
|| ' ) '
) program,
r.phase_code,
r.status_code,
fnd_amp_private.get_phase (r.phase_code,
r.status_code,
r.hold_flag,
p.enabled_flag,
r.requested_start_date,
r.request_id
) phase,
fnd_amp_private.get_status (r.phase_code,
r.status_code,
r.hold_flag,
p.enabled_flag,
r.requested_start_date,
r.request_id
) status,
u.user_name,
rt.responsibility_name,
AT.application_name
,To_Char(r.request_date,'DD-MON-YY HH24:MI:SS') submitted
,To_Char(r.REQUESTED_START_DATE,'DD-MON-YY HH24:MI:SS') REQUESTED_START_DATE
,To_Char(r.ACTUAL_START_DATE,'DD-MON-YY HH24:MI:SS') started
,To_Char(r.ACTUAL_COMPLETION_DATE,'DD-MON-YY HH24:MI:SS') completed,
r.argument_text,
r.oracle_session_id,
r.completion_text,
r.controlling_manager,
r.requested_by,
r.program_application_id,
r.concurrent_program_id,
r.is_sub_request,
r.parent_request_id,
r.queue_method_code,
r.cd_id,
r.hold_flag,
p.enabled_flag,
p.concurrent_program_name,
p.user_concurrent_program_name,
r.os_process_id,
r.nls_language,
r.nls_territory,
r.nls_numeric_characters,
r.description
FROM fnd_concurrent_programs_tl pt,
fnd_responsibility_tl rt,
fnd_application_tl AT,
fnd_concurrent_programs_vl p,
fnd_user u,
fnd_concurrent_requests r
WHERE r.program_application_id = p.application_id
AND r.concurrent_program_id = p.concurrent_program_id
AND pt.concurrent_program_id = p.concurrent_program_id
AND pt.application_id = p.application_id
AND pt.LANGUAGE = USERENV ('LANG')
AND u.user_id = r.requested_by
AND rt.application_id = r.responsibility_application_id
AND rt.responsibility_id = r.responsibility_id
AND rt.LANGUAGE = USERENV ('LANG')
AND AT.application_id = r.program_application_id
AND AT.LANGUAGE = USERENV ('LANG')
--AND r.request_id = :request_id
and r.argument_text like '%@%'
and r.argument_text not like '%EBSDEVC_APPSTEST@agropur.com%'
and u.user_name = 'XXAPPSADM';
--22058896	Archiving AUTO Documents Interface ( Request Set Archiving AUTO Documents Interface ) 	C	C	Completed	Normal	XXAPPSADM	Agropur Custom Dev RETAIL	AGROPUR - EBS	22-NOV-18	22-NOV-18	22-NOV-18	21-NOV-18	20005, 2038	67471653	Normal completion	116519	1429	20005	240448	N	21998750	B	0	N	Y	XXAOL0240_AUTO	Request Set Archiving AUTO Documents Interface	8782112	AMERICAN	CANADA	.,	Archiving AUTO Documents Interface

--------------------------------------------------------------------------------
--Concurrent Requests
--Runtime Delivery Options
--FND_CONC_PP_ACTIONS 
--------------------------------------------------------------------------------
select 
   pp.concurrent_request_id,
   REGEXP_REPLACE(argument3,  '\w+([[:alnum:]+[:punct:][:alnum:]+])+@([[:alnum:][:punct:]]\w+)+', 'EBSDEVC_APPSTEST@agropur.com') "clone_argument3", argument3,
   REGEXP_REPLACE(argument4,  '\w+([[:alnum:]+[:punct:][:alnum:]+])+@([[:alnum:][:punct:]]\w+)+', 'EBSDEVC_APPSTEST@agropur.com') "clone_argument4", argument4
from 
FND_CONC_PP_ACTIONS pp
where pp.action_type = 7  -- 1=Print; 7=email
and (REGEXP_INSTR(pp.argument3,'\w+([[:alnum:]+[:punct:][:alnum:]+])+@([[:alnum:][:punct:]]\w+)+') > 0 
     OR
     REGEXP_INSTR(pp.argument4, '\w+([[:alnum:]+[:punct:][:alnum:]+])+@([[:alnum:][:punct:]]\w+)+') > 0 
    )
;

--------------------------------------------------------------------------------
--Concurrent Requests
--Runtime Delivery Options
--fnd_run_req_pp_actions 
--------------------------------------------------------------------------------
select 
   pp.parent_request_id,
   REGEXP_REPLACE(argument3,  '\w+([[:alnum:]+[:punct:][:alnum:]+])+@([[:alnum:][:punct:]]\w+)+', 'EBSDEVC_APPSTEST@agropur.com') "clone_argument3", argument3,
   REGEXP_REPLACE(argument4,  '\w+([[:alnum:]+[:punct:][:alnum:]+])+@([[:alnum:][:punct:]]\w+)+', 'EBSDEVC_APPSTEST@agropur.com') "clone_argument4", argument4
from 
fnd_run_req_pp_actions pp
where pp.action_type = 7  -- 1=Print; 7=email
 and (REGEXP_INSTR(pp.argument3,'\w+([[:alnum:]+[:punct:][:alnum:]+])+@([[:alnum:][:punct:]]\w+)+') > 0 
     OR
     REGEXP_INSTR(pp.argument4, '\w+([[:alnum:]+[:punct:][:alnum:]+])+@([[:alnum:][:punct:]]\w+)+') > 0 
    )
;

  --Delivery Options email de Request_ID:
 select * from fnd_run_req_pp_actions where
 --parent_request_id = 22067592 and
 ARGUMENT4 not like '%APPSTEST@agropur%' and ARGUMENT3 not like '%@agropur%'
 and(ARGUMENT4 like '%@%' or ARGUMENT3 like '%@%') ; 
 
  select * from FND_CONC_PP_ACTIONS where
 --parent_request_id = 22067592 and
 ARGUMENT4 not like '%APPSTEST@agropur%' and ARGUMENT3 not like '%@agropur%'
 and( ARGUMENT4 like '%@%' or ARGUMENT3 like '%@%' ); 
 
 
/*

  --Delivery Options email de Request_ID:
 select * from fnd_run_req_pp_actions where
 --parent_request_id = 22067592 and
 LOWER(ARGUMENT4) not like '%@agropur%' and LOWER(ARGUMENT3) not like '%@agropur%'
 and(ARGUMENT4 like '%@%' or ARGUMENT3 like '%@%') ; 
 
  select * from FND_CONC_PP_ACTIONS where
 --parent_request_id = 22067592 and
 lower(ARGUMENT4) not like '%@agropur%' and lower(ARGUMENT3) not like '%@agropur%'
 and( ARGUMENT4 like '%@%' or ARGUMENT3 like '%@%' ); */



 select email_address from PER_PEOPLE_F where email_address not like ('%EBSDEV3_APPSTEST@agropur.com%');
 --3033
--  update PER_PEOPLE_F
--     set email_address = 'EBSDEV3_APPSTEST@agropur.com'
--   where email_address is not null;
--  commit;

-- check : post_clone100_apps_step_UsersRoles_PLSQL.sql aussi 