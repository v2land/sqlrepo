select * from fnd_user where USER_NAME like '%PATBOUC%';

select
fnf.application_id,
fa.APPLICATION_NAME,
ff.FUNCTION_NAME ,
ffl.USER_FUNCTION_NAME ,
ffl.description ,
fnf.form_name,
ff.parameters,
ff.type
from fnd_form_functions_tl ffl,
fnd_form_functions ff,
fnd_form fnf,
fnd_application_tl fa
where
--ff.function_name like '******'
ffl.FUNCTION_ID=ff.FUNCTION_ID
and ff.type='FORM'
and fnf.form_id=ff.form_id
and fa.application_id=fnf.application_id
and fnf.form_name ='FNDRSRUN';
--and fa.APPLICATION_ID=******;

--==================Seb version ====================
select a.application_short_name,
          f.form_name,
          f.audit_enabled_flag,
          to_char(f.last_update_date, 'YYYY/MM/DD') last_update_date,
          f.user_form_name,
          f.description,
          fnd_load_util.owner_name(f.LAST_UPDATED_BY) OWNER
   from   fnd_form_vl f,
          fnd_application a
   where  f.application_id         = a.application_id
   and    ((:FORM_APP_SHORT_NAME is NULL) or
             ((:FORM_APP_SHORT_NAME is not NULL)
                  AND (a.application_short_name like :FORM_APP_SHORT_NAME)))
   and    ((:APPLICATION_SHORT_NAME is NULL) or
             ((:APPLICATION_SHORT_NAME is not NULL)
                  AND (a.application_short_name like :APPLICATION_SHORT_NAME)))
   and    ((:FORM_NAME is NULL) or
             ((:FORM_NAME is not NULL) AND (f.form_name like :FORM_NAME)))
