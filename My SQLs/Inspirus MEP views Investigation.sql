--spool /nfs_mnt/TEMP_DBA/Stage_Inspyrus/current/Agropur_views/Agropur_views.log
--@xx_ofr_shipto_v.sql
--@xx_ofr_suppliers_v.sql
--grant select on XX_OFR_SUPPLIERS_V to agr_xxinspyrus;
--exit

--sqlplus agr_xxinspyrus/agr_xxinspyrus
--@OFR-AP-EBS-Views.sql

--create synonym XX_OFR_SUPPLIERS_V for apps.XX_OFR_SUPPLIERS_V;
create synonym XX_OFR_SHIPTO_V for apps.XX_OFR_SUPPLIERS_V;


--@xx_ofr_intercompany_v.sql

select * from apps.xx_ofr_shipto_v;
select * from xx_ofr_suppliers_v;


select * from user_synonyms where SYNONYM_NAME LIKE '%XX_OFR_%';
select * from user_views where VIEW_NAME LIKE '%XX_OFR_%';


Select * --object_name,owner ,status, 	
from dba_objects 	
where 	
status='INVALID' and	
owner = 'APPS'	
and object_name like 'AXF_%';	
	
select 'alter '||object_type||' '||owner||'.'||object_name||' compile;'	
from dba_objects	
where status <> 'VALID'	
and object_type IN ('VIEW','SYNONYM','PROCEDURE','FUNCTION','PACKAGE','PACKAGE BODY','TRIGGER')	
;--and object_name like 'AXF_%';	

alter PACKAGE APPS.AXF_VALIDATION_IMPORT_PKG compile;
