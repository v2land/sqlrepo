select fscpv.parameter_value "SMTP Host Name"
      --SMTP protocol uses default port number 25 for outgoing emails
      ,25                    "SMTP Port Number" 
      ,fscpt.description
  from fnd_svc_comp_params_tl fscpt
      ,fnd_svc_comp_param_vals fscpv
 where fscpt.parameter_id = fscpv.parameter_id
   and fscpt.display_name = 'Outbound Server Name' --'Inbound Server Name'
   and fscpt.language = 'US';

-- Select all mailer parameters S
select v.parameter_id, vl.display_name, v.parameter_value, P.PARAMETER_NAME, v.component_parameter_id comp_param_id, c.component_name, c.component_id
from fnd_svc_comp_param_vals_v v, fnd_svc_comp_params_b p, fnd_svc_components c, fnd_svc_comp_params_vl vl 
where c.component_type = 'WF_MAILER' 
and v.component_id = c.component_id 
and v.parameter_id = p.parameter_id 
AND VL.PARAMETER_ID = P.PARAMETER_ID
and p.parameter_name in ('RESET_NLS','DEBUG_MAIL_SESSION','IMAP_SECURE_PROTOCOL','INBOUND_SSL_ENABLED','INBOUND_SERVER','PROCESSOR_IN_THREAD_COUNT','COMPONENT_LOG_LEVEL','OUTBOUND_SSL_ENABLED','OUTBOUND_SERVER','PROCESSOR_OUT_THREAD_COUNT','REPLYTO','MAILER_SSL_TRUSTSTORE','TEST_ADDRESS','ACCOUNT','INBOUND_PASSWORD','OUTBOUND_SECURE_PROTOCOL','OUTBOUND_SSL_ENABLED', 'OUTBOUND_USER', 'OUTBOUND_PASSWORD', 'FROM', 'HTMLAGENT', 'PROCESSOR_MAX_ERROR_COUNT' )
--and p.parameter_id in ('10090')
order by vl.display_name;

-- Gather information for test e-mail :
select target_node 
from fnd_concurrent_queues where concurrent_queue_name like 'WFMLRSVC%';

SELECT b.component_name,  
       c.parameter_name,  
       a.parameter_value 
FROM fnd_svc_comp_param_vals a,  
     fnd_svc_components b,  
     fnd_svc_comp_params_b c 
WHERE b.component_id = a.component_id  
     AND b.component_type = c.component_type  
     AND c.parameter_id = a.parameter_id 
     AND c.encrypted_flag = 'N' 
     AND b.component_name like '%Mailer%' 
     AND c.parameter_name in ('OUTBOUND_SERVER', 'REPLYTO') 
ORDER BY c.parameter_name;

/*
telnet [outbound server] 25 
EHLO [mailer node] 
MAIL FROM: [reply_to address] 
RCPT TO: [my_test_email_address]
DATA 
Subject: Test message 

Test message body 
. 
quit 
*/


/*Workflow TEST Address etc Update*/
--sqlplus apps/ @$FND_TOP/sql/afsvcpup.sql

/*Enter Component Id: 10006

Enter the Comp Param Id to update : 10093

Enter a value for the parameter : WFdevUsers@abc.com

Setup Test address/override address for WF
Below is the script to update the override address from backend. You do not need the verification code to set the override address using the below script
*/


/*update fnd_svc_comp_param_vals
set    parameter_value = '&EnterEmailID'
where  parameter_id =
( select parameter_id
 from   fnd_svc_comp_params_tl
 where  display_name = 'Test Address'
);*/
--
--Restart the Notification Mailer		
/*	sqlplus apps/$APPS_PWD <<EOF	
	spool /staging/scripts/applmgr/clone/log/Clone_restart_workflow_notification_mailer_$TWO_TASK\_$(date +%Y%m%d).log	
	@/staging/scripts/applmgr/clone/Clone_restart_workflow_notification_mailer.sql	
	spool off;	
	EOF	
*/		
--Verify that Mailer is running		
	select component_id, component_name, component_status 	
	  from fnd_svc_components 	
	  where component_type = 'WF_MAILER';	
