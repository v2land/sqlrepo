SELECT DISTINCT 
    af.request_id,
    b.CONCURRENT_PROGRAM_NAME,
    PT.USER_CONCURRENT_PROGRAM_NAME,
    --af.*,
    af.COMPLETION_TEXT,
    af.STATUS_CODE,
    --round(((sysdate-af.actual_start_date)*24*60*60/60),2) "Process_time",
    TO_CHAR ( TRUNC(sysdate)
               + NUMTODSINTERVAL ((sysdate-af.actual_start_date)*24*60,
                    'minute'),
               'HH24:MI:SS') "Process_TIME",
          
         (SELECT 
          TO_CHAR (
               TRUNC (SYSDATE)
               + NUMTODSINTERVAL (
                    MAX ( (F.ACTUAL_COMPLETION_DATE - F.ACTUAL_START_DATE))
                    * 24*60,
                    'minute'),
               'HH24:MI:SS')
            "MAXIMUM"
            FROM APPLSYS.FND_CONCURRENT_REQUESTS F,
                 APPLSYS.FND_CONCURRENT_PROGRAMS P
            WHERE     PHASE_CODE = 'C'
                 AND STATUS_CODE = 'C'
                 AND F.CONCURRENT_PROGRAM_ID = P.CONCURRENT_PROGRAM_ID
                 AND P.CONCURRENT_PROGRAM_ID=af.CONCURRENT_PROGRAM_ID
            GROUP BY P.CONCURRENT_PROGRAM_NAME
      ) MAX_Time,
      
      (SELECT 
          TO_CHAR (
               TRUNC (SYSDATE)
               + NUMTODSINTERVAL (
                    MIN ( (F.ACTUAL_COMPLETION_DATE - F.ACTUAL_START_DATE))
                    * 24*60,
                    'minute'),
               'HH24:MI:SS')
            "MINIMUM"
            FROM APPLSYS.FND_CONCURRENT_REQUESTS F,
                 APPLSYS.FND_CONCURRENT_PROGRAMS P
            WHERE     PHASE_CODE = 'C'
                 AND STATUS_CODE = 'C'
                 AND F.CONCURRENT_PROGRAM_ID = P.CONCURRENT_PROGRAM_ID
                 AND P.CONCURRENT_PROGRAM_ID=af.CONCURRENT_PROGRAM_ID
            GROUP BY P.CONCURRENT_PROGRAM_NAME
      ) MIN_Time,
      
      (SELECT 
          TO_CHAR (
               TRUNC (SYSDATE)
               + NUMTODSINTERVAL (
                    AVG ( (F.ACTUAL_COMPLETION_DATE - F.ACTUAL_START_DATE))
                    * 24*60,
                    'minute'),
               'HH24:MI:SS')
            "AVERAGE"
            FROM APPLSYS.FND_CONCURRENT_REQUESTS F,
                 APPLSYS.FND_CONCURRENT_PROGRAMS P
            WHERE     PHASE_CODE = 'C'
                 AND STATUS_CODE = 'C'
                 AND F.CONCURRENT_PROGRAM_ID = P.CONCURRENT_PROGRAM_ID
                 AND P.CONCURRENT_PROGRAM_ID=af.CONCURRENT_PROGRAM_ID
            GROUP BY P.CONCURRENT_PROGRAM_NAME
      ) AVG_Time,
      
      (SELECT 
          TO_CHAR (
               TRUNC (SYSDATE)
               + NUMTODSINTERVAL (
               --mean, max, min, median, mode and standard deviation 
                    STATS_MODE( (F.ACTUAL_COMPLETION_DATE - F.ACTUAL_START_DATE))
                    * 24*60,
                    'minute'),
               'HH24:MI:SS')
            "STATS_MODE"
            FROM APPLSYS.FND_CONCURRENT_REQUESTS F,
                 APPLSYS.FND_CONCURRENT_PROGRAMS P
            WHERE     PHASE_CODE = 'C'
                 AND STATUS_CODE = 'C'
                 AND F.CONCURRENT_PROGRAM_ID = P.CONCURRENT_PROGRAM_ID
                 AND P.CONCURRENT_PROGRAM_ID=af.CONCURRENT_PROGRAM_ID
            GROUP BY P.CONCURRENT_PROGRAM_NAME
      )"STATS_MODE"
      ,
      (SELECT 
          TO_CHAR (
               TRUNC (SYSDATE)
               + NUMTODSINTERVAL (
               --mean, max, min, median, mode and standard deviation 
                    STDDEV( (F.ACTUAL_COMPLETION_DATE - F.ACTUAL_START_DATE))
                    * 24*60,
                    'minute'),
               'HH24:MI:SS')
            "STDDEV"
            FROM APPLSYS.FND_CONCURRENT_REQUESTS F,
                 APPLSYS.FND_CONCURRENT_PROGRAMS P
            WHERE     PHASE_CODE = 'C'
                 AND STATUS_CODE = 'C'
                 AND F.CONCURRENT_PROGRAM_ID = P.CONCURRENT_PROGRAM_ID
                 AND P.CONCURRENT_PROGRAM_ID=af.CONCURRENT_PROGRAM_ID
            GROUP BY P.CONCURRENT_PROGRAM_NAME
      )"STDDEV"
    FROM
    apps.fnd_concurrent_requests af
    ,apps.fnd_concurrent_programs b
    ,apps.FND_CONCURRENT_PROGRAMS_TL PT
    WHERE
    af.CONCURRENT_PROGRAM_ID = b.CONCURRENT_PROGRAM_ID
    and b.CONCURRENT_PROGRAM_ID = PT.CONCURRENT_PROGRAM_ID
    AND PT.language = USERENV('Lang')
    and af.status_code='R';

--How to retrieve Summary of concurrent Jobs/status/Count in Last 1 hour?
select
fcpt.USER_CONCURRENT_PROGRAM_NAME,
DECODE(fcr.phase_code,
 'C',  'Completed ',
 'I',  'Inactive ',
 'P',  'Pending ',
 'R',  'Running ',
fcr.phase_code
) PHASE ,
DECODE(fcr.status_code,
 'A',  'Waiting ',
 'B',  'Resuming ',
 'C',  'Normal ',
 'D',  'Cancelled ',
 'E',  'Errored ',
 'F',  'Scheduled ',
 'G',  'Warning ',
 'H',  'On Hold ',
 'I',  'Normal ',
 'M',  'No Manager ',
 'Q',  'Standby ',
 'R',  'Normal ',
 'S',  'Suspended ',
 'T',  'Terminating ',
 'U',  'Disabled ',
 'W',  'Paused ',
 'X',  'Terminated ',
 'Z',  'Waiting ',
fcr.status_code
) STATUS,
count(*)
from apps.fnd_concurrent_programs_tl fcpt,apps.FND_CONCURRENT_REQUESTs fcr
where fcpt.CONCURRENT_PROGRAM_ID=fcr.CONCURRENT_PROGRAM_ID
and fcpt.language = USERENV('Lang')
and fcr.ACTUAL_START_DATE > sysdate - 1/24
group by fcpt.USER_CONCURRENT_PROGRAM_NAME,fcr.phase_code,fcr.status_code
;
--Select Running Request Process_Time and AVG_Time in minutes  V.01
SELECT DISTINCT 
    af.request_id,
    b.CONCURRENT_PROGRAM_NAME,
    round(((sysdate-af.actual_start_date)*24*60*60/60),2) "Process_time",
    (SELECT 
          TO_CHAR (
               TRUNC (SYSDATE)
               + NUMTODSINTERVAL (
                    AVG ( (F.ACTUAL_COMPLETION_DATE - F.ACTUAL_START_DATE))
                    * 24*60,
                    'minute'),
               'MI:SS')
            "AVERAGE"
            FROM APPLSYS.FND_CONCURRENT_REQUESTS F,
                 APPLSYS.FND_CONCURRENT_PROGRAMS P
            WHERE     PHASE_CODE = 'C'
                 AND STATUS_CODE = 'C'
                 AND F.CONCURRENT_PROGRAM_ID = P.CONCURRENT_PROGRAM_ID
                 AND P.CONCURRENT_PROGRAM_ID=af.CONCURRENT_PROGRAM_ID
            GROUP BY P.CONCURRENT_PROGRAM_NAME
      ) AVG_Time
    FROM
    apps.fnd_concurrent_requests af
    ,apps.fnd_concurrent_programs b
    WHERE
    af.CONCURRENT_PROGRAM_ID = b.CONCURRENT_PROGRAM_ID and
    af.status_code='R';
    
--EXCLUSIONS:
--RONA - Concurrent Request Set Submission XXRFND_SUBMIT_REQ_SET
-- Rendre vrai la statistique
          
SELECT DISTINCT 
a.request_id,
round(((sysdate-a.actual_start_date)*24*60*60/60),2) "Process_time"
FROM
apps.fnd_concurrent_requests a
--,apps.fnd_concurrent_programs b
WHERE
--a.concurrent_program_id=b.concurrent_program_id and
a.status_code='R'; 
 
          
SELECT DISTINCT 
        b.CONCURRENT_PROGRAM_NAME
        FROM
        apps.fnd_concurrent_requests a
        ,apps.fnd_concurrent_programs b
       -- ,apps.FND_CONCURRENT_PROGRAMS_TL c
        WHERE
        a.concurrent_program_id=b.concurrent_program_id
       -- AND b.concurrent_program_id=c.concurrent_program_id
        and a.status_code='R'
       -- AND c.language = USERENV('Lang')
        ;
      

        
        