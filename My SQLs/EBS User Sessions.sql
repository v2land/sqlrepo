select last_connect, usr.user_name, resp.responsibility_key, function_type, icx.*
  from apps.icx_sessions icx
  join apps.fnd_user usr on usr.user_id=icx.user_id
  left join apps.fnd_responsibility resp on resp.responsibility_id=icx.responsibility_id
  where last_connect>sysdate-nvl(FND_PROFILE.VALUE('ICX_SESSION_TIMEOUT'),30)/60/24
    and disabled_flag != 'Y' and pseudo_flag = 'N';
    
    select s.SID,s.SERIAL#, p.SPID, to_char(logon_time,'dd-mon hh24:mi') logon_time,
round(((sysdate - logon_time)*24*60),2) "Duration",
last_call_et/60 "Idle",
f.user_name, f.description, s.module,s.action, s.sql_id
from
( select /*+ no_merge */ f.user_name, u.description, f.pid, f.process_spid
from apps.FND_SIGNON_AUDIT_VIEW f,
apps.FND_USER u
where f.user_name = upper('vlacasa')
and f.user_id = u.user_id
) f,
v$process p,
v$session s
where f.pid = p.pid
and f.process_spid = p.spid
and p.addr = s.paddr
order by f.user_name
;
