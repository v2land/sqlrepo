SELECT item_type,
item_key,
to_char(begin_date,
'DD-MON-RR HH24:MI:SS') begin_date,
to_char(end_date,
'DD-MON-RR HH24:MI:SS') end_date,
root_activity activity
FROM apps.wf_items
WHERE item_type = 'POAPPRV'
AND end_date IS NULL
ORDER BY to_date(begin_date,
'DD-MON-YYYY hh24:mi:ss') DESC;
--=====================
--prompt **** Find the Activity Statuses for all workflow activities of a given item type and item key
SELECT execution_time,
       to_char(ias.begin_date,
               'DD-MON-RR HH24:MI:SS') begin_date,
       ap.display_name || '/' || ac.display_name activity,
       ias.activity_status status,
       ias.activity_result_code RESULT,
       ias.assigned_user ass_user
  FROM wf_item_activity_statuses ias,
       wf_process_activities     pa,
       wf_activities_vl          ac,
       wf_activities_vl          ap,
       wf_items                  i
 WHERE ias.item_type = 'POAPPRV'
   AND ias.item_key = '1210433-404519'
   AND ias.process_activity = pa.instance_id
   AND pa.activity_name = ac.name
   AND pa.activity_item_type = ac.item_type
   AND pa.process_name = ap.name
   AND pa.process_item_type = ap.item_type
   AND pa.process_version = ap.version
   AND i.item_type = 'POAPPRV'
   AND i.item_key = ias.item_key
   AND i.begin_date >= ac.begin_date
   AND i.begin_date < nvl(ac.end_date,
                          i.begin_date + 1)
UNION ALL
SELECT execution_time,
       to_char(ias.begin_date,
               'DD-MON-RR HH24:MI:SS') begin_date,
       ap.display_name || '/' || ac.display_name activity,
       ias.activity_status status,
       ias.activity_result_code RESULT,
       ias.assigned_user ass_user
  FROM wf_item_activity_statuses_h ias,
       wf_process_activities       pa,
       wf_activities_vl            ac,
       wf_activities_vl            ap,
       wf_items                    i
 WHERE ias.item_type = 'POAPPRV'
   AND ias.item_key = '1210433-404519'
   AND ias.process_activity = pa.instance_id
   AND pa.activity_name = ac.name
   AND pa.activity_item_type = ac.item_type
   AND pa.process_name = ap.name
   AND pa.process_item_type = ap.item_type
   AND pa.process_version = ap.version
   AND i.item_type = 'POAPPRV'
   AND i.item_key = ias.item_key
   AND i.begin_date >= ac.begin_date
   AND i.begin_date < nvl(ac.end_date,
                          i.begin_date + 1)
 ORDER BY 2,
          1
;

-- 1) Take a snapshot of events in WF_DEFERRED at the beginning of troubleshooting
select wfd.user_data.event_name EVENT_NAME, 
      decode(wfd.state, 
                   0, '0 = Ready', 
                   1, '1 = Delayed', 
                   2, '2 = Retained', 
                   3, '3 = Exception', 
      to_char(substr(wfd.state,1,12))) State, 
      count(*) COUNT 
      from applsys.wf_deferred wfd 
      group by wfd.user_data.event_name, wfd.state 
      order by 3 desc, 1 asc;
--Take note of the results of this query and make sure there are agent listeners running that will include these events or the parent of the events, for example, oracle.apps.fnd.% for all FND events.
-- Check the status of the Workflow Mailer(s) and thier thread counts and when they started
select fsc.COMPONENT_ID, fcq.USER_CONCURRENT_QUEUE_NAME Container_Name,
fsc.COMPONENT_NAME,
DECODE(fcp.OS_PROCESS_ID,NULL,'Not Running',fcp.OS_PROCESS_ID) PROCID,
fcq.MAX_PROCESSES TARGET,
fcq.RUNNING_PROCESSES ACTUAL,
v.PARAMETER_VALUE "#THREADS",
fcq.ENABLED_FLAG ENABLED,
fsc.CORRELATION_ID,
fsc.STARTUP_MODE,
fsc.component_type,
fsc.COMPONENT_STATUS,
to_char(fsc.last_update_date,'DD-MON-YYYY HH24:MI:SS') "STARTED", round(sysdate-fsc.last_update_date,0) "DAYS RUNNING"
from APPS.FND_CONCURRENT_QUEUES_VL fcq, APPS.FND_CP_SERVICES fcs,
APPS.FND_CONCURRENT_PROCESSES fcp, apps.fnd_svc_components fsc, apps.FND_SVC_COMP_PARAM_VALS_V v
where v.COMPONENT_ID=fsc.COMPONENT_ID
and fcq.MANAGER_TYPE = fcs.SERVICE_ID
and fcs.SERVICE_HANDLE = 'FNDCPGSC'
and fsc.concurrent_queue_id = fcq.concurrent_queue_id(+)
and fcq.concurrent_queue_id = fcp.concurrent_queue_id(+)
and fcq.application_id = fcp.queue_application_id(+)
and fcp.process_status_code(+) = 'A'
and v.PARAMETER_NAME = 'PROCESSOR_IN_THREAD_COUNT'
and fsc.component_type = 'WF_MAILER'
order by fsc.COMPONENT_STATUS desc;
--10006	Workflow Mailer Service	Workflow Notification Mailer	1809	1	1	1	Y		AUTOMATIC	WF_MAILER	RUNNING	30-MAR-2017 02:01:12	1

select component_id, component_name, correlation_id
from apps.fnd_svc_components;

---- The following query helps to determine which activities are in the WF_DEFERRED_TABLE_M:

select w.user_data.itemtype "Item Type", w.user_data.itemkey "Item Key", 
decode(w.state, 0, '0 = Ready', 
1, '1 = Delayed', 
2, '2 = Retained/Processed', 
3, '3 = Exception', 
to_char(w.state)) State, 
w.priority, w.ENQ_TIME, w.DEQ_TIME, w.msgid 
from wf_deferred_table_m w;

---
select 
--*
NVL(substr(wfe.corrid,1,50),'NULL - No Value') corrid
--,decode(wfe.state,0,'0 = Ready',1,'1 = Delayed','2=Retained','3=Exception',) State
,count(*) COUNT 
from apps.wf_deferred wfe 
group by wfe.corrid, wfe.state;

--
SELECT t.component_name,
p.owner,
p.queue_table,
t.correlation_id
FROM applsys.fnd_svc_components t,
applsys.wf_agents o,
dba_queues p
WHERE t.inbound_agent_name || t.outbound_agent_name = o.name
AND p.owner || '.' || p.name = o.queue_name
AND t.component_type LIKE 'WF_%AGENT%'; 

     

