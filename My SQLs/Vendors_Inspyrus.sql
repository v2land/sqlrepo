SELECT DISTINCT
    ( vendor_index ),
    vendor_id,
    vendor_name,
    vendor_number,
    vendor_site_id,
    vendor_site_currency,
    address_line1,
    address_line2,
    city,
    state,
    postal_code,
    country,
    vat_registration_num,
    vendor_type,
    invoice_type,
    vendor_site_currency AS currency_code,
    vendor_liability_account,
    default_charge_account,
    vendor_requester,
    org_id,
    org_name,
    org_id_name,
    site_name,
    no_line_vendor,
    distribution_set_id,
    pay_group_code,
    payment_method_code,
    vendor_pymt_terms,
    requestor_full_name,
    alternate_vendor_name,
    vendor_tax_id,
    vendor_contact_first_name,
    vendor_contact_last_name,
    vendor_contact_email,
    NULL AS vendor_bank_account_num,
    NULL AS vendor_bank_account_name,
    NULL AS vendor_iban_number
FROM
    apps.xx_ofr_suppliers_v sv
    LEFT JOIN (
        SELECT
            iao.account_owner_party_id,
            ieba.bank_account_num AS vendor_bank_account_num,
            ieba.bank_account_name AS vendor_bank_account_name,
            ieba.iban AS vendor_iban_number
        FROM
            apps.iby_account_owners iao,
            apps.iby_ext_bank_accounts ieba
        WHERE
            iao.ext_bank_account_id = ieba.ext_bank_account_id
    ) vba ON sv.party_id = vba.account_owner_party_id
WHERE
    1 = 1
    AND   lower(org_name) LIKE ( '%canada retail products%' )
    AND   lower(vendor_name) LIKE ( '%minute%' );