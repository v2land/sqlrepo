
--Check Profiles by Profile for ASCP:       MSC: ASCP Planning URL  : MSC_ASCP_WEBLOGIC_URL
--Check Profiles by Profile for ADF:      External ADF Application URL
-- reuse


select p.profile_option_name SHORT_NAME,
n.user_profile_option_name NAME,
decode(v.level_id,
10001, 'Site',
10002, 'Application',
10003, 'Responsibility',
10004, 'User',
10005, 'Server',
10006, 'Org',
10007, decode(to_char(v.level_value2), '-1', 'Responsibility',
decode(to_char(v.level_value), '-1', 'Server',
'Server+Resp')),
'UnDef') LEVEL_SET,
decode(to_char(v.level_id),
'10001', '',
'10002', app.application_short_name,
'10003', rsp.responsibility_key,
'10004', usr.user_name,
'10005', svr.node_name,
'10006', org.name,
'10007', decode(to_char(v.level_value2), '-1', rsp.responsibility_key,
decode(to_char(v.level_value), '-1',
(select node_name from fnd_nodes
where node_id = v.level_value2),
(select node_name from fnd_nodes
where node_id = v.level_value2)||'-'||rsp.responsibility_key)),
'UnDef') "CONTEXT",
v.profile_option_value VALUE,
p.ZD_EDITION_NAME,
v.last_update_date
--DECODE(FPOV.LEVEL_ID, 10001, 'SITE', 10002, 'Application: '||FA.APPLICATION_SHORT_NAME, 10003, 'Responsibility: '||FR.RESPONSIBILITY_KEY, 10004, 'User: '||FU.USER_NAME) UPDATE_BY
from fnd_profile_options p,
fnd_profile_option_values v,
fnd_profile_options_tl n,
fnd_user usr,
fnd_application app,
fnd_responsibility rsp,
fnd_nodes svr,
hr_operating_units org
where p.profile_option_id = v.profile_option_id (+)
and p.profile_option_name = n.profile_option_name
and upper(p.profile_option_name) in ( select profile_option_name
from fnd_profile_options_tl 
where 
upper(user_profile_option_name) like upper('%IBY: No Proxy Domain%')) --External ADF Application URL, WSH: BPEL Webservice URI for OTM
--upper(profile_option_name) in  ('FND_EXTERNAL_ADF_URL','MSC_ASCP_WEBLOGIC_URL', 'WSH_OTM_OB_SERVICE_ENDPOINT')) -- like upper('%&Profile_SHORT_NAME%'))--External ADF Application URL
and usr.user_id (+) = v.level_value
and rsp.application_id (+) = v.level_value_application_id
and rsp.responsibility_id (+) = v.level_value
and app.application_id (+) = v.level_value
and svr.node_id (+) = v.level_value
and org.organization_id (+) = v.level_value
order by short_name, user_profile_option_name, level_id, level_set;


--Update Profile MSC_ASCP_WEBLOGIC_URL
--MSC: ASCP Planning URL    : MSC_ASCP_WEBLOGIC_URL :   https://ascptestcust.agropur.com

-- VC BUG: INC0330905 DEV9 - Profil avec valeur erron�e : HTTP Proxy.     - IBY: HTTP Proxy - Valeur http://:80   sera chang� par ''
DECLARE	
stat BOOLEAN;	
BEGIN	
stat := FND_PROFILE.SAVE('IBY_HTTP_PROXY','','SITE');	
IF stat THEN	
dbms_output.put_line( 'Profile IBY_HTTP_PROXY updated ' );	
ELSE	
dbms_output.put_line( 'Profile IBY_HTTP_PROXY could NOT be updated ' );	
commit;	
END IF;	
END;	
/ 	
commit;


--Update Profile
--Profile for ADF:      External ADF Application URL : FND_EXTERNAL_ADF_URL
--https://adfxxxcust.agropur.com

DECLARE	
stat BOOLEAN;	
BEGIN	
stat := FND_PROFILE.SAVE('FND_EXTERNAL_ADF_URL','https://adfdevccust.agropur.com','SITE');	
IF stat THEN	
dbms_output.put_line( 'Profile FND_EXTERNAL_ADF_URL updated ' );	
ELSE	
dbms_output.put_line( 'Profile FND_EXTERNAL_ADF_URL could NOT be updated ' );	
commit;	
END IF;	
END;	
/ 	
commit;

-- REUSE Password SIGNON_PASSWORD_NO_REUSE
DECLARE
stat BOOLEAN;
BEGIN
stat := FND_PROFILE.SAVE('SIGNON_PASSWORD_NO_REUSE','0','SITE');
IF stat THEN
dbms_output.put_line( 'Profile SIGNON_PASSWORD_NO_REUSE updated with Enabled' );
ELSE
dbms_output.put_line( 'Profile SIGNON_PASSWORD_NO_REUSE could NOT be updated with Enabled' );
commit;
END IF;
END;
/ 
commit;



--Update Profile MSC_ASCP_WEBLOGIC_URL
--MSC: ASCP Planning URL    : MSC_ASCP_WEBLOGIC_URL :   https://ascptestcust.agropur.com
/*
EBSDEVE
WSH: BPEL Webservice URl for OTM
http://dcssdesoaapps.agropur.com:7003/soa-infra/services/default

EBSACCE
WSH: BPEL Webservice URl for OTM
http://dcssssoaapps.agropur.com:7003/soa-infra/services/default


*/

DECLARE	
stat BOOLEAN;	
BEGIN	
stat := FND_PROFILE.SAVE('WSH_OTM_OB_SERVICE_ENDPOINT','http://dcssssoaapps.agropur.com:7003/soa-infra/services/default','SITE');	
IF stat THEN	
dbms_output.put_line( 'Profile WSH_OTM_OB_SERVICE_ENDPOINT updated ' );	
ELSE	
dbms_output.put_line( 'Profile WSH_OTM_OB_SERVICE_ENDPOINT could NOT be updated ' );	
commit;	
END IF;	
END;	
/ 	
commit;


---------------------------------------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------------------------------------


PROMPT Mettre a jour le profil External ADF Application URL
--SELECT b.profile_option_name, PROFILE_OPTION_VALUE
--FROM applsys.fnd_profile_option_values a, applsys.fnd_profile_options b
--where b.profile_option_name = 'FND_EXTERNAL_ADF_URL'
--and a.application_id = b.application_id
--and a.profile_option_id = b.profile_option_id;
update applsys.fnd_profile_option_values z
set z.profile_option_value = DECODE('&TMPSID', 'EBSDEV1', 'https://adfdev1sso.agropur.com'
                                   , 'EBSDEV2', 'https://adfdev2sso.agropur.com'
                                   , 'EBSDEV3', 'https://adfdev3sso.agropur.com'
                                   , 'EBSDEV4', 'https://adfdev4sso.agropur.com'
                                   , 'EBSDEV5', 'https://adfdev5sso.agropur.com'
                                   , 'EBSDEV6', 'https://adfdev6sso.agropur.com'
                                   , 'EBSDEV7', 'https://adfdev7sso.agropur.com'
                                   , 'EBSDEV8', 'https://adfdev8sso.agropur.com'
                                   , 'EBSDEV9', 'https://adfdev9sso.agropur.com'
                                   , 'EBSDEVE', 'https://adfdevesso.agropur.com'
                                   , 'EBSDEVE', 'https://adfdevesso.agropur.com'
                                   , 'EBSDEVA', 'https://adfdevasso.agropur.com'
                                   , 'EBSDEVB', 'https://adfdevbsso.agropur.com'
                                   , 'EBSDEVC', 'https://adfdevcsso.agropur.com'
                                   , 'EBSDEVD', 'https://adfdevdsso.agropur.com'
                                   , 'EBSACCE', 'https://adfaccesso.agropur.com'
                                   , 'EBSTEST', 'https://adftestsso.agropur.com'
                                  ),
      z.last_update_date = sysdate
where exists (
SELECT *
  FROM applsys.fnd_profile_option_values a, applsys.fnd_profile_options b
  where b.profile_option_name = 'FND_EXTERNAL_ADF_URL'
  and a.application_id = b.application_id
  and a.profile_option_id = b.profile_option_id
  and z.application_id = b.application_id
  and z.profile_option_id= b.profile_option_id
  );

--PROMPT Verifier et Mettre a jour le profil MSC: ASCP - URL de planification
--SELECT b.profile_option_name, PROFILE_OPTION_VALUE			
--FROM applsys.fnd_profile_option_values a, applsys.fnd_profile_options b			
--where b.profile_option_name = 'MSC_ASCP_WEBLOGIC_URL'			
--and a.application_id = b.application_id			
--and a.profile_option_id = b.profile_option_id;			
			
			update applsys.fnd_profile_option_values z
			set z.profile_option_value = DECODE('&TMPSID', 'EBSDEV1', 'https://adfdev1sso.agropur.com'
			                                   , 'EBSDEV2', 'https://ascpdev2cust.agropur.com'
			                                   , 'EBSDEV3', 'https://ascpdev3cust.agropur.com'
			                                   , 'EBSDEV4', 'https://ascpdev4cust.agropur.com'
			                                   , 'EBSDEV5', 'https://ascpdev5cust.agropur.com'
			                                   , 'EBSDEV6', 'https://ascpdev6cust.agropur.com'
			                                   , 'EBSDEV7', 'https://ascpdev7cust.agropur.com'
			                                   , 'EBSDEV8', 'https://ascpdev8cust.agropur.com'
			                                   , 'EBSDEV9', 'https://ascpdev9cust.agropur.com'
			                                   , 'EBSDEVE', 'https://ascpdevecust.agropur.com'
			                                   , 'EBSDEVE', 'https://ascpdevecust.agropur.com'
			                                   , 'EBSDEVA', 'https://ascpdevacust.agropur.com'
			                                   , 'EBSDEVB', 'https://ascpdevbcust.agropur.com'
			                                   , 'EBSDEVC', 'https://ascpdevccust.agropur.com'
			                                   , 'EBSDEVD', 'https://ascpdevdcust.agropur.com'
			                                   , 'EBSACCE', 'https://ascpaccecust.agropur.com'
			                                   , 'EBSTEST', 'https://ascptestcust.agropur.com'
			                                  ),
			      z.last_update_date = sysdate
			where exists (
			SELECT *
			  FROM applsys.fnd_profile_option_values a, applsys.fnd_profile_options b
			  where b.profile_option_name = 'MSC_ASCP_WEBLOGIC_URL'
			  and a.application_id = b.application_id
			  and a.profile_option_id = b.profile_option_id
			  and z.application_id = b.application_id
			  and z.profile_option_id= b.profile_option_id
			  );
  
              

/*
EBSDEVE
WSH: BPEL Webservice URl for OTM
http://dcssdesoaapps.agropur.com:7003/soa-infra/services/default

EBSACCE
WSH: BPEL Webservice URl for OTM
http://dcssssoaapps.agropur.com:7003/soa-infra/services/default


*/

--PROMPT Mettre a jour le profil WSH: BPEL Webservice URl for OTM
--SELECT b.profile_option_name, PROFILE_OPTION_VALUE
--FROM applsys.fnd_profile_option_values a, applsys.fnd_profile_options b
--where b.profile_option_name = 'WSH_OTM_OB_SERVICE_ENDPOINT'
--and a.application_id = b.application_id
--and a.profile_option_id = b.profile_option_id;
			  
update applsys.fnd_profile_option_values z
set z.profile_option_value = DECODE('&TMPSID'
                                  , 'EBSDEV1', ''
                                  , 'EBSDEV2', ''
                                  , 'EBSDEV3', ''
                                  , 'EBSDEV4', ''
                                  , 'EBSDEV5', ''
                                  , 'EBSDEV6', ''
                                  , 'EBSDEV7', ''
                                  , 'EBSDEV8', ''
                                  , 'EBSDEV9', ''
                                  , 'EBSDEVE', 'http://dcssdesoaapps.agropur.com:7003/soa-infra/services/default'
                                  , 'EBSDEVA', ''
                                  , 'EBSDEVB', ''
                                  , 'EBSDEVC', ''
                                  , 'EBSDEVD', ''
                                  , 'EBSACCE', 'http://dcssssoaapps.agropur.com:7003/soa-infra/services/default'
                                  , 'EBSTEST', ''
                                  ),
      z.last_update_date = sysdate
where exists (
SELECT *
  FROM applsys.fnd_profile_option_values a, applsys.fnd_profile_options b
  where b.profile_option_name = 'WSH_OTM_OB_SERVICE_ENDPOINT'
  and a.application_id = b.application_id
  and a.profile_option_id = b.profile_option_id
  and z.application_id = b.application_id
  and z.profile_option_id= b.profile_option_id
  );
  commit;

--========================================================================================  
-- SET FND: Diagnostics Profile at USER Level:
-- This will set value of profile option 'FND_DIAGNOSTICS' at User level to 1
--=====================================================================================
select user_id from fnd_user where user_name='VLCASAPC';

--

SET SERVEROUTPUT ON SIZE 1000000
DECLARE
l_resp_id NUMBER;
l_resp_app_id NUMBER;
l_success BOOLEAN;
BEGIN
l_success := FND_PROFILE.save
( x_name                 => 'FND_DIAGNOSTICS'
, x_value                => 'Y'
, x_level_name           => 'USER'
, x_level_value          => 21971 -- Some User Id -- select user_id from fnd_user where user_name='VLCASAPC';
, x_level_value_app_id   => NULL
) ;
IF l_success
THEN
DBMS_OUTPUT.put_line('Profile Updated successfully at User Level');
ELSE
DBMS_OUTPUT.put_line('Profile Update Failed at User Level. Error:'||sqlerrm);
END IF;
--
-- Commit is needed because this function will not commit
--
Commit;
END;
/


--Check Profiles by Profile Name Ex: FND: Diagnostics 
select 
b.user_profile_option_name "Long Name"
, a.profile_option_name "Short Name"
, decode(to_char(c.level_id),'10001','Site'
,'10002','Application'
,'10003','Responsibility'
,'10004','User'
,'Unknown') "Level"
, decode(to_char(c.level_id),'10001','Site'
,'10002',nvl(h.application_short_name,to_char(c.level_value))
,'10003',nvl(g.responsibility_name,to_char(c.level_value))
,'10004',nvl(e.user_name,to_char(c.level_value))
,'Unknown') "Level Value"
, c.PROFILE_OPTION_VALUE "Profile Value"
, a.application_id " Application ID"
, c.profile_option_id "Profile ID"
, to_char(c.LAST_UPDATE_DATE,'DD-MON-YYYY HH24:MI') "Updated Date"
, nvl(d.user_name,to_char(c.last_updated_by)) "Updated By"
from 
apps.fnd_profile_options a
, apps.FND_PROFILE_OPTIONS_VL b
, apps.FND_PROFILE_OPTION_VALUES c
, apps.FND_USER d
, apps.FND_USER e
, apps.FND_RESPONSIBILITY_VL g
, apps.FND_APPLICATION h
where 
--a.application_id = nvl(401, a.application_id)
--and a.profile_option_name = nvl('INV', a.profile_option_name)
--a.application_id = '1'
--and a.profile_option_id='1996'
b.user_profile_option_name like '&ProfileName' -- 'AFLOG_ENABLED'
and a.profile_option_name = b.profile_option_name
and a.profile_option_id = c.profile_option_id
and a.application_id = c.application_id
and c.last_updated_by = d.user_id (+)
and c.level_value = e.user_id (+)
and c.level_value = g.responsibility_id (+)
and c.level_value = h.application_id (+)
order by 
b.user_profile_option_name, c.level_id, 
decode(to_char(c.level_id),'10001','Site'
,'10002',nvl(h.application_short_name,to_char(c.level_value))
,'10003',nvl(g.responsibility_name,to_char(c.level_value))
,'10004',nvl(e.user_name,to_char(c.level_value))
,'Unknown');


--==================  FWK_HOMEPAGE_BRAND   (on Home page)======================	
SELECT b.profile_option_name, PROFILE_OPTION_VALUE
 FROM applsys.fnd_profile_option_values a, applsys.fnd_profile_options b
 where b.profile_option_name = 'SITENAME'
 and a.application_id = b.application_id
and a.profile_option_id = b.profile_option_id;
--
UPDATE FND_PROFILE_OPTION_VALUES FPOV
SET FPOV.PROFILE_OPTION_VALUE = '&TMPSID   '|| DECODE('&TMPSID', 'EBSTEST', 'Patch'
                       , 'EBSDEV1', 'PRJ Developement'
                       , 'EBSDEV2', 'PRJ Training2 OPS-50'
                       , 'EBSDEV3', 'LIVE Sand Box'
                       , 'EBSDEV4', 'PRJ Training1 OPS-50'
                       , 'EBSDEV5', 'PRJ Training TST2 '
                       , 'EBSDEV6', 'PRJ Sand Box'
                       , 'EBSDEV7', 'PRJ Training Gold OPS-50 '
                       , 'EBSDEV8', 'R2.2 Patch'
                       , 'EBSDEV9', 'Prod Support'
                       , 'EBSDEVE', 'LIVE Developement'
                       , 'EBSDEVA', 'DBA POC'
                       , 'EBSDEVB', 'FUNC-070'
                       , 'EBSDEVC', 'SIT2-070'
                       , 'EBSDEVD', 'SIT-OPS-050'
                       , 'EBSACCE', 'UAT'
                       , 'EBSTEST', 'Patch'
                      ) || ' ('||(select to_char(max(resetlogs_time),'YYYY-MM-DD') from V$DATABASE_INCARNATION)||')'
WHERE EXISTS (SELECT NULL FROM FND_PROFILE_OPTIONS FPO 
              WHERE FPOV.PROFILE_OPTION_ID = FPO.PROFILE_OPTION_ID
              AND FPO.PROFILE_OPTION_NAME = 'SITENAME');
commit;

	
	-- Modifier FWK_HOMEPAGE_BRAND
	--Check
	select user_function_name from FND_FORM_FUNCTIONS fff, FND_FORM_FUNCTIONS_TL ffft
	 where FFF.FUNCTION_ID = FFFT.FUNCTION_ID 
	      and fff.function_name = 'FWK_HOMEPAGE_BRAND';
	 --
	update FND_FORM_FUNCTIONS_TL
	set user_function_name = 'E-Business Suite - '||(SELECT PROFILE_OPTION_VALUE
	  FROM applsys.fnd_profile_option_values a, applsys.fnd_profile_options b
	  where b.profile_option_name = 'SITENAME'
	  and a.application_id = b.application_id
	  and a.profile_option_id = b.profile_option_id)
	where function_id in (select function_id from FND_FORM_FUNCTIONS where function_name = 'FWK_HOMEPAGE_BRAND');
	commit;
--===========================  SITENAME (on Form Page)=============================	
	SELECT b.profile_option_name, PROFILE_OPTION_VALUE
	  FROM apps.fnd_profile_option_values a, apps.fnd_profile_options b
	  where b.profile_option_name = 'SITENAME'
	  and a.application_id = b.application_id
	  and a.profile_option_id = b.profile_option_id;


