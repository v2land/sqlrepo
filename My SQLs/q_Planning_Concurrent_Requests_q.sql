
SELECT fcr.request_id,
        --fcr.parent_request_id,
        fcr.concurrent_program_id,
       DECODE(fcpt.user_concurrent_program_name,
              'Report Set',
              'Report Set:' || fcr.description,
              fcpt.user_concurrent_program_name) CONC_PROG_NAME,
       argument_text PARAMETERS,
      /* NVL2(fcr.resubmit_interval,
            'PERIODICALLY',
            NVL2(fcr.release_class_id, 'ON SPECIFIC DAYS', 'ONCE')) PROG_SCHEDULE_TYPE,
       DECODE(NVL2(fcr.resubmit_interval,
                   'PERIODICALLY',
                   NVL2(fcr.release_class_id, 'ON SPECIFIC DAYS', 'ONCE')),
              'PERIODICALLY',
              'EVERY ' || fcr.resubmit_interval || ' ' ||
              fcr.resubmit_interval_unit_code || ' FROM ' ||
              fcr.resubmit_interval_type_code || ' OF PREV RUN',
              'ONCE',
              'AT :' ||
              TO_CHAR(fcr.requested_start_date, 'DD-MON-RR HH24:MI'),
              'EVERY: ' || fcrc.class_info) PROG_SCHEDULE,*/
    DECODE(fcrc.class_type,'P','Periodic','S','On Specific Days','X','Advanced',fcrc.class_type) schedule_type,
    CASE
            WHEN fcrc.class_type = 'P' THEN 'Repeat every '
            || substr(fcrc.class_info,1,instr(fcrc.class_info,':') - 1)
            || DECODE(substr(fcrc.class_info,instr(fcrc.class_info,':',1,1) + 1,1),'N',' minutes','M',' months','H',' hours','D',' days')
            || DECODE(substr(fcrc.class_info,instr(fcrc.class_info,':',1,2) + 1,1),'S',' from the start of the prior run','C',' from the completion of the prior run'
)
            WHEN fcrc.class_type = 'S' THEN nvl2(dates.dates,'Dates: '
            || dates.dates
            || '. ',NULL)
            || DECODE(substr(fcrc.class_info,32,1),'1','Last day of month ')
            || DECODE(sign(to_number(substr(fcrc.class_info,33) ) ),'1','Days of week: '
            || DECODE(substr(fcrc.class_info,33,1),'1','Su ')
            || DECODE(substr(fcrc.class_info,34,1),'1','Mo ')
            || DECODE(substr(fcrc.class_info,35,1),'1','Tu ')
            || DECODE(substr(fcrc.class_info,36,1),'1','We ')
            || DECODE(substr(fcrc.class_info,37,1),'1','Th ')
            || DECODE(substr(fcrc.class_info,38,1),'1','Fr ')
            || DECODE(substr(fcrc.class_info,39,1),'1','Sa ') )
        END
    AS schedule,
       fu.user_name USER_NAME,
      to_char((select requested_start_date from fnd_concurrent_requests where request_id=fcr.parent_request_id),'DD-MON-YY HH24:MI:SS') as "prior_requested_start_date",
      to_char((select actual_completion_date from fnd_concurrent_requests where request_id=fcr.parent_request_id),'DD-MON-YY HH24:MI:SS') as "prior_completion_date",
     
       to_char(fcr.request_date,'DD-MON-YY HH24:MI:SS') submitted,
       to_char(fcr.requested_start_date,'DD-MON-YY HH24:MI:SS') requested_start_date,
       To_Char(fcr.ACTUAL_START_DATE,'DD-MON-YY HH24:MI:SS') started,
       To_Char(fcr.ACTUAL_COMPLETION_DATE,'DD-MON-YY HH24:MI:SS') completed,
       floor(((fcr.actual_completion_date-fcr.actual_start_date) *24*60*60)/3600) || ' h ' ||
        floor((((fcr.actual_completion_date-fcr.actual_start_date) *24*60*60) - floor(((fcr.actual_completion_date-fcr.actual_start_date)  *24*60*60)/3600)*3600)/60)  || ' m ' ||
        round((((fcr.actual_completion_date-fcr.actual_start_date) *24*60*60) - floor(((fcr.actual_completion_date-fcr.actual_start_date) *24*60*60)/3600)*3600 -
             (floor((((fcr.actual_completion_date-fcr.actual_start_date) *24*60*60) -  floor(((fcr.actual_completion_date-fcr.actual_start_date) *24*60*60)/3600)*3600)/60)*60) ))  || ' s ' time_difference      
  ,fcrc.class_info
  ,(SELECT 
            round(AVG ( (F.ACTUAL_COMPLETION_DATE - F.ACTUAL_START_DATE))* 24*60,0) "AVERAGE"
          --round((F.ACTUAL_COMPLETION_DATE - F.ACTUAL_START_DATE)*1440,0) "AVERAGE" 
            FROM APPLSYS.FND_CONCURRENT_REQUESTS F,
                 APPLSYS.FND_CONCURRENT_PROGRAMS P
            WHERE     PHASE_CODE = 'C'
                 AND STATUS_CODE = 'C'
                 AND F.CONCURRENT_PROGRAM_ID = P.CONCURRENT_PROGRAM_ID
                 AND P.CONCURRENT_PROGRAM_ID=fcr.CONCURRENT_PROGRAM_ID
            GROUP BY P.CONCURRENT_PROGRAM_NAME
      ) AVG_Time,
      round(((select actual_completion_date from fnd_concurrent_requests where request_id=fcr.parent_request_id) - 
      (select requested_start_date from fnd_concurrent_requests where request_id=fcr.parent_request_id))* 24*60,0) as prior_duration
      
  FROM apps.fnd_concurrent_programs_tl fcpt,
       apps.fnd_concurrent_requests    fcr,
       apps.fnd_user                   fu,
       apps.fnd_conc_release_classes   fcrc,
       (WITH date_schedules AS 
            (
            SELECT  release_class_id,
                RANK() OVER( PARTITION BY release_class_id ORDER BY s) a, s
               FROM
                (SELECT c.class_info,l,c.release_class_id,
                        DECODE(substr(c.class_info,l,1),'1',TO_CHAR(l) ) s
                    FROM
                        (SELECT level l FROM  dual  CONNECT BY level <= 31),
                        fnd_conc_release_classes c
                    WHERE
                        c.class_type = 'S'
                        AND   instr(substr(c.class_info,1,31),'1') > 0
                )
                WHERE s IS NOT NULL
            ) 
        SELECT
        release_class_id,
        substr(MAX(sys_connect_by_path(s,' ') ),2) dates
        FROM
        date_schedules
        START WITH  a = 1
        CONNECT BY NOCYCLE
        PRIOR a = a - 1
        GROUP BY release_class_id
      ) dates
 WHERE fcpt.application_id = fcr.program_application_id
   AND fcpt.concurrent_program_id = fcr.concurrent_program_id
   AND fcr.requested_by = fu.user_id
   AND fcr.phase_code in ('P','R') -- Pending (Scheduled)
   AND fcr.requested_start_date > SYSDATE
   AND fcpt.LANGUAGE = 'US'
   AND fcrc.release_class_id(+) = fcr.release_class_id
   AND fcrc.application_id(+) = fcr.release_class_app_id
   AND   dates.release_class_id (+) = fcr.release_class_id
   and round(((select actual_completion_date from fnd_concurrent_requests where request_id=fcr.parent_request_id) - 
      (select requested_start_date from fnd_concurrent_requests where request_id=fcr.parent_request_id))* 24*60,0) >15
   --and fcr.request_id = '24973406' request set id
   and  DECODE(fcpt.user_concurrent_program_name,
              'Report Set',
              'Report Set:' || fcr.description,
              fcpt.user_concurrent_program_name) LIKE '%%'
   order by  fcr.requested_start_date desc;
