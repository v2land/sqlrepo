
-- 1 ########################VC!!!!!!!!! DELAY Start Pending Standby Investigation!!!!!!!!!################################
--#  ACTUAL P/Q that are in delay    (INVESTIGATIOn REQUESTED_DATE differ from STARTED)
--#########################################################################################
select 
f.concurrent_program_id "conc pr ID", f.concurrent_program_name,f.user_concurrent_program_name
,a.request_id, a.PARENT_REQUEST_ID--, a.requested_by
, u.user_name
,To_Char(a.request_date,'DD-MON-YY HH24:MI:SS') submitted
,To_Char(a.REQUESTED_START_DATE,'DD-MON-YY HH24:MI:SS') REQUESTED_START_DATE
,To_Char(a.ACTUAL_START_DATE,'DD-MON-YY HH24:MI:SS') started
,To_Char(a.ACTUAL_COMPLETION_DATE,'DD-MON-YY HH24:MI:SS') completed
,Extract(day from (NUMTODSINTERVAL((sysdate - a.REQUESTED_START_DATE)*24*60,'minute')))  "Delayed Days"
,To_Char(TRUNC(sysdate) + NUMTODSINTERVAL((sysdate - a.REQUESTED_START_DATE)*1440,'minute'),'HH24:MI:SS') "Delayed time"
,a.phase_code, a.status_code, a.HOLD_FLAG, a.argument_text, 
a.RESUBMIT_INTERVAL, a.RESUBMIT_INTERVAL_TYPE_CODE, a.RESUBMIT_INTERVAL_UNIT_CODE
,uu.user_name "Updated By"
from    
APPLSYS.FND_CONCURRENT_REQUESTS a,
fnd_concurrent_programs_vl f,
fnd_user u, 
fnd_user uu
where 
a.HOLD_FLAG ='N'
and f.CONCURRENT_PROGRAM_ID=a.CONCURRENT_PROGRAM_ID
and u.user_id = a.requested_by
and uu.user_id = a.LAST_UPDATED_BY
and  a.phase_code = 'P' and a.status_code = 'Q'
--# Not Yet started bacause of Delay 
and sysdate > (a.REQUESTED_START_DATE + INTERVAL '1' MINUTE)  
--and a.ACTUAL_COMPLETION_DATE> SYSDATE-30
---------and a.REQUESTED_START_DATE > SYSDATE-1
--and request_id in ('21654900','21654549','21647375','21646020','21645974')
--and f.concurrent_program_name LIKE 'XXAOL0240_AUTO%'     ---Request Set Archiving AUTO Documents Interface
--and u.user_name = 'XXAPPSADM'
--# STARTED with Delay of specified Interval
--and a.ACTUAL_START_DATE > (a.REQUESTED_START_DATE + INTERVAL '5' MINUTE)  -- 
--# RunTime more than specific Interval
--and (a.ACTUAL_COMPLETION_DATE - a.ACTUAL_START_DATE)*60*24  > 10  -- PLUS QUE 30 MIN 
----and f.user_concurrent_program_name like 'Archiving%'
----# Running at specific moment
---and a.ACTUAL_START_DATE < To_Date(:State_CHECK_DATE,'DD-MON-YY HH24:MI:SS')
---and a.ACTUAL_COMPLETION_DATE > To_Date(:State_CHECK_DATE,'DD-MON-YY HH24:MI:SS')
--#RUNNING between specific period:
--and a.ACTUAL_COMPLETION_DATE > To_Date(:State_Begin_CHECK_DATE,'DD-MON-YY HH24:MI:SS')
--and a.ACTUAL_START_DATE <= To_Date(:State_End_CHECK_DATE,'DD-MON-YY HH24:MI:SS')
order by a.requested_start_date,a.ACTUAL_COMPLETION_DATE desc;
--Request Set Archiving CLAIMS Documents Interface


-- 2 ######################################################################################
--# List Incompatibilities   
--#########################################################################################
SELECT 
                            a1.user_concurrent_program_name Prog,
                            a3.concurrent_program_name Short,
                            a1.CONCURRENT_PROGRAM_ID CONC_PR_ID,
                            b1.CONCURRENT_PROGRAM_ID Incomp_CONC_PR_ID,
                            b1.user_concurrent_program_name Incompatible_Prog,
                            b3.concurrent_program_name Short_Prog,
                            DECODE (to_run_type,
                            'P', 'Program',
                            'S', 'Request set',
                            'UNKNOWN'
                            ) incompatible_type,
                            b2.application_name "Incompatible App"
                        FROM apps.fnd_concurrent_program_serial cps
                            ,apps.fnd_concurrent_programs_tl a1
                            ,apps.fnd_application_tl a2
                            ,apps.fnd_concurrent_programs a3
                            ,apps.fnd_concurrent_programs_tl b1
                            ,apps.fnd_application_tl b2
                            ,apps.fnd_concurrent_programs b3
                            WHERE a1.application_id = cps.running_application_id
                            AND a1.concurrent_program_id = cps.running_concurrent_program_id
                            AND a2.application_id = cps.running_application_id
                            AND a3.concurrent_program_id = a1.concurrent_program_id
                            AND b1.application_id = cps.to_run_application_id
                            AND b1.concurrent_program_id = cps.to_run_concurrent_program_id
                            AND b2.application_id = cps.to_run_application_id
                            AND b3.concurrent_program_id = b1.concurrent_program_id
                            AND a1.language = 'US'
                            AND a2.language = 'US'
                            AND b1.language = 'US'
                            AND b2.language = 'US'
                            and a1.user_concurrent_program_name like '%'||:user_concurrent_program_name||'%' --:user_concurrent_program_name --Request Set Archiving CLAIMS Documents Interface
                            ;
--Offers Earned Amt by Cust and Item - (CCBU)	XXONT0720	129393	40849	Funds Accrual Engine	OZF_FUND_ACCRUAL	Request set	Trade Management


--  ######################VC!!!!!!!! LIVE Investigation !!!!2 in 1!!!!!!!!!!############################################
--3 # What are the Actual Running that are in the list of the Incompatible Programs of a Any actual PENDING STANDBY 
--  # What RUNNING Program is Blocking the Start of the PENDING STANDBY one due to the incompatibilities 
--  ######################################################################################################

select 
a.request_id "Waiting P/Q Request_ID"
,inc.Prog "Waiting Program P/Q"
,inc.Short 
--,u.user_name "Waiting User"
--,inc.CONC_PR_ID "Waiting Prog_ID"
,a.phase_code
,a.status_code
,a.HOLD_FLAG
,To_Char(a.request_date,'DD-MON-YY HH24:MI:SS') submitted
,To_Char(a.REQUESTED_START_DATE,'DD-MON-YY HH24:MI:SS') REQUESTED_START_DATE
,To_Char(a.ACTUAL_START_DATE,'DD-MON-YY HH24:MI:SS') started
,To_Char(a.ACTUAL_COMPLETION_DATE,'DD-MON-YY HH24:MI:SS') completed
,Extract(day from (NUMTODSINTERVAL((sysdate - a.REQUESTED_START_DATE)*24*60,'minute')))  "Delayed Days"
,To_Char(TRUNC(sysdate) + NUMTODSINTERVAL((sysdate - a.REQUESTED_START_DATE)*1440,'minute'),'HH24:MI:SS') "Delayed time"
,'--->' "--->"
,ai.request_id "Running Req_ID"
,ai.PARENT_REQUEST_ID "Parent Req_ID"
,inc.Incompatible_Prog "Delayed By Incomaptible:"
,inc.Incompatible_Short 
--,inc.Incomp_CONC_PR_ID "Inc.Prog_ID"
--,ui.user_name "Inc User"
,To_Char(ai.request_date,'DD-MON-YY HH24:MI:SS') submitted
,To_Char(ai.REQUESTED_START_DATE,'DD-MON-YY HH24:MI:SS') REQUESTED_START_DATE
,To_Char(ai.ACTUAL_START_DATE,'DD-MON-YY HH24:MI:SS') started
,To_Char(ai.ACTUAL_COMPLETION_DATE,'DD-MON-YY HH24:MI:SS') completed
,Extract(day from (NUMTODSINTERVAL((ai.ACTUAL_START_DATE - ai.REQUESTED_START_DATE)*24*60,'minute')))  "Delayed Days"
,TO_CHAR (TRUNC(sysdate) + NUMTODSINTERVAL((ai.ACTUAL_START_DATE - ai.REQUESTED_START_DATE)*24*60,'minute'),'HH24:MI:SS') "Started Late delay"
,ai.phase_code
,ai.status_code
,ai.argument_text
--Schedule Type:
--,DECODE(c.class_type,'P','Periodic','S','On Specific Days','X','Advanced',c.class_type) schedule_type
,ai.RESUBMIT_INTERVAL_TYPE_CODE,ai.RESUBMIT_INTERVAL,ai.RESUBMIT_INTERVAL_UNIT_CODE

from    
FND_CONCURRENT_REQUESTS a,
FND_CONCURRENT_REQUESTS ai, 
--fnd_conc_release_classes c,
--fnd_user u,
--fnd_user ui,
--#Check Incompatibilities:
(SELECT 
                            a1.user_concurrent_program_name Prog,
                            a3.concurrent_program_name Short,
                            a1.CONCURRENT_PROGRAM_ID CONC_PR_ID,
                            b1.CONCURRENT_PROGRAM_ID Incomp_CONC_PR_ID,
                            b1.user_concurrent_program_name Incompatible_Prog,
                            b3.concurrent_program_name Incompatible_Short,
                            DECODE (to_run_type,
                            'P', 'Program',
                            'S', 'Request set',
                            'UNKNOWN'
                            ) incompatible_type,
                            b2.application_name "Incompatible App"
                        FROM apps.fnd_concurrent_program_serial cps
                            ,apps.fnd_concurrent_programs_tl a1
                            ,apps.fnd_application_tl a2
                            ,apps.fnd_concurrent_programs a3
                            ,apps.fnd_concurrent_programs_tl b1
                            ,apps.fnd_application_tl b2
                            ,apps.fnd_concurrent_programs b3
                            WHERE a1.application_id = cps.running_application_id
                            AND a1.concurrent_program_id = cps.running_concurrent_program_id
                            AND a2.application_id = cps.running_application_id
                            AND a3.concurrent_program_id = a1.concurrent_program_id
                            AND b1.application_id = cps.to_run_application_id
                            AND b1.concurrent_program_id = cps.to_run_concurrent_program_id
                            AND b2.application_id = cps.to_run_application_id
                            AND b3.concurrent_program_id = b1.concurrent_program_id
                            AND a1.language = 'US'
                            AND a2.language = 'US'
                            AND b1.language = 'US'
                            AND b2.language = 'US'
                            --and a1.user_concurrent_program_name like '%'||:user_concurrent_program_name||'%' --:user_concurrent_program_name --Request Set Archiving CLAIMS Documents Interface
                            ) inc
where a.HOLD_FLAG ='N' 
and a.CONCURRENT_PROGRAM_ID = inc.CONC_PR_ID
and  ai.CONCURRENT_PROGRAM_ID = inc.Incomp_CONC_PR_ID
--and u.user_id = a.requested_by
--and ui.user_id = ai.requested_by
--and c.release_class_id=a.release_class_id
and  a.phase_code = 'P' and a.status_code = 'Q' -- Waiting :PENDING/StandBy
--    and  ai.phase_code = 'R' -- RUNNING
and (a.REQUESTED_START_DATE + INTERVAL '10' second) < sysdate -- (Delay > Specified Interval) , to exclude the actual Running that starts just 2-3 seconds in late
--#where the incompatibles are in Running state or scheduled in the interval from the moment the Pending one should be started
and   (   ai.phase_code = 'R' -- Inc RUNNING
         or
            ( ai.phase_code = 'P' -- Inc SCHEDULED in the actual DELAY interval of Waitings
             and ai.REQUESTED_START_DATE > a.REQUESTED_START_DATE -- (si l'incompatible est planifi� entre le requested_start et sysdate )
             and ai.REQUESTED_START_DATE <= sysdate -- (si l'incompatible est planifi� entre le requested_start et sysdate )
            )
        )
order by a.requested_start_date,a.ACTUAL_COMPLETION_DATE desc;






--######################################################################
----------Details Drilldown of a Concurrent Request Set that IS Running or WAS Executed:
--  using connect by prior
-- what child subprogram status is.. ? 

SELECT /*+ ORDERED USE_NL(x fcr fcp fcptl)*/
                                fcr.request_id "Request ID", fcr.PARENT_REQUEST_ID "Parent R ID",
                                fcptl.user_concurrent_program_name "Program Name",
                                fcp.CONCURRENT_PROGRAM_ID C_PROG_ID,
                                fcr.phase_code P_code,
                                fcr.status_code S_code
                                 --( fcr.actual_completion_date - fcr.actual_start_date ) * 1440 "Elapsed",
                                ,To_Char(fcr.request_date,'DD-MON-YY HH24:MI:SS') submitted
                                ,To_Char(fcr.REQUESTED_START_DATE,'DD-MON-YY HH24:MI:SS') REQUESTED_START_DATE
                                ,To_Char(fcr.ACTUAL_START_DATE,'DD-MON-YY HH24:MI:SS') started
                                ,To_Char(fcr.ACTUAL_COMPLETION_DATE,'DD-MON-YY HH24:MI:SS') completed
                                ,TO_CHAR (TRUNC(sysdate) + NUMTODSINTERVAL((fcr.actual_start_date - fcr.REQUESTED_START_DATE)*24*60,'minute'),'HH24:MI:SS') "StartDelay"
                                ,TO_CHAR ( TRUNC(sysdate) + NUMTODSINTERVAL ((fcr.actual_completion_date - fcr.actual_start_date)*24*60,'minute'),'HH24:MI:SS') "Process_TIME",
                                fcr.oracle_process_id
                                "Trace ID"
FROM   (SELECT /*+ index (fcr1 fnd_concurrent_requests_n3) */ fcr1.request_id
        FROM   apps.fnd_concurrent_requests fcr1
        WHERE  1 = 1
        START WITH fcr1.request_id = '22064153'
        CONNECT BY PRIOR fcr1.request_id = fcr1.parent_request_id) x,
       apps.fnd_concurrent_requests fcr,
       apps.fnd_concurrent_programs fcp,
       apps.fnd_concurrent_programs_tl fcptl
WHERE  fcr.request_id = x.request_id
       AND fcr.concurrent_program_id = fcp.concurrent_program_id
       AND fcr.program_application_id = fcp.application_id
       AND fcp.application_id = fcptl.application_id
       AND fcp.concurrent_program_id = fcptl.concurrent_program_id
       AND fcptl.LANGUAGE = 'US'
       and fcptl.user_concurrent_program_name != 'Request Set Stage'  --

ORDER  BY fcr.actual_start_date;






-- ######################VC!!!! PAST Investigation !!!!!!!!2 in 1!!!!!!!!!!############################################
--5# What are the Historical Running that were in the list of the Incompatible Programs of a Any historical PENDING STANDBY 
-- What RUNNING Program was Blocking the Start of the PENDING STANDBY one due to the incompatibilities 
-- ######################################################################################################
/*
select 
a.request_id "Waiting P/Q Request_ID"
,inc.Prog "Waiting Program P/Q"
,inc.Short 
--,u.user_name "Waiting User"
--,inc.CONC_PR_ID "Waiting Prog_ID"
,a.phase_code
,a.status_code
,a.HOLD_FLAG
,To_Char(a.request_date,'DD-MON-YY HH24:MI:SS') submitted
,To_Char(a.REQUESTED_START_DATE,'DD-MON-YY HH24:MI:SS') REQUESTED_START_DATE
,To_Char(a.ACTUAL_START_DATE,'DD-MON-YY HH24:MI:SS') started
,To_Char(a.ACTUAL_COMPLETION_DATE,'DD-MON-YY HH24:MI:SS') completed
,Extract(day from (NUMTODSINTERVAL((sysdate - a.REQUESTED_START_DATE)*24*60,'minute')))  "Delayed Days"
,To_Char(TRUNC(sysdate) + NUMTODSINTERVAL((sysdate - a.REQUESTED_START_DATE)*1440,'minute'),'HH24:MI:SS') "Delayed time"
,'--->' "--->"
,ai.request_id "Running Req_ID"
,ai.PARENT_REQUEST_ID "Parent Req_ID"
,inc.Incompatible_Prog "Delayed By Incomaptible:"
,inc.Incompatible_Short 
--,inc.Incomp_CONC_PR_ID "Inc.Prog_ID"
--,ui.user_name "Inc User"
,To_Char(ai.request_date,'DD-MON-YY HH24:MI:SS') submitted
,To_Char(ai.REQUESTED_START_DATE,'DD-MON-YY HH24:MI:SS') REQUESTED_START_DATE
,To_Char(ai.ACTUAL_START_DATE,'DD-MON-YY HH24:MI:SS') started
,To_Char(ai.ACTUAL_COMPLETION_DATE,'DD-MON-YY HH24:MI:SS') completed
,Extract(day from (NUMTODSINTERVAL((ai.ACTUAL_START_DATE - ai.REQUESTED_START_DATE)*24*60,'minute')))  "Delayed Days"
,TO_CHAR (TRUNC(sysdate) + NUMTODSINTERVAL((ai.ACTUAL_START_DATE - ai.REQUESTED_START_DATE)*24*60,'minute'),'HH24:MI:SS') "Started Late delay"
,ai.phase_code
,ai.status_code
,ai.argument_text
--Schedule Type:
--,DECODE(c.class_type,'P','Periodic','S','On Specific Days','X','Advanced',c.class_type) schedule_type
,ai.RESUBMIT_INTERVAL_TYPE_CODE,ai.RESUBMIT_INTERVAL,ai.RESUBMIT_INTERVAL_UNIT_CODE

from    
FND_CONCURRENT_REQUESTS a,
FND_CONCURRENT_REQUESTS ai, 
--fnd_conc_release_classes c,
--fnd_user u,
--fnd_user ui,
--#Check Incompatibilities:
(SELECT 
                            a1.user_concurrent_program_name Prog,
                            a3.concurrent_program_name Short,
                            a1.CONCURRENT_PROGRAM_ID CONC_PR_ID,
                            b1.CONCURRENT_PROGRAM_ID Incomp_CONC_PR_ID,
                            b1.user_concurrent_program_name Incompatible_Prog,
                            b3.concurrent_program_name Incompatible_Short,
                            DECODE (to_run_type,
                            'P', 'Program',
                            'S', 'Request set',
                            'UNKNOWN'
                            ) incompatible_type,
                            b2.application_name "Incompatible App"
                        FROM apps.fnd_concurrent_program_serial cps
                            ,apps.fnd_concurrent_programs_tl a1
                            ,apps.fnd_application_tl a2
                            ,apps.fnd_concurrent_programs a3
                            ,apps.fnd_concurrent_programs_tl b1
                            ,apps.fnd_application_tl b2
                            ,apps.fnd_concurrent_programs b3
                            WHERE a1.application_id = cps.running_application_id
                            AND a1.concurrent_program_id = cps.running_concurrent_program_id
                            AND a2.application_id = cps.running_application_id
                            AND a3.concurrent_program_id = a1.concurrent_program_id
                            AND b1.application_id = cps.to_run_application_id
                            AND b1.concurrent_program_id = cps.to_run_concurrent_program_id
                            AND b2.application_id = cps.to_run_application_id
                            AND b3.concurrent_program_id = b1.concurrent_program_id
                            AND a1.language = 'US'
                            AND a2.language = 'US'
                            AND b1.language = 'US'
                            AND b2.language = 'US'
                            and a1.user_concurrent_program_name like '%'||:user_concurrent_program_name||'%' --'%Archiving % Documents Interface%' --
                            --and a1.user_concurrent_program_name like '%'||:user_concurrent_program_name||'%' --:user_concurrent_program_name --Request Set Archiving CLAIMS Documents Interface
                            ) inc
where a.HOLD_FLAG ='N' 
and a.CONCURRENT_PROGRAM_ID = inc.CONC_PR_ID
and  ai.CONCURRENT_PROGRAM_ID = inc.Incomp_CONC_PR_ID
--and u.user_id = a.requested_by
--and ui.user_id = ai.requested_by
--and c.release_class_id=a.release_class_id
--and  a.phase_code = 'P' and a.status_code = 'Q' -- Waiting :PENDING/StandBy
--    and  ai.phase_code = 'R' -- RUNNING
and (a.REQUESTED_START_DATE + INTERVAL '10' second) < To_Date(:State_End_CHECK_DATE,'DD-MON-YY HH24:MI:SS') -- (Delay > Specified Interval) , to exclude the actual Running that starts just 2-3 seconds in late
--#where the incompatibles are in Running state or scheduled in the interval from the moment the Pending one should be started
and   (  -- ai.phase_code = 'R' -- Inc RUNNING
         or
            (-- ai.phase_code = 'P' -- Inc SCHEDULED in the actual DELAY interval of Waitings
             and ai.REQUESTED_START_DATE > a.REQUESTED_START_DATE -- (si l'incompatible est planifi� entre le requested_start et sysdate )
             and ai.REQUESTED_START_DATE <= To_Date(:State_End_CHECK_DATE,'DD-MON-YY HH24:MI:SS') -- (si l'incompatible est planifi� entre le requested_start et sysdate )
            )
        )
and ai.ACTUAL_COMPLETION_DATE > To_Date(:State_Begin_CHECK_DATE,'DD-MON-YY HH24:MI:SS')
and ai.ACTUAL_START_DATE <= To_Date(:State_End_CHECK_DATE,'DD-MON-YY HH24:MI:SS')
and ai.ACTUAL_COMPLETION_DATE > To_Date(:State_Begin_CHECK_DATE,'DD-MON-YY HH24:MI:SS')
and ai.ACTUAL_START_DATE <= To_Date(:State_End_CHECK_DATE,'DD-MON-YY HH24:MI:SS')
order by a.requested_start_date,a.ACTUAL_COMPLETION_DATE desc;

*/








--####################################################################################################################
--####################################################################################################################
--####################################################################################################################

-- REQUEST SET:
select frt.responsibility_name, frg.request_group_name,
   frgu.request_unit_type,frgu.request_unit_id,
   fcpt.user_request_set_name
   From apps.fnd_Responsibility fr, apps.fnd_responsibility_tl frt,
   apps.fnd_request_groups frg, apps.fnd_request_group_units frgu,
   apps.fnd_request_Sets_tl fcpt
   where frt.responsibility_id = fr.responsibility_id
   and frg.request_group_id = fr.request_group_id
   and frgu.request_group_id = frg.request_group_id
   and fcpt.request_set_id = frgu.request_unit_id
   and frt.language = USERENV('LANG')
   and fcpt.language = USERENV('LANG')
   and fcpt.user_request_set_name = :request_set_name
   order by 1,2,3,4;
   
   
   
    --##QUERY for RUNNING##

select fcr.request_id req_id,
fcr.phase_code||'/'||fcr.status_code sta,
fcr.pid pid,
sess.sid sid,
sess.inst_id inst_id,
fcr.running_time time,
nvl(t.used_urec,0) u_rec,
substrb(fcr.user_desc,1,8) user_desc,
substrb(decode(fcr.pgm_code,
'FNDRSSUB',fcr.pgm_name||'-'||rs.user_request_set_name,
'FNDRSSTG',fcr.pgm_name||'-'||rs.user_request_set_name
||'-'||rss.user_stage_name,
fcr.pgm_name), 1,48) pgm_name
from gv$session sess,
gv$transaction t,
fnd_request_sets_vl rs,
fnd_request_set_stages_vl rss,
(select /*+ ordered
index (r fnd_concurrent_requests_n7)
index (pt fnd_concurrent_programs_tl_u1) */
r.request_id request_id,
r.program_application_id application_id,
c.os_process_id pid,
r.oracle_session_id audsid,
r.concurrent_program_id concurrent_program_id,
p.concurrent_program_name pgm_code,
u.description user_desc,
decode(p.concurrent_program_name,
'FNDRSSUB','SET',
'FNDRSSTG','Set Stage',
pt.user_concurrent_program_name) pgm_name,
r.phase_code phase_code,
r.status_code status_code,
to_char(r.request_date,'yymmdd hh24:mi:ss') request_date, to_char(r.actual_completion_date,'yymmdd hh24:mi:ss') actual_completion_date,
to_char(r.requested_start_date,'yymmdd hh24:mi:ss') requested_start_date,
ceil((nvl(r.actual_completion_date,sysdate)-r.actual_start_date)*1440) running_time,
r.actual_start_date actual_start_date_org,
u.user_id user_id,
u.user_name user_name,
r.argument_text arguments,
decode(p.concurrent_program_name,
'FNDRSSUB',r.argument2,
'FNDRSSTG',r.argument2,-1) request_set_id,
decode(p.concurrent_program_name,
'FNDRSSTG',r.argument3,
-1) request_set_stage_id
from
fnd_concurrent_requests r,
fnd_concurrent_programs_tl pt,
fnd_concurrent_programs p,
fnd_user u,
fnd_concurrent_processes c
where r.requested_by = u.user_id
and r.program_application_id = pt.application_id
and r.concurrent_program_id = pt.concurrent_program_id
and pt.language = 'US'
and pt.application_id = p.application_id
and pt.concurrent_program_id = p.concurrent_program_id
and r.controlling_manager = c.concurrent_process_id
and r.phase_code = 'R'
and r.status_code = 'R') fcr
where fcr.audsid = sess.audsid(+)
and sess.saddr = t.ses_addr(+)
and fcr.request_set_id = rs.request_set_id(+)
and fcr.request_set_id = rss.request_set_id(+)
and fcr.request_set_stage_id = rss.request_set_stage_id(+)
order by fcr.actual_start_date_org desc, fcr.running_time, fcr.request_id desc;

/*
22022980	R/R	17891338	1653	1	2	82242	Millette	Create receipt transactions into lockbox interfa
22022466	R/R	6291776	741	1	21	1	DAOUST, 	Detailed Report On Rebate Paid By Check
22017448	R/R	3080548	1589	1	60	1	COOK, JE	Number of days under status Report
22021558	R/R	18612502	1941	1	64	12	Generic 	Agropur - Buy Back Worker
22018175	R/R	24248522	28	1	187	0	Generic 	Agropur - Buy Back Process
22018019	R/R	13894142	2917	1	194	1	Wiersma,	Send Receipt of Payment Notifications
22014103	R/R	21102604	3769	1	426	31	ARCONTRO	AR Invoice new
22013975	R/R	15007880	330	1	431	0	ARCONTRO	AR New Invoices
*/
  
  
  --## QUERY for  Request SET ##
    --Query to find out Request set details
SELECT 
rs.REQUEST_SET_ID
,rs.CONCURRENT_PROGRAM_ID
,rs.user_request_set_name "Request Set"
, rss.display_sequence Seq
, cp.concurrent_program_id as "Child CP_ID"
, cp.user_concurrent_program_name "Concurrent Program"
, e.EXECUTABLE_NAME
, e.execution_file_name
, lv.meaning file_type
,fat.application_name "Application Name"
-- ,get_appl_name(e.application_id) "Application Name"
FROM apps.fnd_request_sets_vl rs
, apps.fnd_req_set_stages_form_v rss
, applsys.fnd_request_set_programs rsp
, apps.fnd_concurrent_programs_vl cp
, apps.fnd_executables e
, apps.fnd_lookup_values lv
, apps.fnd_application_tl fat
WHERE 1=1
--and rs.application_id IN ( 20006 )
AND rs.application_id = rss.set_application_id
AND rs.request_set_id = rss.request_set_id
AND rs.user_request_set_name like 'Number of days under status Report'--'%AR - Print new invoices%'--:p_request_set_name --%Archiving AUTO Documents Interface%
AND e.APPLICATION_ID =FAT.APPLICATION_ID
AND rss.set_application_id = rsp.set_application_id
AND rss.request_set_id = rsp.request_set_id
AND rss.request_set_stage_id = rsp.request_set_stage_id
AND rsp.program_application_id = cp.application_id
AND rsp.concurrent_program_id = cp.concurrent_program_id
AND cp.executable_id = e.executable_id
AND cp.executable_application_id = e.application_id
AND lv.lookup_type = 'CP_EXECUTION_METHOD_CODE'
AND lv.lookup_code = e.execution_method_code
and lv.language='US'
and fat.language='US'
AND rs.end_date_active IS NULL
ORDER BY 1,2;
/*Request Set sequences:
Archiving AUTO Documents Interface	140	Copy File Process Ebs Auto Archiving (Shell)	       XXAOL0248	XXAOL0248	Host	AGROPUR - EBS  
Archiving AUTO Documents Interface	150	Archiving Document Process Invoice (single Org.)    	XXAOL0244	XXAOL0240_ARCHIVING_DOCU_INT.Process_Invoice	PL/SQL Stored Procedure	AGROPUR - EBS
Archiving AUTO Documents Interface	160	Create Directory Ebs Archiving (Shell)	XXAOL0246	   XXAOL0246	Host	AGROPUR - EBS
Archiving AUTO Documents Interface	170	Copy, Remove file Ebs erp_archiving attachment (Shell)	XXAOL0247	XXAOL0247	Host	AGROPUR - EBS
Archiving AUTO Documents Interface	180	Archiving Documents Interface (Control report)      	XXAOL0245	XXAOL0245	Oracle Reports	AGROPUR - EBS
*/


--Locate Details Run of Those Programs os Set, and get Request ID
select
     f.request_id,f.PARENT_REQUEST_ID
    ,pt.user_concurrent_program_name user_conc_program_name
    ,To_Char(f.request_date,'DD-MON-YY HH24:MI:SS') submitted
    ,To_Char(f.REQUESTED_START_DATE,'DD-MON-YY HH24:MI:SS') REQUESTED_START_DATE
    ,To_Char(f.ACTUAL_START_DATE,'DD-MON-YY HH24:MI:SS') started
    ,To_Char(f.ACTUAL_COMPLETION_DATE,'DD-MON-YY HH24:MI:SS') completed
       ,floor(((f.actual_completion_date-f.actual_start_date)*24*60*60)/3600)|| ' h ' || 
        floor((((f.actual_completion_date-f.actual_start_date)*24*60*60) - floor(((f.actual_completion_date-f.actual_start_date)*24*60*60)/3600)*3600)/60)|| ' m ' ||
        round((((f.actual_completion_date-f.actual_start_date)*24*60*60) - floor(((f.actual_completion_date-f.actual_start_date)*24*60*60)/3600)*3600 - (floor((((f.actual_completion_date-f.actual_start_date)*24*60*60) -
        floor(((f.actual_completion_date-f.actual_start_date)*24*60*60)/3600)*3600)/60)*60) )) || ' s ' time_difference
      ,p.concurrent_program_name concurrent_program_name
      ,decode(f.phase_code,'R','Running','C','Complete',f.phase_code) Phase
      ,f.status_code
      ,substr(f.completion_text,1,20) completion
       ,substr(f.argument1,1,25)       arg1
       ,substr(f.argument2,1,25)       arg2
       ,substr(f.argument3,1,25)       arg3
       ,substr(f.argument4,1,25)       arg4
       ,substr(f.argument5,1,25)       arg5
       ,substr(f.argument6,1,25)       arg6
       ,substr(f.argument7,1,25)       arg7
       ,substr(f.argument8,1,25)       arg8
       ,substr(f.argument9,1,25)       arg9
       ,substr(f.argument10,1,25)      arg1
       ,substr(f.argument11,1,25)      arg11
       ,substr(f.argument12,1,25)      arg12
from  apps.fnd_concurrent_programs p,
      apps.fnd_concurrent_programs_tl pt,
      apps.fnd_concurrent_requests f
where --PHASE_CODE = 'C'
      --AND STATUS_CODE = 'C'
      --AND
      f.concurrent_program_id = p.concurrent_program_id
      and f.program_application_id = p.application_id
      and f.concurrent_program_id = pt.concurrent_program_id
      and f.program_application_id = pt.application_id
      AND pt.language = USERENV('Lang')
 -- and f.actual_start_date is not null
      AND f.request_date > '21-NOV-2018'
      --and p.concurrent_program_name in ('XXAOL0240_AUTO')
      and pt.user_concurrent_program_name like ('%Archiving % Documents Interface%')  --Request Set Archiving INVOICE Documents Interface
      --and TRUNC(((f.actual_completion_date-f.actual_start_date)/(1/24))*60) > 10
order by
      f.actual_start_date desc;
--21998750	Request Set Archiving AUTO Documents Interface					 h  m  s 	XXAOL0240_AUTO	P	Q		20005	2038										
--22-NOV-18 05:00:00	22-NOV-18 08:16:18
--check if the request_id is � Request Set



--###########################################################################################
--#  Start, Differ, Stop, date or requests   (INVESTIGATIOn REQUESTED_DATE differ from STARTED)
--#########################################################################################
select 
f.concurrent_program_id "conc pr ID", f.concurrent_program_name,f.user_concurrent_program_name
,a.request_id, a.PARENT_REQUEST_ID--, a.requested_by
, u.user_name
,To_Char(a.request_date,'DD-MON-YY HH24:MI:SS') submitted
,To_Char(a.REQUESTED_START_DATE,'DD-MON-YY HH24:MI:SS') REQUESTED_START_DATE
,To_Char(a.ACTUAL_START_DATE,'DD-MON-YY HH24:MI:SS') started
,To_Char(a.ACTUAL_COMPLETION_DATE,'DD-MON-YY HH24:MI:SS') completed
,a.phase_code, a.status_code, a.HOLD_FLAG, a.argument_text, 
a.RESUBMIT_INTERVAL, a.RESUBMIT_INTERVAL_TYPE_CODE, a.RESUBMIT_INTERVAL_UNIT_CODE
from    
APPLSYS.FND_CONCURRENT_REQUESTS a,
fnd_concurrent_programs_vl f,
fnd_user u 
where 
f.CONCURRENT_PROGRAM_ID=a.CONCURRENT_PROGRAM_ID
and u.user_id = a.requested_by
--and a.status_code = 'C' and a.phase_code = 'C'
--and a.ACTUAL_COMPLETION_DATE> SYSDATE-30
and a.REQUESTED_START_DATE > SYSDATE-7
--and request_id in ('21654900','21654549','21647375','21646020','21645974')
--and f.concurrent_program_name LIKE 'XXAOL0240_AUTO%'     ---Request Set Archiving AUTO Documents Interface
--and u.user_name = 'XXAPPSADM'
--# STARTED with Delay of specified Interval
--and a.ACTUAL_START_DATE > (a.REQUESTED_START_DATE + INTERVAL '5' MINUTE)  -- !!! CHECHK The BUG SR		Severity 4		SR 3-18426826121 : The requested_start_date in the fnd_concurrent_requests is some time one day late
--# RunTime more than specific Interval
and (a.ACTUAL_COMPLETION_DATE - a.ACTUAL_START_DATE)*60*24  > 10  -- PLUS QUE 30 MIN 
----and f.user_concurrent_program_name like 'Archiving%'
----# Running at specific moment
---and a.ACTUAL_START_DATE < To_Date(:State_CHECK_DATE,'DD-MON-YY HH24:MI:SS')
---and a.ACTUAL_COMPLETION_DATE > To_Date(:State_CHECK_DATE,'DD-MON-YY HH24:MI:SS')
--#RUNNING between specific period:
and a.ACTUAL_COMPLETION_DATE > To_Date(:State_Begin_CHECK_DATE,'DD-MON-YY HH24:MI:SS')
and a.ACTUAL_START_DATE <= To_Date(:State_End_CHECK_DATE,'DD-MON-YY HH24:MI:SS')
order by a.requested_start_date,a.ACTUAL_COMPLETION_DATE desc;

--1) add a day
select sysdate + INTERVAL '1' DAY from dual;
--2) add 20 days
select sysdate + INTERVAL '20' DAY from dual;
--2) add some minutes
select sysdate + INTERVAL '1' DAY  from dual;
select numtodsinterval(5,'MINUTE'),INTERVAL '5' MINUTE from dual;
/*
Request Set AR - Print new invoices (CRP)
Number of days under status Report
Inventory On-Hand Report
*/

SELECT program_application_id, concurrent_program_id, cd_id
		  --INTO   conc_app_id, conc_id, conc_cd_id
		  FROM   fnd_concurrent_requests
		  WHERE  request_id = :p_req_id;

--###########################################################################################
-- ###############           ALL INCOMPATIBILITIES    ###################################--
--###########################################################################################
--How to check  Incompatibilities in concurrent requests

SELECT a2.application_name, 
DECODE (running_type,
'P', 'Program',
'S', 'Request set',
'UNKNOWN'
) "Type",
a1.user_concurrent_program_name,
a1.CONCURRENT_PROGRAM_ID CONC_PR_ID,
b1.CONCURRENT_PROGRAM_ID Incomp_CONC_PR_ID,
b1.user_concurrent_program_name "Incompatible_Prog",
DECODE (to_run_type,
'P', 'Program',
'S', 'Request set',
'UNKNOWN'
) incompatible_type,
b2.application_name "Incompatible App"
FROM apps.fnd_concurrent_program_serial cps,
apps.fnd_concurrent_programs_tl a1,
apps.fnd_concurrent_programs_tl b1,
apps.fnd_application_tl a2,
apps.fnd_application_tl b2
WHERE a1.application_id = cps.running_application_id
AND a1.concurrent_program_id = cps.running_concurrent_program_id
AND a2.application_id = cps.running_application_id
AND b1.application_id = cps.to_run_application_id
AND b1.concurrent_program_id = cps.to_run_concurrent_program_id
AND b2.application_id = cps.to_run_application_id
AND a1.language = 'US'
AND a2.language = 'US'
AND b1.language = 'US'
AND b2.language = 'US'
and a1.user_concurrent_program_name like '%Archiving CLAIMS Documents Interface%'
order by 4,5;
/* INCOMPATIBLES:
240408	OUT: EDI 882 Invoices Summary
240448	Request Set Archiving AUTO Documents Interface
240449	Request Set Archiving CLAIMS Documents Interface
240450	Request Set Archiving INVOICE Documents Interface
240451	Request Set Archiving MANUAL Documents Interface
240452	Request Set AGR: �INVOICING INTERFACE � SUBSET (CRP)
240453	Request Set AGR: INVOICING INTERFACE � MINI (CRP)
240454	Request Set AGR: INVOICING INTERFACE � FULL (CRP)
240455	Request Set AGR: INVOICING INTERFACE � CORRECTION (CRP)
240456	Request Set AGR: INVOICING DAILY VALIDATION (CRP)
240458	Request Set AR - Print new invoices (CRP)
*/


--######################################################################
----------Details Drilldown of a Concurrent Request Set that IS Running or WAS Executed:
--  using connect by prior

SELECT /*+ ORDERED USE_NL(x fcr fcp fcptl)*/
                                fcr.request_id "Request ID", fcr.PARENT_REQUEST_ID "Parent R ID",
                                fcptl.user_concurrent_program_name "Program Name",
                                fcr.phase_code P_code,
                                fcr.status_code S_code,
                                fcr.COMPLETION_TEXT
                                 --( fcr.actual_completion_date - fcr.actual_start_date ) * 1440 "Elapsed",
                                ,To_Char(fcr.request_date,'DD-MON-YY HH24:MI:SS') submitted
                                ,To_Char(fcr.REQUESTED_START_DATE,'DD-MON-YY HH24:MI:SS') REQUESTED_START_DATE
                                ,To_Char(fcr.ACTUAL_START_DATE,'DD-MON-YY HH24:MI:SS') started
                                ,To_Char(fcr.ACTUAL_COMPLETION_DATE,'DD-MON-YY HH24:MI:SS') completed
                                ,TO_CHAR (TRUNC(sysdate) + NUMTODSINTERVAL((fcr.actual_start_date - fcr.REQUESTED_START_DATE)*24*60,'minute'),'HH24:MI:SS') "Delay"
                                ,TO_CHAR ( TRUNC(sysdate) + NUMTODSINTERVAL ((fcr.actual_completion_date - fcr.actual_start_date)*24*60,'minute'),'HH24:MI:SS') "Process_TIME",
                                fcr.oracle_process_id
                                "Trace ID"
FROM   (SELECT /*+ index (fcr1 fnd_concurrent_requests_n3) */ fcr1.request_id
        FROM   apps.fnd_concurrent_requests fcr1
        WHERE  1 = 1
        START WITH fcr1.request_id = '22090020'
        CONNECT BY PRIOR fcr1.request_id = fcr1.parent_request_id) x,
       apps.fnd_concurrent_requests fcr,
       apps.fnd_concurrent_programs fcp,
       apps.fnd_concurrent_programs_tl fcptl
WHERE  fcr.request_id = x.request_id
       AND fcr.concurrent_program_id = fcp.concurrent_program_id
       AND fcr.program_application_id = fcp.application_id
       AND fcp.application_id = fcptl.application_id
       AND fcp.concurrent_program_id = fcptl.concurrent_program_id
       AND fcptl.LANGUAGE = 'US'
       and fcptl.user_concurrent_program_name != 'Request Set Stage'  --

ORDER  BY fcr.actual_start_date;

--######################!!!!!!!!!!!!BEST for past investigations!!!!!!!!!!############################################
--# What are the Incompatible Programs of a Specific Programm, that Ran in the specific period of time 
--#########################################################################################
select 
f.concurrent_program_id "conc pr ID", f.concurrent_program_name,f.user_concurrent_program_name
,a.request_id, a.PARENT_REQUEST_ID--, a.requested_by
, u.user_name
,To_Char(a.request_date,'DD-MON-YY HH24:MI:SS') submitted
,To_Char(a.REQUESTED_START_DATE,'DD-MON-YY HH24:MI:SS') REQUESTED_START_DATE
,To_Char(a.ACTUAL_START_DATE,'DD-MON-YY HH24:MI:SS') started
,To_Char(a.ACTUAL_COMPLETION_DATE,'DD-MON-YY HH24:MI:SS') completed
,a.phase_code, a.status_code, a.HOLD_FLAG, a.argument_text, 
a.RESUBMIT_INTERVAL, a.RESUBMIT_INTERVAL_TYPE_CODE, a.RESUBMIT_INTERVAL_UNIT_CODE
from    
APPLSYS.FND_CONCURRENT_REQUESTS a,
fnd_concurrent_programs_vl f,
fnd_user u 
where 
f.CONCURRENT_PROGRAM_ID=a.CONCURRENT_PROGRAM_ID
and u.user_id = a.requested_by
--and a.status_code = 'C' and a.phase_code = 'C'
--and a.ACTUAL_COMPLETION_DATE> SYSDATE-30
and a.REQUESTED_START_DATE > SYSDATE-7
--and request_id in ('21654900','21654549','21647375','21646020','21645974')
--and f.concurrent_program_name LIKE 'XXAOL0240_AUTO%'     ---Request Set Archiving AUTO Documents Interface
   --Check  Incompatibile Programs                            
    and f.CONCURRENT_PROGRAM_ID in
                        (SELECT 
                            b1.CONCURRENT_PROGRAM_ID Incomp_CONC_PR_ID
                            FROM apps.fnd_concurrent_program_serial cps,
                            apps.fnd_concurrent_programs_tl a1,
                            apps.fnd_concurrent_programs_tl b1
                            --,apps.fnd_application_tl a2
                            --,apps.fnd_application_tl b2
                            WHERE a1.application_id = cps.running_application_id
                            AND a1.concurrent_program_id = cps.running_concurrent_program_id
                            --AND a2.application_id = cps.running_application_id
                            AND b1.application_id = cps.to_run_application_id
                            AND b1.concurrent_program_id = cps.to_run_concurrent_program_id
                            --AND b2.application_id = cps.to_run_application_id
                            AND a1.language = 'US'
                            --AND a2.language = 'US'
                            AND b1.language = 'US'
                            --AND b2.language = 'US'
                            and a1.user_concurrent_program_name like '%'||:user_concurrent_program_name||'%' --'%Archiving % Documents Interface%' --
                        )
--and u.user_name = 'XXAPPSADM'
--# STARTED with Delay of specified Interval
--and a.ACTUAL_START_DATE > (a.REQUESTED_START_DATE + INTERVAL '5' MINUTE)  -- !!! CHECHK The BUG SR		Severity 4		SR 3-18426826121 : The requested_start_date in the fnd_concurrent_requests is some time one day late
--# RunTime more than specific Interval
--and (a.ACTUAL_COMPLETION_DATE - a.ACTUAL_START_DATE)*60*24  > 10  -- PLUS QUE 30 MIN 
----and f.user_concurrent_program_name like 'Archiving%'
----# Running at specific moment
---and a.ACTUAL_START_DATE < To_Date(:State_CHECK_DATE,'DD-MON-YY HH24:MI:SS')
---and a.ACTUAL_COMPLETION_DATE > To_Date(:State_CHECK_DATE,'DD-MON-YY HH24:MI:SS')
--#RUNNING between specific period:
and a.ACTUAL_COMPLETION_DATE > To_Date(:State_Begin_CHECK_DATE,'DD-MON-YY HH24:MI:SS')
and a.ACTUAL_START_DATE <= To_Date(:State_End_CHECK_DATE,'DD-MON-YY HH24:MI:SS')
order by a.requested_start_date,a.ACTUAL_COMPLETION_DATE desc;









--check  Incompatibilities of a list of concurrent programs
SELECT distinct 
b1.CONCURRENT_PROGRAM_ID Incomp_CONC_PR_ID,
b1.user_concurrent_program_name "Incompatible_Prog",
DECODE (to_run_type,
'P', 'Program',
'S', 'Request set',
'UNKNOWN'
) incompatible_type,
b2.application_name "Incompatible App"
FROM apps.fnd_concurrent_program_serial cps,
apps.fnd_concurrent_programs_tl a1,
apps.fnd_concurrent_programs_tl b1,
apps.fnd_application_tl a2,
apps.fnd_application_tl b2
WHERE a1.application_id = cps.running_application_id
AND a1.concurrent_program_id = cps.running_concurrent_program_id
AND a2.application_id = cps.running_application_id
AND b1.application_id = cps.to_run_application_id
AND b1.concurrent_program_id = cps.to_run_concurrent_program_id
AND b2.application_id = cps.to_run_application_id
AND a1.language = 'US'
AND a2.language = 'US'
AND b1.language = 'US'
AND b2.language = 'US'
--and a1.user_concurrent_program_name like '%Archiving CLAIMS Documents Interface%'
and a1.CONCURRENT_PROGRAM_ID in (
'240458'
,'240448'
,'240449'
,'240450'
,'240451'
,'240456'
)
;

-- Request Set of specific Concurrent Program
SELECT DISTINCT user_request_set_name
  FROM FND_REQUEST_SETS_TL
 WHERE request_set_id IN
          (SELECT request_set_id
             FROM FND_REQUEST_SET_PROGRAMS
            WHERE concurrent_program_id =
                     (SELECT CONCURRENT_PROGRAM_ID
                        FROM fnd_concurrent_programs_tl
                       WHERE upper(USER_CONCURRENT_PROGRAM_NAME) = upper( '&Enter_Prog_name')));


--====   #2. Query to find the details of the Scheduled Concurrent Requests & Request Sets including the programs under the Request Set.
select request_id  
      ,conc_prog_name  
      ,params  
      ,prog_schedule_type  
      ,prog_schedule  
      ,user_name  
      ,requested_start_date   
 from (  
    select fcr.request_id  
          ,1 seq  
          ,decode(fcpt.user_concurrent_program_name,  
                  'Report Set','Report Set:' || fcr.description,  
                  fcpt.user_concurrent_program_name) conc_prog_name  
          ,(fcr.argument1||','||fcr.argument2||','||fcr.argument3||','||fcr.argument4||','||fcr.argument5||','||  
            fcr.argument6||','||fcr.argument7||','||fcr.argument8||','||fcr.argument9||','||fcr.argument10) params -- Add more parameters if needed or use column 'argument_text'  
          ,nvl2(fcr.resubmit_interval,'Periodically',nvl2(fcr.release_class_id, 'On Specific Days', 'Once')) prog_schedule_type  
          ,(case nvl2(fcr.resubmit_interval,'PERIODICALLY',nvl2(fcr.release_class_id, 'ON SPECIFIC DAYS', 'ONCE'))  
                 when 'PERIODICALLY'  
                     then 'EVERY ' || fcr.resubmit_interval || ' ' || fcr.resubmit_interval_unit_code || ' FROM ' ||fcr.resubmit_interval_type_code || ' OF PREV RUN'  
                 when 'ONCE'  
                     then 'AT :' ||to_char(fcr.requested_start_date, 'DD-MON-YYYY HH24:MI')  
                 else  
                     'EVERY: ' || fcrc.class_info  
            end) prog_schedule  
          ,fu.user_name user_name  
          ,to_char(fcr.requested_start_date, 'DD-MON-YYYY HH24:MI') requested_start_date  
     from apps.fnd_concurrent_programs_tl fcpt  
         ,apps.fnd_concurrent_requests    fcr  
         ,apps.fnd_user                   fu  
         ,apps.fnd_conc_release_classes   fcrc  
     where fcpt.application_id        = fcr.program_application_id  
     and   fcpt.concurrent_program_id = fcr.concurrent_program_id  
     and   fcr.requested_by           = fu.user_id  
     and   fcr.phase_code             = 'P'  
     and   fcr.requested_start_date   > sysdate  
     and   fcpt.language              = 'US'  
     and   fcrc.release_class_id(+)   = fcr.release_class_id  
     and   fcrc.application_id(+)     = fcr.release_class_app_id  
     union  
     select fcr.request_id  
           ,2 seq  
           ,'-->' || fcp.user_concurrent_program_name conc_prog_name  
           ,(frr.argument1||','||frr.argument2||','||frr.argument3||','||frr.argument4||','||frr.argument5||','||  
             frr.argument6||','||frr.argument7||','||frr.argument8||','||frr.argument9||','||frr.argument10) params -- Add more parameters if needed  
           ,null prog_schedule_type  
           ,null prog_schedule  
           ,null user_name  
           ,null requested_start_date  
     from apps.fnd_concurrent_programs_tl fcpt
         ,apps.fnd_concurrent_requests    fcr
         ,apps.fnd_user                   fu
         ,apps.fnd_conc_release_classes   fcrc  
         ,apps.fnd_run_requests           frr
         ,apps.fnd_concurrent_programs_tl fcp  
     where fcpt.application_id               = fcr.program_application_id  
     and   fcpt.concurrent_program_id        = fcr.concurrent_program_id  
     and   fcr.requested_by                  = fu.user_id  
     and   fcr.phase_code                    = 'P'  
     and   fcr.requested_start_date          > sysdate  
     and   fcpt.language                     = 'US'  
     and   fcrc.release_class_id(+)          = fcr.release_class_id  
     and   fcrc.application_id(+)            = fcr.release_class_app_id  
     and   fcpt.user_concurrent_program_name = 'Report Set'  
     and   frr.parent_request_id             = fcr.request_id  
     and   frr.concurrent_program_id         = fcp.concurrent_program_id  
 ) qrslt  
 order by request_id,seq;