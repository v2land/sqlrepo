
-- valider tout les plans d'�x�cution 
select * from TABLE(DBMS_XPLAN.DISPLAY_AWR('gfzm6ghrr2d3k'));


-- valider si un baseline existe
SELECT * FROM dba_sql_plan_baselines
--where accepted = 'NO'
ORDER BY CREATED DESC ;

-- Create SQL Tuning Set (STS)
BEGIN
  DBMS_SQLTUNE.CREATE_SQLSET(
    sqlset_name => 'STS_ACCOUNTING_PROGRAM',
    description => 'STS for Accounting program');
END;

-- identifi� les snap avec le sql en question 
SELECT BEGIN_INTERVAL_TIME,
  END_INTERVAL_TIME,
  sql.*
FROM dba_hist_sqlstat SQL,
  dba_hist_snapshot s
WHERE s.snap_id = sql.snap_id
AND SQL.sql_id  ='gfzm6ghrr2d3k'
ORDER BY BEGIN_INTERVAL_TIME

-- Load STS avec les plans du AWR pour le sql_Id dans l'intervale de snap 
DECLARE
  cur sys_refcursor;
BEGIN
  OPEN cur FOR
    SELECT VALUE(P)
    FROM TABLE(
       dbms_sqltune.select_workload_repository(begin_snap=>36279, end_snap=>36908,basic_filter=>'sql_id = ''gfzm6ghrr2d3k''',attribute_list=>'ALL')
              ) p;
     DBMS_SQLTUNE.LOAD_SQLSET( sqlset_name=> 'STS_ACCOUNTING_PROGRAM', populate_cursor=>cur);
  CLOSE cur;
END;
/



-- valider que les plans sont bien loader dans le STS
SELECT
  first_load_time          ,
  executions as execs              ,
  parsing_schema_name      ,
  elapsed_time  / 1000000 as elapsed_time_secs  ,
  cpu_time / 1000000 as cpu_time_secs           ,
  buffer_gets              ,
  disk_reads               ,
  direct_writes            ,
  rows_processed           ,
  fetches                  ,
  optimizer_cost           ,
  sql_plan                ,
  plan_hash_value          ,
  sql_id                   ,
  sql_text
   FROM TABLE(DBMS_SQLTUNE.SELECT_SQLSET(sqlset_name => 'STS_ACCOUNTING_PROGRAM')
             );
 
-- List out the Baselines to see what's there
SELECT * FROM dba_sql_plan_baselines ;
 
-- Load desired plan from STS as SQL Plan Baseline
-- Filter explicitly for the plan_hash_value here if you want
DECLARE
my_plans pls_integer;
BEGIN
  my_plans := DBMS_SPM.LOAD_PLANS_FROM_SQLSET(
    sqlset_name => 'STS_ACCOUNTING_PROGRAM', 
    basic_filter=>'plan_hash_value = ''3892985237'''
    );
END;
/

-- valider si ancien plan est en m�moire, si oui flusher du shared pool
select *
from v$sqlarea
where sql_id = 'gfzm6ghrr2d3k';


SELECT inst_id,
  child_number,
  plan_hash_value,
  executions,
  is_shareable
FROM gv$sql
WHERE sql_id IN('gfzm6ghrr2d3k')
ORDER BY 1,
  2;

-- flush shared_pool for specific sql
exec DBMS_SHARED_POOL.PURGE('0700010048D1E3A8,3303084936','C');

 