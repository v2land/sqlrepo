select owner, object_type, object_name from dba_objects where object_name like 'AXF%' and object_type != 'SYNONYM' and owner!= 'AXF' order by 1,2,3;
select owner, object_type, object_name from dba_objects where object_name like 'AXF%' and owner= 'AXF' order by 1,2,3;
/*
APPS	FUNCTION	AXF_MANAGED_ATTACH_AVAIL
APPS	FUNCTION	AXF_MANAGED_ATTACH_VALUES
APPS	PACKAGE	AXF_ADD_EBS_ATTACHMENT_PKG
APPS	PACKAGE	AXF_GET_APPROVAL_STATUS_PKG
APPS	PACKAGE	AXF_VALIDATE_SEGMENTS_PKG
APPS	PACKAGE BODY	AXF_ADD_EBS_ATTACHMENT_PKG
APPS	PACKAGE BODY	AXF_GET_APPROVAL_STATUS_PKG
APPS	PACKAGE BODY	AXF_VALIDATE_SEGMENTS_PKG
APPS	TABLE	AXF_VALIDATION_REJECTIONS
SYS	PACKAGE	AXF_PREIMPORT_CUSTOM_PKG
*/
select dbms_metadata.get_ddl('PACKAGE','PO_UOM_S') from dual ;

select  'select dbms_metadata.get_ddl('''||object_type||''','''||object_name||''') from dual ;','--'||owner,object_type from dba_objects where object_name like 'AXF%' and object_type != 'SYNONYM' and owner!= 'AXF' order by 2,3,1;
--select  (select dbms_metadata.get_ddl('''object_type''','''||object_name||''') from dual ),'--'||owner,object_type from dba_objects where object_name like 'AXF%' and object_type != 'SYNONYM' and owner!= 'AXF' order by 1,2,3;

select dbms_metadata.get_ddl('FUNCTION','AXF_MANAGED_ATTACH_AVAIL') from dual ;	--APPS	FUNCTION
select dbms_metadata.get_ddl('FUNCTION','AXF_MANAGED_ATTACH_VALUES') from dual ;	--APPS	FUNCTION
select dbms_metadata.get_ddl('PACKAGE','AXF_ADD_EBS_ATTACHMENT_PKG') from dual ;	--APPS	PACKAGE
select dbms_metadata.get_ddl('PACKAGE','AXF_GET_APPROVAL_STATUS_PKG') from dual ;	--APPS	PACKAGE
select dbms_metadata.get_ddl('PACKAGE','AXF_VALIDATE_SEGMENTS_PKG') from dual ;	--APPS	PACKAGE
select dbms_metadata.get_ddl('PACKAGE BODY','AXF_ADD_EBS_ATTACHMENT_PKG') from dual ;	--APPS	PACKAGE BODY
select dbms_metadata.get_ddl('PACKAGE BODY','AXF_GET_APPROVAL_STATUS_PKG') from dual ;	--APPS	PACKAGE BODY
select dbms_metadata.get_ddl('PACKAGE BODY','AXF_VALIDATE_SEGMENTS_PKG') from dual ;	--APPS	PACKAGE BODY
select dbms_metadata.get_ddl('TABLE','AXF_VALIDATION_REJECTIONS') from dual ;	--APPS	TABLE
select dbms_metadata.get_ddl('PACKAGE','AXF_PREIMPORT_CUSTOM_PKG') from dual ;	--SYS	PACKAGE

--===========================
select  'select dbms_metadata.get_ddl('''||object_type||''','''||object_name||''') from dual ;','--'||owner,object_type from dba_objects where object_name like 'AXF%' and  object_type = 'SYNONYM' and owner='PUBLIC'
union select  'select dbms_metadata.get_ddl('''||object_type||''','''||object_name||''') from dual ;','--'||owner,object_type from dba_objects where object_name like 'AXF%' and  object_type='PACKAGE'
union select  'select dbms_metadata.get_ddl('''||object_type||''','''||object_name||''') from dual ;','--'||owner,object_type from dba_objects where object_name like 'AXF%' and  object_type='SEQUENCE'
union select  'select dbms_metadata.get_ddl('''||object_type||''','''||object_name||''') from dual ;','--'||owner,object_type from dba_objects where object_name like 'AXF%' and  object_type='TYPE'
union select  'select dbms_metadata.get_ddl('''||object_type||''','''||object_name||''') from dual ;','--'||owner,object_type from dba_objects where object_name like 'AXF%' and  object_type='PROCEDURE'
union select  'select dbms_metadata.get_ddl('''||object_type||''','''||object_name||''') from dual ;','--'||owner,object_type from dba_objects where object_name like 'AXF%' and  object_type='FUNCTION';





select owner, object_type, 'drop public synonym '||object_name||';' from dba_objects where object_name like 'AXF%' and object_type = 'SYNONYM' and owner='PUBLIC'
union select owner, object_type,'drop package '||owner||'.'||object_name||';' from dba_objects where object_name like 'AXF%' and object_type='PACKAGE'
union select owner, object_type,'drop sequence '||owner||'.'||object_name||';' from dba_objects where object_name like 'AXF%' and object_type='SEQUENCE'
union select owner, object_type,'drop type '||owner||'.'||object_name||';' from dba_objects where object_name like 'AXF%' and object_type='TYPE'
union select owner, object_type,'drop procedure '||owner||'.'||object_name||';' from dba_objects where object_name like 'AXF%' and object_type='PROCEDURE'
union select owner, object_type,'drop function '||owner||'.'||object_name||';' from dba_objects where object_name like 'AXF%' and object_type='FUNCTION';

/*
APPS	FUNCTION	drop function APPS.AXF_MANAGED_ATTACH_AVAIL;
APPS	FUNCTION	drop function APPS.AXF_MANAGED_ATTACH_VALUES;
APPS	PACKAGE	drop package APPS.AXF_ADD_EBS_ATTACHMENT_PKG;
APPS	PACKAGE	drop package APPS.AXF_GET_APPROVAL_STATUS_PKG;
APPS	PACKAGE	drop package APPS.AXF_VALIDATE_SEGMENTS_PKG;
AXF	FUNCTION	drop function AXF.AXF_MANAGED_ATTACH_AVAIL;
AXF	FUNCTION	drop function AXF.AXF_MANAGED_ATTACH_VALUES;
AXF	PACKAGE	drop package AXF.AXF_PREIMPORT_CUSTOM_PKG;
AXF	PACKAGE	drop package AXF.AXF_VALIDATION_IMPORT_PKG;
AXF	SEQUENCE	drop sequence AXF.AXF_COMMANDS_SEQ;
AXF	SEQUENCE	drop sequence AXF.AXF_COMMAND_PARAMETERS_SEQ;
AXF	SEQUENCE	drop sequence AXF.AXF_CONFIGS_SEQ;
AXF	SEQUENCE	drop sequence AXF.AXF_FND_MAP_SEQ;
AXF	SEQUENCE	drop sequence AXF.AXF_MA_PARAMETERS_SEQ;
AXF	SEQUENCE	drop sequence AXF.AXF_VALIDATION_REJECTION_SEQ;
AXF	TYPE	drop type AXF.AXF_PREIMPORT_CUSTOM_PKG_R_IN;
AXF	TYPE	drop type AXF.AXF_PREIMPORT_CUSTOM_PKG_R_LI;
AXF	TYPE	drop type AXF.AXF_PREIMPORT_CUSTOM_PKG_T_LI;
AXF	TYPE	drop type AXF.AXF_VALIDATION_IMPORT_PKG_R_3;
AXF	TYPE	drop type AXF.AXF_VALIDATION_IMPORT_PKG_R_5;
AXF	TYPE	drop type AXF.AXF_VALIDATION_IMPORT_PKG_R_7;
AXF	TYPE	drop type AXF.AXF_VALIDATION_IMPORT_PKG_R_H;
AXF	TYPE	drop type AXF.AXF_VALIDATION_IMPORT_PKG_R_I;
AXF	TYPE	drop type AXF.AXF_VALIDATION_IMPORT_PKG_R_LR;
AXF	TYPE	drop type AXF.AXF_VALIDATION_IMPORT_PKG_R_LT;
AXF	TYPE	drop type AXF.AXF_VALIDATION_IMPORT_PKG_T_6;
AXF	TYPE	drop type AXF.AXF_VALIDATION_IMPORT_PKG_T_8;
AXF	TYPE	drop type AXF.AXF_VALIDATION_IMPORT_PKG_T_H;
AXF	TYPE	drop type AXF.AXF_VALIDATION_IMPORT_PKG_T_LR;
AXF	TYPE	drop type AXF.AXF_VALIDATION_IMPORT_PKG_T_LT;
PUBLIC	SYNONYM	drop public synonym AXF_COMMANDS_SYN;
PUBLIC	SYNONYM	drop public synonym AXF_COMMAND_PARAMETERS_SYN;
PUBLIC	SYNONYM	drop public synonym AXF_CONFIGS_SYN;
PUBLIC	SYNONYM	drop public synonym AXF_FND_MAP_SEQ_SYN;
PUBLIC	SYNONYM	drop public synonym AXF_FND_MAP_SYN;
PUBLIC	SYNONYM	drop public synonym AXF_MA_PARAMETERS_SYN;
PUBLIC	SYNONYM	drop public synonym AXF_PROPERTIES_SYN;
SYS	PACKAGE	drop package SYS.AXF_PREIMPORT_CUSTOM_PKG;
*/


select owner, object_type, 'drop synonym '||owner||'.'||object_name||';' from dba_objects where object_name like 'AXF%' and object_type = 'SYNONYM';
/*
PUBLIC	SYNONYM	drop synonym PUBLIC.AXF_COMMANDS_SYN;
PUBLIC	SYNONYM	drop synonym PUBLIC.AXF_COMMAND_PARAMETERS_SYN;
PUBLIC	SYNONYM	drop synonym PUBLIC.AXF_CONFIGS_SYN;
PUBLIC	SYNONYM	drop synonym PUBLIC.AXF_FND_MAP_SEQ_SYN;
PUBLIC	SYNONYM	drop synonym PUBLIC.AXF_FND_MAP_SYN;
PUBLIC	SYNONYM	drop synonym PUBLIC.AXF_MA_PARAMETERS_SYN;
PUBLIC	SYNONYM	drop synonym PUBLIC.AXF_PROPERTIES_SYN;
APPS	SYNONYM	drop synonym APPS.AXF_COMMANDS_SEQ;
APPS	SYNONYM	drop synonym APPS.AXF_COMMAND_PARAMETERS_SEQ;
APPS	SYNONYM	drop synonym APPS.AXF_CONFIGS_SEQ;
APPS	SYNONYM	drop synonym APPS.AXF_MA_PARAMETERS_SEQ;
*/



--=======================================
select
        owner,
        --Java object names may need to be converted with DBMS_JAVA.LONGNAME.
        --That code is not included since many database don't have Java installed.
        object_name,
        decode(object_type,
            'DATABASE LINK',      'DB_LINK',
            'JOB',                'PROCOBJ',
            'RULE SET',           'PROCOBJ',
            'RULE',               'PROCOBJ',
            'EVALUATION CONTEXT', 'PROCOBJ',
            'PACKAGE',            'PACKAGE_SPEC',
            'PACKAGE BODY',       'PACKAGE_BODY',
            'TYPE',               'TYPE_SPEC',
            'TYPE BODY',          'TYPE_BODY',
            'MATERIALIZED VIEW',  'MATERIALIZED_VIEW',
            'QUEUE',              'AQ_QUEUE',
            'JAVA CLASS',         'JAVA_CLASS',
            'JAVA TYPE',          'JAVA_TYPE',
            'JAVA SOURCE',        'JAVA_SOURCE',
            'JAVA RESOURCE',      'JAVA_RESOURCE',
            object_type
        ) object_type
    from dba_objects 
    where owner in ('AXF')
        --These objects are included with other object types.
        and object_type not in ('INDEX PARTITION','INDEX SUBPARTITION',
           'LOB','LOB PARTITION','TABLE PARTITION','TABLE SUBPARTITION')
        --Ignore system-generated types that support collection processing.
        and not (object_type = 'TYPE' and object_name like 'SYS_PLSQL_%')
        --Exclude nested tables, their DDL is part of their parent table.
        and (owner, object_name) not in (select owner, table_name from dba_nested_tables)
        --Exclude overflow segments, their DDL is part of their parent table.
        and (owner, object_name) not in (select owner, table_name from dba_tables where iot_type = 'IOT_OVERFLOW')
;