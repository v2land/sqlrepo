--9i - 12c  Connecting 12c Db to 9i Db and vis-versa using db-link (Doc ID 2320287.1)
select * from dual@LINK_GRCPROD_EBSPROD.WORLD;

select * from dba_db_links;
select * from dba_dependencies where referenced_link_name is not null;
select * from dual@LINK_VELO_EBSDEVB.WORLD;
select count(*) from XXARVELO0020_EBS_CUST_VOLUME_V@LINK_VELO_EBSDEVB.WORLD;


--sqlplus / as sysdba
--drop public database link AGR_INTERFACE_VELO;
CREATE PUBLIC DATABASE LINK "AGR_INTERFACE_VELO.AGROPUR.COM" CONNECT TO VELOR IDENTIFIED BY velor USING 'GRCACCE';

-- ALTER DATABASE LINK
select * from dba_db_links;
--LINK_GRCACCE_EBSACCE.WORLD	APPS	EBSACCE
--LINK_GRCACCE_EBSDEV3.WORLD	APPS	EBSDEV3
select * from dual@LINK_GRCACCE_EBSDEV3.WORLD;
grant alter database link to system;
ALTER public DATABASE LINK LINK_GRCACCE_EBSDEV3.WORLD CONNECT TO APPS IDENTIFIED BY "NEW APPS PASSWORD";--Pasd3NetEbsap;

drop public database link LINK_GRCACCE_EBSDEV3.WORLD;
CREATE PUBLIC DATABASE LINK "LINK_GRCACCE_EBSDEV3" CONNECT TO EBSDEV3 IDENTIFIED BY APPS USING 'NEW APPS PASSWORD';

--XXPO0290 (Oracle cemli that update VELO inventory) is failing with that error in the log
--Cause: FDPSTP failed due to ORA-04045: errors during recompilation/revalidation of APPS.XXPO0290_RECEP_RETURN_VELO_PKG
--ORA-04052: error occurred when looking up remote object VELO.PROTORESSOURCE@AGR_INTERFACE_VELO.AGROPUR.COM

/*ACCE:
PUBLIC	EBSACCE.AGROPUR.COM	AGROPUR_LINK	(DESCRIPTION=(ADDRESS=(PROTOCOL=tcp)(HOST=ebsaccedb.agropur.com)(PORT=1530))(CONNECT_DATA=(SID=EBSACCE)))	20-JAN-15
PUBLIC	AGR_INTERFACE_VELO.AGROPUR.COM	VELO	GRCACCE	29-DEC-17
APPS	EBSACCE.X	APPS	(DESCRIPTION=(ADDRESS=(PROTOCOL=tcp)(HOST=ebsaccedb.agropur.com)(PORT=1530))(CONNECT_DATA=(SID=EBSACCE)))	05-NOV-15
APPS	EBSDEVB.AGROPUR.COM	APPS	(DESCRIPTION=(ADDRESS=(PROTOCOL=tcp)(HOST=ebsdevbdb.agropur.com)(PORT=1532))(CONNECT_DATA=(SID=EBSDEVB)))	26-OCT-16
APPS	APPS_TO_APPS	APPS	RI16	01-NOV-04
APPS	EDW_APPS_TO_WH	APPS	RI16	01-NOV-04
*/


Select
TYPE || ' ' ||
OWNER || '.' || NAME || ' references ' ||
REFERENCED_TYPE || ' ' ||
REFERENCED_OWNER || '.' || REFERENCED_NAME 
||' through '|| referenced_link_name 
as DEPENDENCIES
From all_dependencies
Where referenced_link_name is not null and
--name = UPPER(LTRIM(RTRIM( '&ls_REF_name' )))AND
(REFERENCED_OWNER <> 'SYS'
AND REFERENCED_OWNER <> 'SYSTEM'
AND REFERENCED_OWNER <> 'PUBLIC'
)
AND (OWNER <> 'SYS'
AND OWNER <> 'SYSTEM'
AND OWNER <> 'PUBLIC'
)
order by OWNER, name,
REFERENCED_TYPE ,
REFERENCED_OWNER ,
REFERENCED_name;

select * from all_db_links;
select * from dba_dependencies where referenced_link_name is not null;


--sqlplus / as sysdba
--drop public database link AGR_INTERFACE_VELO;
CREATE PUBLIC DATABASE LINK "AGR_INTERFACE_VELO.AGROPUR.COM" CONNECT TO VELOR IDENTIFIED BY velor USING 'GRCACCE';

--XXPO0290 (Oracle cemli that update VELO inventory) is failing with that error in the log
--Cause: FDPSTP failed due to ORA-04045: errors during recompilation/revalidation of APPS.XXPO0290_RECEP_RETURN_VELO_PKG
--ORA-04052: error occurred when looking up remote object VELO.PROTORESSOURCE@AGR_INTERFACE_VELO.AGROPUR.COM

/*ACCE:
PUBLIC	EBSACCE.AGROPUR.COM	AGROPUR_LINK	(DESCRIPTION=(ADDRESS=(PROTOCOL=tcp)(HOST=ebsaccedb.agropur.com)(PORT=1530))(CONNECT_DATA=(SID=EBSACCE)))	20-JAN-15
PUBLIC	AGR_INTERFACE_VELO.AGROPUR.COM	VELO	GRCACCE	29-DEC-17
APPS	EBSACCE.X	APPS	(DESCRIPTION=(ADDRESS=(PROTOCOL=tcp)(HOST=ebsaccedb.agropur.com)(PORT=1530))(CONNECT_DATA=(SID=EBSACCE)))	05-NOV-15
APPS	EBSDEVB.AGROPUR.COM	APPS	(DESCRIPTION=(ADDRESS=(PROTOCOL=tcp)(HOST=ebsdevbdb.agropur.com)(PORT=1532))(CONNECT_DATA=(SID=EBSDEVB)))	26-OCT-16
APPS	APPS_TO_APPS	APPS	RI16	01-NOV-04
APPS	EDW_APPS_TO_WH	APPS	RI16	01-NOV-04
*/


Select
TYPE ,
OWNER , ' references ' ,
REFERENCED_TYPE ,
REFERENCED_OWNER , REFERENCED_NAME 
,' through ', referenced_link_name 
as DEPENDENCIES
From all_dependencies
Where referenced_link_name is not null and
--name = UPPER(LTRIM(RTRIM( '&ls_REF_name' )))AND
(REFERENCED_OWNER <> 'SYS'
AND REFERENCED_OWNER <> 'SYSTEM'
AND REFERENCED_OWNER <> 'PUBLIC'
)
AND (OWNER <> 'SYS'
AND OWNER <> 'SYSTEM'
AND OWNER <> 'PUBLIC'
)
order by OWNER, name,
REFERENCED_TYPE ,
REFERENCED_OWNER ,
REFERENCED_name;