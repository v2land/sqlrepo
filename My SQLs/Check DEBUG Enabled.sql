--It is well known that enabling debug, trace or log by setting some profile options has impact on E-Business Suite performance. The following queries are proposed to easy find enabled related profile options at the different levels. Of course you can adapt these SQL statements to fit your requirement.

--1. This will display values of profile options "FND: Debug Log Enabled" (AFLOG_ENABLED) and "Initialization SQL Statement - Custom'" (FND_INIT_SQL) at each level. If the values are respectively 'Y' and/or contains sql statement for these profile options then log and/or trace are enable for that level (which could be at site, user, ... level):
select po.profile_option_name "NAME",
po.USER_PROFILE_OPTION_NAME,
decode(to_char(pov.level_id),
'10001', 'SITE',
'10002', 'APP',
'10003', 'RESP',
'10005', 'SERVER',
'10006', 'ORG',
'10004', 'USER', '???') "LEV",
decode(to_char(pov.level_id),
'10001', '',
'10002', app.application_short_name,
'10003', rsp.responsibility_key,
'10005', svr.node_name,
'10006', org.name,
'10004', usr.user_name,
'???') "CONTEXT",
pov.profile_option_value "VALUE"
from FND_PROFILE_OPTIONS_VL po,
FND_PROFILE_OPTION_VALUES pov,
fnd_user usr,
fnd_application app,
fnd_responsibility rsp,
fnd_nodes svr,
hr_operating_units org
where (po.profile_option_name like '%AFLOG_ENABLED%' or po.profile_option_name like
'%FND_INIT_SQL%')
and pov.application_id = po.application_id
and pov.profile_option_id = po.profile_option_id
and usr.user_id (+) = pov.level_value
and rsp.application_id (+) = pov.level_value_application_id
and rsp.responsibility_id (+) = pov.level_value
and app.application_id (+) = pov.level_value
and svr.node_id (+) = pov.level_value
and org.organization_id (+) = pov.level_value
and pov.profile_option_value !='N'
order by "NAME", pov.level_id, "VALUE";


--2. This will display all profile options with values and levels where the word TRACE, LOG, or DEBUG is found in the PROFILE_OPTION_NAME:

select po.profile_option_name "NAME",
po.USER_PROFILE_OPTION_NAME,
decode(to_char(pov.level_id),
'10001', 'SITE',
'10002', 'APP',
'10003', 'RESP',
'10005', 'SERVER',
'10006', 'ORG',
'10004', 'USER', '???') "LEV",
decode(to_char(pov.level_id),
'10001', '',
'10002', app.application_short_name,
'10003', rsp.responsibility_key,
'10005', svr.node_name,
'10006', org.name,
'10004', usr.user_name,
'???') "CONTEXT",
pov.profile_option_value "VALUE"
from FND_PROFILE_OPTIONS_VL po,
FND_PROFILE_OPTION_VALUES pov,
fnd_user usr,
fnd_application app,
fnd_responsibility rsp,
fnd_nodes svr,
hr_operating_units org
where (po.PROFILE_OPTION_NAME like '%TRACE%' or po.PROFILE_OPTION_NAME like '%DEBUG%' or po.PROFILE_OPTION_NAME like '%LOG%')
and pov.application_id = po.application_id
and pov.profile_option_id = po.profile_option_id
and usr.user_id (+) = pov.level_value
and rsp.application_id (+) = pov.level_value_application_id
and rsp.responsibility_id (+) = pov.level_value
and app.application_id (+) = pov.level_value
and svr.node_id (+) = pov.level_value
and org.organization_id (+) = pov.level_value
--and pov.profile_option_value ='Y'
and pov.profile_option_value ='1'
and pov.profile_option_value !='N'
and pov.profile_option_value != 'LOCAL' 
and pov.profile_option_value !='SSO'
order by "NAME", pov.level_id, "VALUE";
