-- Event : latch: cache buffers chains
SELECT DISTINCT vs.USERNAME,
  vs.OSUSER,
  NVL(vs.client_identifier, fu.user_name) EBS_USER,
  fu2.description EBS_USER_DESC,
  fl.login_name,
  fl.login_type,
  vp.SPID UNIX_Process_ID,
  vs.sid Oracle_SID,
  vs.SERIAL#,
  vs.MACHINE,
  vs.PROCESS "Apps Server Process",
  vs.PROGRAM,
  vs.MODULE,
  vs.ACTION,
  vs.SERVICE_NAME,
  vs.CLIENT_INFO,
  vs.status,
  vs.logon_time,
  (sysdate - last_call_et / 86400) last_activity,
  vs.SERVER,
  vss.name Shared_Server,
  vd.name Dispatcher,
  'alter system kill session '''
  || vs.sid
  || ','
  || vs.serial#
  || ''';' kill,
  vs.sql_address,
  vs.sql_hash_value,
  vsp.plan_hash_value,
 -- vsql.ELAPSED_TIME, vsql.DISK_READS, vsql.USER_IO_WAIT_TIME,
  vs.wait_class,
  vs.wait_time,
  vs.seconds_in_wait,
  vs.event,
  vs.final_blocking_session_status,
  vs.final_blocking_session,
  'EXEC DBMS_SUPPORT.start_trace_in_session(sid => '
  ||vs.sid
  ||', serial => '
  ||vs.serial#
  ||', waits => true, binds=> true);' trace_on,
  'EXEC DBMS_SUPPORT.stop_trace_in_session(sid => '
  ||vs.sid
  ||', serial => '
  ||vs.serial#
  ||');' trace_off
FROM v$process vp,
  v$session vs,
  v$circuit vc,
  v$shared_server vss,
  v$dispatcher vd,
  APPLSYS.fnd_logins fl,
  APPLSYS.fnd_user fu,
  APPLSYS.fnd_user fu2,
  V$SQL_PLAN vsp
--  v$sql vsql
WHERE vp.ADDR            = vs.PADDR(+)
AND vsp.address=vs.sql_address
--AND vsql.address=vs.sql_address
--AND vsql.plan_hash_value=vsp.plan_hash_value
--AND ELAPSED_TIME > 1013764663229
AND vc.SADDR(+)          = vs.SADDR
AND VC.SERVER            = VSS.PADDR(+)
AND VC.DISPATCHER        = VD.PADDR(+)
AND VP.SPID              = FL.PROCESS_SPID(+)
AND VP.PID               = FL.PID(+)
AND FL.USER_ID           = FU.USER_ID(+)
AND vs.client_identifier = fu2.user_name(+)
AND( vs.sql_address  =:SQL_ADDRESS) --000000115A3CA048
AND(vsp.plan_hash_value=:PLAN_HASH_VALUE) --4074830191
AND (VS.USERNAME LIKE NVL(UPPER(:USERNAME),'%')
OR (:USERNAME   IS NULL
AND vs.username IS NULL))
AND (vs.client_identifier LIKE NVL(UPPER(:EBS_USERNAME),'%')
OR (fu.user_name LIKE NVL(UPPER(:EBS_USERNAME),'%'))
OR (:EBS_USERNAME        IS NULL
AND vs.client_identifier IS NULL))
AND (VP.SPID LIKE NVL(:UNIX_PROCESS_ID, '%')
OR (:UNIX_PROCESS_ID IS NULL
AND VP.SPID          IS NULL))
AND (VS.PROCESS LIKE NVL(:UNIX_APPS_PROCESS_ID, '%')
OR (:UNIX_APPS_PROCESS_ID IS NULL
AND VS.PROCESS            IS NULL))
AND (UPPER(VS.MACHINE) LIKE NVL(UPPER(:MACHINE), '%')
OR (:MACHINE   IS NULL
AND VS.MACHINE IS NULL))
AND (UPPER(VS.OSUSER) LIKE NVL(UPPER(:OS_USER), '%')
OR (:OS_USER  IS NULL
AND VS.OSUSER IS NULL))
AND (VS.SID LIKE NVL(:ORACLE_SID, '%')
OR (:ORACLE_SID IS NULL
AND VS.SID      IS NULL))
AND (UPPER(VS.STATUS) LIKE NVL(UPPER(:STATUS), '%')
OR (:STATUS   IS NULL
AND VS.STATUS IS NULL))
AND (UPPER(VS.SERVICE_NAME) LIKE NVL(UPPER(:SERVICE), '%')
OR (:SERVICE                              IS NULL
AND VS.SERVICE_NAME                       IS NULL))
AND ((nvl2(vs.final_blocking_session,0,-1) = nvl2(:IS_LOCKING,0,-1))
OR (vs.SID LIKE NVL(:IS_LOCKING,'%')
OR vs.final_blocking_session LIKE NVL(:IS_LOCKING, '%')));

------------------------------------------------------------------
SELECT vs.USERNAME,
  vs.OSUSER,
  NVL(vs.client_identifier, fu.user_name) EBS_USER,
  fu2.description EBS_USER_DESC,
  fl.login_name,
  fl.login_type,
  vp.SPID UNIX_Process_ID,
  vs.sid Oracle_SID,
  vs.SERIAL#,
  vs.MACHINE,
  vs.PROCESS "Apps Server Process",
  vs.PROGRAM,
  vs.MODULE,
  vs.ACTION,
  vs.SERVICE_NAME,
  vs.CLIENT_INFO,
  vs.status,
  vs.logon_time,
  (sysdate - last_call_et / 86400) last_activity,
  vs.SERVER,
  vss.name Shared_Server,
  vd.name Dispatcher,
  'alter system kill session '''
  || vs.sid
  || ','
  || vs.serial#
  || ''';' kill,
  vs.sql_address,
  vs.sql_hash_value,
  vs.wait_class,
  vs.wait_time,
  vs.seconds_in_wait,
  vs.event,
  vs.final_blocking_session_status,
  vs.final_blocking_session,
  'EXEC DBMS_SUPPORT.start_trace_in_session(sid => '
  ||vs.sid
  ||', serial => '
  ||vs.serial#
  ||', waits => true, binds=> true);' trace_on,
  'EXEC DBMS_SUPPORT.stop_trace_in_session(sid => '
  ||vs.sid
  ||', serial => '
  ||vs.serial#
  ||');' trace_off
FROM v$process vp,
  v$session vs,
  v$circuit vc,
  v$shared_server vss,
  v$dispatcher vd,
  APPLSYS.fnd_logins fl,
  APPLSYS.fnd_user fu,
  APPLSYS.fnd_user fu2
WHERE vp.ADDR            = vs.PADDR(+)
AND vc.SADDR(+)          = vs.SADDR
AND VC.SERVER            = VSS.PADDR(+)
AND VC.DISPATCHER        = VD.PADDR(+)
AND VP.SPID              = FL.PROCESS_SPID(+)
AND VP.PID               = FL.PID(+)
AND FL.USER_ID           = FU.USER_ID(+)
AND vs.client_identifier = fu2.user_name(+)
AND (VS.USERNAME LIKE NVL(UPPER(:USERNAME),'%')
OR (:USERNAME   IS NULL
AND vs.username IS NULL))
AND (vs.client_identifier LIKE NVL(UPPER(:EBS_USERNAME),'%')
OR (fu.user_name LIKE NVL(UPPER(:EBS_USERNAME),'%'))
OR (:EBS_USERNAME        IS NULL
AND vs.client_identifier IS NULL))
AND (VP.SPID LIKE NVL(:UNIX_PROCESS_ID, '%')
OR (:UNIX_PROCESS_ID IS NULL
AND VP.SPID          IS NULL))
AND (VS.PROCESS LIKE NVL(:UNIX_APPS_PROCESS_ID, '%')
OR (:UNIX_APPS_PROCESS_ID IS NULL
AND VS.PROCESS            IS NULL))
AND (UPPER(VS.MACHINE) LIKE NVL(UPPER(:MACHINE), '%')
OR (:MACHINE   IS NULL
AND VS.MACHINE IS NULL))
AND (UPPER(VS.OSUSER) LIKE NVL(UPPER(:OS_USER), '%')
OR (:OS_USER  IS NULL
AND VS.OSUSER IS NULL))
AND (VS.SID LIKE NVL(:ORACLE_SID, '%')
OR (:ORACLE_SID IS NULL
AND VS.SID      IS NULL))
AND (UPPER(VS.STATUS) LIKE NVL(UPPER(:STATUS), '%')
OR (:STATUS   IS NULL
AND VS.STATUS IS NULL))
AND (UPPER(VS.SERVICE_NAME) LIKE NVL(UPPER(:SERVICE), '%')
OR (:SERVICE                              IS NULL
AND VS.SERVICE_NAME                       IS NULL))
AND ((nvl2(vs.final_blocking_session,0,-1) = nvl2(:IS_LOCKING,0,-1))
OR (vs.SID LIKE NVL(:IS_LOCKING,'%')
OR vs.final_blocking_session LIKE NVL(:IS_LOCKING, '%')));