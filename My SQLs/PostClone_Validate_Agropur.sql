PROMPT Executing post_clone_apps_step_1_EBSPROD_EBSDEV9.sql 

DEFINE SOURCE_URL = 'ebsprod.agropur.com';
DEFINE TARGET_URL = 'ebsdev9.agropur.com';
DEFINE SOURCE_PORT = '8009';
DEFINE SOURCE_SSL_PORT = '443';
DEFINE TARGET_PORT = '8009';
DEFINE TARGET_SSL_PORT = '443';
DEFINE SOURCE_DB_PORT = '1530';
DEFINE TARGET_DB_PORT = '1530';
DEFINE SOURCE_DB_HOSTNAME = 'ebsproddb';
DEFINE TARGET_DB_HOSTNAME = 'ebsdev9db';
DEFINE TARGET_PHYSICAL_DB_HOSTNAME = 'DCSSDEBSAPPS';
DEFINE SOURCE_WEB_HOSTNAME = 'dcpspebsapps';
DEFINE TARGET_WEB_HOSTNAME = 'dcssdebsapps';
DEFINE TARGET_PHYSICAL_WEB_HOSTNAME = 'dcssdebsapps';
DEFINE TARGET_VI_HOSTNAME = 'ebsdev9';
DEFINE SOURCE_DOMAIN = 'agropur.com'
DEFINE TARGET_DOMAIN = 'agropur.com'
DEFINE SOURCE_SID = 'EBSPROD';
DEFINE TMPSID = 'EBSDEV9';
DEFINE WCI_SOURCE = 'WLSPRODAPPS';
DEFINE WCI_TARGET = 'WLSDEV9APPS';
DEFINE CONC_DIRECTORY = '/EBS_CONC/EBSDEV9'
DEFINE TARGET_URL_PROTOCOL = 'https'


PROMPT Executing post_clone_apps_step_2.sql 

--------------------------------------------------------
--------------          WF        ----------------------
--------------------------------------------------------

PROMPT Modifier table => WF_NOTIFICATION_ATTRIBUTES
--SELECT TEXT_VALUE FROM APPLSYS.WF_NOTIFICATION_ATTRIBUTES WHERE TEXT_VALUE LIKE '%&SOURCE_URL%';
UPDATE APPS.WF_NOTIFICATION_ATTRIBUTES SET TEXT_VALUE = REPLACE(TEXT_VALUE, '&SOURCE_URL', '&TARGET_URL') WHERE TEXT_VALUE LIKE '%&SOURCE_URL%';

PROMPT Modifier table => WF_ITEM_ATTRIBUTES_VALUES
--select text_value from applsys.wf_item_attribute_values Where text_value like '%&SOURCE_URL%';
UPDATE APPS.WF_ITEM_ATTRIBUTE_VALUES SET TEXT_VALUE = REPLACE(TEXT_VALUE, '&SOURCE_URL', '&TARGET_URL') WHERE TEXT_VALUE LIKE '%&SOURCE_URL%';

PROMPT Verifier table => WF_SYSTEMS	
--Select a.GUID, a.Name, a.* from applsys.wf_systems a;
UPDATE APPS.WF_SYSTEMS SET DISPLAY_NAME = '&TMPSID';

PROMPT Verifier table => WF_AGENTS
--select a.address, a.* from applsys.wf_agents a;

--------------------------------------------------------
--------------          FND        ----------------------
--------------------------------------------------------

PROMPT Verifier table => FND_FORM_FUNCTIONS
--select a.web_host_name, a.web_agent_name, a.* from applsys.fnd_form_functions a where a.web_host_name is not null or a.web_agent_name is not null;

PROMPT Verifier table => FND_CONCURRENT_REQUESTS
--select logfile_name,logfile_node_name, outfile_name, outfile_node_name from FND_CONCURRENT_REQUESTS;

PROMPT Modifier table =>FND_CONCURRENT_REQUESTS Hold

Update APPLSYS.FND_CONCURRENT_REQUESTS
Set    Hold_flag ='Y'
Where  phase_code != 'C'
       and not (requested_by = 0 --SYSADMIN
                and concurrent_program_id in ('38121', --Gather Schema Statistics
                                              '32592', --Purge Signon Audit data
                                              '36888', --Workflow Background Process
                                              '38089', --Purge Obsolete Workflow Runtime Data
                                              '41993', --Purge Logs and Closed System Alerts
                                              '42852', --OAM Applications Dashboard Collection
                                              '43593', --Workflow Control Queue Cleanup
                                              '46792', --Resend Failed/Error Workflow Notifications
                                              '46797', --Workflow Work Items Statistics Concurrent Program
                                              '46798', --Workflow Agent Activity Statistics Concurrent Program
                                              '46799', --Workflow Mailer Statistics Concurrent Program
                                              '142393', --PURGE CONC REQUEST - G�n�rer T�ches
                                              '206395', --Agropur - Analyse CUSTOM Tables and Indexes - Apps
                                              '50769',  --Purge Inactive Sessions
                                              --'32263',  --Purge Concurrent Request and/or Manager Data   (included in PURGE CONC REQUEST)
                                              '46790',	--Workflow Directory Services User/Role Validation
                                              '66349'	--Audit: Dequeue Process
                                                       )
                )
;



delete FND_CONCURRENT_REQUESTS where hold_flag != 'Y' 
   and not (requested_by = 0 --SYSADMIN
                and concurrent_program_id in ('38121', --Gather Schema Statistics
                                              '32592', --Purge Signon Audit data
                                              '36888', --Workflow Background Process
                                              '38089', --Purge Obsolete Workflow Runtime Data
                                              '41993', --Purge Logs and Closed System Alerts
                                              '42852', --OAM Applications Dashboard Collection
                                              '43593', --Workflow Control Queue Cleanup
                                              '46792', --Resend Failed/Error Workflow Notifications
                                              '46797', --Workflow Work Items Statistics Concurrent Program
                                              '46798', --Workflow Agent Activity Statistics Concurrent Program
                                              '46799', --Workflow Mailer Statistics Concurrent Program
                                              '142393', --PURGE CONC REQUEST - G�n�rer T�ches
                                              '206395', --Agropur - Analyse CUSTOM Tables and Indexes - Apps
                                              '50769',  --Purge Inactive Sessions
                                              --'32263',  --Purge Concurrent Request and/or Manager Data   (included in PURGE CONC REQUEST)
                                              '46790',	--Workflow Directory Services User/Role Validation
                                              '66349'	--Audit: Dequeue Process
                                                       )
                and phase_code !='C')
;


PROMPT Modifier table => FND_CONCURRENT_REQUESTS
update FND_CONCURRENT_REQUESTS
set logfile_name = null,
    logfile_node_name = null, 
    outfile_name = null, 
    outfile_node_name = null;

PROMPT Modifier table => FND_CONCURRENT_REQUESTS Cancel Running
--set the bad rows RUNNING to Completed Terminated
UPDATE fnd_concurrent_requests
SET phase_code = 'C', status_code = 'X'
WHERE phase_code IN ('R','I');
Commit;

--           WCI      --------------------------------
--PROMPT Modifier table ==> AXF.AXF_CONFIGS
--select solutionendpoint from AXF.AXF_CONFIGS;
--update AXF.AXF_CONFIGS set solutionendpoint = REPLACE(solutionendpoint, '&WCI_SOURCE', '&WCI_TARGET') WHERE solutionendpoint LIKE '%&WCI_SOURCE%';

-----------------------------------------------------------------------
--------------    XDO XML Publisher et IBY       ----------------------
-----------------------------------------------------------------------
PROMPT Changer le repertoire temporaire pour XML Publisher
--select VALUE from XDO.XDO_CONFIG_VALUES where PROPERTY_CODE = 'SYSTEM_TEMP_DIR';
UPDATE XDO.XDO_CONFIG_VALUES SET VALUE = '/&TMPSID._Apps/fs_ne/EBSapps/comn/temp' where PROPERTY_CODE = 'SYSTEM_TEMP_DIR';

PROMPT Mettre � jour les valeur des directories des Payment Profiles
--select system_profile_code, positive_pay_file_directory, outbound_pmt_file_directory from IBY_SYS_PMT_PROFILES_B where positive_pay_file_directory is not null or outbound_pmt_file_directory is not null;

PROMPT Modifier table => IBY_SYS_PMT_PROFILES_B
update IBY_SYS_PMT_PROFILES_B
set positive_pay_file_directory = '/interfaces/&TMPSID/outbound'
where system_profile_code in  ('XXAGR_CHK_CAD_PPP','AGR_CHK_CAD_PPP','AGR_CHK_USD_PPP');

PROMPT Modifier table => IBY_SYS_PMT_PROFILES_B (2)
update IBY_SYS_PMT_PROFILES_B
set outbound_pmt_file_directory= '/interfaces/&TMPSID/ap'
    , outbound_pmt_file_prefix = 'TT'
where system_profile_code in  ('AGR_EFT_CAD_PPP','AGR_EFT_USD_PPP');

PROMPT Modifier valeur setup Payment
update IBY_TRANSMIT_VALUES
set TRANSMIT_VARCHAR2_VALUE = '/interfaces/&TMPSID/process_ppa_ebs_bank/outbound'
where TRANSMIT_PARAMETER_CODE = 'FILE_DIR';

PROMPT Modifier valeur URL Payment system AGR DUMMY PAYMENT SYSTEM
update IBY_BEPINFO
set BASEURL = '&TARGET_URL_PROTOCOL://&TARGET_VI_HOSTNAME..agropur.com:&TARGET_SSL_PORT/OA_HTML'
where name = 'AGR DUMMY PAYMENT SYSTEM';

-----------------! IBY Pour EBS US !--------------------------------
--------------          FND               ----------------------
----------------------------------------------------------------
-- Ajout pour la partie EBS US
-- Ajout par N.Connan
-- VC corrected M&I_EFT_US for : M&'||'I_EFT_US
SELECT outbound_pmt_file_directory,system_profile_code
FROM iby_sys_pmt_profiles_vl WHERE system_profile_code in ('Agropur LSBO Wire Profile','M&'||'I_EFT_US');
set define off
UPDATE iby_sys_pmt_profiles_vl 
SET outbound_pmt_file_directory = '/interfaces/&TMPSID/process_EFT_EBS_bank/outbound' 
WHERE system_profile_code in ('Agropur LSBO Wire Profile','M&I_EFT_US');
set define on

---------------------------------------------------------
--------------    PROFILES         ----------------------
----- JTF COLOR SITENAME FWK_HOMEPAGE --------------

--SELECT b.profile_option_name, PROFILE_OPTION_VALUE
--  FROM applsys.fnd_profile_option_values a, applsys.fnd_profile_options b
--  where b.profile_option_name = 'JTF_INACTIVE_SESSION_TIMEOUT'
--  and a.application_id = b.application_id
--  and a.profile_option_id = b.profile_option_id;

PROMPT Modifier Profile System  JTF: Session Timeout
update applsys.fnd_profile_option_values z
set z.profile_option_value = '300',
      z.last_update_date = sysdate
where exists (
SELECT *
  FROM applsys.fnd_profile_option_values a, applsys.fnd_profile_options b
  where b.profile_option_name = 'JTF_INACTIVE_SESSION_TIMEOUT'
  and a.application_id = b.application_id
  and a.profile_option_id = b.profile_option_id
  and z.application_id = b.application_id
  and z.profile_option_id = b.profile_option_id
  );


PROMPT Modifier Profile System  Java Color Scheme
--SELECT b.profile_option_name, PROFILE_OPTION_VALUE
--  FROM applsys.fnd_profile_option_values a, applsys.fnd_profile_options b
--  where b.profile_option_name = 'FND_COLOR_SCHEME'
--  and a.application_id = b.application_id
--  and a.profile_option_id = b.profile_option_id;
update applsys.fnd_profile_option_values z
set z.profile_option_value = DECODE('&TMPSID', 'EBSDEV1', 'RED'
                                   , 'UEBSDEV1', 'RED'
                                   , 'EBSDEV2', 'PURPLE'
                                   , 'UEBSDEV2', 'PURPLE'
                                   , 'EBSDEV3', 'KHAKI'
                                   , 'EBSDEV4', 'BLUE'
                                   , 'EBSDEV5', 'OLIVE'
                                   , 'EBSDEV6', 'TEAL'
                                   , 'EBSDEV7', 'RED'
                                   , 'EBSDEV8', 'PURPLE'
                                   , 'EBSDEV9', 'KHAKI'
                                   , 'EBSDEVE', 'TEAL'
                                   , 'UEBSDEVE', 'TEAL'
                                   , 'EBSACCE', 'TITANIUM'
                                   , 'UEBSACCE', 'TITANIUM'
                                   , 'EBSACC2', 'TITANIUM'
                                  ),
      z.last_update_date = sysdate
where exists (
SELECT *
  FROM applsys.fnd_profile_option_values a, applsys.fnd_profile_options b
  where b.profile_option_name = 'FND_COLOR_SCHEME'
  and a.application_id = b.application_id
  and a.profile_option_id = b.profile_option_id
  and z.application_id = b.application_id
  and z.profile_option_id= b.profile_option_id
  );


PROMPT Modifier Profile System  Site Name
SELECT b.profile_option_name, PROFILE_OPTION_VALUE
  FROM applsys.fnd_profile_option_values a, applsys.fnd_profile_options b
  where b.profile_option_name = 'SITENAME'
  and a.application_id = b.application_id
  and a.profile_option_id = b.profile_option_id;
UPDATE FND_PROFILE_OPTION_VALUES FPOV
SET FPOV.PROFILE_OPTION_VALUE = '&TMPSID   '|| DECODE('&TMPSID', 'EBSTEST', 'Patch'
                       , 'EBSDEV1', 'PRJ Developement'
                       , 'EBSDEV2', 'PRJ Training2 OPS-50'
                       , 'EBSDEV3', 'LIVE Sand Box'
                       , 'EBSDEV4', 'PRJ Training1 OPS-50'
                       , 'EBSDEV5', 'PRJ TRAINING-USERS CCC070'
                       , 'EBSDEV6', 'PRJ Sand Box'
                       , 'EBSDEV7', 'PRJ Training Gold OPS-50 '
                       , 'EBSDEV8', 'R2.2 Patch'
                       , 'EBSDEV9', 'Prod Support'
                       , 'EBSDEVE', 'LIVE Developement'
                       , 'EBSDEVA', 'DBA POC'
                       , 'EBSDEVB', 'SIT-OPS-070'
                       , 'EBSDEVC', 'FUNC-050'
                       , 'EBSDEVD', 'FUNC'
                       , 'EBSACCE', 'UAT'
                       , 'UEBSACCE', 'UAT'
                       , 'EBSTEST', 'Patch'
                      ) || ' ('||to_char(sysdate,'YYYY-MM-DD')||')'
WHERE EXISTS (SELECT NULL FROM FND_PROFILE_OPTIONS FPO 
              WHERE FPOV.PROFILE_OPTION_ID = FPO.PROFILE_OPTION_ID
              AND FPO.PROFILE_OPTION_NAME = 'SITENAME');

PROMPT Modifier FWK_HOMEPAGE_BRAND
select user_function_name from FND_FORM_FUNCTIONS fff, FND_FORM_FUNCTIONS_TL ffft
 where FFF.FUNCTION_ID = FFFT.FUNCTION_ID 
      and fff.function_name = 'FWK_HOMEPAGE_BRAND';
update FND_FORM_FUNCTIONS_TL
set user_function_name = 'E-Business Suite - '||(SELECT PROFILE_OPTION_VALUE
  FROM apps.fnd_profile_option_values a, apps.fnd_profile_options b
  where b.profile_option_name = 'SITENAME'
  and a.application_id = b.application_id
  and a.profile_option_id = b.profile_option_id)
where function_id in (select function_id from FND_FORM_FUNCTIONS where function_name = 'FWK_HOMEPAGE_BRAND');

PROMPT Modifier le profil FND: Personalization Document Root Path
--SELECT b.profile_option_name, PROFILE_OPTION_VALUE
--FROM applsys.fnd_profile_option_values a, applsys.fnd_profile_options b
--where b.profile_option_name = 'FND_PERZ_DOC_ROOT_PATH'
--and a.application_id = b.application_id
--and a.profile_option_id = b.profile_option_id;
UPDATE FND_PROFILE_OPTION_VALUES FPOV
SET FPOV.PROFILE_OPTION_VALUE = '/&TMPSID._Apps/fs1/EBSapps/appl/xxagr/12.0.0/personalizations'
WHERE EXISTS (SELECT NULL FROM FND_PROFILE_OPTIONS FPO 
              WHERE FPOV.PROFILE_OPTION_ID = FPO.PROFILE_OPTION_ID
              AND FPO.PROFILE_OPTION_NAME = 'FND_PERZ_DOC_ROOT_PATH');

PROMPT Modifier le profil BNE Server Log Path
--SELECT b.profile_option_name, PROFILE_OPTION_VALUE
--  FROM applsys.fnd_profile_option_values a, applsys.fnd_profile_options b
--  where b.profile_option_name = 'BNE_SERVER_LOG_PATH'
--  and a.application_id = b.application_id
--  and a.profile_option_id = b.profile_option_id;
UPDATE FND_PROFILE_OPTION_VALUES FPOV
SET FPOV.PROFILE_OPTION_VALUE = '/&TMPSID._Apps/inst/apps/&TMPSID._&TARGET_WEB_HOSTNAME/temp'
WHERE EXISTS (SELECT NULL FROM FND_PROFILE_OPTIONS FPO 
              WHERE FPOV.PROFILE_OPTION_ID = FPO.PROFILE_OPTION_ID
              AND FPO.PROFILE_OPTION_NAME = 'BNE_SERVER_LOG_PATH');


PROMPT Modifier le profil MSC: PS/SNO Data Store Path
--SELECT b.profile_option_name, PROFILE_OPTION_VALUE
--  FROM applsys.fnd_profile_option_values a, applsys.fnd_profile_options b
--  where b.profile_option_name = 'MSC_SCP_DATA_STORE_PATH'
--  and a.application_id = b.application_id
--  and a.profile_option_id = b.profile_option_id;
UPDATE FND_PROFILE_OPTION_VALUES FPOV
SET FPOV.PROFILE_OPTION_VALUE = '&CONC_DIRECTORY'
WHERE EXISTS (SELECT NULL FROM FND_PROFILE_OPTIONS FPO 
              WHERE FPOV.PROFILE_OPTION_ID = FPO.PROFILE_OPTION_ID
              AND FPO.PROFILE_OPTION_NAME = 'MSC_SCP_DATA_STORE_PATH');


PROMPT Modifier le profil IBY: XML Publisher Delivery Manager Configuration File
--SELECT b.profile_option_name, PROFILE_OPTION_VALUE
--FROM applsys.fnd_profile_option_values a, applsys.fnd_profile_options b
--where b.profile_option_name = 'IBY_XDO_DELIVERY_CFG_FILE'
--and a.application_id = b.application_id
--and a.profile_option_id = b.profile_option_id;
UPDATE FND_PROFILE_OPTION_VALUES FPOV
SET FPOV.PROFILE_OPTION_VALUE = '/&TMPSID._Apps/fs_ne/EBSapps/appl/xdo/resource/xdodelivery.cfg'
WHERE EXISTS (SELECT NULL FROM FND_PROFILE_OPTIONS FPO 
              WHERE FPOV.PROFILE_OPTION_ID = FPO.PROFILE_OPTION_ID
              AND FPO.PROFILE_OPTION_NAME = 'IBY_XDO_DELIVERY_CFG_FILE');

PROMPT Modifier le profil Database Wallet Directory 
UPDATE FND_PROFILE_OPTION_VALUES FPOV
SET FPOV.PROFILE_OPTION_VALUE = '&1/appsutil/wallet'
WHERE EXISTS (SELECT NULL FROM FND_PROFILE_OPTIONS FPO
              WHERE FPOV.PROFILE_OPTION_ID = FPO.PROFILE_OPTION_ID
              AND FPO.PROFILE_OPTION_NAME = 'FND_DB_WALLET_DIR');

PROMPT Modifier le profil IBY: ECAPP URL
UPDATE FND_PROFILE_OPTION_VALUES FPOV
SET FPOV.PROFILE_OPTION_VALUE = '&TARGET_URL_PROTOCOL://&TARGET_VI_HOSTNAME..agropur.com:&TARGET_SSL_PORT./OA_HTML/ibyecapp'
WHERE EXISTS (SELECT NULL FROM FND_PROFILE_OPTIONS FPO
              WHERE FPOV.PROFILE_OPTION_ID = FPO.PROFILE_OPTION_ID
              AND FPO.PROFILE_OPTION_NAME = 'IBY_ECAPP_URL');


PROMPT Modifier le profil ICX: Oracle Payment Server URL
UPDATE FND_PROFILE_OPTION_VALUES FPOV
SET FPOV.PROFILE_OPTION_VALUE = '&TARGET_URL_PROTOCOL://&TARGET_VI_HOSTNAME..agropur.com:&TARGET_SSL_PORT./OA_HTML/ibyecapp' 
WHERE EXISTS (SELECT NULL FROM FND_PROFILE_OPTIONS FPO
              WHERE FPOV.PROFILE_OPTION_ID = FPO.PROFILE_OPTION_ID
              AND FPO.PROFILE_OPTION_NAME = 'ICX_PAY_SERVER');

PROMPT Mettre  jour le profil HZ: API Debug File Directory
--SELECT b.profile_option_name, PROFILE_OPTION_VALUE
--FROM applsys.fnd_profile_option_values a, applsys.fnd_profile_options b
--where b.profile_option_name = 'HZ_API_DEBUG_FILE_PATH'
--and a.application_id = b.application_id
--and a.profile_option_id = b.profile_option_id;
UPDATE FND_PROFILE_OPTION_VALUES FPOV
SET FPOV.PROFILE_OPTION_VALUE = '/&TMPSID._Apps/inst/apps/&TMPSID._&TARGET_WEB_HOSTNAME/temp'
WHERE EXISTS (SELECT NULL FROM FND_PROFILE_OPTIONS FPO 
              WHERE FPOV.PROFILE_OPTION_ID = FPO.PROFILE_OPTION_ID
              AND FPO.PROFILE_OPTION_NAME = 'HZ_API_DEBUG_FILE_PATH');

PROMPT Mettre  jour le profil Tax: Debug File Directory
--SELECT b.profile_option_name, PROFILE_OPTION_VALUE
--FROM applsys.fnd_profile_option_values a, applsys.fnd_profile_options b
--where b.profile_option_name = 'TAX_DEBUG_FILE_LOCATION'
--and a.application_id = b.application_id
--and a.profile_option_id = b.profile_option_id;
UPDATE FND_PROFILE_OPTION_VALUES FPOV
SET FPOV.PROFILE_OPTION_VALUE = '/&TMPSID._Apps/inst/apps/&TMPSID._&TARGET_WEB_HOSTNAME/temp'
WHERE EXISTS (SELECT NULL FROM FND_PROFILE_OPTIONS FPO 
              WHERE FPOV.PROFILE_OPTION_ID = FPO.PROFILE_OPTION_ID
              AND FPO.PROFILE_OPTION_NAME = 'TAX_DEBUG_FILE_LOCATION');

PROMPT Mettre  jour le profil JTF : Debug log directory
--SELECT b.profile_option_name, PROFILE_OPTION_VALUE
--FROM applsys.fnd_profile_option_values a, applsys.fnd_profile_options b
--where b.profile_option_name = 'JTF_DEBUG_LOG_DIRECTORY'
--and a.application_id = b.application_id
--and a.profile_option_id = b.profile_option_id;
UPDATE FND_PROFILE_OPTION_VALUES FPOV
SET FPOV.PROFILE_OPTION_VALUE = '/&TMPSID._Apps/fs1/inst/apps/&TMPSID._&TARGET_WEB_HOSTNAME/temp'
WHERE EXISTS (SELECT NULL FROM FND_PROFILE_OPTIONS FPO 
              WHERE FPOV.PROFILE_OPTION_ID = FPO.PROFILE_OPTION_ID
              AND FPO.PROFILE_OPTION_NAME = 'JTF_DEBUG_LOG_DIRECTORY');

PROMPT Mettre a null le profil Tax: XXAR: Default Statement Printer
UPDATE FND_PROFILE_OPTION_VALUES FPOV
SET FPOV.PROFILE_OPTION_VALUE = null
WHERE EXISTS (SELECT NULL FROM FND_PROFILE_OPTIONS FPO
              WHERE FPOV.PROFILE_OPTION_ID = FPO.PROFILE_OPTION_ID
              AND FPO.PROFILE_OPTION_NAME = 'XXAR_STATEMENT_DEFAULT_PRINTER');


PROMPT Mettre  jour le profil PA: Debug Log Directory
--SELECT b.profile_option_name, PROFILE_OPTION_VALUE
--FROM applsys.fnd_profile_option_values a, applsys.fnd_profile_options b
--where b.profile_option_name = 'PA_DEBUG_LOG_DIRECTORY'
--and a.application_id = b.application_id
--and a.profile_option_id = b.profile_option_id;
UPDATE FND_PROFILE_OPTION_VALUES FPOV
SET FPOV.PROFILE_OPTION_VALUE = '/&TMPSID._Apps/fs1/inst/apps/&TMPSID._&TARGET_WEB_HOSTNAME/temp'
WHERE EXISTS (SELECT NULL FROM FND_PROFILE_OPTIONS FPO 
              WHERE FPOV.PROFILE_OPTION_ID = FPO.PROFILE_OPTION_ID
              AND FPO.PROFILE_OPTION_NAME = 'PA_DEBUG_LOG_DIRECTORY');

PROMPT Mettre a  jour le profil ICX: Oracle Payment Server URL			  
--SELECT b.profile_option_name, PROFILE_OPTION_VALUE
--FROM applsys.fnd_profile_option_values a, applsys.fnd_profile_options b
--where b.profile_option_name = 'ICX_PAY_SERVER'
--and a.application_id = b.application_id
--and a.profile_option_id = b.profile_option_id;
UPDATE FND_PROFILE_OPTION_VALUES FPOV
SET FPOV.PROFILE_OPTION_VALUE = 'http://&TARGET_VI_HOSTNAME..agropur.com:&TARGET_SSL_PORT/OA_HTML/ibyecapp'
WHERE EXISTS (SELECT NULL FROM FND_PROFILE_OPTIONS FPO 
              WHERE FPOV.PROFILE_OPTION_ID = FPO.PROFILE_OPTION_ID
              AND FPO.PROFILE_OPTION_NAME = 'ICX_PAY_SERVER');

PROMPT Mettre a  jour le profil Application Framework Agent
--SELECT b.profile_option_name, PROFILE_OPTION_VALUE
--FROM applsys.fnd_profile_option_values a, applsys.fnd_profile_options b
--where b.profile_option_name = 'APPS_FRAMEWORK_AGENT'
--and a.application_id = b.application_id
--and a.profile_option_id = b.profile_option_id;
UPDATE FND_PROFILE_OPTION_VALUES FPOV
SET FPOV.PROFILE_OPTION_VALUE = 'http://&TARGET_VI_HOSTNAME..agropur.com:&TARGET_SSL_PORT'
WHERE EXISTS (SELECT NULL FROM FND_PROFILE_OPTIONS FPO 
              WHERE FPOV.PROFILE_OPTION_ID = FPO.PROFILE_OPTION_ID
              AND FPO.PROFILE_OPTION_NAME = 'APPS_FRAMEWORK_AGENT');

PROMPT Mettre a jour le profil Apps Servlet Agent
--SELECT b.profile_option_name, PROFILE_OPTION_ALUE
--FROM applsys.fnd_profile_option_values a, applsys.fnd_profile_options b
--where b.profile_option_name = 'APPS_SERVLET_AGENT'
--and a.application_id = b.application_id
--and a.profile_option_id = b.profile_option_id;
UPDATE FND_PROFILE_OPTION_VALUES FPOV
SET FPOV.PROFILE_OPTION_VALUE = 'http://&TARGET_VI_HOSTNAME..agropur.com:&TARGET_SSL_PORT/OA_HTML'
WHERE EXISTS (SELECT NULL FROM FND_PROFILE_OPTIONS FPO 
              WHERE FPOV.PROFILE_OPTION_ID = FPO.PROFILE_OPTION_ID
              AND FPO.PROFILE_OPTION_NAME = 'APPS_SERVLET_AGENT');

PROMPT Mettre a  jour le profil POS: External URL			  
--SELECT b.profile_option_name, PROFILE_OPTION_VALUE
--FROM applsys.fnd_profile_option_values a, applsys.fnd_profile_options b
--where b.profile_option_name = 'POS_EXTERNAL_URL'
--and a.application_id = b.application_id
--and a.profile_option_id = b.profile_option_id;
UPDATE FND_PROFILE_OPTION_VALUES FPOV
SET FPOV.PROFILE_OPTION_VALUE = 'http://&TARGET_VI_HOSTNAME..agropur.com:&TARGET_SSL_PORT'
WHERE EXISTS (SELECT NULL FROM FND_PROFILE_OPTIONS FPO 
              WHERE FPOV.PROFILE_OPTION_ID = FPO.PROFILE_OPTION_ID
              AND FPO.PROFILE_OPTION_NAME = 'POS_EXTERNAL_URL');

PROMPT Mettre a  jour le profil POS: Internal URL
--SELECT b.profile_option_name, PROFILE_OPTION_VALUE
--FROM applsys.fnd_profile_option_values a, applsys.fnd_profile_options b
--where b.profile_option_name = 'POS_INTERNAL_URL'
--and a.application_id = b.application_id
--and a.profile_option_id = b.profile_option_id;
UPDATE FND_PROFILE_OPTION_VALUES FPOV
SET FPOV.PROFILE_OPTION_VALUE = 'http://&TARGET_VI_HOSTNAME..agropur.com:&TARGET_SSL_PORT'
WHERE EXISTS (SELECT NULL FROM FND_PROFILE_OPTIONS FPO 
              WHERE FPOV.PROFILE_OPTION_ID = FPO.PROFILE_OPTION_ID
              AND FPO.PROFILE_OPTION_NAME = 'POS_INTERNAL_URL');

PROMPT Mettre a jour le profil External ADF Application URL
--SELECT b.profile_option_name, PROFILE_OPTION_VALUE
--FROM applsys.fnd_profile_option_values a, applsys.fnd_profile_options b
--where b.profile_option_name = 'FND_EXTERNAL_ADF_URL'
--and a.application_id = b.application_id
--and a.profile_option_id = b.profile_option_id;
update applsys.fnd_profile_option_values z
set z.profile_option_value = DECODE('&TMPSID', 'EBSDEV1', 'https://adfdev1sso.agropur.com'
                                   , 'EBSDEV2', 'https://adfdev2sso.agropur.com'
                                   , 'EBSDEV3', 'https://adfdev3sso.agropur.com'
                                   , 'EBSDEV4', 'https://adfdev4sso.agropur.com'
                                   , 'EBSDEV5', 'https://adfdev5sso.agropur.com'
                                   , 'EBSDEV6', 'https://adfdev6sso.agropur.com'
                                   , 'EBSDEV7', 'https://adfdev7sso.agropur.com'
                                   , 'EBSDEV8', 'https://adfdev8sso.agropur.com'
                                   , 'EBSDEV9', 'https://adfdev9sso.agropur.com'
                                   , 'EBSDEVE', 'https://adfdevesso.agropur.com'
                                   , 'EBSDTSTHA', 'https://adftsthasso.agropur.com'
                                   , 'EBSDEVA', 'https://adfdevasso.agropur.com'
                                   , 'EBSDEVB', 'https://adfdevbsso.agropur.com'
                                   , 'EBSDEVC', 'https://adfdevcsso.agropur.com'
                                   , 'EBSDEVD', 'https://adfdevdsso.agropur.com'
                                   , 'EBSACCE', 'https://adfaccesso.agropur.com'
                                   , 'EBSTEST', 'https://adftestsso.agropur.com'
                                  ),
      z.last_update_date = sysdate
where exists (
SELECT *
  FROM applsys.fnd_profile_option_values a, applsys.fnd_profile_options b
  where b.profile_option_name = 'FND_EXTERNAL_ADF_URL'
  and a.application_id = b.application_id
  and a.profile_option_id = b.profile_option_id
  and z.application_id = b.application_id
  and z.profile_option_id= b.profile_option_id
  );

--PROMPT Verifier et Mettre a jour le profil MSC: ASCP - URL de planification
--SELECT b.profile_option_name, PROFILE_OPTION_VALUE			
--FROM applsys.fnd_profile_option_values a, applsys.fnd_profile_options b			
--where b.profile_option_name = 'MSC_ASCP_WEBLOGIC_URL'			
--and a.application_id = b.application_id			
--and a.profile_option_id = b.profile_option_id;			
			
			update applsys.fnd_profile_option_values z
			set z.profile_option_value = DECODE('&TMPSID', 'EBSDEV1', 'https://adfdev1sso.agropur.com'
			                                   , 'EBSDEV2', 'https://ascpdev2cust.agropur.com'
			                                   , 'EBSDEV3', 'https://ascpdev3cust.agropur.com'
			                                   , 'EBSDEV4', 'https://ascpdev4cust.agropur.com'
			                                   , 'EBSDEV5', 'https://ascpdev5cust.agropur.com'
			                                   , 'EBSDEV6', 'https://ascpdev6cust.agropur.com'
			                                   , 'EBSDEV7', 'https://ascpdev7cust.agropur.com'
			                                   , 'EBSDEV8', 'https://ascpdev8cust.agropur.com'
			                                   , 'EBSDEV9', 'https://ascpdev9cust.agropur.com'
			                                   , 'EBSDEVE', 'https://ascpdevecust.agropur.com'
			                                   , 'EBSTSTHA', 'https://ascptsthacust.agropur.com'
			                                   , 'EBSDEVA', 'https://ascpdevacust.agropur.com'
			                                   , 'EBSDEVB', 'https://ascpdevbcust.agropur.com'
			                                   , 'EBSDEVC', 'https://ascpdevccust.agropur.com'
			                                   , 'EBSDEVD', 'https://ascpdevdcust.agropur.com'
			                                   , 'EBSACCE', 'https://ascpaccecust.agropur.com'
			                                   , 'EBSTEST', 'https://ascptestcust.agropur.com'
			                                  ),
			      z.last_update_date = sysdate
			where exists (
			SELECT *
			  FROM applsys.fnd_profile_option_values a, applsys.fnd_profile_options b
			  where b.profile_option_name = 'MSC_ASCP_WEBLOGIC_URL'
			  and a.application_id = b.application_id
			  and a.profile_option_id = b.profile_option_id
			  and z.application_id = b.application_id
			  and z.profile_option_id= b.profile_option_id
			  );


--PROMPT Mettre a jour le profil WSH: BPEL Webservice URl for OTM
--SELECT b.profile_option_name, PROFILE_OPTION_VALUE
--FROM applsys.fnd_profile_option_values a, applsys.fnd_profile_options b
--where b.profile_option_name = 'WSH_OTM_OB_SERVICE_ENDPOINT'
--and a.application_id = b.application_id
--and a.profile_option_id = b.profile_option_id;
			  
update applsys.fnd_profile_option_values z
set z.profile_option_value = DECODE('&TMPSID'
                                  , 'EBSDEV1', ''
                                  , 'EBSDEV2', ''
                                  , 'EBSDEV3', ''
                                  , 'EBSDEV4', ''
                                  , 'EBSDEV5', ''
                                  , 'EBSDEV6', ''
                                  , 'EBSDEV7', ''
                                  , 'EBSDEV8', ''
                                  , 'EBSDEV9', ''
                                  , 'EBSDEVE', 'http://dcssdesoaapps.agropur.com:7003/soa-infra/services/default'
                                  , 'EBSDEVA', ''
                                  , 'EBSDEVB', ''
                                  , 'EBSDEVC', ''
                                  , 'EBSDEVD', ''
                                  , 'EBSACCE', 'http://dcssssoaapps.agropur.com:7003/soa-infra/services/default'
                                  , 'EBSTEST', ''
                                  ),
      z.last_update_date = sysdate
where exists (
SELECT *
  FROM applsys.fnd_profile_option_values a, applsys.fnd_profile_options b
  where b.profile_option_name = 'WSH_OTM_OB_SERVICE_ENDPOINT'
  and a.application_id = b.application_id
  and a.profile_option_id = b.profile_option_id
  and z.application_id = b.application_id
  and z.profile_option_id= b.profile_option_id
  );
  commit;
  
PROMPT Mettre a jour le profil Allow Unrestricted Redirects
--SELECT b.profile_option_name, PROFILE_OPTION_VALUE
--FROM applsys.fnd_profile_option_values a, applsys.fnd_profile_options b
--where b.profile_option_name = 'FND_SEC_ALLOW_UNRESTRICTED_REDIRECT'
--and a.application_id = b.application_id
--and a.profile_option_id = b.profile_option_id;

UPDATE FND_PROFILE_OPTION_VALUES FPOV
SET FPOV.PROFILE_OPTION_VALUE='Y'
WHERE EXISTS (SELECT NULL FROM FND_PROFILE_OPTIONS FPO
              WHERE FPOV.PROFILE_OPTION_ID = FPO.PROFILE_OPTION_ID
              AND FPO.PROFILE_OPTION_NAME = 'FND_SEC_ALLOW_UNRESTRICTED_REDIRECT');


--  en Revision : REQ0147787 par Andr� Lacombe

PROMPT Mettre a  jour le profil FND: Oracle Business Intelligence Suite EE base URL
--SELECT b.profile_option_name, PROFILE_OPTION_VALUE
--FROM applsys.fnd_profile_option_values a, applsys.fnd_profile_options b
--where b.profile_option_name = 'FND_OBIEE_URL'
--and a.application_id = b.application_id
--and a.profile_option_id = b.profile_option_id;
UPDATE FND_PROFILE_OPTION_VALUES FPOV
SET FPOV.PROFILE_OPTION_VALUE='http://dcssdobiapps:9704'
WHERE EXISTS (SELECT NULL FROM FND_PROFILE_OPTIONS FPO 
              WHERE FPOV.PROFILE_OPTION_ID = FPO.PROFILE_OPTION_ID
              AND FPO.PROFILE_OPTION_NAME = 'FND_OBIEE_URL');

PROMPT Mettre a  jour le profil BNE Upload Staging Directory
--SELECT b.profile_option_name, PROFILE_OPTION_VALUE
--FROM applsys.fnd_profile_option_values a, applsys.fnd_profile_options b
--where b.profile_option_name = 'BNE_UPLOAD_STAGING_DIRECTORY'
--and a.application_id = b.application_id
--and a.profile_option_id = b.profile_option_id;
UPDATE FND_PROFILE_OPTION_VALUES FPOV
SET FPOV.PROFILE_OPTION_VALUE = '/&TMPSID._Apps/fs1/EBSapps/comn/temp'
WHERE EXISTS (SELECT NULL FROM FND_PROFILE_OPTIONS FPO 
              WHERE FPOV.PROFILE_OPTION_ID = FPO.PROFILE_OPTION_ID
              AND FPO.PROFILE_OPTION_NAME = 'BNE_UPLOAD_STAGING_DIRECTORY');

PROMPT Mettre a jour le profil OM: Debug Log Directory
--SELECT b.profile_option_name, PROFILE_OPTION_VALUE
--FROM applsys.fnd_profile_option_values a, applsys.fnd_profile_options b
--where b.profile_option_name = 'OE_DEBUG_LOG_DIRECTORY'
--and a.application_id = b.application_id
--and a.profile_option_id = b.profile_option_id;
UPDATE FND_PROFILE_OPTION_VALUES FPOV
SET FPOV.PROFILE_OPTION_VALUE = '/&TMPSID._Apps/fs1/EBSapps/comn/temp'
WHERE EXISTS (SELECT NULL FROM FND_PROFILE_OPTIONS FPO 
              WHERE FPOV.PROFILE_OPTION_ID = FPO.PROFILE_OPTION_ID
              AND FPO.PROFILE_OPTION_NAME = 'OE_DEBUG_LOG_DIRECTORY');

PROMPT Mettre a jour le profil XXMSC: PS_Attribute_file_path
--SELECT b.profile_option_name, PROFILE_OPTION_VALUE
--FROM applsys.fnd_profile_option_values a, applsys.fnd_profile_options b
--where b.profile_option_name = 'XXMSC: PS_ATTRIBUTE_FILE_PATH'
--and a.application_id = b.application_id
--and a.profile_option_id = b.profile_option_id;
UPDATE FND_PROFILE_OPTION_VALUES FPOV
SET FPOV.PROFILE_OPTION_VALUE = '\\dcpspsftp\&TMPSID\outbound'
WHERE EXISTS (SELECT NULL FROM FND_PROFILE_OPTIONS FPO 
              WHERE FPOV.PROFILE_OPTION_ID = FPO.PROFILE_OPTION_ID
              AND FPO.PROFILE_OPTION_NAME = 'XXMSC: PS_ATTRIBUTE_FILE_PATH');

-------------------------------------------------------------------              
-----  Update URLs Attributes and Profiles: SOURCE_URL with TARGET_URL  -------              
-------------------------------------------------------------------
PROMPT SSL
DEFINE SOURCE_URL = 'http://&TARGET_WEB_HOSTNAME..agropur.com:&TARGET_PORT';
DEFINE TARGET_URL = 'https://&TARGET_VI_HOSTNAME..agropur.com:&TARGET_SSL_PORT';

PROMPT Modifier table => WF_NOTIFICATION_ATTRIBUTES
UPDATE APPS.WF_NOTIFICATION_ATTRIBUTES SET TEXT_VALUE = REPLACE(TEXT_VALUE, '&SOURCE_URL', '&TARGET_URL') WHERE TEXT_VALUE LIKE '%&SOURCE_URL%';

PROMPT Modifier table => WF_ITEM_ATTRIBUTES_VALUES
UPDATE APPS.WF_ITEM_ATTRIBUTE_VALUES SET TEXT_VALUE = REPLACE(TEXT_VALUE, '&SOURCE_URL', '&TARGET_URL') WHERE TEXT_VALUE LIKE '%&SOURCE_URL%';

PROMPT Mettre a jour le profil APPS_SERVLET_AGENT
UPDATE FND_PROFILE_OPTION_VALUES FPOV
SET FPOV.PROFILE_OPTION_VALUE = '&TARGET_URL_PROTOCOL://&TARGET_VI_HOSTNAME..agropur.com:&TARGET_SSL_PORT/OA_HTML'
WHERE EXISTS (SELECT NULL FROM FND_PROFILE_OPTIONS FPO 
              WHERE FPOV.PROFILE_OPTION_ID = FPO.PROFILE_OPTION_ID
              AND FPO.PROFILE_OPTION_NAME = 'APPS_SERVLET_AGENT');

PROMPT Mettre a jour le profil POS_EXTERNAL_URL
UPDATE FND_PROFILE_OPTION_VALUES FPOV
SET FPOV.PROFILE_OPTION_VALUE = '&TARGET_URL_PROTOCOL://&TARGET_VI_HOSTNAME..agropur.com:&TARGET_SSL_PORT'
WHERE EXISTS (SELECT NULL FROM FND_PROFILE_OPTIONS FPO 
              WHERE FPOV.PROFILE_OPTION_ID = FPO.PROFILE_OPTION_ID
              AND FPO.PROFILE_OPTION_NAME = 'POS_EXTERNAL_URL');

PROMPT Mettre a jour le profil POS_INTERNAL_URL
UPDATE FND_PROFILE_OPTION_VALUES FPOV
SET FPOV.PROFILE_OPTION_VALUE = '&TARGET_URL_PROTOCOL://&TARGET_VI_HOSTNAME..agropur.com:&TARGET_SSL_PORT'
WHERE EXISTS (SELECT NULL FROM FND_PROFILE_OPTIONS FPO 
              WHERE FPOV.PROFILE_OPTION_ID = FPO.PROFILE_OPTION_ID
              AND FPO.PROFILE_OPTION_NAME = 'POS_INTERNAL_URL');


PROMPT Mettre a jour le profil ICX_PAY_SERVER
UPDATE FND_PROFILE_OPTION_VALUES FPOV
SET FPOV.PROFILE_OPTION_VALUE = '&TARGET_URL_PROTOCOL://&TARGET_VI_HOSTNAME..agropur.com:&TARGET_SSL_PORT/OA_HTML/ibyecapp'
WHERE EXISTS (SELECT NULL FROM FND_PROFILE_OPTIONS FPO 
              WHERE FPOV.PROFILE_OPTION_ID = FPO.PROFILE_OPTION_ID
              AND FPO.PROFILE_OPTION_NAME = 'ICX_PAY_SERVER');

PROMPT Mettre a jour le profil HELP_WEB_BASE_URL
UPDATE FND_PROFILE_OPTION_VALUES FPOV
SET FPOV.PROFILE_OPTION_VALUE = '&TARGET_URL_PROTOCOL://&TARGET_VI_HOSTNAME..agropur.com:&TARGET_SSL_PORT/OA_HTML/'
WHERE EXISTS (SELECT NULL FROM FND_PROFILE_OPTIONS FPO 
              WHERE FPOV.PROFILE_OPTION_ID = FPO.PROFILE_OPTION_ID
              AND FPO.PROFILE_OPTION_NAME = 'HELP_WEB_BASE_URL');

PROMPT Mettre a jour le profil FND_DISABLE_INLINE_ATTACHMENTS
UPDATE FND_PROFILE_OPTION_VALUES FPOV
SET FPOV.PROFILE_OPTION_VALUE = 'TRUE'
WHERE EXISTS (SELECT NULL FROM FND_PROFILE_OPTIONS FPO
              WHERE FPOV.PROFILE_OPTION_ID = FPO.PROFILE_OPTION_ID
              AND FPO.PROFILE_OPTION_NAME = 'FND_DISABLE_INLINE_ATTACHMENTS');

PROMPT Mettre a jour le profil BNE_OOXML_ENABLED
update FND_PROFILE_OPTION_VALUES
set profile_option_value = 'N'
WHERE profile_option_id =
  (SELECT profile_option_id
  FROM FND_PROFILE_OPTIONS
  WHERE profile_option_name = 'BNE_OOXML_ENABLED'
  )
and level_value = 0;

PROMPT Mettre a jour le profil POR : CA Certificate File Name
--SELECT b.profile_option_name, PROFILE_OPTION_VALUE
--FROM applsys.fnd_profile_option_values a, applsys.fnd_profile_options b
--where b.profile_option_name = 'POR_CACERT_FILE_NAME'
--and a.application_id = b.application_id
--and a.profile_option_id = b.profile_option_id;
UPDATE FND_PROFILE_OPTION_VALUES FPOV
SET FPOV.PROFILE_OPTION_VALUE = '/&TMPSID._Apps/ORA_Tools/10.1.2/sysman/config/b64InternetCertificate.txt'
WHERE EXISTS (SELECT NULL FROM FND_PROFILE_OPTIONS FPO
              WHERE FPOV.PROFILE_OPTION_ID = FPO.PROFILE_OPTION_ID
              AND FPO.PROFILE_OPTION_NAME = 'POR_CACERT_FILE_NAME');

PROMPT Mettre a jour le profil EDW: Log file directory
--SELECT b.profile_option_name, PROFILE_OPTION_VALUE
--FROM applsys.fnd_profile_option_values a, applsys.fnd_profile_options b
--where b.profile_option_name = 'EDW_LOGFILE_DIR'
--and a.application_id = b.application_id
--and a.profile_option_id = b.profile_option_id;
UPDATE FND_PROFILE_OPTION_VALUES FPOV
SET FPOV.PROFILE_OPTION_VALUE = '/&TMPSID._Apps/fs_ne/EBSapps/comn/temp'
WHERE EXISTS (SELECT NULL FROM FND_PROFILE_OPTIONS FPO
              WHERE FPOV.PROFILE_OPTION_ID = FPO.PROFILE_OPTION_ID
              AND FPO.PROFILE_OPTION_NAME = 'EDW_LOGFILE_DIR');

PROMPT Mettre a jour le profil INV: Debug file (Including the complete path)
--SELECT b.profile_option_name, PROFILE_OPTION_VALUE
--FROM applsys.fnd_profile_option_values a, applsys.fnd_profile_options b
--where b.profile_option_name = 'INV_DEBUG_FILE'
--and a.application_id = b.application_id
--and a.profile_option_id = b.profile_option_id;
UPDATE FND_PROFILE_OPTION_VALUES FPOV
SET FPOV.PROFILE_OPTION_VALUE = REPLACE(FPOV.PROFILE_OPTION_VALUE, '&SOURCE_SID', '&TMPSID')
WHERE EXISTS (SELECT NULL FROM FND_PROFILE_OPTIONS FPO
              WHERE FPOV.PROFILE_OPTION_ID = FPO.PROFILE_OPTION_ID
              AND FPO.PROFILE_OPTION_NAME = 'INV_DEBUG_FILE');

-------------------------------------------------------------------              
-----  Agropur Mailer Parameter Setup  -------              
-------------------------------------------------------------------
PROMPT Mettre parametres pour notification mailer
UPDATE FND_SVC_COMP_PARAM_VALS
SET PARAMETER_VALUE = 'Agropur Automatic Email - '  ||  (SELECT INSTANCE_NAME FROM v$instance)
WHERE component_parameter_id IN (SELECT v.component_parameter_id
                                  FROM fnd_svc_comp_param_vals_v v,
                                    fnd_svc_comp_params_b p,
                                    fnd_svc_components c,
                                    fnd_svc_comp_params_vl vl
                                  WHERE c.component_type = 'WF_MAILER'
                                  AND p.parameter_name   = 'FROM'
                                  AND v.component_id     = c.component_id
                                  AND v.parameter_id     = p.parameter_id
                                  AND VL.PARAMETER_ID    = P.PARAMETER_ID);

-------------------------------------------------------
--        PCG - � detruire eventuellement--------------
-------------------------------------------------------
PROMPT Modifier table ==> XXLAAPPS.LA_BR_CONFIGURATIONS
update XXLAAPPS.LA_BR_CONFIGURATIONS
set utl_pathname = '/&TMPSID._Apps/fs1/EBSapps/comn/temp';

PROMPT Modifier table ==> XXLAAPPS.LAAM_MACHINES
update XXLAAPPS.LAAM_MACHINES
set machine_name = '&TARGET_PHYSICAL_DB_HOSTNAME'
where machine_id = 20000;

PROMPT Modifier table ==> XXLAAPPS.LAAM_DATABASE_INSTANCES
update XXLAAPPS.LAAM_DATABASE_INSTANCES
set INSTANCE_NAME = '&TMPSID'
    , HOST_NAME = '&TARGET_PHYSICAL_WEB_HOSTNAME'
    , CONNECTED_STRING = 'jdbc:oracle:thin:@&TARGET_PHYSICAL_DB_HOSTNAME:&TARGET_DB_PORT:&TMPSID'
    , PORT_NUMBER = '&TARGET_DB_PORT'
    , PASSWORD = null
WHERE INSTANCE_NAME = '&SOURCE_SID';
------------------------------------------------------

PROMPT End of Executing post_clone_apps_step_2.sql 
