show parameter OPTIMIZER_MODE; --ALL_ROWS
SHOW PARAMETER statistics_level; --TYPICAL  
show parameter TIMED_STATISTICS; --TRUE
show parameter OPTIMIZER_ADAPTIVE_FEATURES; -- 12.2
show parameter OPTIMIZER_ADAPTIVE_STATISTICS;-- 12.2
show parameter optimizer_use_pending_statistics; -- FALSE
/*
ALTER SYSTEM SET statistics_level=basic; --BASIC: No advisories or statistics are collected.
ALTER SYSTEM SET statistics_level=typical;
ALTER SYSTEM SET statistics_level=all;

SQL> ALTER SYSTEM SET statistics_level=basic;
ALTER SYSTEM SET statistics_level=basic
*
ERROR at line 1:
ORA-02097: parameter cannot be modified because specified value is invalid
ORA-00830: cannot set statistics_level to BASIC with auto-tune SGA enabled
show parameter sga;

--Check auto stats collection status DISABLED
SELECT CLIENT_NAME,
       STATUS
FROM   DBA_AUTOTASK_CLIENT
WHERE  CLIENT_NAME = 'auto optimizer stats collection';

select client_name, JOB_SCHEDULER_STATUS 
 from DBA_AUTOTASK_CLIENT_JOB
 where client_name='auto optimizer stats collection';
 
 SELECT client_name, window_name, jobs_created, jobs_started, jobs_completed
 FROM dba_autotask_client_history
 WHERE client_name like '%stats%';
*/

SELECT statistics_name,
           session_status,
           system_status,
           activation_level,
           session_settable
    FROM   v$statistics_level
    ORDER BY statistics_name;

--Restoring Previous Versions of Statistics
--These views are useful in determining the time stamp to be used for statistics restoration.
select * from DBA_OPTSTAT_OPERATIONS where target like  '%_AUTOPAY_%';

--view partitions ans soub partitions
select * from dba_TAB_STATS_HISTORY where owner = 'AGR_APPS' and table_name like '%AUTOPAY%';


select * from dba_objects where object_name = 'DBA_TAB_STATISTICS';
select * from dba_views where view_name = 'DBA_TAB_STATISTICS';
select * from dba_tables where table_name = 'DBA_TAB_STATISTICS';

select * from dba_objects where object_name = 'DBA_TABLES';
select * from dba_views where view_name = 'DBA_TABLES';
select * from dba_tables where table_name = 'DBA_TABLES';

show parameter optimizer_dynamic_sampling;
ALTER SESSION SET optimizer_dynamic_sampling=4;
--OZF_OBJECT_FUND_SUMMARY_TEST
select * from OZF_FUNDS_UTILIZED_ALL_B;
/*
#How to identify STALE Statistics(Table/Column) Ex: OZF_FUNDS_UTILIZED_ALL_B
*/
--Check dba_tables last analyzed column
select owner,table_name,last_analyzed,NUM_ROWS  From dba_Tables where table_name = 'OZF_FUNDS_UTILIZED_ALL_B';--
select owner,table_name,last_analyzed From dba_Tables where table_name = 'OZF_OBJECT_FUND_SUMMARY_TEST';
select * From dba_objects where object_name = 'OZF_OBJECT_FUND_SUMMARY_TEST';
create SYNONYM APPS.OZF_OBJECT_FUND_SUMMARY_TEST for OZF.OZF_OBJECT_FUND_SUMMARY_TEST;
select * From dba_objects where object_name = 'OZF_OBJECT_FUND_SUMMARY';
--Check dba_tab_statistics
select table_name, stale_stats, last_analyzed,d.*
from dba_tab_statistics d
where --stale_stats='YES' and 
table_name in ('OZF_FUNDS_UTILIZED_ALL_B','OZF_OBJECT_FUND_SUMMARY');

--OZF_OBJECT_FUND_SUMMARY	YES	17-DEC-18

SELECT PLAN_TABLE_OUTPUT FROM TABLE(DBMS_XPLAN.DISPLAY_CURSOR(NULL, NULL, 'ALLSTATS LAST'));


show parameter statistics_level;
/*
NAME             TYPE   VALUE   
---------------- ------ ------- 
statistics_level string TYPICAL 
*/
--Call DBMS_STATS.GATHER_SCHEMA_STATS with option
--LIST STALE==>Works similar to option 2. Returns list of stale objects by query dba_tab_modifications table).
--LIST EMPTY==>Returns list of objects with no stats

--DBMS_STATS.GATHER_SCHEMA_STATS
--LIST STALE
--LIST EMPTY
/*GATHER - gather statistics for all objects 
GATHER AUTO - gather statistics for all objects needing new statistics
GATHER STALE - gather statistics for all objects having stale statistics
GATHER EMPTY- gather statistics for all objects having no statistics
LIST AUTO - list objects needing new statistics
LIST STALE - list objects having stale statistics
LIST EMPTY - list objects having stale statistics

!!!!!!!--SQL&gt; DBMS_STAT.set_table_prefs('SH','SALES','STALE_PERCENT',5);
STALE_PERCENT - This value determines the percentage of rows in a table that have to change before the statistics on that table are deemed stale and should be regathered. The default value is 10%
You can also set the stale_percent at database level and schema level using dbms_stats.set_database_prefs & dbms_stats.set_schema_prefs subprograms.
*/
--exec dbms_stats.gather_schema_stats('OZF', options=>'LIST EMPTY');
--exec dbms_stats.gather_schema_stats('OZF', options=>'LIST STALE');

SELECT dbms_stats.get_prefs('INCREMENTAL','OZF','OZF_FUNDS_UTILIZED_ALL_B') "INCREMENTAL" FROM   dual;
--FALSE  --INCREMENTAL
SELECT dbms_stats.get_prefs('STALE_PERCENT','OZF','OZF_FUNDS_UTILIZED_ALL_B') "STALE_PERCENT" FROM   dual;
--10
select * from user_tab_modifications where table_name='OZF_FUNDS_UTILIZED_ALL_B';
select * from user_tab_modifications where table_name='OZF_OBJECT_FUND_SUMMARY'; 

--## CHEVK for STALE Statistic Objects  in Schemas
set serveroutput on
declare 
mystaleobjs dbms_stats.objecttab; 
begin 
Dbms_Output.Put_Line(Systimestamp);
-- check whether there is any stale objects 
dbms_stats.gather_schema_stats(ownname=>'OZF', options=>'LIST STALE',objlist=>mystaleobjs); 
for i in 1 .. mystaleobjs.count loop 
dbms_output.put_line(mystaleobjs(i).objname); 
end loop; 
end; 
/
--OZF_OBJECT_FUND_SUMMARY

--Query DBA_TAB_COL_STATISTICS to check for stale column statistics.
--Check for NUM_DISTINCT,LOW_VALUE,HIGH_VALUE,NUM_NULLS and compare it with actual data from table.
--NUM_DISTINCT can be validated by query SELECT COUNT(DISTINCT COLUMN_NAME) FROM TABLE_NAME. LOW/MIN_VALUE can be validated with SELECT MIN(COLUMN_NAME),MAX(COLUMN_NAME).

select * from dba_objects where object_name = 'OZF_FUNDS_UTILIZED_ALL_B';--OZF_OBJECT_FUND_SUMMARY
select * from dba_objects where object_name = 'OZF_OBJECT_FUND_SUMMARY';

select * from DBA_TAB_COL_STATISTICS where owner ='OZF' and table_name='OZF_FUNDS_UTILIZED_ALL_B';
select * from DBA_TAB_COL_STATISTICS where owner ='OZF' and table_name='OZF_OBJECT_FUND_SUMMARY';

--############ Script to find missing or stale statistics #############################################################################
select m.TABLE_OWNER,
 m.TABLE_NAME,
 m.INSERTS,
 m.UPDATES,
 m.DELETES,
 m.TRUNCATED,
 to_char(m.TIMESTAMP,'yyyy/mm/dd hh24:mi:ss') as LAST_MODIFIED, 
 round((m.inserts+m.updates+m.deletes)*100/NULLIF(t.num_rows,0),2) as EST_PCT_MODIFIED,
 t.num_rows as last_known_rows_number,
 to_char(t.last_analyzed,'yyyy/mm/dd hh24:mi:ss') as last_analyszed
From dba_tab_modifications m,
 dba_tables t
where m.table_owner=t.owner
and m.table_name=t.table_name
--and m.TABLE_NAME ='OZF_OBJECT_FUND_SUMMARY'
and table_owner in ('OZF')
--and table_owner not in ('SYS','SYSTEM')
--and ((m.inserts+m.updates+m.deletes)*100/NULLIF(t.num_rows,0) > 10 or t.last_analyzed is null)
--order by timestamp desc;
order by 10 desc;
--OZF	OZF_FUNDS_ALL_B	0	22	0	NO	2018/12/17 13:28:08	10.48	210	2018/12/17 08:00:15
--OZF	OZF_OBJECT_FUND_SUMMARY	1	5860	0	NO	2018/12/17 13:28:08	64.35	9108	2018/12/17 12:00:09

-- ########################################################################################################################################
select m.TABLE_OWNER,
 'NO' as IS_PARTITION,
 m.TABLE_NAME as NAME, 
 m.INSERTS,
 m.UPDATES,
 m.DELETES,
 m.TRUNCATED,
 to_char(m.TIMESTAMP,'yyyy/mm/dd hh24:mi:ss') as LAST_MODIFIED, 
 round((m.inserts+m.updates+m.deletes)*100/NULLIF(t.num_rows,0),2) as EST_PCT_MODIFIED,
 t.num_rows as last_known_rows_number,
 to_char(t.last_analyzed,'yyyy/mm/dd hh24:mi:ss') as last_analyszed
From dba_tab_modifications m,
 dba_tables t 
where m.table_owner=t.owner
and m.table_name=t.table_name
and m.table_owner in ('MCS')
--and m.table_owner not in ('SYS','SYSTEM')
--and ((m.inserts+m.updates+m.deletes)*100/NULLIF(t.num_rows,0) >= 10 or t.last_analyzed is null)
union
select m.TABLE_OWNER,
 'YES' as IS_PARTITION, 
 m.PARTITION_NAME as NAME, 
 m.INSERTS,
 m.UPDATES,
 m.DELETES,
 m.TRUNCATED,
 to_char(m.TIMESTAMP,'yyyy/mm/dd hh24:mi:ss') as LAST_MODIFIED, 
 round((m.inserts+m.updates+m.deletes)*100/NULLIF(p.num_rows,0),2) as EST_PCT_MODIFIED,
 p.num_rows as last_known_rows_number,
 to_char(p.last_analyzed,'yyyy/mm/dd hh24:mi:ss') as last_analyszed
From dba_tab_modifications m, 
 dba_tab_partitions p
where m.table_owner=p.table_owner
and m.table_name=p.table_name
and m.PARTITION_NAME = p.PARTITION_NAME--
and m.table_owner in ('MCS')
--and m.table_owner not in ('SYS','SYSTEM')
--and ((m.inserts+m.updates+m.deletes)*100/NULLIF(p.num_rows,0) >= 10 or p.last_analyzed is null)
order by 8 desc;
--OZF	NO	OZF_FUNDS_ALL_B	0	22	0	NO	2018/12/17 13:28:08	10.48	210	2018/12/17 08:00:15
--OZF	NO	OZF_OBJECT_FUND_SUMMARY	1	5860	0	NO	2018/12/17 13:28:08	64.35	9108	2018/12/17 12:00:09

--###--How do I know if an object is partitioned or not?
SELECT * FROM dba_part_tables;
SELECT * FROM dba_part_indexes;
SELECT * FROM dba_tab_partitions WHERE table_name = 'OZF_FUNDS_UTILIZED_ALL_B';
SELECT * FROM dba_ind_partitions WHERE index_name = '%OZF_FUNDS_UTILIZED_ALL%';

--#####OLTP systems####
select status
from dba_autotask_client
where client_name = 'auto optimizer stats collection';
--DISABLED

show parameter optimizer_dynamic_sampling;
--optimizer_dynamic_sampling integer 2 
show parameter optimizer_adaptive_features;
--optimizer_adaptive_features boolean FALSE 

-- Get Last DML on table --
SELECT m.TABLE_OWNER, 
   m.TABLE_NAME, 
   m.INSERTS,
   m.UPDATES,
   m.DELETES,
   t.num_rows,
   round((m.inserts+m.updates+m.deletes)*100/NULLIF(t.num_rows,0),2) as EST_PCT_MODIFIED,
   TIMESTAMP AS "LAST_CHANGE"
   FROM  DBA_TAB_MODIFICATIONS m,
   dba_tables t
WHERE
m.table_owner=t.owner
and m.table_name=t.table_name
and TO_CHAR(TIMESTAMP,'DD/MM/YYYY') = TO_CHAR(sysdate,'DD/MM/YYYY') 
and TABLE_OWNER='OZF'
ORDER BY LAST_CHANGE DESC;




--#############-------TEST SYMULATION------------       
create table OZF_OBJECT_FUND_SUMMARY_TEST as (select * from OZF_OBJECT_FUND_SUMMARY);
create synonym OZF_OBJECT_FUND_SUMMARY_TEST for OZF.OZF_OBJECT_FUND_SUMMARY_TEST_H;
select count(*) from OZF_OBJECT_FUND_SUMMARY_TEST;

--1
--drop table OZF_OBJECT_FUND_SUMMARY_TEST_H;
create table OZF.OZF_OBJECT_FUND_SUMMARY_TEST_H as select * from OZF_OBJECT_FUND_SUMMARY_TEST where rownum<1;
create synonym OZF_OBJECT_FUND_SUMMARY_TEST_H for OZF.OZF_OBJECT_FUND_SUMMARY_TEST_H;
alter table OZF.OZF_OBJECT_FUND_SUMMARY_TEST nologging;
alter table OZF.OZF_OBJECT_FUND_SUMMARY_TEST_H nologging;
select count(*) from OZF_OBJECT_FUND_SUMMARY_TEST_H;
--2
select
    insert /*+ append parallel (xyzhold,12) */ 
        into OZF_OBJECT_FUND_SUMMARY_TEST_H -- xyzhold (f1, f2, f3,f4,f5,f6,f7,f8,f9,f10,f11,f12,f13,f14,f15,f16,f17,f18,f19,f20,f21,f22,f23,f24,f25,f26,f27,f28,f29,f30,f31,f32,f33,f34,f35,f36,f37,f37,f38,f39,f40,f41,f42,f43,f44,f45,f46,f47,f48,f49) 
            select /*+ parallel (x,12) */ * from OZF_OBJECT_FUND_SUMMARY_TEST;
from OZF_OBJECT_FUND_SUMMARY_TEST;
--commit;

select * from OZF_OBJECT_FUND_SUMMARY_TEST_H;
select count(*) from OZF_OBJECT_FUND_SUMMARY_TEST_H;
--8678

--Check Statistics and -- Get Last DML on table --
Exec DBMS_STATS.FLUSH_DATABASE_MONITORING_INFO;

select m.TABLE_OWNER,
 m.TABLE_NAME,
 m.INSERTS,
 m.UPDATES,
 m.DELETES,
 m.TRUNCATED,
 to_char(m.TIMESTAMP,'yyyy/mm/dd hh24:mi:ss') as LAST_MODIFIED, 
 round((m.inserts+m.updates+m.deletes)*100/NULLIF(t.num_rows,0),2) as EST_PCT_MODIFIED,
 t.num_rows as last_known_rows_number,
 to_char(t.last_analyzed,'yyyy/mm/dd hh24:mi:ss') as last_analyszed
From dba_tab_modifications m,
 dba_tables t
where m.table_owner=t.owner
and m.table_name=t.table_name
and table_owner in ('OZF')
and m.TABLE_NAME ='OZF_OBJECT_FUND_SUMMARY_TEST_H'
--and table_owner not in ('SYS','SYSTEM')
--and ((m.inserts+m.updates+m.deletes)*100/NULLIF(t.num_rows,0) > 10 or t.last_analyzed is null)
--order by timestamp desc;
order by 10 desc;

--Check dba_tab_statistics for STALE stats
select table_name, stale_stats, last_analyzed 
from dba_tab_statistics 
where stale_stats='YES'
and table_name='OZF_OBJECT_FUND_SUMMARY_TEST_H';
--
select * from DBA_TAB_COL_STATISTICS where owner ='OZF' and table_name='OZF_OBJECT_FUND_SUMMARY_TEST_H';

exec dbms_stats.gather_schema_stats('OZF', options=>'LIST STALE');

--gather statistics 
EXEC DBMS_STATS.gather_table_stats('OZF', 'OZF_OBJECT_FUND_SUMMARY_TEST_H');
--quand tu rouleras les statistiques pour OZF, fait le de la facons suivante :
EXEC FND_STATS.GATHER_SCHEMA_STATS('OZF',DBMS_STATS.AUTO_SAMPLE_SIZE); 
--Gather only table Statistic:
EXEC DBMS_STATS.gather_table_stats('OZF', 'OZF_OBJECT_FUND_SUMMARY_TEST_H');

-- Check last_analyszed
select
owner
,table_name
,to_char(last_analyzed,'yyyy/mm/dd hh24:mi:ss') as last_analyszed
From dba_Tables
where table_name = 'OZF_OBJECT_FUND_SUMMARY_TEST_H'
;

-- symulate UPDATEs--
select ATTRIBUTE2 from OZF_OBJECT_FUND_SUMMARY_TEST_H;
select count(*) from OZF_OBJECT_FUND_SUMMARY_TEST_H;
update OZF_OBJECT_FUND_SUMMARY_TEST_H set ATTRIBUTE2 = '-updated'where rownum <=867; --ATTRIBUTE2 || 
--rollback;
commit;



-- search select in Active users 
   SELECT   b.status, substr(a.spid,1,9) pid,    substr(b.sid,1,5) sid,    substr(b.serial#,1,5) ser#,    substr(b.machine,1,6) box,    substr(b.username,1,10) username, b.server, b.program,
    substr(b.osuser,1,8) os_user,    substr(b.program,1,30) program,    s.sql_id,    b.SQL_EXEC_START,    b.MACHINE, b.CLIENT_IDENTIFIER,   SQL_FULLTEXT
    FROM    v$session b,    v$process a,    V$SQLAREA s 
    WHERE 
    b.paddr = a.addr    AND b.sql_hash_value = s.hash_value    AND    type = 'USER'   -- AND   b.username in ('APPS')
    -- and OSUSER LIKE '%wls%'
  -- and b.program like '%adfuser%'
  and SQL_TEXT LIKE '%OZF_OBJECT_FUND_SUMMARY_TEST_H%'
--  and s.sql_id = '0fjtyz6kf7p6y'
    ORDER BY    spid;
    
    
--parallel query, with nologging 

alter table OZF.OZF_FUNDS_UTILIZED_ALL_B nologging; --logging
select OWNER,table_name,logging, NUM_ROWS  from dba_tables where TABLE_NAME --like ('OZF_CLAIM%') order by 2;
in ('OZF_FUNDS_UTILIZED_ALL_B', 'OZF_CLAIMS_ALL','OZF_CLAIM_LINES_ALL','OZF_CLAIM_LINES_UTIL_ALL');

UPDATE /*+ PARALLEL(OZF_FUNDS_UTILIZED_ALL_B) */ OZF_FUNDS_UTILIZED_ALL_B t
    SET ATTRIBUTE2 = (SELECT ATTRIBUTE2
                FROM OZF_FUNDS_UTILIZED_ALL_B t2
                WHERE t.UTILIZATION_ID = t2.UTILIZATION_ID
               ) 
   WHERE  rownum <=87000;

alter table OZF.OZF_CLAIM_LINES_UTIL_ALL nologging; --logging
select * from OZF_CLAIM_LINES_UTIL_ALL;
select OWNER,table_name,logging, NUM_ROWS  from dba_tables where TABLE_NAME --like ('OZF_CLAIM%') order by 2;
in ('OZF_FUNDS_UTILIZED_ALL_B', 'OZF_CLAIMS_ALL','OZF_CLAIM_LINES_ALL','OZF_CLAIM_LINES_UTIL_ALL');

UPDATE /*+ PARALLEL(OZF_CLAIM_LINES_UTIL_ALL) */ OZF_CLAIM_LINES_UTIL_ALL t
    SET QUANTITY = (SELECT QUANTITY
                FROM OZF_CLAIM_LINES_UTIL_ALL t2
                WHERE t.CLAIM_LINE_UTIL_ID = t2.CLAIM_LINE_UTIL_ID
               ) 
   WHERE  rownum <=4000000;
   rollback;
--commit;
select * from OZF_CLAIM_LINES_ALL;
alter table OZF.OZF_CLAIM_LINES_ALL logging; --logging

UPDATE /*+ PARALLEL(OZF_CLAIM_LINES_ALL) */ OZF_CLAIM_LINES_ALL t
    SET QUANTITY = (SELECT QUANTITY
                FROM OZF_CLAIM_LINES_ALL t2
                WHERE t.CLAIM_LINE_ID = t2.CLAIM_LINE_ID
               ) 
   WHERE  rownum <=1010557;
   rollback;
   
--commit;
select * from OZF_CLAIMS_ALL;
alter table OZF.OZF_CLAIMS_ALL logging; --logging

UPDATE /*+ PARALLEL(OZF_CLAIMS_ALL) */ OZF_CLAIMS_ALL t
    SET ATTRIBUTE2 = (SELECT ATTRIBUTE2
                FROM OZF_CLAIMS_ALL t2
                WHERE t.CLAIM_ID = t2.CLAIM_ID
               ) 
   WHERE  rownum <=40174;
   rollback;
   
--commit;
alter table OZF.OZF_CLAIM_LINES_UTIL_ALL logging; --logging

exec dbms_stats.set_table_prefs('OZF', 'OZF_FUNDS_UTILIZED_ALL_B','STALE_PERCENT', '0.1'); 
select * from DBA_TAB_STAT_PREFS;
exec DBMS_STATS.FLUSH_DATABASE_MONITORING_INFO;
select dbms_stats.get_prefs('STALE_PERCENT','OZF','OZF_FUNDS_UTILIZED_ALL_B') from dual;

--Check dba_tab_statistics
select table_name, stale_stats, last_analyzed,d.*
from dba_tab_statistics d
where --stale_stats='YES' and 
table_name in ('OZF_FUNDS_UTILIZED_ALL_B', 'OZF_CLAIMS_ALL','OZF_CLAIM_LINES_ALL','OZF_CLAIM_LINES_UTIL_ALL');

SELECT TABLE_NAME FROM DBA_TAB_STATISTICS WHERE STATTYPE_LOCKED = 'ALL'; --check locked Statistics
--==============================================================  
  --Check dba_tab_statistics for STALE TABLES stats
--==============================================================
select table_name, stale_stats,
to_char(last_analyzed,'yyyy/mm/dd hh24:mi:ss') as last_analyszed
,STATTYPE_LOCKED, d.*
from dba_tab_statistics d --dba_ind_statistics d
where stale_stats='YES'
and owner not in ('SYS','SYSTEM','MDSYS','ORDSYS','OUTLN','CTXSYS','DBSNMP','GSMADMIN_INTERNAL','MGDSYS','ORDDATA','XDB')
--and d.last_analyzed > '07-FEB-19'
--and table_name in ('OZF_FUNDS_UTILIZED_ALL_B','MRP_AD_BOMS')
and owner='OZF'
--and owner in (select distinct(upper(oracle_username)) owner from fnd_oracle_userid d, fnd_product_installations f where d.oracle_id = f.oracle_id)
/*and owner in -- All Modules EBS
            (SELECT --a.application_name,
            a.product_code -- ,DECODE (b.status, 'I', 'Installed', 'S', 'Shared', 'N/A') status,patch_level
            FROM apps.fnd_application_vl a,
            apps.fnd_product_installations b
            WHERE a.application_id = b.application_id --and b.status='I'
            )
*/
;

--==============================================================  
  --Check dba_ind_statistics for STALE INDEXES stats
--==============================================================
select table_name, stale_stats,
to_char(last_analyzed,'yyyy/mm/dd hh24:mi:ss') as last_analyszed
,STATTYPE_LOCKED, d.*
from dba_ind_statistics d
where stale_stats='YES'
and owner not in ('SYS','SYSTEM','MDSYS','ORDSYS','OUTLN','CTXSYS','DBSNMP','GSMADMIN_INTERNAL','MGDSYS','ORDDATA','XDB')
--and d.last_analyzed > '07-FEB-19'
--and table_name in ('OZF_FUNDS_UTILIZED_ALL_B','MRP_AD_BOMS')
and owner='OZF'
--and owner in (select distinct(upper(oracle_username)) owner from fnd_oracle_userid d, fnd_product_installations f where d.oracle_id = f.oracle_id)
/*and owner in -- All Modules EBS
            (SELECT --a.application_name,
            a.product_code -- ,DECODE (b.status, 'I', 'Installed', 'S', 'Shared', 'N/A') status,patch_level
            FROM apps.fnd_application_vl a,
            apps.fnd_product_installations b
            WHERE a.application_id = b.application_id --and b.status='I'
            )
*/
;


EXEC FND_STATS.GATHER_TABLE_STATS (ownname => 'OZF',tabname => 'OZF_FUNDS_UTILIZED_ALL_B');              
       
select table_name, stale_stats, last_analyzed,STATTYPE_LOCKED, d.*
from dba_tab_statistics d
where --stale_stats='YES' and
table_name in ('OZF_FUNDS_UTILIZED_ALL_B', 'OZF_CLAIMS_ALL','OZF_CLAIM_LINES_ALL','OZF_CLAIM_LINES_UTIL_ALL')
--and owner='OZF'
and owner in -- All Modules EBS
            (SELECT --a.application_name,
            a.product_code -- ,DECODE (b.status, 'I', 'Installed', 'S', 'Shared', 'N/A') status,patch_level
            FROM apps.fnd_application_vl a,
            apps.fnd_product_installations b
            WHERE a.application_id = b.application_id --and b.status='I'
            );

-- List Modules
SELECT --a.application_name,
a.product_code -- ,DECODE (b.status, 'I', 'Installed', 'S', 'Shared', 'N/A') status,patch_level
FROM apps.fnd_application_vl a,
apps.fnd_product_installations b
WHERE a.application_id = b.application_id --and b.status='I'
order by product_code asc;

--070001016C52EFB0	1117990038	87903319	e:XXEBS:cp:xxebs/XXONT0720-CRP	4tqnw651a6b4q	291237980	1887453	156576093		VALID	INSERT INTO XXONT0720_FUNDS_UTL_TMP (GL_DATE, CUST_ACCOUNT_ID, OBJECT_ID, OBJECT_TYPE, UTILIZATION_ID, UTILIZATION_TYPE, YR, MON, PRODUCT_ID, CURRENCY, ORDER_LINE_ID, ADJUSTMENT_DATE, PRICE_ADJUSTMENT_ID, EARNED_AMT, SHIP_TO_SITE_USE_ID, BILL_TO_SITE_USE_ID, FUND_ID, PLAN_ID, PLAN_TYPE, GL_POSTED_FLAG) SELECT OFU.GL_DATE, OFU.CUST_ACCOUNT_ID, OFU.OBJECT_ID, OFU.OBJECT_TYPE, OFU.UTILIZATION_ID, OFU.UTILIZATION_TYPE, TO_CHAR(OFU.GL_DATE, 'YYYY') YR, DECODE(:B4 , 'FRC',TO_CHAR(OFU.GL_DATE, 'MON', 'NLS_DATE_LANGUAGE = French'),TO_CHAR(OFU.GL_DATE, 'MON')) MON, OFU.PRODUCT_ID, DECODE(NVL(OFU.ORDER_LINE_ID, -999), -999, NULL, OFU.CURRENCY_CODE) CURRENCY, OFU.ORDER_LINE_ID, OFU.ADJUSTMENT_DATE, OFU.PRICE_ADJUSTMENT_ID, OFU.ACCTD_AMOUNT EARNED_AMT, OFU.SHIP_TO_SITE_USE_ID, OFU.BILL_TO_SITE_USE_ID, OFU.FUND_ID, OFU.PLAN_ID, OFU.PLAN_TYPE, OFU.GL_POSTED_FLAG FROM OZF_FUNDS_UTILIZED_ALL_B OFU WHERE OFU.GL_POSTED_FLAG = 'Y' AND OFU.PRODUCT_LEVEL_TYPE = 'PRODUCT' AND OFU.ACCTD_AMOUNT <> 0 AND OFU.ORG_ID = NVL(:B3 , OFU.ORG_ID) AND EXISTS ( SELECT 1 FROM HZ_CUST_ACCOUNTS HCA INNER JOIN HZ_CUST_ACCT_SITES HCAS ON ( HCA.CUST_ACCOUNT_ID = HCAS.CUST_ACCOUNT_ID ) INNER JOIN HZ_CUST_SITE_USES HCSU ON ( HCAS.CUST_ACCT_SITE_ID = HCSU.CUST_ACCT_SITE_ID ) WHERE HCA.CUST_ACCOUNT_ID = NVL( :B5 , HCA.CUST_ACCOUNT_ID ) AND ( ( HCSU.SITE_USE_CODE = 'SHIP_TO' AND HCSU.SITE_USE_ID = OFU.SHIP_TO_SITE_USE_ID ) OR ( HCSU.SITE_USE_CODE = 'BILL_TO' AND HCSU.SITE_USE_ID = OFU.BILL_TO_SITE_USE_ID ) ) ) AND NVL(TRUNC(OFU.GL_DATE),TRUNC(SYSDATE)) BETWEEN NVL(:B2 , NVL(TRUNC(OFU.GL_DATE),TRUNC(SYSDATE))) AND NVL(:B1 , NVL(TRUNC(OFU.GL_DATE),TRUNC(SYSDATE))) AND EXISTS (SELECT 1 FROM XXONT0720_OFFERS_TMP OFFERS WHERE OFFERS.BUDGET_SOURCE_ID = OFU.FUND_ID AND OFFERS.QP_LIST_HEADER_ID = OFU.PLAN_ID ) 
select ADDRESS, HASH_VALUE, PLAN_HASH_VALUE, MODULE, ELAPSED_TIME, DISK_READS, USER_IO_WAIT_TIME, sql_profile, OBJECT_STATUS, SQL_FULLTEXT  from v$sql where sql_id = '5f93f78fkyz6a' order by ELAPSED_TIME desc;
--cz8n1jq30sjjv  b5ds90337ynyq
exec DBMS_SHARED_POOL.PURGE ('070001016C52EFB0, 1117990038', 'C');
--070001016C52EFB0	1117990038	87903319	e:XXEBS:cp:xxebs/XXONT0720-CRP	4tqnw651a6b4q	19802495	29377	19266573		VALID	INSERT INTO XXONT0720_FUNDS_UTL_TMP (GL_DATE, CUST_ACCOUNT_ID, OBJECT_ID, OBJECT_TYPE, UTILIZATION_ID, UTILIZATION_TYPE, YR, MON, PRODUCT_ID, CURRENCY, ORDER_LINE_ID, ADJUSTMENT_DATE, PRICE_ADJUSTMENT_ID, EARNED_AMT, SHIP_TO_SITE_USE_ID, BILL_TO_SITE_USE_ID, FUND_ID, PLAN_ID, PLAN_TYPE, GL_POSTED_FLAG) SELECT OFU.GL_DATE, OFU.CUST_ACCOUNT_ID, OFU.OBJECT_ID, OFU.OBJECT_TYPE, OFU.UTILIZATION_ID, OFU.UTILIZATION_TYPE, TO_CHAR(OFU.GL_DATE, 'YYYY') YR, DECODE(:B4 , 'FRC',TO_CHAR(OFU.GL_DATE, 'MON', 'NLS_DATE_LANGUAGE = French'),TO_CHAR(OFU.GL_DATE, 'MON')) MON, OFU.PRODUCT_ID, DECODE(NVL(OFU.ORDER_LINE_ID, -999), -999, NULL, OFU.CURRENCY_CODE) CURRENCY, OFU.ORDER_LINE_ID, OFU.ADJUSTMENT_DATE, OFU.PRICE_ADJUSTMENT_ID, OFU.ACCTD_AMOUNT EARNED_AMT, OFU.SHIP_TO_SITE_USE_ID, OFU.BILL_TO_SITE_USE_ID, OFU.FUND_ID, OFU.PLAN_ID, OFU.PLAN_TYPE, OFU.GL_POSTED_FLAG FROM OZF_FUNDS_UTILIZED_ALL_B OFU WHERE OFU.GL_POSTED_FLAG = 'Y' AND OFU.PRODUCT_LEVEL_TYPE = 'PRODUCT' AND OFU.ACCTD_AMOUNT <> 0 AND OFU.ORG_ID = NVL(:B3 , OFU.ORG_ID) AND EXISTS ( SELECT 1 FROM HZ_CUST_ACCOUNTS HCA INNER JOIN HZ_CUST_ACCT_SITES HCAS ON ( HCA.CUST_ACCOUNT_ID = HCAS.CUST_ACCOUNT_ID ) INNER JOIN HZ_CUST_SITE_USES HCSU ON ( HCAS.CUST_ACCT_SITE_ID = HCSU.CUST_ACCT_SITE_ID ) WHERE HCA.CUST_ACCOUNT_ID = NVL( :B5 , HCA.CUST_ACCOUNT_ID ) AND ( ( HCSU.SITE_USE_CODE = 'SHIP_TO' AND HCSU.SITE_USE_ID = OFU.SHIP_TO_SITE_USE_ID ) OR ( HCSU.SITE_USE_CODE = 'BILL_TO' AND HCSU.SITE_USE_ID = OFU.BILL_TO_SITE_USE_ID ) ) ) AND NVL(TRUNC(OFU.GL_DATE),TRUNC(SYSDATE)) BETWEEN NVL(:B2 , NVL(TRUNC(OFU.GL_DATE),TRUNC(SYSDATE))) AND NVL(:B1 , NVL(TRUNC(OFU.GL_DATE),TRUNC(SYSDATE))) AND EXISTS (SELECT 1 FROM XXONT0720_OFFERS_TMP OFFERS WHERE OFFERS.BUDGET_SOURCE_ID = OFU.FUND_ID AND OFFERS.QP_LIST_HEADER_ID = OFU.PLAN_ID )        

--quand tu rouleras les statistiques pour OZF, fait le de la facons suivante  FND_STATS :
-- EXEC FND_STATS.GATHER_SCHEMA_STATS('OZF',DBMS_STATS.AUTO_SAMPLE_SIZE); 
-- FND_STAT ALL SCHEMA LEVEL
EXEC FND_STATS.GATHER_SCHEMA_STATS('AGR_APPS',DBMS_STATS.AUTO_SAMPLE_SIZE);

    fnd_stats.gather_schema_stats(schemaname => 'JTF',
                                estimate_percent => 50,
                                internal_flag => 'NOBACKUP',
                                hmode => 'LASTRUN',
                                options => 'GATHER',
                                invalidate => 'Y');

--Gather FND_STATS SCHEMA LEVEL
--FND_STATS
fnd_stats.gather_schema_stats(schemaname => 'ALL',
                                estimate_percent => 50,
                                internal_flag => 'NOBACKUP',
                                hmode => 'LASTRUN',
                                options => 'GATHER',
                                invalidate => 'Y');

-- Gather FND_STATS TABLE Level : https://docs.oracle.com/cd/E18727_01/doc.121/e12893/T174296T174306.htm
EXEC FND_STATS.GATHER_TABLE_STATS (ownname => 'AR',tabname => 'DR$HZ_CLASS_CODE_DENORM_T1$I');
                                
FND_STATS.GATHER_TABLE_STATS (
    ownname 		VARCHAR2,
    tabname 		VARCHAR2,
    percent 		NUMBER DEFAULT NULL,
    degree 		NUMBER DEFAULT NULL,
    partname 		VARCHAR2 DEFAULT NULL,
    backup_flag 	VARCHAR2 DEFAULT NULL,
    cascade 		BOOLEAN DEFAULT TRUE,
    granularity VARCHAR2 DEFAULT �DEFAULT�,
    hmode 		VARCHAR2 DEFAULT 'LASTRUN',
    invalidate VARCHAR2 DEFAULT 'Y'
);
								
dbms_stats
--Gather DBMS_STATS only table Statistic:
EXEC DBMS_STATS.gather_table_stats('JTF', 'DR$HZ_CLASS_CODE_DENORM_T1$I');
-- Gather DBMS_STATS SCHEMA 
 DBMS_STATS.GATHER_SCHEMA_STATS(
           ownname          => '''|| schema_stat.OWNNAME ||''',
           estimate_percent => '''|| schema_stat.ESTIMATE_PERCENT ||''',
           method_opt       => '''|| schema_stat.METHOD_OPT ||''',
           cascade          => '|| schema_stat.CASCADE ||',
           degree           => '|| schema_stat.DEGREE ||');
       END;

-- GATHER_DICTIONARY_STATS The following command can be used to gather statistics for all system schemas:
--EXECUTE DBMS_STATS.GATHER_DICTIONARY_STATS( estimate_percent => DBMS_STATS.AUTO_SAMPLE_SIZE options => 'GATHER AUTO' );  -- Run as SYS

select dbms_stats.get_prefs('STALE_PERCENT',null,'OZF_FUNDS_UTILIZED_ALL_B') from dual;

select partition_name,
       subpartition_name,
       stale_stats               /* YES or NO */
from   dba_tab_statistics
where  1=1
and partition_name is not NULL
--and table_name = 'RG_REPORT_REQUEST_LOBS'
;

select  *
from    dba_tab_modifications
where   table_name = 'RG_REPORT_REQUEST_LOBS';

-- CHECK particular Table modifications:
select m.TABLE_OWNER,
 m.TABLE_NAME as NAME, 
 m.INSERTS,
 m.UPDATES,
 m.DELETES,
 m.TRUNCATED,
 to_char(m.TIMESTAMP,'yyyy/mm/dd hh24:mi:ss') as LAST_MODIFIED, 
 round((m.inserts+m.updates+m.deletes)*100/NULLIF(t.num_rows,0),2) as EST_PCT_MODIFIED,
 t.num_rows as last_known_rows_number,
 to_char(t.last_analyzed,'yyyy/mm/dd hh24:mi:ss') as last_analyszed
From dba_tab_modifications m,
 dba_tables t 
where m.table_owner=t.owner
and m.table_name=t.table_name
and m.table_owner in ('ONT')
--and m.table_name in ('OZF_FUNDS_UTILIZED_ALL_B', 'OZF_CLAIMS_ALL','OZF_CLAIM_LINES_ALL','OZF_CLAIM_LINES_UTIL_ALL')
--and m.table_name= 'OZF_FUNDS_UTILIZED_ALL_B'
--and m.table_owner not in ('SYS','SYSTEM')
and ((m.inserts+m.updates+m.deletes)*100/NULLIF(t.num_rows,0) >= 10 or t.last_analyzed is null)
;

-- CHECK All Modules Modifications:
select m.TABLE_OWNER,
 m.TABLE_NAME as NAME, 
 m.INSERTS,
 m.UPDATES,
 m.DELETES,
 m.TRUNCATED,
 to_char(m.TIMESTAMP,'yyyy/mm/dd hh24:mi:ss') as LAST_MODIFIED, 
 round((m.inserts+m.updates+m.deletes)*100/NULLIF(t.num_rows,0),2) as EST_PCT_MODIFIED,
 t.num_rows as last_known_rows_number,
 to_char(t.last_analyzed,'yyyy/mm/dd hh24:mi:ss') as last_analyszed
From dba_tab_modifications m,
 dba_tables t 
where m.table_owner=t.owner
and m.table_name=t.table_name
--and m.table_owner in ('OZF')
--and m.table_name= 'OZF_FUNDS_UTILIZED_ALL_B'
--and m.table_owner not in ('SYS','SYSTEM')
and m.table_owner in -- All Modules EBS
            (SELECT --a.application_name,
            a.product_code -- ,DECODE (b.status, 'I', 'Installed', 'S', 'Shared', 'N/A') status,patch_level
            FROM apps.fnd_application_vl a,
            apps.fnd_product_installations b
            WHERE a.application_id = b.application_id --and b.status='I'
            )
and ((m.inserts+m.updates+m.deletes)*100/NULLIF(t.num_rows,0) >= 10 or t.last_analyzed is null)
order by 8 desc
;


