select APP_SHORT_NAME, max(PATCH_LEVEL) from AD_PATCH_DRIVER_MINIPKS GROUP BY APP_SHORT_NAME;

-------------------------------------------------------------------------------
-- Query to find all APPLICATION (module) information
-------------------------------------------------------------------------------
SELECT fa.application_id           "Application ID",
       fat.application_name        "Application Name",
       fa.application_short_name   "Application Short Name",
       fa.basepath                 "Basepath"
  FROM fnd_application     fa,
       fnd_application_tl  fat
 WHERE fa.application_id = fat.application_id
   AND fat.language      = USERENV('LANG')
   -- AND fat.application_name = 'Payables'  -- <change it>
 ORDER BY fat.application_name;


-------------------------------------------------------------------------------
-- Query to find all APPLICATION (module) information with CODELEVEL PATCHLEVEL
------------------------------------------------------------------------------- 
 SELECT fav.application_id,
fav.application_short_name, 
fav.application_name,
fav.basepath,
fav.creation_date,
--fpi.status,
DECODE (fpi.status, 'I', 'Installed', 'S', 'Shared', 'N/A') status,
fpi.patch_level,
fpi.product_version,
fpi.TABLESPACE,
fpi.index_tablespace,
fpi.temporary_tablespace
FROM fnd_application_vl fav,
fnd_product_installations fpi
WHERE fav.application_id = fpi.application_id(+) 
and fpi.status in ('I','S')
ORDER BY creation_date DESC;


select '('||language||') - '||n.user_profile_option_name Profilename, 
 decode(v.level_id,10001,'Site',10002,'Application',10003,'Responsibility',10004,'User',10005,'Server',10006,'Organization',10007,'ServResp', 'Undefined') LEVEL_SET, 
 decode(to_char(v.level_id),'10001', ' ','10002', app.application_short_name,'10003', rsp.responsibility_key,'10004', usr.user_name,'10005', svr.node_name,'10006', org.name,'10007', 
 (select n.node_name from fnd_nodes n where n.node_id=level_value2)||'/'||(decode(to_char(v.level_value), -1,'Default', 
 (select responsibility_key from fnd_responsibility where responsibility_id=level_value))), v.level_id) "User-ID", 
 (select timezone_code from fnd_timezones_b where upgrade_tz_id=profile_option_value) "Timezone" 
 from fnd_profile_options p, fnd_profile_option_values v, fnd_profile_options_tl n, fnd_user usr, fnd_application app, 
 fnd_responsibility rsp, fnd_nodes svr, hr_operating_units org 
 where v.level_id = '10004' 
 and p.profile_option_id = v.profile_option_id (+) 
 and p.profile_option_name = n.profile_option_name 
 and p.profile_option_name = 'ICX_NUMERIC_CHARACTERS' 
 and usr.user_id (+) = v.level_value 
 and rsp.application_id (+) = v.level_value_application_id 
 and rsp.responsibility_id (+) = v.level_value 
 and app.application_id (+) = v.level_value 
 and v.profile_option_value <> '1'
 and v.profile_option_value <> (select v.profile_option_value
 from fnd_profile_options p, fnd_profile_option_values v
 where v.level_id = '10001' 
 and p.profile_option_id = v.profile_option_id (+)
 and p.profile_option_name = 'ICX_NUMERIC_CHARACTERS')
 and svr.node_id (+) = v.level_value 
 and org.organization_id (+) = v.level_value 
 order by name, v.level_id;
