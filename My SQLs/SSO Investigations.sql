select count(*) from APPS.FND_USER_PREFERENCES
 where MODULE_NAME='LDAP_SYNCH';
 
select z.tablespace_name tspace_name, 
(select sum(b.bytes)/(1024*1024) from dba_free_space b where b.tablespace_name = z.tablespace_name) MB_Free,
(select sum(a.bytes)/(1024*1024) from dba_data_files a where a.tablespace_name = z.tablespace_name) MB_Total,
((select sum(b.bytes)/(1024*1024) from dba_free_space b where b.tablespace_name = z.tablespace_name)/(select sum(a.bytes)/(1024*1024) from dba_data_files a where a.tablespace_name = z.tablespace_name)*100) Pcent_Free
from dba_tablespaces z
order by z.tablespace_name;

select adop_session_id, apply_status, cleanup_status
from AD_ADOP_SESSIONS
where adop_session_id = (select max(adop_session_id) from AD_ADOP_SESSIONS);

select text from dba_source where name = 'FND_SSO_REGISTRATION' and line < 3;

SELECT * FROM AD_BUGS WHERE BUG_NUMBER IN ('24008856','21229697','24691100');

--============== http://www.oraworld.co.uk/ora20001-not-able-to-create-users-in-oracle-app-unable-to-call-fnd_ldap_wrapper-create_user-or-update_user/

SELECT * FROM fnd_user_preferences WHERE
user_name='#INTERNAL' AND module_name='OID_CONF';

--==============https://myoraclemadeeasy.wordpress.com/2015/06/03/users-not-syncing-from-oid-to-ebs-oid_conf-missing/
select module_name, count(*) from apps.fnd_user_preferences where user_name='#INTERNAL' group by module_name;

/*SQL> execute fnd_oid_plug.setPlugin;
BEGIN fnd_oid_plug.setPlugin; END;

*
ERROR at line 1:
ORA-31203: DBMS_LDAP: PL/SQL - Init Failed.
ORA-06512: at "APPS.FND_OID_PLUG", line 916
ORA-06512: at line 1*/

Select * from dba_source where name= 'FND_OID_PLUG' and line between 875 and 930;
--==============
--1. Determine the current AppsDN username and password stored in EBS
select fnd_preference.get('#INTERNAL', 'LDAP_SYNCH','USERNAME') Apps_Instance_OID_Account from dual;
select fnd_preference.eget('#INTERNAL', 'LDAP_SYNCH','EPWD','LDAP_PWD') Apps_Password from dual;

-- Marc Check Edition
SELECT property_value
FROM   database_properties
WHERE  property_name = 'DEFAULT_EDITION';

SELECT * FROM dba_editions;

SELECT SYS_CONTEXT('USERENV', 'SESSION_EDITION_NAME') AS edition FROM dual;

ALTER SESSION SET EDITION = ora$base; 


--------- Check the Profiles SSO --------------------
SELECT * FROM fnd_user_preferences WHERE
user_name='#INTERNAL' AND module_name='OID_CONF';

UPDATE FND_PROFILE_OPTION_VALUES FPOV					
SET FPOV.PROFILE_OPTION_VALUE = (select name from v$database) || ' ('||nvl('&CLONE_DATE',to_char(sysdate,'YYYY-MM-DD'))||')'					
WHERE EXISTS (SELECT NULL FROM FND_PROFILE_OPTIONS FPO 					
              WHERE FPOV.PROFILE_OPTION_ID = FPO.PROFILE_OPTION_ID					
              AND FPO.PROFILE_OPTION_NAME = 'APPS_SSO_LDAP_SYNC');	
              
SELECT a.*			
  FROM apps.fnd_profile_option_values a, apps.fnd_profile_options b					
  where b.profile_option_name = 'APPS_SSO_LDAP_SYNC'					
  and a.application_id = b.application_id					
  and a.profile_option_id = b.profile_option_id
  and a.LEVEL_ID='10001';					
-- APPS_SSO_LDAP_SYNC

SELECT NULL FROM FND_PROFILE_OPTIONS FPO 					
              WHERE FPOV.PROFILE_OPTION_ID = FPO.PROFILE_OPTION_ID					
              AND FPO.PROFILE_OPTION_NAME = 'APPS_SSO_LDAP_SYNC';

DECLARE
stat BOOLEAN;
BEGIN
stat := FND_PROFILE.SAVE('APPS_SSO_LDAP_SYNC','Y','SITE');
IF stat THEN
dbms_output.put_line( 'Profile APPS_SSO_LDAP_SYNC updated with Enabled' );
ELSE
dbms_output.put_line( 'Profile APPS_SSO_LDAP_SYNC could NOT be updated with Enabled' );
commit;
END IF;
END;
/ 			
commit;
DECLARE	
stat BOOLEAN;	
BEGIN	
stat := FND_PROFILE.SAVE('APPS_SSO_OID_IDENTITY','Y','SITE');	
IF stat THEN	
dbms_output.put_line( 'Profile APPS_SSO_OID_IDENTITY updated with Enabled ' );	
ELSE	
dbms_output.put_line( 'Profile APPS_SSO_OID_IDENTITY could NOT be updated with Enabled' );	
commit;	
END IF;	
END;	
/ 	
	

DECLARE	
stat BOOLEAN;	
BEGIN	
stat := FND_PROFILE.SAVE('APPS_SSO_LINK_SAME_NAMES','Y','SITE');	
IF stat THEN	
dbms_output.put_line( 'Profile APPS_SSO_LINK_SAME_NAMES updated with Enabled' );	
ELSE	
dbms_output.put_line( 'Profile APPS_SSO_LINK_SAME_NAMES could NOT be updated with Enabled' );	
commit;	
END IF;	
END;	
/ 	
	

DECLARE	
stat BOOLEAN;	
begin	
stat := FND_PROFILE.SAVE('APPS_SSO', 'SSWA_SSO', 'SITE');	
IF stat THEN	
dbms_output.put_line( 'Profile APPS_SSO updated with SSWA_SSO' );	
ELSE	
dbms_output.put_line( 'Profile APPS_SSO could NOT be updated with SSWA_SSO' );	
commit;	
END IF;	
end;	
/ 	
	

DECLARE	
stat BOOLEAN;	
BEGIN	
stat := FND_PROFILE.SAVE('APPS_SSO_AUTO_LINK_USER','Y','SITE');	
IF stat THEN	
dbms_output.put_line( 'Profile APPS_SSO_AUTO_LINK_USER updated with Enabled' );	
ELSE	
dbms_output.put_line( 'Profile APPS_SSO_AUTO_LINK_USER could NOT be updated with Enabled' );	
commit;	
END IF;	
END;	
/ 	

commit;

select p.profile_option_name SHORT_NAME,
n.user_profile_option_name NAME,
decode(v.level_id,
10001, 'Site',
10002, 'Application',
10003, 'Responsibility',
10004, 'User',
10005, 'Server',
10006, 'Org',
10007, decode(to_char(v.level_value2), '-1', 'Responsibility',
decode(to_char(v.level_value), '-1', 'Server',
'Server+Resp')),
'UnDef') LEVEL_SET,
decode(to_char(v.level_id),
'10001', '',
'10002', app.application_short_name,
'10003', rsp.responsibility_key,
'10004', usr.user_name,
'10005', svr.node_name,
'10006', org.name,
'10007', decode(to_char(v.level_value2), '-1', rsp.responsibility_key,
decode(to_char(v.level_value), '-1',
(select node_name from fnd_nodes
where node_id = v.level_value2),
(select node_name from fnd_nodes
where node_id = v.level_value2)||'-'||rsp.responsibility_key)),
'UnDef') "CONTEXT",
v.profile_option_value VALUE,
p.ZD_EDITION_NAME
from fnd_profile_options p,
fnd_profile_option_values v,
fnd_profile_options_tl n,
fnd_user usr,
fnd_application app,
fnd_responsibility rsp,
fnd_nodes svr,
hr_operating_units org
where p.profile_option_id = v.profile_option_id (+)
and p.profile_option_name = n.profile_option_name
and upper(p.profile_option_name) 
in ( select profile_option_name
    from fnd_profile_options_tl 
    where 
    --upper(user_profile_option_name) like upper('%&user_profile_name%'))
    --upper(profile_option_name) like upper('%&Profile_SHORT_NAME%'))
    upper(profile_option_name) in ('APPS_SSO_LDAP_SYNC','APPS_SSO_AUTO_LINK_USER','APPS_SSO_OID_IDENTITY','APPS_SSO_LINK_SAME_NAMES','APPS_SSO')
    )
and usr.user_id (+) = v.level_value
and rsp.application_id (+) = v.level_value_application_id
and rsp.responsibility_id (+) = v.level_value
and app.application_id (+) = v.level_value
and svr.node_id (+) = v.level_value
and org.organization_id (+) = v.level_value
order by short_name, user_profile_option_name, level_id, level_set;
--$FND_TOP/patch/115/sql/fndssouu.sql 


select * from fnd_user where user_name = 'BEGOSSEL';
--19712
-- Application SSO LDAP Synchronization

--APPS_SSO_LDAP_SYNC
--APPS_SSO_AUTO_LINK_USER
--APPS_SSO_OID_IDENTITY
--APPS_SSO_LINK_SAME_NAMES
--APPS_SSO