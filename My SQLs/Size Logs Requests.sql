
select
--file_size,
lfile_size,
round(ofile_size/1024/1024/1024,1)||'G' Ofile_size,
logfile_name,
OUTFILE_NAME
from fnd_concurrent_requests fcr
,fnd_conc_req_outputs fcro
where  round(ofile_size/1024/1024/1024,1)>1
and fcr.request_id = fcro.concurrent_request_id (+)
--and fcr.request_id = '15316853' 
--order by OFILE_SIZE desc
--limit 5
;
--=======Size summary of Concurent Programs====
SELECT fcp.application_id,
  fcp.CONCURRENT_PROGRAM_NAME,fcpt.USER_CONCURRENT_PROGRAM_NAME,
  round(SUM(ofile_size)/1024/1024/1024,1) OUT_FILE_SIZE_GB,
  round(SUM(lfile_size)/1024/1024/1024,2) LOG_FILE_SIZE_GB,
  round(SUM(file_size)/1024/1024/1024,2) XML_PUB_FILE_SIZE_GB,
  COUNT(*)
FROM fnd_concurrent_requests fcr,
  fnd_concurrent_programs fcp,
  fnd_conc_req_outputs fcro,
  fnd_concurrent_programs_tl fcpt
WHERE fcr.program_application_id = fcp.application_id
AND fcr.concurrent_program_id    = fcp.CONCURRENT_PROGRAM_ID
AND fcpt.concurrent_program_id    = fcp.CONCURRENT_PROGRAM_ID
and fcpt.SOURCE_LANG='US'
and fcr.request_id(+) = fcro.concurrent_request_id
AND ofile_size                  IS NOT NULL
--and fcr.ACTUAL_COMPLETION_DATE < sysdate-45
--and fcp.CONCURRENT_PROGRAM_NAME like ('%XXR_GL_SLA_RPT%')
--and fcp.CONCURRENT_PROGRAM_NAME in ('XXRHR_HOURS_REC')
GROUP BY fcp.application_id, fcp.CONCURRENT_PROGRAM_NAME, fcpt.USER_CONCURRENT_PROGRAM_NAME
ORDER BY 4 desc;

--=======Biggest Size of Concurent Requests/Programs details files ====
SELECT fcp.application_id,
fcr.ACTUAL_COMPLETION_DATE,
  fcp.CONCURRENT_PROGRAM_NAME,fcpt.USER_CONCURRENT_PROGRAM_NAME,
  round((ofile_size)/1024/1024/1024,1) OUT_FILE_SIZE_GB,
  round((lfile_size)/1024/1024/1024,2) LOG_FILE_SIZE_GB,
  round((file_size)/1024/1024/1024,2) XML_PUB_FILE_SIZE_GB,
  OUTFILE_NAME,
  logfile_name,
  file_name xmlfile_name
FROM fnd_concurrent_requests fcr,
  fnd_concurrent_programs fcp,
  fnd_conc_req_outputs fcro,
  fnd_concurrent_programs_tl fcpt
WHERE fcr.program_application_id = fcp.application_id
AND fcr.concurrent_program_id    = fcp.CONCURRENT_PROGRAM_ID
AND fcpt.concurrent_program_id    = fcp.CONCURRENT_PROGRAM_ID
and fcpt.SOURCE_LANG='US'
and fcr.request_id = fcro.concurrent_request_id(+)
AND ofile_size                  IS NOT NULL
 AND ( ((ofile_size)/1024/1024/1024 >0.4) --output >1G
      OR ((lfile_size)/1024/1024/1024 >0.4) -- log > 1G
      OR ((file_size)/1024/1024/1024 >0.4) -- xml Pub File >1G
   )
--and fcr.ACTUAL_COMPLETION_DATE < sysdate-90
-- and fcr.request_id='15316853'
--and fcp.CONCURRENT_PROGRAM_NAME in ('XXRHR_HOURS_REC')
ORDER BY 4 desc;
