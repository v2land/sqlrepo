show parameter audit_trail  -- NONE
NAME        TYPE   VALUE 
----------- ------ ----- 
audit_trail string NONE 

show parameter audit_file_dest
NAME            TYPE   VALUE                                  
--------------- ------ -------------------------------------- 
audit_file_dest string /EBSTEST_ORA/ORA_DB/12.1.0/rdbms/audit 

alter system set AUDIT_TRAIL=db scope=spfile;
alter system set AUDIT_TRAIL=none scope=spfile;
--audit_file_dest='/EBSTEST_ORA/ORA_DB/12.1.0/rdbms/audit'  -- deja set/
audit create session whenever not successful;
noaudit all WHENEVER NOT SUCCESSFUL;

select 
   os_username,
   username,
   userhost,
   terminal,
   to_char(timestamp,'MM-DD-YYYY HH24:MI:SS')
from
   dba_audit_trail
   where os_username not in ('wlsteadf')
   order by 4 desc;
   
   
   
   select *
from dba_audit_session
where action_name = 'LOGON'
and returncode > 0
 and  os_username not in ('wlsteadf')
order by timestamp ;