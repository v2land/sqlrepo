SELECT fcp.CONCURRENT_PROGRAM_ID, fcp.REQUEST_SET_FLAG "SET",
 --fcr.request_id
-- parent_request_id "parent_request_id"
      -- DECODE(fcpt.user_concurrent_program_name 'Report Set'  'Report Set:' || fcr.description fcpt.user_concurrent_program_name) CONC_PROG_NAME
       --DECODE(fcr.description, NULL, fcpt.user_concurrent_program_name, fcr.description || ' ( ' || fcpt.user_concurrent_program_name|| ' ) ') program,
       fcpt.User_Concurrent_Program_Name,
       fcr.description,
       argument_text PARAMETERS,
       fu.user_name USER_NAME,
    fcrc.class_info,
        --to_char(fcr.requested_start_date 'DD-MON-YY HH24:MI:SS') requested_start_date
        to_char(fcr.requested_start_date, 'HH24:MI') start_time,
  'DLMMeJVS' DLMMeJVS,
 /*     round(((select actual_completion_date from fnd_concurrent_requests where request_id=fcr.parent_request_id) -
      (select requested_start_date from fnd_concurrent_requests where request_id=fcr.parent_request_id))* 24*60 0) as prior_duration */
       (SELECT
            round(AVG ( (F.ACTUAL_COMPLETION_DATE - F.ACTUAL_START_DATE))* 24*60, 0) "AVERAGE"
          --round((F.ACTUAL_COMPLETION_DATE - F.ACTUAL_START_DATE)*1440 0) "AVERAGE"
            FROM APPLSYS.FND_CONCURRENT_REQUESTS F,
                 APPLSYS.FND_CONCURRENT_PROGRAMS P
            WHERE     PHASE_CODE = 'C'
                 AND STATUS_CODE = 'C'
                 AND F.CONCURRENT_PROGRAM_ID = P.CONCURRENT_PROGRAM_ID
                 AND P.CONCURRENT_PROGRAM_ID=fcr.CONCURRENT_PROGRAM_ID
                 and F.ACTUAL_START_DATE between sysdate-&nr_of_days and sysdate
            GROUP BY P.CONCURRENT_PROGRAM_NAME
      ) AVG_Time
  FROM apps.fnd_concurrent_programs_tl fcpt,
       apps.fnd_concurrent_requests    fcr,
       apps.fnd_user                   fu,
       apps.fnd_conc_release_classes   fcrc,
       apps.fnd_concurrent_programs fcp
 WHERE fcp.concurrent_program_ID=fcpt.concurrent_program_ID
 and  fcp.application_id=fcpt.application_id
   and fcpt.application_id = fcr.program_application_id
   AND fcpt.concurrent_program_id = fcr.concurrent_program_id
   AND fcr.requested_by = fu.user_id
  AND fcr.phase_code in ('P','R') -- Pending (Scheduled)
   AND fcr.requested_start_date > SYSDATE
   AND fcpt.LANGUAGE = 'US'
   AND fcrc.release_class_id(+) = fcr.release_class_id
   AND fcrc.application_id(+) = fcr.release_class_app_id
   --and fcr.request_id = '37480159'
   --and  DECODE(fcpt.user_concurrent_program_name
   --           'Report Set'
   --           'Report Set:' || fcr.description
   --           fcpt.user_concurrent_program_name) LIKE '%%'
   order by  9 desc, requested_start_date desc;




/*SELECT rs.user_request_set_name "user_request_set_name",
         rs.request_set_name "Set   code",
         --rs.description "Description",
         rss.display_sequence Seq,
         cp.user_concurrent_program_name "Concurrent Program",
         ap.application_name "Application",
         e.executable_name "Executable",
         e.execution_file_name "Executable File",
         lv.meaning "Executable Type"
    FROM apps.fnd_request_sets_vl rs,
         apps.fnd_req_set_stages_form_v rss,
         applsys.fnd_request_set_programs rsp,
         apps.fnd_concurrent_programs_vl cp,
         apps.fnd_executables E,
         apps.fnd_lookup_values lv,
         apps.fnd_application_vl ap
   WHERE     rs.application_id = rss.set_application_id
         AND rs.request_set_id = rss.request_set_id
         AND rss.set_application_id = rsp.set_application_id
         AND rss.request_set_id = rsp.request_set_id
         AND rss.request_set_stage_id = rsp.request_set_stage_id
         AND rsp.program_application_id = cp.application_id
         AND rsp.concurrent_program_id = cp.concurrent_program_id
         AND cp.executable_id = e.executable_id
         AND cp.executable_application_id = e.application_id
         AND e.application_id = ap.application_id
         AND lv.lookup_type = 'CP_EXECUTION_METHOD_CODE'
         AND lv.lookup_code = e.execution_method_code
         --AND rs.user_request_set_name like 'XXAA Accounts Payable Request Set'
         AND lv.LANGUAGE = 'US'
ORDER BY rs.request_set_name*/
/*
select 
USER_REQUEST_SET_NAME  "user_request_set_name",
REQUEST_SET_NAME,
REQUEST_SET_ID,
APPLICATION_ID,
CONCURRENT_PROGRAM_ID,
DESCRIPTION
from fnd_request_sets_vl
order by 1 asc;
*/
-- SCHEDULED Request Sets and them request IDs
SELECT 
frst.user_request_set_name "user_request_set_name",
fct.user_concurrent_program_name,--fcr.*,
fcr.request_id Req_ID,
parent_request_id "parent_id",
fcr.status_code "S",
fcr.ARGUMENT_TEXT,
fu.user_name,
TO_CHAR((fcr.requested_start_date), 'DD-MM-YYYY HH24:MI:SS') requested_start_date,
NVL2( fcr.resubmit_interval, 'Periodically', NVL2(fcr.release_class_id, 'On specific days', 'Once')) AS schedule_type
, fcr.resubmit_interval every
, fcr.resubmit_interval_unit_code period
FROM fnd_concurrent_requests fcr,
fnd_concurrent_programs_tl fct,
fnd_user fu,
fnd_request_sets_tl frst 
WHERE fcr.concurrent_program_id = fct.concurrent_program_id
AND fcr.phase_code IN ('P')
AND fu.user_id=fcr.requested_by
AND frst.request_set_id = TO_NUMBER(fcr.argument2)
--AND user_concurrent_program_name = 'Report Set'
--and fcp.CONCURRENT_PROGRAM_NAME = 'XXAGR_CSO_MEMO_RELEVE_DETAILLE'
and fcr.CONCURRENT_PROGRAM_ID in(select fcp.CONCURRENT_PROGRAM_ID from fnd_concurrent_programs fcp where fcp.REQUEST_SET_FLAG = 'Y')
and fct.LANGUAGE=USERENV('Lang')
and frst.LANGUAGE=USERENV('Lang')
order by fcr.requested_start_date asc;

SELECT DISTINCT r.request_id
      , u.user_name requestor
      , u.description requested_by
      , CASE
           WHEN pt.user_concurrent_program_name like ('Report Set')
              THEN DECODE(
                  r.description
                   , NULL, pt.user_concurrent_program_name
                   ,    r.description
                     || ' ('
                     || pt.user_concurrent_program_name
                     || ')'
                  )
           ELSE pt.user_concurrent_program_name
        END job_name
      , u.email_address
      , frt.responsibility_name requested_by_resp
      , r.

request_date
      , r.requested_start_date
      , DECODE(
           r.hold_flag
         , 'Y', 'Yes'
         , 'N', 'No'
        ) on_hold
      , r.printer
      , r.number_of_copies print_count
      , r.argument_text PARAMETERS
      , r.resubmit_interval resubmit_every
      , r.resubmit_interval_unit_code resubmit_time_period
      , TO_CHAR((r.requested_start_date), 'HH24:MI:SS') start_time,
        NVL2(
           r.resubmit_interval
         , 'Periodically'
         , NVL2(
              r.release_class_id
            , 'On specific days'
            , 'Once'
           )
        ) AS schedule_type
   FROM apps.fnd_user u
      , apps.fnd_printer_styles_tl s
      , apps.fnd_concurrent_requests r
      , apps.fnd_responsibility_tl frt
      , apps.fnd_concurrent_programs_tl pt
      , apps.fnd_concurrent_programs pb
  WHERE pb.application_id = r.program_application_id
    AND r.responsibility_id = frt.responsibility_id
    AND pb.concurrent_program_id = pt.concurrent_program_id
    AND u.user_id = r.requested_by
    AND s.printer_style_name(+) = r.print_style
    AND r.phase_code = 'P'
    AND pb.concurrent_program_id = r.concurrent_program_id
    AND pb.application_id = pt.application_id
    AND pt.user_concurrent_program_name = 'Report Set'; -- - pas tout