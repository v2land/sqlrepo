--check last password change date
Select name, ptime from sys.USER$ 
where TYPE# = 1
and ASTATUS <3 -- Account Status OPEN, and EXPIRED
--and name in (select USERNAME from dba_users where username like '%AGR%')
and ptime < '01-SEP-18' --SYSDATE - 100
order by ptime, name ;

/*
AGROPUR_LINK	    21-MAY-14
AGR_APPS_WCI	    21-MAY-14
PI_USER     	    03-DEC-14
AGR_AGR	            24-DEC-14
AGRCONV	            24-DEC-14
AGRPKG	            24-DEC-14
AGRTEMP	            24-DEC-14
AGR_COMPILE	        24-DEC-14
APPLSYS	            24-DEC-14
AGR_OBI_READ	    06-JAN-15
AGR_OBI_READ_DBLINK	06-JAN-15
AGROPUR	            06-JAN-15
AGR_INTERFACE_BCVL	19-JAN-15
AGR_INTERFACE_BONC	19-JAN-15
AGR_INTERFACE_DIAM	19-JAN-15
AGR_INTERFACE_GRBY	19-JAN-15
AGR_INTERFACE_PLES	19-JAN-15
AGR_INTERFACE_WOOD	19-JAN-15
AGR_INTERFACE_HYAC	27-MAY-15
AGR_GELS_PKG	    17-FEB-16
AGR_INTERFACE_DCPSP	09-MAY-16
AGR_UPK_APP_PROD	01-JUN-16
AGR_UPK_PROD	    01-JUN-16
AGR_INTERFACE_LUX	21-OCT-16
AGR_INTERFACE_VELO	28-OCT-16
APPS_NE	            02-SEP-17
APPS	            02-SEP-17
AXF	                16-MAR-18
*/


-- CHECK  'SYS', 'SYSTEM'
SELECT name, ptime FROM sys.USER$ WHERE TYPE#=1
    AND name IN('SYS', 'SYSTEM');


select
'alter user '|| username || ' identified by '|| '"NEW_PASSWORD";' as "ALTER"
  from dba_users where username in 
  (SELECT name
    FROM sys.USER$
    WHERE TYPE#=1
    AND name  IN
            (SELECT username
              FROM dba_users where username NOT IN (SELECT oracle_username FROM APPLSYS.FND_ORACLE_USERID) 
              --AND ACCOUNT_STATUS != 'EXPIRED &' || ' LOCKED'
              --AND username NOT IN ('SYS','SYSTEM','DBSNMP')
              --AND username NOT LIKE '%AGR%'
              --AND username NOT in ('PI_USER') -- Exclusion , to not change
             -- AND username NOT in ('OUTLN','') --Predefined Administrative Accounts --Oracle Created Database Users: Password, Usage and Files References (Doc ID 160861.1)
              )
    and ptime < '01-SEP-18' --SYSDATE - 100
        )
    order by username;




SELECT name,ptime  FROM sys.USER$
    WHERE TYPE#=1
    AND (name  IN
            (SELECT username
              FROM dba_users where
               username IN (SELECT oracle_username FROM APPLSYS.FND_ORACLE_USERID) 
              --username NOT IN (SELECT oracle_username FROM APPLSYS.FND_ORACLE_USERID) 
              --AND ACCOUNT_STATUS != 'EXPIRED &' || ' LOCKED'
              --AND username NOT IN ('SYS','SYSTEM','DBSNMP')
              --AND username NOT LIKE '%AGR%'
              --AND username NOT in ('PI_USER') -- Exclusion , to not change
             -- AND username NOT in ('OUTLN','') --Predefined Administrative Accounts --Oracle Created Database Users: Password, Usage and Files References (Doc ID 160861.1)
              )
        OR name LIKE '%APPS%'
        )
    and ptime < '01-DEC-25' --SYSDATE - 100
    AND name LIKE '%APPS%'
    ;

