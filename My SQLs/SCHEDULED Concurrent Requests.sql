--List ALL SCHEDULED Concurrent Programs, Requests, Details , Schedule Type
SELECT
    r.request_id,
    p.user_concurrent_program_name
    || nvl2(r.description,' ('
    || r.description
    || ')',NULL) conc_prog,
    s.user_name requestor,
    r.argument_text arguments,
    To_Char(r.requested_start_date,'DD-MON-YY HH24:MI:SS') next_run,
    To_Char(r.last_update_date,'DD-MON-YY HH24:MI:SS') last_run,
    r.hold_flag on_hold,
    r.increment_dates,
    DECODE(c.class_type,'P','Periodic','S','On Specific Days','X','Advanced',c.class_type) schedule_type,
    CASE
            WHEN c.class_type = 'P' THEN 'Repeat every '
            || substr(c.class_info,1,instr(c.class_info,':') - 1)
            || DECODE(substr(c.class_info,instr(c.class_info,':',1,1) + 1,1),'N',' minutes','M',' months','H',' hours','D',' days')
            || DECODE(substr(c.class_info,instr(c.class_info,':',1,2) + 1,1),'S',' from the start of the prior run','C',' from the completion of the prior run'
)
            WHEN c.class_type = 'S' THEN nvl2(dates.dates,'Dates: '
            || dates.dates
            || '. ',NULL)
            || DECODE(substr(c.class_info,32,1),'1','Last day of month ')
            || DECODE(sign(to_number(substr(c.class_info,33) ) ),'1','Days of week: '
            || DECODE(substr(c.class_info,33,1),'1','Su ')
            || DECODE(substr(c.class_info,34,1),'1','Mo ')
            || DECODE(substr(c.class_info,35,1),'1','Tu ')
            || DECODE(substr(c.class_info,36,1),'1','We ')
            || DECODE(substr(c.class_info,37,1),'1','Th ')
            || DECODE(substr(c.class_info,38,1),'1','Fr ')
            || DECODE(substr(c.class_info,39,1),'1','Sa ') )
        END
    AS schedule,
    To_Char(c.date1,'DD-MON-YY HH24:MI:SS') start_date,
    --c.date1 start_date,
    c.date2 end_date,
    To_Char(c.date2,'DD-MON-YY HH24:MI:SS') end_date,
    c.class_info
FROM
    fnd_concurrent_requests r,
    fnd_conc_release_classes c,
    fnd_concurrent_programs_tl p,
    fnd_user s,
    (
        WITH date_schedules AS (
            SELECT
                release_class_id,
                RANK() OVER(
                    PARTITION BY release_class_id
                    ORDER BY
                        s
                ) a,
                s
            FROM
                (
                    SELECT
                        c.class_info,
                        l,
                        c.release_class_id,
                        DECODE(substr(c.class_info,l,1),'1',TO_CHAR(l) ) s
                    FROM
                        (
                            SELECT
                                level l
                            FROM
                                dual
                            CONNECT BY
                                level <= 31
                        ),
                        fnd_conc_release_classes c
                    WHERE
                        c.class_type = 'S'
                        AND   instr(substr(c.class_info,1,31),'1') > 0
                )
            WHERE
                s IS NOT NULL
        ) SELECT
            release_class_id,
            substr(MAX(sys_connect_by_path(s,' ') ),2) dates
          FROM
            date_schedules
        START WITH
            a = 1
        CONNECT BY NOCYCLE
            PRIOR a = a - 1
        GROUP BY
            release_class_id
    ) dates
WHERE
    r.phase_code = 'P'
    AND   c.application_id = r.release_class_app_id
    AND   c.release_class_id = r.release_class_id
    AND   nvl(c.date2,SYSDATE + 1) > SYSDATE
    AND   c.class_type IS NOT NULL
    AND   p.concurrent_program_id = r.concurrent_program_id
    and   p.USER_CONCURRENT_PROGRAM_NAME like '%INVOICING INTERFACE � MINI%'
    AND   p.language = 'US'
    AND   dates.release_class_id (+) = r.release_class_id
    AND   r.requested_by = s.user_id
ORDER BY
    conc_prog,
    on_hold,
    next_run;
    
    
/*##############################################################################
#        CONCURRENT REQUESTS - not optimised
/*############################################################################*/
SELECT fcr.request_id
     , fcpt.user_concurrent_program_name|| NVL2(fcr.description, ' (' || fcr.description || ')', NULL) conc_prog
     , fu.user_name requestor
     , fu.description requested_by
     , fu.email_address
     , frt.responsibility_name requested_by_resp
     , trim(fl.meaning) status
     , fcr.phase_code
     , fcr.status_code
     , fcr.argument_text "PARAMETERS"
     , '------>' dates
     , TO_CHAR(fcr.request_date, 'DD-MON-YYYY HH24:MI:SS') requested
     , TO_CHAR(fcr.requested_start_date, 'DD-MON-YYYY HH24:MI:SS') requested_start   
     , TO_CHAR((fcr.requested_start_date), 'HH24:MI:SS') start_time
     , '------>' holds
     , DECODE(fcr.hold_flag, 'Y', 'Yes', 'N', 'No') on_hold
     , CASE
          WHEN fcr.hold_flag = 'Y'
             THEN SUBSTR(
                    u2.description
                  , 0
                  , 40
                 )
       END last_update_by
     , CASE
          WHEN fcr.hold_flag = 'Y'
             THEN fcr.last_update_date
       END last_update_date
     , '------>' prints
     , fcr.number_of_copies print_count
     , fcr.printer
     , fcr.print_style
     , '------>' schedule
     , fcr.increment_dates
     , CASE WHEN fcrc.CLASS_INFO IS NULL THEN
        'Yes: ' || TO_CHAR(fcr.requested_start_date, 'DD-MON-YYYY HH24:MI:SS')
       ELSE
        'n/a'
       END run_once
     , CASE WHEN fcrc.class_type = 'P' THEN
        'Repeat every ' ||
        substr(fcrc.class_info, 1, instr(fcrc.class_info, ':') - 1) ||
        decode(substr(fcrc.class_info, instr(fcrc.class_info, ':', 1, 1) + 1, 1),
               'N', ' minutes',
               'M', ' months',
               'H', ' hours',
               'D', ' days') ||
        decode(substr(fcrc.class_info, instr(fcrc.class_info, ':', 1, 2) + 1, 1),
               'S', ' from the start of the prior run',
               'C', ' from the completion of the prior run')
       ELSE
         'n/a'
       END set_days_of_week
       , CASE WHEN fcrc.class_type = 'S' AND instr(substr(fcrc.class_info, 33),'1',1) > 0 THEN
          'Days of week: ' ||
                  decode(substr(fcrc.class_info, 33, 1), '1', 'Sun, ') ||
                  decode(substr(fcrc.class_info, 34, 1), '1', 'Mon, ') ||
                  decode(substr(fcrc.class_info, 35, 1), '1', 'Tue, ') ||
                  decode(substr(fcrc.class_info, 36, 1), '1', 'Wed, ') ||
                  decode(substr(fcrc.class_info, 37, 1), '1', 'Thu, ') ||
                  decode(substr(fcrc.class_info, 38, 1), '1', 'Fri, ') ||
                  decode(substr(fcrc.class_info, 39, 1), '1', 'Sat ')
         ELSE
           'n/a'
         end  days_of_week 
       , CASE WHEN fcrc.class_type = 'S' AND instr(substr(fcrc.class_info, 1, 31),'1',1) > 0 THEN
          'Set Days of Month: ' ||
                  decode(substr(fcrc.class_info, 1, 1), '1', '1st, ')   ||
                  decode(substr(fcrc.class_info, 2, 1), '1', '2nd, ')   ||
                  decode(substr(fcrc.class_info, 3, 1), '1', '3rd, ')   ||
                  decode(substr(fcrc.class_info, 4, 1), '1', '4th, ')   ||
                  decode(substr(fcrc.class_info, 5, 1), '1', '5th, ')   ||
                  decode(substr(fcrc.class_info, 6, 1), '1', '6th, ')   ||
                  decode(substr(fcrc.class_info, 7, 1), '1', '7th, ')   ||
                  decode(substr(fcrc.class_info, 8, 1), '1', '8th, ')   ||
                  decode(substr(fcrc.class_info, 9, 1), '1', '9th, ')   ||
                  decode(substr(fcrc.class_info, 10, 1), '1', '10th, ') ||
                  decode(substr(fcrc.class_info, 11, 1), '1', '11th, ') ||
                  decode(substr(fcrc.class_info, 12, 1), '1', '12th, ') ||
                  decode(substr(fcrc.class_info, 13, 1), '1', '13th, ') ||
                  decode(substr(fcrc.class_info, 14, 1), '1', '14th, ') ||
                  decode(substr(fcrc.class_info, 15, 1), '1', '15th, ') ||
                  decode(substr(fcrc.class_info, 16, 1), '1', '16th, ') ||
                  decode(substr(fcrc.class_info, 17, 1), '1', '17th, ') ||
                  decode(substr(fcrc.class_info, 18, 1), '1', '18th, ') ||
                  decode(substr(fcrc.class_info, 19, 1), '1', '19th, ') ||
                  decode(substr(fcrc.class_info, 20, 1), '1', '20th, ') ||
                  decode(substr(fcrc.class_info, 21, 1), '1', '21st, ') ||
                  decode(substr(fcrc.class_info, 22, 1), '1', '22nd, ') ||
                  decode(substr(fcrc.class_info, 23, 1), '1', '23rd,' ) ||
                  decode(substr(fcrc.class_info, 24, 1), '1', '24th, ') ||
                  decode(substr(fcrc.class_info, 25, 1), '1', '25th, ') ||
                  decode(substr(fcrc.class_info, 26, 1), '1', '26th, ') ||
                  decode(substr(fcrc.class_info, 27, 1), '1', '27th, ') ||
                  decode(substr(fcrc.class_info, 28, 1), '1', '28th, ') ||
                  decode(substr(fcrc.class_info, 29, 1), '1', '29th, ') ||
                  decode(substr(fcrc.class_info, 30, 1), '1', '30th, ') ||
                  decode(substr(fcrc.class_info, 31, 1), '1', '31st. ')
         ELSE
           'n/a'
         END days_of_month
       , CASE WHEN fcrc.class_type = 'S' AND substr(fcrc.class_info, 32, 1) = '1' THEN
                  'Yes'
         ELSE
           'n/a'
         END last_day_of_month_ticked
       , fcrc.CLASS_INFO
  FROM applsys.fnd_concurrent_requests fcr
     , applsys.fnd_user fu
     , applsys.fnd_user u2
     , applsys.fnd_concurrent_programs fcp
     , applsys.fnd_concurrent_programs_tl fcpt
     , applsys.fnd_printer_styles_tl fpst
     , applsys.fnd_conc_release_classes fcrc
     , applsys.fnd_responsibility_tl frt
     , apps.fnd_lookups fl
 WHERE fcp.application_id = fcpt.application_id
   AND fcr.requested_by = fu.user_id
   AND fcr.concurrent_program_id = fcp.concurrent_program_id
   AND fcr.program_application_id = fcp.application_id
   AND fcr.concurrent_program_id = fcpt.concurrent_program_id
   AND fcr.responsibility_id = frt.responsibility_id
   AND fcr.last_updated_by = u2.user_id
   AND fcr.print_style = fpst.printer_style_name(+)
   AND fcr.release_class_id = fcrc.release_class_id(+)
   AND fcr.status_code = fl.lookup_code
   AND fl.lookup_type = 'CP_STATUS_CODE'
   AND fcr.phase_code = 'P'
   AND 1=1
ORDER BY fu.description, fcr.requested_start_date asc;