--   TOP SQL consumer in the last 30s (With module information and limited by load %)
--
-------------------------------------------------------------------------------------
/*col module for a26 trun
break on inst_id skip page on sql_id
COMPUTE SUM OF "ON_CPU" ON inst_id
COMPUTE SUM OF WAITING ON inst_id
COMPUTE SUM OF "LOAD%" ON inst_id
col  "Mod AS(distinct)" for a16
col "SQL_ID (PLAN_HASH)" for a32
*/
SELECT
	inst_id,sql_id || ' (' || SQL_PLAN_HASH_VALUE || ')' "SQL_ID (PLAN_HASH)",
	MODULE,LPAD (AS_TOT_MOD || ' (' || dist_ses_per_mod || ')',10,' ') "Mod AS(distinct)",
	ON_CPU "ON_CPU",WAITING,round((AS_TOT_MOD/AS_TOT_INST)*100,1) "LOAD%"
	FROM
		(SELECT
		inst_id,sql_id,MODULE,SQL_PLAN_HASH_VALUE,ON_CPU,WAITING,AS_TOT_INST,AS_TOT_SQL,
		AS_TOT_MOD,dist_ses_per_mod,
		SUM(AS_TOT_SQL) OVER (PARTITION BY INST_ID ORDER BY AS_TOT_SQL DESC ROWS UNBOUNDED PRECEDING) LOAD_INC,
		row_number() OVER (PARTITION BY inst_id ORDER BY AS_TOT_SQL desc) ord
		FROM
			(SELECT distinct
			inst_id,
			sql_id,
			MODULE,
			count(distinct session_id) OVER (PARTITION BY INST_ID,SQL_ID,MODULE) dist_ses_per_mod,
			SQL_PLAN_HASH_VALUE,
			count(1) over (partition by inst_id )  AS_TOT_INST,
			count(1) over (partition by inst_id ,sql_id)  AS_TOT_SQL,
			count(1) over (partition by inst_id ,sql_id,MODULE)  AS_TOT_MOD,
			sum(decode(SESSION_STATE,'ON CPU',1,0)) OVER ( PARTITION BY inst_id ,sql_id,MODULE)  ON_CPU,
			sum(decode(SESSION_STATE,'WAITING',1,0)) OVER ( PARTITION BY inst_id ,sql_id,MODULE) WAITING
			FROM gV$ACTIVE_SESSION_HISTORY
			WHERE SAMPLE_TIME > (sysdate-30/(3600*24)) 
			AND SESSION_TYPE = 'FOREGROUND'
			and sql_id is not null))
WHERE round((LOAD_INC/AS_TOT_INST)*100,2) < 70 OR ord <=3
ORDER BY inst_id,AS_TOT_SQL desc ,AS_TOT_MOD desc;
-------------------------
--=========================================================
-- ========  BEST ADF Session Identification - EBS User
--================================

-- ADF Sessions Details : ADFX	VLCASAPC	XXEBS_AGR_CUSTOM_DEV_3_R	EBSDEVD_APPSTEST@agropur.com  Agropur Custom Dev RETAIL	Customer Details

SELECT to_char(first_connect, 'dd-mm-yyyy hh24:mi:ss') "First Connect" 
       ,to_char(last_connect, 'dd-mm-yyyy hh24:mi:ss') "Last Connect" 
       ,a.function_type
       ,b.user_name
       ,resp.responsibility_key
       ,p.email_address 
       ,p.employee_number 
       ,(SELECT description 
            FROM fnd_responsibility_tl r 
            WHERE 1=1
            and r.responsibility_id = a.responsibility_id 
            AND    r.LANGUAGE = 'US') "Responsibility" 
       ,(SELECT user_function_name 
        FROM   fnd_form_functions_vl fffv 
        WHERE  (fffv.function_id = a.function_id)) "Current Function" 
        ,bg.NAME
        ,a.session_id
        ,a.login_id
FROM   icx_sessions        a --user_id 
      ,fnd_user            b --user_id,employee_id
      ,fnd_responsibility resp --responsability_id
      ,per_people_x        p --employee_id, business_group_id
      ,per_business_groups bg --business_group_id
WHERE  a.user_id = b.user_id 
and resp.responsibility_id(+)=a.responsibility_id
AND    b.employee_id = p.person_id 
AND    p.business_group_id = bg.business_group_id 
--AND    bg.NAME = '&business_group' -- AGROPUR
and a.last_connect>sysdate-nvl(FND_PROFILE.VALUE('ICX_SESSION_TIMEOUT'),30)/60/24  --select FND_PROFILE.VALUE('ICX_SESSION_TIMEOUT') from dual;
and disabled_flag != 'Y' and pseudo_flag = 'N'
and a.function_type like '%ADFX%'
and b.user_name = 'VLCASAPC'
ORDER  BY first_connect DESC;
-- 16-04-2019 06:31:08	16-04-2019 07:14:20	ADFX	VLCASAPC	XXEBS_AGR_CUSTOM_DEV_3_R	EBSDEVD_APPSTEST@agropur.com	9000347	Agropur Custom Dev RETAIL	Customer Details	AGROPUR COOPERATIVE	30546096	18639521


-- --- Query Identify session ADF
SELECT s.saddr, s.SID, s.serial#, s.audsid, s.paddr, s.user#, s.username,
s.command, s.ownerid, s.taddr, s.lockwait, s.status, s.server,
s.schema#, s.schemaname, s.osuser, s.process, s.machine, s.terminal,
UPPER (s.program) program, s.TYPE, s.sql_address, s.sql_hash_value,
s.sql_id, s.sql_child_number, s.sql_exec_start, s.sql_exec_id,
s.prev_sql_addr, s.prev_hash_value, s.prev_sql_id,
s.prev_child_number, s.prev_exec_start, s.prev_exec_id,
s.plsql_entry_object_id, s.plsql_entry_subprogram_id,
s.plsql_object_id, s.plsql_subprogram_id, s.module, s.module_hash,
s.action, s.action_hash, s.client_info, s.fixed_table_sequence,
s.row_wait_obj#, s.row_wait_file#, s.row_wait_block#,
s.row_wait_row#, s.logon_time, s.last_call_et, s.pdml_enabled,
s.failover_type, s.failover_method, s.failed_over,
s.resource_consumer_group, s.pdml_status, s.pddl_status, s.pq_status,
s.current_queue_duration, s.client_identifier,
s.blocking_session_status, s.blocking_instance, s.blocking_session,
s.seq#, s.event#, s.event, s.p1text, s.p1, s.p1raw, s.p2text, s.p2,
s.p2raw, s.p3text, s.p3, s.p3raw, s.wait_class_id, s.wait_class#,
s.wait_class, s.wait_time, s.seconds_in_wait, s.state,
s.wait_time_micro, s.time_remaining_micro,
s.time_since_last_wait_micro, s.service_name, s.sql_trace,
s.sql_trace_waits, s.sql_trace_binds, s.sql_trace_plan_stats,
s.session_edition_id, s.creator_addr, s.creator_serial#
FROM v$session s
    WHERE ( (s.username IS NOT NULL)
            AND (NVL (s.osuser, 'x') <> 'SYSTEM')
            AND (s.TYPE <> 'BACKGROUND') AND STATUS='ACTIVE'
            )
and UPPER(s.program) like UPPER('%adf%')
and UPPER(s.machine) like UPPER('%dcpspebsapps2%')
--and MODULE='e:IBY:cp:iby/IBY_FC_PAYER_NOTIF_FORMAT'  ---MODULE -- 7xu16mtua5xkj
ORDER BY "PROGRAM";
AUDSID: 73495306

07000101FBC40B48	890	64404	73495306	07000101FBA357C8	44	APPS	3	2147483644			ACTIVE	DEDICATED	44	APPS	wlsddadf	1234	dcssaebsapps.agropur.com	unknown	EBSDEVD_DB_ADFUSER	USER	07000101A12B0CA0	2643044365	1mjps76fsma0d	1	16-APR-19	16777223	0700010193E41EF0	2824633629	djtxk5yn5sz8x	1	16-APR-19	16777222					EBSDEVD_DB_adfuser	211292004		0	123                                                   0         	4523793	-1	0	0	0	16-APR-19	3	NO	NONE	NONE	NO	OTHER_GROUPS	DISABLED	ENABLED	ENABLED	0		NOT IN WAIT			101	388	SQL*Net message from client	driver id	1952673792	0000000074637000	#bytes	1	0000000000000001		0	00	2723168908	6	Idle	24	3	WAITED KNOWN TIME	236564		3005892	SYS$USERS	DISABLED	FALSE	FALSE	FIRST EXEC	350102	07000101FBA357C8	106

SELECT
    to_char(icx.first_connect, 'dd-mm-yyyy hh24:mi:ss') "First Connect", 
    to_char(icx.last_connect, 'dd-mm-yyyy hh24:mi:ss') "Last Connect", 
    vs.username,
    vs.osuser,
    nvl(vs.client_identifier, fu.user_name) ebs_user,
    fu.description   ebs_user_desc,
    icx.FUNCTION_TYPE,        --ADFX
    --/*
      (SELECT description 
                FROM fnd_responsibility_tl r 
                WHERE 1=1
                and r.responsibility_id = icx.responsibility_id 
                AND    r.LANGUAGE = 'US') "Responsibility" 
       ,(SELECT user_function_name 
            FROM   fnd_form_functions_vl fffv 
            WHERE  (fffv.function_id = icx.function_id)) "Current Function",
    --*/
    vp.spid           unix_process_id,
    vs.sid            oracle_sid,
    vs.serial#,
    vs.machine,
    vs.process        "Apps Server Process",
    vs.program,
    vs.module,
    vs.action,
    vs.service_name,
    --vs.client_info,
    vs.status,
    --vs.logon_time,
    --(SYSDATE - last_call_et / 86400 ) last_activity,
    vs.server
FROM
    icx_sessions         icx,--user_id, responsability_id
    v$process            vp,--spid,addr
    v$session            vs,--client_identifier,saddr
    v$circuit            vc,--dispatcher,server
    v$shared_server      vss,--paddr
    v$dispatcher         vd,--paddr
    fnd_logins   fl,--process_spid
    fnd_user     fu, --user_id
    fnd_user     fu2 --user_id
    ,fnd_responsibility resp --responsability_id
    ,per_people_x        p --employee_id, business_group_id
    ,per_business_groups bg --business_group_id
WHERE 1=1
   and vp.addr = vs.paddr (+)
   AND vc.saddr (+) = vs.saddr
   AND vc.server = vss.paddr (+)
   AND vc.dispatcher = vd.paddr (+)
   AND vp.spid = fl.process_spid (+)
   AND vp.pid = fl.pid (+)
   AND fl.user_id = fu.user_id (+)
   AND icx.user_id=fu.user_id
   and fu.user_name != 'GUEST'  
   and fu.user_name = 'VLCASAPC' --!
   
and icx.last_connect>sysdate-nvl(FND_PROFILE.VALUE('ICX_SESSION_TIMEOUT'),30)/60/24  --select FND_PROFILE.VALUE('ICX_SESSION_TIMEOUT') from dual;
and icx.disabled_flag != 'Y' and pseudo_flag = 'N'
and resp.responsibility_id(+)=icx.responsibility_id
AND    fu2.employee_id = p.person_id 
AND    p.business_group_id = bg.business_group_id 
order by 1,2 desc
;
--===================================================================



SELECT
    *
FROM
    icx_sessions;-- where session_id in ('2675','2015','1360','3694');

SELECT
    *
FROM
    v$session where user_;
--FUNCTION_TYPE:ADFX
-- Function_ID:
--USER_ID
--NODE_ID
--LOGIN_ID



SELECT
    to_char(icx.first_connect, 'dd-mm-yyyy hh24:mi:ss') "First Connect" 
    ,to_char(icx.last_connect, 'dd-mm-yyyy hh24:mi:ss') "Last Connect", 
    icx.user_id,fu2.user_id,
    vs.username,
    vs.osuser,
    nvl(vs.client_identifier, fu.user_name) ebs_user,
    fu2.description   ebs_user_desc,
    icx.FUNCTION_TYPE --ADFX
   --    ,p.employee_number 
       ,(SELECT description 
            FROM fnd_responsibility_tl r 
            WHERE 1=1
            and r.responsibility_id = icx.responsibility_id 
            AND    r.LANGUAGE = 'US') "Responsibility" 
       ,(SELECT user_function_name 
        FROM   fnd_form_functions_vl fffv 
        WHERE  (fffv.function_id = icx.function_id)) "Current Function", 
   -- bg.NAME, 
   
 --   fl.login_name,
  --  fl.login_type,
    vp.spid           unix_process_id,
    vs.sid            oracle_sid,
    vs.serial#,
    vs.machine,
    vs.process        "Apps Server Process",
    vs.program,
    vs.module,
    vs.action,
    vs.service_name,
    vs.client_info,
    vs.status,
    vs.logon_time,
    ( SYSDATE - last_call_et / 86400 ) last_activity,
    vs.server
  --  vss.name          shared_server,
  --  vd.name           dispatcher,
  --  'alter system kill session '''
  --  || vs.sid
   --    || ','
  --        || vs.serial#
  --           || ''';' kill,
  --  vs.sql_address,
  --  vs.sql_hash_value,
  --  vs.wait_class,
  --  vs.wait_time,
  --  vs.seconds_in_wait,
  --  vs.event,
   -- vs.final_blocking_session_status,
   -- vs.final_blocking_session
FROM
    icx_sessions         icx,--user_id, responsability_id
    v$process            vp,--spid,addr
    v$session            vs,--client_identifier,saddr
    v$circuit            vc,--dispatcher,server
    v$shared_server      vss,--paddr
    v$dispatcher         vd,--paddr
    applsys.fnd_logins   fl,--process_spid
    applsys.fnd_user     fu, --user_id
    applsys.fnd_user     fu2 --user_id
    ,fnd_responsibility resp --responsability_id
    ,per_people_x        p --employee_id, business_group_id
    ,per_business_groups bg --business_group_id
WHERE 1=1
    and fu2.user_id(+)=icx.user_id
    --and fu.user_id=icx.user_id
    and vp.addr = vs.paddr (+)
    AND vc.saddr (+) = vs.saddr
        AND vc.server = vss.paddr (+)
            AND vc.dispatcher = vd.paddr (+)
                AND vp.spid = fl.process_spid (+)
                    AND vp.pid = fl.pid (+)
                        AND fl.user_id = fu.user_id (+)
                            AND vs.client_identifier = fu2.user_name (+)
 /*     AND (VS.USERNAME LIKE NVL(UPPER(:USERNAME),'%') or (:USERNAME is null and vs.username is null))
      AND (vs.client_identifier LIKE NVL(UPPER(:EBS_USERNAME),'%') OR (:EBS_USERNAME is null and vs.client_identifier IS NULL))
      AND (VP.SPID LIKE NVL(:UNIX_PROCESS_ID, '%') OR (:UNIX_PROCESS_ID is null and VP.SPID IS NULL))
      AND (VS.PROCESS LIKE NVL(:UNIX_APPS_PROCESS_ID, '%') OR (:UNIX_APPS_PROCESS_ID is null and VS.PROCESS IS NULL))
      AND (UPPER(VS.MACHINE) LIKE NVL(UPPER(:MACHINE), '%') OR  (:MACHINE is null and VS.MACHINE IS NULL))
      AND (UPPER(VS.OSUSER) LIKE NVL(UPPER(:OS_USER), '%') OR (:OS_USER is null and VS.OSUSER IS NULL))
      AND (VS.SID LIKE NVL(:ORACLE_SID, '%') OR (:ORACLE_SID is null and VS.SID IS NULL))
      AND (UPPER(VS.STATUS) LIKE NVL(UPPER(:STATUS), '%') OR (:STATUS is null and VS.STATUS IS NULL))
      AND (UPPER(VS.SERVICE_NAME) LIKE NVL(UPPER(:SERVICE), '%') OR (:SERVICE is null and VS.SERVICE_NAME IS NULL))    
      AND (UPPER(VS.MODULE) LIKE NVL(UPPER(:MODULE), '%') OR (:MODULE is null and VS.MODULE IS NULL))
      and ((nvl2(vs.final_blocking_session,0,-1) = nvl2(:IS_LOCKING,0,-1)) or (vs.SID like nvl(:IS_LOCKING,'%') or vs.final_blocking_session like nvl(:IS_LOCKING, '%')))
      AND (UPPER(VS.program) LIKE NVL(UPPER(:program), '%') OR (:program is null and VS.program IS NULL))*/
and icx.last_connect>sysdate-nvl(FND_PROFILE.VALUE('ICX_SESSION_TIMEOUT'),30)/60/24  --select FND_PROFILE.VALUE('ICX_SESSION_TIMEOUT') from dual;
and fu2.user_name != 'GUEST'  
and icx.disabled_flag != 'Y' and pseudo_flag = 'N'
and resp.responsibility_id(+)=icx.responsibility_id
AND    fu2.employee_id = p.person_id 
AND    p.business_group_id = bg.business_group_id 
and vs.status = 'ACTIVE'
and icx.function_type like '%ADF%'
order by 1,2 desc
;

-- CHECK ICX Sessions
SELECT *
FROM ICX_SESSIONS
WHERE (nvl(disabled_flag,'N') = 'Y')
OR (nvl(disabled_flag,'N') = 'N'
AND (last_connect + 1 + (fnd_profile.value_specific( 'ICX_LIMIT_TIME', user_id, responsibility_id, responsibility_application_id, org_id))/24)< sysdate);




--===================


select last_connect, usr.user_name, resp.responsibility_key, function_type, icx.*
  from apps.icx_sessions icx
  join apps.fnd_user usr on usr.user_id=icx.user_id
  left join apps.fnd_responsibility resp on resp.responsibility_id=icx.responsibility_id
  where 1=1
    --last_connect>sysdate-nvl(FND_PROFILE.VALUE('ICX_SESSION_TIMEOUT'),30)/60/24
    and disabled_flag != 'Y' and pseudo_flag = 'N'
    and function_type like '%ADF%';
    
select last_connect, usr.user_name, resp.responsibility_key, function_type--, icx.*
  from apps.icx_sessions icx,--user_id, responsability_id
       apps.fnd_user usr,-- user_id
       apps.fnd_responsibility resp --responsability_id
where 1=1
    and usr.user_id=icx.user_id
    and resp.responsibility_id(+)=icx.responsibility_id
    and last_connect>sysdate-nvl(FND_PROFILE.VALUE('ICX_SESSION_TIMEOUT'),30)/60/24  --select FND_PROFILE.VALUE('ICX_SESSION_TIMEOUT') from dual;
    and disabled_flag != 'Y' and pseudo_flag = 'N'
    and function_type like '%ADF%'
    ;
    


--===============  Hicham discutions
select last_connect, usr.user_name, resp.responsibility_key, function_type--, icx.*
  from apps.icx_sessions icx,--user_id, responsability_id
       apps.fnd_user usr,-- user_id
       apps.fnd_responsibility resp --responsability_id
       fnd_appl_sessions fas,
       icx_sessions icx,
v$session ses
where 1=1
    and usr.user_id=icx.user_id
    and resp.responsibility_id(+)=icx.responsibility_id
    and last_connect>sysdate-nvl(FND_PROFILE.VALUE('ICX_SESSION_TIMEOUT'),30)/60/24  --select FND_PROFILE.VALUE('ICX_SESSION_TIMEOUT') from dual;
    and disabled_flag != 'Y' and pseudo_flag = 'N'
    --and function_type like '%ADF%'
    ;

SELECT *
FROM
--fnd_appl_sessions fas,
v$session ses--,
--fnd_user
WHERE 1=1
--AND fas.audsid = ses.audsid
and ses.status='ACTIVE'
and program like '%adf%'
; 


SELECT vsql.sql_id,
  icxs.login_id,
  icxs.session_id,
  icxsa.value userorsswaportalurl,
  icx_sec.check_session (icxs.session_id) session_status,
  icxs.user_id,
  icxs.responsibility_id,
  icxs.responsibility_application_id,
  icxs.security_group_id,
  icxs.org_id ,
  icxs.menu_id,
  icxs.function_id,
  icxs.nls_language,
  icxs.language_code,
  icxs.date_format_mask,
  icxs.first_connect,
  icxs.last_connect,
  icxs.nls_date_language,
  icxs.nls_numeric_characters,
  icxs.nls_sort,
  icxs.nls_territory,
  icxs.disabled_flag,
  icxt.transaction_id,
  icxt.function_type,
  frv.responsibility_key,
  frv.responsibility_name,
  frv.description,
  fu.user_name,
  fu.employee_id,
  fu.email_address,
  fu.supplier_id,
  fu.customer_id,
  fu.person_party_id
FROM icx_sessions icxs,
  icx_session_attributes icxsa,
  icx_transactions icxt ,
  fnd_user fu ,
  fnd_responsibility_vl frv,
  fnd_appl_sessions fas,
  v$session vs,
  v$sql vsql
WHERE
vs.sql_address=vsql.address
and fas.audsid=vs.audsid
and icxs.session_id     = icxsa.session_id
AND icxs.session_id       = icxt.session_id
AND icxs.user_id          = fu.user_id
AND icxs.responsibility_id= frv.responsibility_id
AND icxt.function_type    ='ADFX'
--AND icxsa.NAME            = '_USERORSSWAPORTALURL'
AND fu.user_name = 'VLCASAPC'
AND icxt.last_connect                       =
  (
     SELECT
      MAX(icxt1.last_connect)
       FROM
      icx_sessions icxs1           ,
      icx_transactions icxt1
      WHERE
        icxs1.session_id                         = icxt1.session_id
    AND icxs1.user_id                            = fu.user_id
    AND icxt1.function_type                      ='ADFX'
  );

select * from dba_tab_columns where column_name like '%AUDSID%' and table_name like '%%';
select * from dba_tab_columns where column_name like '%LOGIN_ID%' and table_name like '%$%' ;
select * from dba_tab_columns where column_name like '%LOGIN_ID%' and table_name like '%%' ;
select * from dba_tables where table_name like '%ICX%';
select * from v$session;
select * from fnd_appl_sessions where audsid = '73495306'; login_id audsid

