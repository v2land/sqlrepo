--Select Running Request Process_TIME, AVG_Time, MODE and Standard Deviation in minutes  V.03 vcasapciuc
--15381185	POXPOPDF	12:09	01:00  PO Output for Communication
--15381236	APXPBASL	00:11	02:07
--15381574	XXR_GL_SLA_RPT	39:12	01:16	00:03	08:05  dw16w4wf6xmax
--15381675	XXRHR_MS_REFRESH_MVIEWS	05:01	55:36	22:46	13:02
--15414447	XXR_GL_SLA_RPT2	14:27	01:53	00:04	04:15 > 25 min   - OK
--15415379	XXRHR_MS_REFRESH_MVIEWS	02:43	56:57	22:46	13:08 
--15415572	XXRAR_GUARANTEES_FWUP	00:04			null		nul nul nul nul null  - NEW

--15415474	XXR_GL_SLA_RPT	01:07:36	max03:00:01	min00:00:02	00:01:20	00:00:03	00:08:18
--15465083	XLABAPUB	05:57:25	09:33:23	00:00:00	00:34:26	00:00:01	01:43:04
--dependence : XLAACCPB
--               XLAACCUP
--14907162	XXRHR_EMPL_EXTRACT_CURRENT	RONA - Employee Extract - Assignment with current data	00:17:27

--14062064	FNDWFBG	Workflow Background Process	09:39:25	00:04:25	00:00:00	00:00:00	00:00:00	00:00:03
--14067187	XXRFND_SUBMIT_REQ_SET	RONA - Concurrent Request Set Submission	09:38:46					
--14067197	XXRHR_MS_REFRESH_MVIEWS	RONA - Refresh Materialized Views for Microstrategy	09:38:30		
--15584983	XXR_GL_SLA_RPT3	RONA - GL Analysis Retail	04:04:37	01:11:20	00:00:02	00:00:57	00:00:03	00:04:02
--1va7rp03xq81w
-- COMPLETION text: COMPLETED NORMAL, mais STATUS_CODE
SELECT  
    af.phase_code P,
    af.request_id,
    af.STATUS_CODE S,
    af.parent_request_id Parent,
    --b.CONCURRENT_PROGRAM_ID CP_ID,
    b.CONCURRENT_PROGRAM_NAME CP_NAME,
    --PT.USER_CONCURRENT_PROGRAM_NAME
    DECODE (af.description,NULL, pt.user_concurrent_program_name,af.description || ' ( ' || pt.user_concurrent_program_name|| ' ) ') program
    ,To_Char(af.request_date,'DD-MON-YY HH24:MI:SS') submitted
    ,To_Char(af.REQUESTED_START_DATE,'DD-MON-YY HH24:MI:SS') REQUESTED_START_DATE
    ,To_Char(af.ACTUAL_START_DATE,'DD-MON-YY HH24:MI:SS') started
    ,To_Char(af.ACTUAL_COMPLETION_DATE,'DD-MON-YY HH24:MI:SS') completed
    ,Extract(day from (NUMTODSINTERVAL((af.ACTUAL_START_DATE - af.REQUESTED_START_DATE)*24*60,'minute')))  "D.Days"
    ,TO_CHAR (TRUNC(sysdate) + NUMTODSINTERVAL((af.ACTUAL_START_DATE - af.REQUESTED_START_DATE)*24*60,'minute'),'HH24:MI:SS') "Started delay",
    af.COMPLETION_TEXT,
    --round(((sysdate-af.actual_start_date)*24*60*60/60),2) "Process_time",
    TO_CHAR ( TRUNC(sysdate)
               + NUMTODSINTERVAL ((sysdate-af.actual_start_date)*24*60,
                    'minute'),
               'HH24:MI:SS') "Process_TIME",
          
         (SELECT 
          TO_CHAR (
               TRUNC (SYSDATE)
               + NUMTODSINTERVAL (
                    MAX ( (F.ACTUAL_COMPLETION_DATE - F.ACTUAL_START_DATE))
                    * 24*60,
                    'minute'),
               'HH24:MI:SS')
            "MAXIMUM"
            FROM APPLSYS.FND_CONCURRENT_REQUESTS F,
                 APPLSYS.FND_CONCURRENT_PROGRAMS P
            WHERE     PHASE_CODE = 'C'
                 AND STATUS_CODE = 'C'
                 AND F.CONCURRENT_PROGRAM_ID = P.CONCURRENT_PROGRAM_ID
                 AND P.CONCURRENT_PROGRAM_ID=af.CONCURRENT_PROGRAM_ID
                 --and f.root_request_id=af.root_request_id
            GROUP BY P.CONCURRENT_PROGRAM_NAME
      ) MAX_Time,
      
      (SELECT 
          TO_CHAR (
               TRUNC (SYSDATE)
               + NUMTODSINTERVAL (
                    MIN ( (F.ACTUAL_COMPLETION_DATE - F.ACTUAL_START_DATE))
                    * 24*60,
                    'minute'),
               'HH24:MI:SS')
            "MINIMUM"
            FROM APPLSYS.FND_CONCURRENT_REQUESTS F,
                 APPLSYS.FND_CONCURRENT_PROGRAMS P
            WHERE     PHASE_CODE = 'C'
                 AND STATUS_CODE = 'C'
                 AND F.CONCURRENT_PROGRAM_ID = P.CONCURRENT_PROGRAM_ID
                 AND P.CONCURRENT_PROGRAM_ID=af.CONCURRENT_PROGRAM_ID
                 and f.root_request_id=af.root_request_id
            GROUP BY P.CONCURRENT_PROGRAM_NAME
      ) MIN_Time,
      
      (SELECT 
          TO_CHAR (
               TRUNC (SYSDATE)
               + NUMTODSINTERVAL (
                    AVG ( (F.ACTUAL_COMPLETION_DATE - F.ACTUAL_START_DATE))
                    * 24*60,
                    'minute'),
               'HH24:MI:SS')
            "AVERAGE"
            FROM APPLSYS.FND_CONCURRENT_REQUESTS F,
                 APPLSYS.FND_CONCURRENT_PROGRAMS P
            WHERE     PHASE_CODE = 'C'
                 AND STATUS_CODE = 'C'
                 AND F.CONCURRENT_PROGRAM_ID = P.CONCURRENT_PROGRAM_ID
                 AND P.CONCURRENT_PROGRAM_ID=af.CONCURRENT_PROGRAM_ID
                 and f.root_request_id=af.root_request_id
            GROUP BY P.CONCURRENT_PROGRAM_NAME
      ) AVG_Time,
      
      (SELECT 
          TO_CHAR (
               TRUNC (SYSDATE)
               + NUMTODSINTERVAL (
               --mean, max, min, median, mode and standard deviation 
                    STATS_MODE( (F.ACTUAL_COMPLETION_DATE - F.ACTUAL_START_DATE))
                    * 24*60,
                    'minute'),
               'HH24:MI:SS')
            "STATS_MODE"
            FROM APPLSYS.FND_CONCURRENT_REQUESTS F,
                 APPLSYS.FND_CONCURRENT_PROGRAMS P
            WHERE     PHASE_CODE = 'C'
                 AND STATUS_CODE = 'C'
                 AND F.CONCURRENT_PROGRAM_ID = P.CONCURRENT_PROGRAM_ID
                 AND P.CONCURRENT_PROGRAM_ID=af.CONCURRENT_PROGRAM_ID
                 and f.root_request_id=af.root_request_id
            GROUP BY P.CONCURRENT_PROGRAM_NAME
      )"STATS_MODE"
      ,
      (SELECT 
          TO_CHAR (
               TRUNC (SYSDATE)
               + NUMTODSINTERVAL (
               --mean, max, min, median, mode and standard deviation 
                    STDDEV( (F.ACTUAL_COMPLETION_DATE - F.ACTUAL_START_DATE))
                    * 24*60,
                    'minute'),
               'HH24:MI:SS')
            "STDDEV"
            FROM APPLSYS.FND_CONCURRENT_REQUESTS F,
                 APPLSYS.FND_CONCURRENT_PROGRAMS P
            WHERE     PHASE_CODE = 'C'
                 AND STATUS_CODE = 'C'
                 AND F.CONCURRENT_PROGRAM_ID = P.CONCURRENT_PROGRAM_ID
                 AND P.CONCURRENT_PROGRAM_ID=af.CONCURRENT_PROGRAM_ID
                 and f.root_request_id=af.root_request_id
            GROUP BY P.CONCURRENT_PROGRAM_NAME
      )"STDDEV"
    FROM
    apps.fnd_concurrent_requests af
    ,apps.fnd_concurrent_programs b
    ,apps.FND_CONCURRENT_PROGRAMS_TL PT
    WHERE
    af.CONCURRENT_PROGRAM_ID = b.CONCURRENT_PROGRAM_ID
    and b.CONCURRENT_PROGRAM_ID = PT.CONCURRENT_PROGRAM_ID
    AND PT.language = USERENV('Lang')
    and 
    af.phase_code in ('R');
    --af.status_code='R';
    --af.status_code='W';
    --af.phase_code in ('R','I','P')
     --and af.status_code in ('Q','P','W','Z')
     --af.request_id ='21987268';
    UNION
    
    SELECT  
    af.phase_code PHASE,
    af.request_id,
    af.STATUS_CODE STATUS,
    af.parent_request_id Parent,
    b.CONCURRENT_PROGRAM_ID,
    b.CONCURRENT_PROGRAM_NAME,
    PT.USER_CONCURRENT_PROGRAM_NAME,
    --af.*,
    af.COMPLETION_TEXT,
    --round(((sysdate-af.actual_start_date)*24*60*60/60),2) "Process_time",
    TO_CHAR ( TRUNC(sysdate)
               + NUMTODSINTERVAL ((sysdate-af.actual_start_date)*24*60,
                    'minute'),
               'HH24:MI:SS') "Process_TIME",
          
         (SELECT 
          TO_CHAR (
               TRUNC (SYSDATE)
               + NUMTODSINTERVAL (
                    MAX ( (F.ACTUAL_COMPLETION_DATE - F.ACTUAL_START_DATE))
                    * 24*60,
                    'minute'),
               'HH24:MI:SS')
            "MAXIMUM"
            FROM APPLSYS.FND_CONCURRENT_REQUESTS F,
                 APPLSYS.FND_CONCURRENT_PROGRAMS P
            WHERE     PHASE_CODE = 'C'
                 AND STATUS_CODE = 'C'
                 AND F.CONCURRENT_PROGRAM_ID = P.CONCURRENT_PROGRAM_ID
                 AND P.CONCURRENT_PROGRAM_ID=af.CONCURRENT_PROGRAM_ID
            GROUP BY P.CONCURRENT_PROGRAM_NAME
      ) MAX_Time,
      
      (SELECT 
          TO_CHAR (
               TRUNC (SYSDATE)
               + NUMTODSINTERVAL (
                    MIN ( (F.ACTUAL_COMPLETION_DATE - F.ACTUAL_START_DATE))
                    * 24*60,
                    'minute'),
               'HH24:MI:SS')
            "MINIMUM"
            FROM APPLSYS.FND_CONCURRENT_REQUESTS F,
                 APPLSYS.FND_CONCURRENT_PROGRAMS P
            WHERE     PHASE_CODE = 'C'
                 AND STATUS_CODE = 'C'
                 AND F.CONCURRENT_PROGRAM_ID = P.CONCURRENT_PROGRAM_ID
                 AND P.CONCURRENT_PROGRAM_ID=af.CONCURRENT_PROGRAM_ID
            GROUP BY P.CONCURRENT_PROGRAM_NAME
      ) MIN_Time,
      
      (SELECT 
          TO_CHAR (
               TRUNC (SYSDATE)
               + NUMTODSINTERVAL (
                    AVG ( (F.ACTUAL_COMPLETION_DATE - F.ACTUAL_START_DATE))
                    * 24*60,
                    'minute'),
               'HH24:MI:SS')
            "AVERAGE"
            FROM APPLSYS.FND_CONCURRENT_REQUESTS F,
                 APPLSYS.FND_CONCURRENT_PROGRAMS P
            WHERE     PHASE_CODE = 'C'
                 AND STATUS_CODE = 'C'
                 AND F.CONCURRENT_PROGRAM_ID = P.CONCURRENT_PROGRAM_ID
                 AND P.CONCURRENT_PROGRAM_ID=af.CONCURRENT_PROGRAM_ID
            GROUP BY P.CONCURRENT_PROGRAM_NAME
      ) AVG_Time,
      
      (SELECT 
          TO_CHAR (
               TRUNC (SYSDATE)
               + NUMTODSINTERVAL (
               --mean, max, min, median, mode and standard deviation 
                    STATS_MODE( (F.ACTUAL_COMPLETION_DATE - F.ACTUAL_START_DATE))
                    * 24*60,
                    'minute'),
               'HH24:MI:SS')
            "STATS_MODE"
            FROM APPLSYS.FND_CONCURRENT_REQUESTS F,
                 APPLSYS.FND_CONCURRENT_PROGRAMS P
            WHERE     PHASE_CODE = 'C'
                 AND STATUS_CODE = 'C'
                 AND F.CONCURRENT_PROGRAM_ID = P.CONCURRENT_PROGRAM_ID
                 AND P.CONCURRENT_PROGRAM_ID=af.CONCURRENT_PROGRAM_ID
            GROUP BY P.CONCURRENT_PROGRAM_NAME
      )"STATS_MODE"
      ,
      (SELECT 
          TO_CHAR (
               TRUNC (SYSDATE)
               + NUMTODSINTERVAL (
               --mean, max, min, median, mode and standard deviation 
                    STDDEV( (F.ACTUAL_COMPLETION_DATE - F.ACTUAL_START_DATE))
                    * 24*60,
                    'minute'),
               'HH24:MI:SS')
            "STDDEV"
            FROM APPLSYS.FND_CONCURRENT_REQUESTS F,
                 APPLSYS.FND_CONCURRENT_PROGRAMS P
            WHERE     PHASE_CODE = 'C'
                 AND STATUS_CODE = 'C'
                 AND F.CONCURRENT_PROGRAM_ID = P.CONCURRENT_PROGRAM_ID
                 AND P.CONCURRENT_PROGRAM_ID=af.CONCURRENT_PROGRAM_ID
            GROUP BY P.CONCURRENT_PROGRAM_NAME
      )"STDDEV"
    FROM
    apps.fnd_concurrent_requests af
    ,apps.fnd_concurrent_programs b
    ,apps.FND_CONCURRENT_PROGRAMS_TL PT
    WHERE
    af.CONCURRENT_PROGRAM_ID = b.CONCURRENT_PROGRAM_ID
    and b.CONCURRENT_PROGRAM_ID = PT.CONCURRENT_PROGRAM_ID
    AND PT.language = USERENV('Lang')
    and  af.status_code in ('Q','P','W','Z')
    --af.phase_code in ('R','I')
    and af.request_ID in (select PARENT_REQUEST_ID from fnd_concurrent_requests where phase_code in ('R','P','I'))
    order by 4,2;

--================  SCHEDULED Analisys ============================-
undefine nr_of_days
SELECT 
fcp.CONCURRENT_PROGRAM_ID, fcp.REQUEST_SET_FLAG "SET",
 fcr.root_request_id,
 fcr.request_id,
 parent_request_id "parent_request_id",
      -- DECODE(fcpt.user_concurrent_program_name 'Report Set'  'Report Set:' || fcr.description fcpt.user_concurrent_program_name) CONC_PROG_NAME
       DECODE(fcr.description, NULL, fcpt.user_concurrent_program_name, fcr.description || ' ( ' || fcpt.user_concurrent_program_name|| ' ) ') program,
       argument_text PARAMETERS,
       fu.user_name USER_NAME,
    fcrc.class_info,
        --to_char(fcr.requested_start_date 'DD-MON-YY HH24:MI:SS') requested_start_date
        to_char(fcr.requested_start_date, 'HH24:MI') start_time,
  'DLMMeJVS' DLMMeJVS,
  (select fcr.actual_start_date from fnd_concurrent_requests where request_id=fcr.parent_request_id),
    --1--
      round(((select actual_completion_date from fnd_concurrent_requests where request_id=fcr.parent_request_id) -
      (select requested_start_date from fnd_concurrent_requests where request_id=fcr.parent_request_id))* 24*60, 0) as prior_duration ,
  --/1--     
       (SELECT
            round(AVG ( (F.ACTUAL_COMPLETION_DATE - F.ACTUAL_START_DATE))* 24*60, 0) "AVERAGE"
          --round((F.ACTUAL_COMPLETION_DATE - F.ACTUAL_START_DATE)*1440 0) "AVERAGE"
            FROM APPLSYS.FND_CONCURRENT_REQUESTS F,
                 APPLSYS.FND_CONCURRENT_PROGRAMS P
            WHERE     PHASE_CODE = 'C'
                 AND STATUS_CODE = 'C'
                 AND F.CONCURRENT_PROGRAM_ID = P.CONCURRENT_PROGRAM_ID
                 AND P.CONCURRENT_PROGRAM_ID=fcr.CONCURRENT_PROGRAM_ID
                 and f.root_request_id=fcr.root_request_id
                 and F.ACTUAL_START_DATE between sysdate-&nr_of_days and sysdate
            GROUP BY P.CONCURRENT_PROGRAM_NAME
      ) AVG_Time
  FROM apps.fnd_concurrent_programs_tl fcpt,
       apps.fnd_concurrent_requests    fcr,
       apps.fnd_user                   fu,
       apps.fnd_conc_release_classes   fcrc,
       apps.fnd_concurrent_programs fcp
 WHERE fcp.concurrent_program_ID=fcpt.concurrent_program_ID
 and  fcp.application_id=fcpt.application_id
   and fcpt.application_id = fcr.program_application_id
   AND fcpt.concurrent_program_id = fcr.concurrent_program_id
   AND fcr.requested_by = fu.user_id
  AND fcr.phase_code in ('P','R') -- Pending (Scheduled)
   AND fcr.requested_start_date > SYSDATE
   AND fcpt.LANGUAGE = 'US'
   AND fcrc.release_class_id(+) = fcr.release_class_id
   AND fcrc.application_id(+) = fcr.release_class_app_id
   --and fcr.request_id = '37480159'
   --and  DECODE(fcpt.user_concurrent_program_name
   --           'Report Set'
   --           'Report Set:' || fcr.description
   --           fcpt.user_concurrent_program_name) LIKE '%%'
   order by  fcp.CONCURRENT_PROGRAM_ID,13 desc,requested_start_date desc;
 --========================================================================--

-- Running CP PID SID and User, 

select fcr.request_id req_id,
fcr.phase_code||'/'||fcr.status_code sta,
fcr.pid pid,
sess.sid sid,
sess.inst_id inst_id,
fcr.running_time time,
nvl(t.used_urec,0) u_rec,
substrb(fcr.user_desc,1,8) user_desc,
substrb(decode(fcr.pgm_code,
'FNDRSSUB',fcr.pgm_name||'-'||rs.user_request_set_name,
'FNDRSSTG',fcr.pgm_name||'-'||rs.user_request_set_name
||'-'||rss.user_stage_name,
fcr.pgm_name), 1,48) pgm_name
from gv$session sess,
gv$transaction t,
fnd_request_sets_vl rs,
fnd_request_set_stages_vl rss,
(select /*+ ordered
index (r fnd_concurrent_requests_n7)
index (pt fnd_concurrent_programs_tl_u1) */
r.request_id request_id,
r.program_application_id application_id,
c.os_process_id pid,
r.oracle_session_id audsid,
r.concurrent_program_id concurrent_program_id,
p.concurrent_program_name pgm_code,
u.description user_desc,
decode(p.concurrent_program_name,
'FNDRSSUB','SET',
'FNDRSSTG','Set Stage',
pt.user_concurrent_program_name) pgm_name,
r.phase_code phase_code,
r.status_code status_code,
to_char(r.request_date,'yymmdd hh24:mi:ss') request_date, to_char(r.actual_completion_date,'yymmdd hh24:mi:ss') actual_completion_date,
to_char(r.requested_start_date,'yymmdd hh24:mi:ss') requested_start_date,
ceil((nvl(r.actual_completion_date,sysdate)-r.actual_start_date)*1440) running_time,
r.actual_start_date actual_start_date_org,
u.user_id user_id,
u.user_name user_name,
r.argument_text arguments,
decode(p.concurrent_program_name,
'FNDRSSUB',r.argument2,
'FNDRSSTG',r.argument2,-1) request_set_id,
decode(p.concurrent_program_name,
'FNDRSSTG',r.argument3,
-1) request_set_stage_id
from
fnd_concurrent_requests r,
fnd_concurrent_programs_tl pt,
fnd_concurrent_programs p,
fnd_user u,
fnd_concurrent_processes c
where r.requested_by = u.user_id
and r.program_application_id = pt.application_id
and r.concurrent_program_id = pt.concurrent_program_id
and pt.language = 'US'
and pt.application_id = p.application_id
and pt.concurrent_program_id = p.concurrent_program_id
and r.controlling_manager = c.concurrent_process_id
and r.phase_code = 'R'
and r.status_code = 'R') fcr
where fcr.audsid = sess.audsid(+)
and sess.saddr = t.ses_addr(+)
and fcr.request_set_id = rs.request_set_id(+)
and fcr.request_set_id = rss.request_set_id(+)
and fcr.request_set_stage_id = rss.request_set_stage_id(+)
order by fcr.actual_start_date_org desc, fcr.running_time, fcr.request_id desc;

/*
22022980	R/R	17891338	1653	1	2	82242	Millette	Create receipt transactions into lockbox interfa
22022466	R/R	6291776	741	1	21	1	DAOUST, 	Detailed Report On Rebate Paid By Check
22017448	R/R	3080548	1589	1	60	1	COOK, JE	Number of days under status Report
22021558	R/R	18612502	1941	1	64	12	Generic 	Agropur - Buy Back Worker
22018175	R/R	24248522	28	1	187	0	Generic 	Agropur - Buy Back Process
22018019	R/R	13894142	2917	1	194	1	Wiersma,	Send Receipt of Payment Notifications
22014103	R/R	21102604	3769	1	426	31	ARCONTRO	AR Invoice new
22013975	R/R	15007880	330	1	431	0	ARCONTRO	AR New Invoices
*/
  
--######################################################################
----------Details Drilldown of a Concurrent Request Set that IS Running or WAS Executed:
--  using connect by prior

SELECT /*+ ORDERED USE_NL(x fcr fcp fcptl)*/
                                fcr.request_id
                                "Request ID",
                                             fcptl.user_concurrent_program_name
                                "Program Name"
                                ,
                                fcr.phase_code,
                                fcr.status_code,
                                --     to_char(fcr.request_date,'DD-MON-YYYY HH24:MI:SS') "Submitted", 
                                --     (fcr.actual_start_date - fcr.request_date)*1440 "Delay",
                                To_char(fcr.actual_start_date,
                                'DD-MON-YYYY HH24:MI:SS')      "Start Time",
                                To_char(fcr.actual_completion_date,
                                'DD-MON-YYYY HH24:MI:SS') "End Time",
                                --( fcr.actual_completion_date - fcr.actual_start_date ) * 1440 "Elapsed",
                                TO_CHAR ( TRUNC(sysdate) + NUMTODSINTERVAL ((fcr.actual_completion_date - fcr.actual_start_date)*24*60,'minute'),'HH24:MI:SS') "Process_TIME",
                                fcr.oracle_process_id
                                "Trace ID"
FROM   (SELECT /*+ index (fcr1 fnd_concurrent_requests_n3) */ fcr1.request_id
        FROM   apps.fnd_concurrent_requests fcr1
        WHERE  1 = 1
        START WITH fcr1.request_id = 22018019
        CONNECT BY PRIOR fcr1.request_id = fcr1.parent_request_id) x,
       apps.fnd_concurrent_requests fcr,
       apps.fnd_concurrent_programs fcp,
       apps.fnd_concurrent_programs_tl fcptl
WHERE  fcr.request_id = x.request_id
       AND fcr.concurrent_program_id = fcp.concurrent_program_id
       AND fcr.program_application_id = fcp.application_id
       AND fcp.application_id = fcptl.application_id
       AND fcp.concurrent_program_id = fcptl.concurrent_program_id
       AND fcptl.LANGUAGE = 'US'
       and fcptl.user_concurrent_program_name != 'Request Set Stage'  --

ORDER  BY fcr.actual_start_date;




--How to retrieve Summary of concurrent Jobs/status/Count in Last 1 hour?
select
fcpt.USER_CONCURRENT_PROGRAM_NAME,
DECODE(fcr.phase_code,
 'C',  'Completed ',
 'I',  'Inactive ',
 'P',  'Pending ',
 'R',  'Running ',
fcr.phase_code
) PHASE ,
DECODE(fcr.status_code,
 'A',  'Waiting ',
 'B',  'Resuming ',
 'C',  'Normal ',
 'D',  'Cancelled ',
 'E',  'Errored ',
 'F',  'Scheduled ',
 'G',  'Warning ',
 'H',  'On Hold ',
 'I',  'Normal ',
 'M',  'No Manager ',
 'Q',  'Standby ',
 'R',  'Normal ',
 'S',  'Suspended ',
 'T',  'Terminating ',
 'U',  'Disabled ',
 'W',  'Paused ',
 'X',  'Terminated ',
 'Z',  'Waiting ',
fcr.status_code
) STATUS,
count(*)
from apps.fnd_concurrent_programs_tl fcpt,apps.FND_CONCURRENT_REQUESTs fcr
where fcpt.CONCURRENT_PROGRAM_ID=fcr.CONCURRENT_PROGRAM_ID
and fcpt.language = USERENV('Lang')
--and (fcr.ACTUAL_START_DATE > sysdate - 1/24 or fcr.ACTUAL_COMPLETION_DATE > sysdate - 1/24)
and fcr.phase_code in ('P') 
and fcr.status_code in('I') --('W','R')
group by fcpt.USER_CONCURRENT_PROGRAM_NAME,fcr.phase_code,fcr.status_code
;
--Select Running Request Process_Time and AVG_Time in minutes  V.01
SELECT DISTINCT 
    af.request_id,
    b.CONCURRENT_PROGRAM_NAME,
    round(((sysdate-af.actual_start_date)*24*60*60/60),2) "Process_time",
    (SELECT 
          TO_CHAR (
               TRUNC (SYSDATE)
               + NUMTODSINTERVAL (
                    AVG ( (F.ACTUAL_COMPLETION_DATE - F.ACTUAL_START_DATE))
                    * 24*60,
                    'minute'),
               'MI:SS')
            "AVERAGE"
            FROM APPLSYS.FND_CONCURRENT_REQUESTS F,
                 APPLSYS.FND_CONCURRENT_PROGRAMS P
            WHERE     PHASE_CODE = 'C'
                 AND STATUS_CODE = 'C'
                 AND F.CONCURRENT_PROGRAM_ID = P.CONCURRENT_PROGRAM_ID
                 AND P.CONCURRENT_PROGRAM_ID=af.CONCURRENT_PROGRAM_ID
            GROUP BY P.CONCURRENT_PROGRAM_NAME
      ) AVG_Time
    FROM
    apps.fnd_concurrent_requests af
    ,apps.fnd_concurrent_programs b
    WHERE
    af.CONCURRENT_PROGRAM_ID = b.CONCURRENT_PROGRAM_ID and
   -- af.status_code='R';
   af.phase_code='R';
    
--EXCLUSIONS:
--RONA - Concurrent Request Set Submission XXRFND_SUBMIT_REQ_SET
-- Rendre vrai la statistique
          
SELECT DISTINCT 
a.request_id,
round(((sysdate-a.actual_start_date)*24*60*60/60),2) "Process_time" , a.*
FROM
apps.fnd_concurrent_requests a
--,apps.fnd_concurrent_programs b
WHERE
--a.concurrent_program_id=b.concurrent_program_id and
a.status_code in ('H')
--and a.phase_code in ('P')
; 
 
 ------------------ PENDING STANDBY Investigation (exclude On Hold):         
SELECT DISTINCT 
        a.request_id, v.PARENT_REQUEST_ID
        --, b.CONCURRENT_PROGRAM_NAME, a.phase_code, a.STATUS_CODE, a.PRIORITY,a.*
        FROM
        apps.fnd_concurrent_requests a
        ,apps.fnd_concurrent_programs b
        --,apps.FND_CONCURRENT_PROGRAMS_TL c
       ,fnd_conc_req_summary_v v
        WHERE
        a.concurrent_program_id=b.concurrent_program_id
        and a.concurrent_program_id=v.concurrent_program_id
        --AND b.concurrent_program_id=c.concurrent_program_id
        and a.phase_code='P'
        and a.status_code in ('I')
        --and a.status_code not in ('R','I','C')
       -- AND c.language = USERENV('Lang')
      --  and a.request_id='20767451'
        ;

SELECT
    TO_CHAR(rpad(TO_CHAR(request_id),10)
    || ' '
    || rpad(nvl(TO_CHAR(actual_start_date,'dd-mon-yyyy hh24:mi:ss'),' '),20)
    || ' '
    || rpad(substr(program,1,65),65)
    || ' '
    || rpad(substr(requestor,1,10),10)
    || ' '
    || TO_CHAR(parent_request_id) ) a,
    'E' b,
    2 srt
FROM
    fnd_conc_req_summary_v conc
WHERE
    SYSDATE - request_date > 0.00694444444444444
    AND   requested_start_date < SYSDATE
    AND   phase_code = 'P'
    AND   status_code = 'I';
        
 -- ------------------------- TROUVER les RUNNINNG PHANTOMES ------       
SELECT DISTINCT 
a.request_id,
round(((sysdate-a.actual_start_date)*24*60*60/60),2) "Process_time" , a.*
FROM
apps.fnd_concurrent_requests a
--,apps.fnd_concurrent_programs b
WHERE
--a.concurrent_program_id=b.concurrent_program_id and
--a.phase_code in ('R');
a.phase_code in ('I','R');
--and status_code not in ('R'); 
/*
Update APPLSYS.FND_CONCURRENT_REQUESTS
Set STATUS_CODE='D',phase_code='C'
Where request_id in (SELECT
a.request_id
from applsys.fnd_concurrent_requests a
WHERE
request_id in ('21002269','20987228'));
--a.status_code='R'); -- or W: Paused
Commit;

UPDATE fnd_concurrent_requests
SET phase_code = 'C', status_code = 'X'
WHERE phase_code IN ('R','I')
and status_code not in ('R'); 
Commit;  
*/

SELECT
    TO_CHAR(rpad(TO_CHAR(request_id),10)
    || ' '
    || rpad(nvl(TO_CHAR(actual_start_date,'dd-mon-yyyy hh24:mi:ss'),' '),20)
    || ' '
    || rpad(substr(program,1,65),65)
    || ' '
    || rpad(substr(requestor,1,10),10)
    || ' '
    || TO_CHAR(parent_request_id) ) a,
    'A' b,
    1.4 srt
FROM
    fnd_conc_req_summary_v conc
WHERE
    actual_completion_date > SYSDATE - ( 1 / 24 )
    AND   phase_code = 'C'
    AND   status_code = 'E';
    
    
    
    
    
/*    
    Update APPLSYS.FND_CONCURRENT_REQUESTS
Set phase_code='C',STATUS_CODE='X'
Where request_id in(
'22060835'
,'22060851'
,'22060788'
,'22060850'
,'22060778'
);

--commit;*/


-- Check Locked Session and Concurrent Request in EBS R12
select
b.process " Process id ",
b.osuser,
b.machine,
b.sid,
b.serial#,
b.schemaname,
b.module,
b.action,
trunc(b.SECONDS_IN_WAIT/60/60,3) " Waiting Time in H ",
blocking_session,
'*** KILL ONLY IF BLOCKING_SESSION IS NULL, otherwise trace that blocking session till has null *** -- alter system kill session '''||b.sid||','||b.SERIAL#||''' immediate;' " SQL kill session statement" ,
'*** KILL ONLY IF BLOCKING_SESSION IS NULL, otherwise trace that blocking session till has null --- kill -9 '||b.process " OS kill process command",
b.PADDR,
p.SPID,
b.PROCESS
from v$session b , v$process p
where b.PADDR=p.ADDR and b.sid in (select BLOCKING_SESSION from v$session s where  BLOCKING_SESSION is not null);