--undefine nr_of_days
SELECT 
fcp.CONCURRENT_PROGRAM_ID, fcp.REQUEST_SET_FLAG "SET",
fcr.priority_request_id,
 fcr.root_request_id,
 fcr.request_id,
 fcr.parent_request_id,
      -- DECODE(fcpt.user_concurrent_program_name 'Report Set'  'Report Set:' || fcr.description fcpt.user_concurrent_program_name) CONC_PROG_NAME
      fcpt.user_concurrent_program_name "user_concurrent_program_name",
      fcr.description,
       argument_text PARAMETERS,
       fu.user_name USER_NAME,
    fcrc.class_type,
    fcrc.class_info,
        to_char(fcr.requested_start_date, 'YYYY-MM-DD') requested,
        to_char(fcr.requested_start_date, 'HH24:MI') start_time,
  --'DLMMeJVS' DLMMeJVS,
  to_char((select actual_start_date from fnd_concurrent_requests where request_id=fcr.parent_request_id), 'YYYY-MM-DD HH24:MI:SS')  previeus_start_date,
    --1--
      round(((select actual_completion_date from fnd_concurrent_requests where request_id=fcr.parent_request_id) -
      (select requested_start_date from fnd_concurrent_requests where request_id=fcr.parent_request_id))* 24*60, 0) as prior_duration ,
  --/1--     
       (SELECT
            round(AVG ( (F.ACTUAL_COMPLETION_DATE - F.ACTUAL_START_DATE))* 24*60, 0) "AVERAGE"
          --round((F.ACTUAL_COMPLETION_DATE - F.ACTUAL_START_DATE)*1440 0) "AVERAGE"
            FROM APPLSYS.FND_CONCURRENT_REQUESTS F,
                 APPLSYS.FND_CONCURRENT_PROGRAMS P
            WHERE     PHASE_CODE = 'C'
                 AND STATUS_CODE = 'C'
                 AND F.CONCURRENT_PROGRAM_ID = P.CONCURRENT_PROGRAM_ID
                 AND P.CONCURRENT_PROGRAM_ID=fcr.CONCURRENT_PROGRAM_ID
                 and f.root_request_id=fcr.root_request_id
                 and F.ACTUAL_START_DATE between sysdate-&nr_of_days and sysdate
            GROUP BY P.CONCURRENT_PROGRAM_NAME
      ) AVG_Time
  FROM apps.fnd_concurrent_programs_tl fcpt,
       apps.fnd_concurrent_requests    fcr,
       apps.fnd_user                   fu,
       apps.fnd_conc_release_classes   fcrc,
       apps.fnd_concurrent_programs fcp
 WHERE fcp.concurrent_program_ID=fcpt.concurrent_program_ID
 and  fcp.application_id=fcpt.application_id
   and fcpt.application_id = fcr.program_application_id
   AND fcpt.concurrent_program_id = fcr.concurrent_program_id
   AND fcr.requested_by = fu.user_id
  AND fcr.phase_code in ('P','R') -- Pending (Scheduled)
   AND fcr.requested_start_date > SYSDATE
   AND fcpt.LANGUAGE = 'US'
   AND fcrc.release_class_id(+) = fcr.release_class_id
   AND fcrc.application_id(+) = fcr.release_class_app_id
   and fcrc.class_info is not NULL
   and fcr.root_request_id is not null
   --and fcr.request_id = '37480159'
   --and  DECODE(fcpt.user_concurrent_program_name
   --           'Report Set'
   --           'Report Set:' || fcr.description
   --and fcpt.user_concurrent_program_name LIKE '%EDI Orders%'
   --and fcr.description LIKE '%Monitor - EDI Orders%'
   order by  fcr.root_request_id,fcr.requested_start_date,fcr.priority ;
