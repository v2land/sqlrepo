--===========   CONTROL FILE MULTIPLEXING ON ASM =====================:
--OEMAMP (load OEMAMP environement)
--sqlplus / as sysdba
select name from v$controlfile;
-- +DATA/OEMAMP/CONTROLFILE/current.256.893584183
--shutdown immediate
--rman target /
--startup nomount;
--restore controlfile to '+FRA' from '+DATA/OEMAMP/CONTROLFILE/current.256.893584183';

--asm (load asm environement)
--asmcmd   
--cd +FRA/OEMAMP/CONTROLFILE/
--ls -l
--current.375.942153407  - take note of the Name an use it in next step

--OEMAMP (load OEMAMP environement)
--sqlplus / as sysdba
alter system set control_files='+DATA/OEMAMP/CONTROLFILE/current.256.893584183','+FRA/OEMAMP/CONTROLFILE/current.375.942153407' scope=spfile;
--shutdown
--create pfile from spfile;
--startup
select name from v$controlfile;
-- +DATA/OEMAMP/CONTROLFILE/current.256.893584183
-- +FRA/OEMAMP/CONTROLFILE/current.375.942153407

--At this point the controlfile is multiplexed between two diskgroups  +FRA and +DATA


--========= Multiplexing Redo Log Files on ASM=========================================
--OEMAMP (load OEMAMP environement)
--sqlplus / as sysdba
alter system set db_create_online_log_dest_1='+FRA' scope=both;
alter system set db_create_online_log_dest_2='+DATA' scope=both;
show parameter db_create_online_log_dest;
create pfile from spfile;

--1. check existing status	
select l.group#, l.thread#,f.member,l.archived,l.status,(bytes/1024/1024) fsize			
from v$log l, v$logfile f			
where f.group# = l.group#			
order by 1,2;
--1	1	+FRA/OEMAMP/ONLINELOG/group_1.258.894366143	YES	INACTIVE	400
--2	1	+FRA/OEMAMP/ONLINELOG/group_2.257.894366151	YES	INACTIVE	400
--3	1	+FRA/OEMAMP/ONLINELOG/group_3.256.894366157	NO	CURRENT	  400

--	2. Notre but c'est de Multiplexer les redo dans
     --  +FRA
     --  +DATA
alter system set db_create_online_log_dest_1='+FRA' scope=both;
alter system set db_create_online_log_dest_2='+DATA' scope=both;
show parameter db_create_online_log_dest;
create pfile from spfile;
shutdown immediate
startup

select l.group#, l.thread#,f.member,l.archived,l.status,(bytes/1024/1024) fsize			
from v$log l, v$logfile f			
where f.group# = l.group#			
order by 1,2; --(group3 CURRENT)

alter database drop logfile group 1; -- (INACTIVE)
alter database add logfile group 1 size 400M;
alter database drop logfile group 2;-- (INACTIVE)
alter database add logfile group 2 size 400M;
alter system switch logfile; --(switch the Current group 3 ti group 1)
alter system switch logfile; --(switch the Current group 3 ti group 1)
 select l.group#, l.thread#,f.member,l.archived,l.status,(bytes/1024/1024) fsize			
 from v$log l, v$logfile f			
 where f.group# = l.group#			
 order by 1,2;
alter system checkpoint global;
 select l.group#, l.thread#,f.member,l.archived,l.status,(bytes/1024/1024) fsize			
 from v$log l, v$logfile f			
 where f.group# = l.group#			
 order by 1,2;
alter database drop logfile group 3;
alter database add logfile group 3 size 400M;
--==================================================
