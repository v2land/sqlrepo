-------------------------------------------------------------------------------------
--
--   TOP SQL consumer in the last 30s (With module information and limited by load %)
--
-------------------------------------------------------------------------------------
/*col module for a26 trun
break on inst_id skip page on sql_id
COMPUTE SUM OF "ON_CPU" ON inst_id
COMPUTE SUM OF WAITING ON inst_id
COMPUTE SUM OF "LOAD%" ON inst_id
col  "Mod AS(distinct)" for a16
col "SQL_ID (PLAN_HASH)" for a32
*/
SELECT
	inst_id,sql_id || ' (' || SQL_PLAN_HASH_VALUE || ')' "SQL_ID (PLAN_HASH)",
	MODULE,LPAD (AS_TOT_MOD || ' (' || dist_ses_per_mod || ')',10,' ') "Mod AS(distinct)",
	ON_CPU "ON_CPU",WAITING,round((AS_TOT_MOD/AS_TOT_INST)*100,1) "LOAD%"
	FROM
		(SELECT
		inst_id,sql_id,MODULE,SQL_PLAN_HASH_VALUE,ON_CPU,WAITING,AS_TOT_INST,AS_TOT_SQL,
		AS_TOT_MOD,dist_ses_per_mod,
		SUM(AS_TOT_SQL) OVER (PARTITION BY INST_ID ORDER BY AS_TOT_SQL DESC ROWS UNBOUNDED PRECEDING) LOAD_INC,
		row_number() OVER (PARTITION BY inst_id ORDER BY AS_TOT_SQL desc) ord
		FROM
			(SELECT distinct
			inst_id,
			sql_id,
			MODULE,
			count(distinct session_id) OVER (PARTITION BY INST_ID,SQL_ID,MODULE) dist_ses_per_mod,
			SQL_PLAN_HASH_VALUE,
			count(1) over (partition by inst_id )  AS_TOT_INST,
			count(1) over (partition by inst_id ,sql_id)  AS_TOT_SQL,
			count(1) over (partition by inst_id ,sql_id,MODULE)  AS_TOT_MOD,
			sum(decode(SESSION_STATE,'ON CPU',1,0)) OVER ( PARTITION BY inst_id ,sql_id,MODULE)  ON_CPU,
			sum(decode(SESSION_STATE,'WAITING',1,0)) OVER ( PARTITION BY inst_id ,sql_id,MODULE) WAITING
			FROM gV$ACTIVE_SESSION_HISTORY
			WHERE SAMPLE_TIME > (sysdate-30/(3600*24)) 
			AND SESSION_TYPE = 'FOREGROUND'
			and sql_id is not null))
WHERE round((LOAD_INC/AS_TOT_INST)*100,2) < 70 OR ord <=3
ORDER BY inst_id,AS_TOT_SQL desc ,AS_TOT_MOD desc;
-------------------------
--https://pnmazzei.wordpress.com/2013/05/17/a-different-view-on-vactive_session_history/
-------------------------
--  Live 5 sec Sub Query
SELECT distinct
			inst_id,
			sql_id,
			MODULE,
			count(distinct session_id) OVER (PARTITION BY INST_ID,SQL_ID,MODULE) dist_ses_per_mod,
			SQL_PLAN_HASH_VALUE,
			count(1) over (partition by inst_id )  AS_TOT_INST,
			count(1) over (partition by inst_id ,sql_id)  AS_TOT_SQL,
			count(1) over (partition by inst_id ,sql_id,MODULE)  AS_TOT_MOD,
			sum(decode(SESSION_STATE,'ON CPU',1,0)) OVER ( PARTITION BY inst_id ,sql_id,MODULE)  ON_CPU,
			sum(decode(SESSION_STATE,'WAITING',1,0)) OVER ( PARTITION BY inst_id ,sql_id,MODULE) WAITING
            ,gv.* -- remove that for distinct
			FROM gV$ACTIVE_SESSION_HISTORY gv
			WHERE SAMPLE_TIME > (sysdate-600/(3600*24)) -- 10 min   for Life Info
			AND SESSION_TYPE = 'FOREGROUND'
            and module like '%adf%'
			and sql_id is not null;
            
-----------------------------------
select * from fnd_user;--user_id <->User_name -> Responsabil etc
select * from fnd_logins; -- user_id <-> Login_id
select * from fnd_appl_sessions where login_id is not NULL order by 4 desc;-- login_id <->audsid
select * from v$session;--audsid <-> SID, SERIAL#
select * from gV$ACTIVE_SESSION_HISTORY; --session_id, session_serial# <-> SAMPLE TIME, MODULE, SQL_ID etc.

select * from sys.aud$;
select * from icx_sessions; 
select * from DBA_TAB_COLS where column_name like '%AUDSID%';


----------
--  Live 5 sec Sub Query
-- add vs.audsid
--add fas.login_id
SELECT distinct
            fas.login_id,
            vs.audsid,
			vs.sid,
            vs.serial#,
            gv.inst_id,
			gv.sql_id,
			gv.MODULE,
			count(distinct gv.session_id) OVER (PARTITION BY gv.INST_ID,gv.SQL_ID,gv.MODULE) dist_ses_per_mod,
			gv.SQL_PLAN_HASH_VALUE,
			count(1) over (partition by gv.inst_id )  AS_TOT_INST,
			count(1) over (partition by gv.inst_id ,gv.sql_id)  AS_TOT_SQL,
			count(1) over (partition by gv.inst_id ,gv.sql_id,gv.MODULE)  AS_TOT_MOD,
			sum(decode(gv.SESSION_STATE,'ON CPU',1,0)) OVER ( PARTITION BY gv.inst_id ,gv.sql_id,gv.MODULE)  ON_CPU,
			sum(decode(gv.SESSION_STATE,'WAITING',1,0)) OVER ( PARTITION BY gv.inst_id ,gv.sql_id,gv.MODULE) WAITING
           -- ,vs.*
            --,fas.*
            --,gv.* -- remove that for distinct
FROM gV$ACTIVE_SESSION_HISTORY gv,
    v$session vs,  --sys.v_$session vs
    fnd_appl_sessions fas
WHERE gv.SAMPLE_TIME > (sysdate-6000/(3600*24)) -- 5 sec  for Life Info
			AND gv.SESSION_TYPE = 'FOREGROUND'
            and gv.module like '%adf%'
			and gv.sql_id is not null
            and vs.sid = gv.session_id
            and vs.serial#=gv.session_serial#
            and fas.audsid = vs.audsid
            ;
            
----------------------------

SELECT
inst_id,
sql_id || ' (' || SQL_PLAN_HASH_VALUE || ')' "SQL_ID (PLAN_HASH)",
LPAD (total || ' (' || dist_sess || ')',10,' ') "Total (distinct)",
ON_CPU,
WAITING
FROM (
	SELECT
	inst_id,
	sql_id,
	SQL_PLAN_HASH_VALUE,
	total,
	dist_sess,
	ON_CPU,
	WAITING,
	row_number() OVER (PARTITION BY inst_id ORDER BY total desc) ord
	FROM
		(SELECT
		inst_id,
		sql_id,
		SQL_PLAN_HASH_VALUE,
		count(1) total,
		count(distinct session_id) dist_sess,
		sum(decode(SESSION_STATE,'ON CPU',1,0)) ON_CPU,
		sum(decode(SESSION_STATE,'WAITING',1,0)) WAITING
		FROM gV$ACTIVE_SESSION_HISTORY
		WHERE SAMPLE_TIME > (sysdate-30/(3600*24)) 
		AND SESSION_TYPE = 'FOREGROUND'
		and sql_id is not null
		GROUP BY inst_id ,sql_id,SQL_PLAN_HASH_VALUE))
WHERE ord < 6;

SELECT
inst_id,
WAIT_CLASS,
sess_count,
round(sess_count/30,1) ACT_SESSIONS
FROM
	(SELECT 
	inst_id,
	WAIT_CLASS,
	sess_count,
	row_number() OVER (PARTITION BY inst_id ORDER BY sess_count desc) ord
	FROM
		(SELECT
		inst_id,
		NVL(WAIT_CLASS,'ON_CPU') WAIT_CLASS,
		count(1) sess_count
		FROM gV$ACTIVE_SESSION_HISTORY
		WHERE SAMPLE_TIME > (sysdate-30/(3600*24))
		group by inst_id ,NVL(WAIT_CLASS,'ON_CPU')))
WHERE ord <=5
order by inst_id,sess_count desc;

select
ash.inst_id
,ash.sample_time
,ash.session_id,ash.session_serial#,
ash.sql_id
,ash.WAIT_TIME,vs.*,vp.*
from gv$active_session_history ash,
    v$session vs,
    v$process vp
where 1=1
and vs.paddr=vp.addr
and ash.sql_id is not null
and ash.session_id=vs.sid
and ash.session_serial#=vs.SERIAL#
and vs.program like '%adf%'
and vs.status = 'ACTIVE'
and ROWNUM <=20
order by 2 desc;
------------------------------------

SELECT
  S.SQL_ID,
  s.ADDRESS,
  S.MODULE, 
  U.USERNAME,
  s.program_id, s.program_line#,
  S.LAST_LOAD_TIME,
  S.ELAPSED_TIME,
  ROUND((S.DISK_READS/DECODE(S.EXECUTIONS,0,1, S.EXECUTIONS)),2) DISK_READS_PER_EXEC, 
  S.DISK_READS, 
  S.BUFFER_GETS, 
  S.PARSE_CALLS, 
  S.SORTS, 
  S.EXECUTIONS, 
  S.ROWS_PROCESSED, 
  100 - ROUND(100 *  S.DISK_READS/GREATEST(S.BUFFER_GETS,1),2) HIT_RATIO, 
  S.SHARABLE_MEM, 
  S.PERSISTENT_MEM, 
  S.RUNTIME_MEM, 
  S.CPU_TIME, 
--  'alter system kill session '''  || sid  || ','  || serial#  || ''';' kill,
  S.SQL_TEXT
FROM
    sys.V_$SQL S, --PROGRAM_ID, PROGRAM_LINE
    dba_USERS U
WHERE 1=1
    and S.PARSING_USER_ID=U.USER_ID 
    --AND sess.SQL_ADDRESS = S.ADDRESS
    /*FROM V$SESSION sess JOIN V$SQL sql 
on  (sess.SQL_ADDRESS = sql.ADDRESS) 
where sess.STATUS = 'ACTIVE'*/
    AND UPPER(U.USERNAME) NOT IN ('SYS','SYSTEM')
    and to_date(S.FIRST_LOAD_TIME,'YYYY-MM-DD/HH24:MI:SS') > sysdate-1
    and module like '%adf%'
ORDER BY 
   7 desc;

   
-- ======= Locate PROGRAM_ID and Line    
select object_name
from   dba_objects
where  object_id = 2378909; --PROGRAM_ID: XXAOL0250_INVOICE_ARCHIVE , PACKAGE BODY

select * from dba_source where name = 'XXAOL0250_INVOICE_ARCHIVE' and line >= 2072 and line < 2182 ;

select * from dba_source ds, dba_objects do
where 
ds.name = do.object_name
and do.object_id = 2378909
and ds.line >= 2072 and ds.line < 2182
;

select last_connect, usr.user_name, resp.responsibility_key, function_type--, icx.*
  from apps.icx_sessions icx,
       apps.fnd_user usr,
       apps.fnd_responsibility resp
where 1=1
    and usr.user_id=icx.user_id
    and resp.responsibility_id(+)=icx.responsibility_id
    and last_connect>sysdate-nvl(FND_PROFILE.VALUE('ICX_SESSION_TIMEOUT'),30)/60/24  --select FND_PROFILE.VALUE('ICX_SESSION_TIMEOUT') from dual;
    and disabled_flag != 'Y' and pseudo_flag = 'N'
    and function_type like '%ADF%';












-- KILL
-- Find Session info on sqlid
SELECT 'alter system kill session '''  || sid  || ','  || serial#  || ''';' kill, OSUSER, SID, SERIAL#, executions,
sql.SQL_ID ,sql.child_number, SQL_FULLTEXT 
FROM V$SESSION sess JOIN V$SQL sql 
on  (sess.SQL_ID = sql.SQL_ID) 
where sess.STATUS = 'ACTIVE' and
sql.sql_id = '1yr6prz7n274z'; --('2b7jsh1b1acfd','344ts62mfmrb1');--'8m1m8x201j0st'; --fbajpvqg1fux6






  
--set pages 1000 lines 4000
--col sql_profile for a32

select executions_total,
trunc(decode(executions_total, 0, 0, rows_processed_total/executions_total)) rows_avg,
trunc(decode(executions_total, 0, 0, fetches_total/executions_total)) fetches_avg,
trunc(decode(executions_total, 0, 0, disk_reads_total/executions_total)) disk_reads_avg,
trunc(decode(executions_total, 0, 0, buffer_gets_total/executions_total)) buffer_gets_avg,
trunc(decode(executions_total, 0, 0, cpu_time_total/executions_total)) cpu_time_avg,
trunc(decode(executions_total, 0, 0, elapsed_time_total/executions_total)) elapsed_time_avg,
trunc(decode(executions_total, 0, 0, iowait_total/executions_total)) iowait_time_avg,
trunc(decode(executions_total, 0, 0, clwait_total/executions_total)) clwait_time_avg,
trunc(decode(executions_total, 0, 0, apwait_total/executions_total)) apwait_time_avg,
trunc(decode(executions_total, 0, 0, ccwait_total/executions_total)) ccwait_time_avg,
trunc(decode(executions_total, 0, 0, plsexec_time_total/executions_total)) plsexec_time_avg,
trunc(decode(executions_total, 0, 0, javexec_time_total/executions_total)) javexec_time_avg 
from dba_hist_sqlstat
where sql_id = '3yw2fv0qk4jwk'
order by sql_id, snap_id;

select sum(executions_delta)
,      sum(elapsed_time_delta)
from dba_hist_sqlstat
where sql_id = '3yw2fv0qk4jwk';

select executions, elapsed_time, cpu_time from gv$sql where sql_id = 'sql_id' ;
--Something like this for history ?
select 
     snap_id, 
     sum(executions_total) 
from 
     dba_hist_sqlstat 
where 
     sql_id = 'SQL_ID'
group by 
     snap_id;


SELECT
    COUNT(*)
FROM
    ar_docs_receivables_v arv,
    iby_trxn_summaries_all trxn
WHERE
    arv.order_ext_id = trxn.initiator_extension_id
    AND   payer_notification_required = 'Y'
    AND   trxn.mbatchid =:1
;

select * from dba_objects where object_name like UPPER('%ar_document_lines%');
SELECT /*+ NO_MERGE(inv_header) PUSH_PRED(inv_header) PUSH_PRED(inv_line) OPT_PARAM('_optimizer_join_factorization','FALSE')*/
                          inv_line.line_number,
                          inv_line.po_number,
                          inv_line.line_type,
                          inv_line.description,
                          inv_line.extended_amount,inv_line.invoice_currency_code,inv_line.unit_price,
                          inv_line.quantity,
                          inv_line.unit_of_measure,
                          inv_line.inventory_item_id,
                          inv_line.discount_amount,
                          inv_line.sales_tax_amount,
                          inv_line.tax_rate,
                          inv_line.vat_tax_amount,
                          inv_line.doc_unique_ref
                        FROM ar_document_lines_v inv_line,ar_docs_receivables_v inv_header,iby_trxn_summaries_all txn
                        WHERE(inv_header.doc_unique_ref = inv_line.doc_unique_ref(+))
                         AND(txn.initiator_extension_id = inv_header.order_ext_id)
                         AND(txn.trxntypeid IN('8',   '9',   '100'))
                         AND(txn.mbatchid = :1);
                         
-- -- TEMP Monitoring
/*sid_n_serial	username	osuser	spid	module	program	mb_used	tablespace	nbr_statements
2943,64403	APPS	ebsprora	25559938	e:AR:cp:xxebs/XXAR1200	oracle@dcpspebsdb	108650	TEMP1	12
1474,21536	APPS	ebsprora	54068104	e:INV:cp:xxebs/XXINV0030	oracle@dcpspebsdb	3470	TEMP1	13
3051,65438	APPS	ebsprora	57279306	e:AR:cp:xxebs/XXAR0320	oracle@dcpspebsdb	2728	TEMP1	4
1186,51658	APPS	ebsprora	7407096	e:SYSADMIN:gsm:WFMLRSVC:10006:InboundThread1	oracle@dcpspebsdb	734	TEMP1	2
919,48428	APPS	ebsprora	34341624	e:AR:cp:xxebs/XXAR1200	oracle@dcpspebsdb	446	TEMP1	5
 */
 
 --Query dans OEM
select 
       h.tablespace_name as tablespace_name,
       100 - round((sum((h.bytes_free + h.bytes_used) - nvl(p.bytes_used, 0)) / sum(h.bytes_used + h.bytes_free)) * 100) pct_used
from   sys.gv_$TEMP_SPACE_HEADER h, sys.gv_$Temp_extent_pool p, dba_temp_files f
where  p.file_id(+) = h.file_id
and    p.tablespace_name(+) = h.tablespace_name
and    f.file_id = h.file_id
and    f.tablespace_name = h.tablespace_name
group by h.tablespace_name
ORDER BY 1 asc ;

--tablespace usage
select tablespace,
       mb_total,
       mb_used,
       mb_free,
       round((mb_free/mb_total*100),0) pct_free,
       round((mb_used/mb_total*100),0) pct_used
from   (
        SELECT   A.tablespace_name tablespace, 
                 D.mb_total,
                 SUM (A.used_blocks * D.block_size) / 1024 / 1024 mb_used,
                 D.mb_total - SUM (A.used_blocks * D.block_size) / 1024 / 1024 mb_free
        FROM     v$sort_segment A,
                 (SELECT B.name, C.block_size, SUM (C.bytes) / 1024 / 1024 mb_total
                  FROM v$tablespace B, v$tempfile C
                  WHERE B.ts#= C.ts#
                  GROUP BY B.name, C.block_size) D
        WHERE    A.tablespace_name = D.name
        GROUP by A.tablespace_name, D.mb_total)
order by round((mb_used/mb_total*100),0) desc;

--tablespace usage by session
select   s.sid|| ',' || s.serial# sid_n_serial, 
         s.username, 
         s.osuser, 
         p.spid, 
         s.module,
         p.program, 
         sum (t.blocks) * tbs.block_size / 1024 / 1024 mb_used, 
         t.tablespace,
         count(*) nbr_statements
from     v$sort_usage   t, 
         v$session       s,  
         dba_tablespaces tbs, 
         v$process      p
where    t.session_addr = s.saddr
and      s.paddr = p.addr
and      t.tablespace =tbs.tablespace_name
group by s.sid,  
         s.serial#, 
         s.username, 
         s.osuser, 
         p.spid, 
         s.module,
         p.program, 
         tbs.block_size, 
         t.tablespace
order by 7 desc ;