-- Pending requests at specific moment in time
select
fcr.request_id,
usr.user_name FND_USER,
fcpt.user_concurrent_program_name,
fcr.description,
--fcp.concurrent_program_name pname,
fcr.root_request_id,
To_Char(fcr.requested_start_date, 'DD-MON-YYYY HH24:MI:SS') requested_start_date,
To_Char(fcr.actual_start_date, 'DD-MON-YYYY HH24:MI:SS') actual_start_date,
TO_CHAR(fcr.Actual_Completion_Date, 'DD-MON-YYYY HH24:MI:SS')Actual_Completion_Date,
round((fcr.actual_start_date - fcr.requested_start_date)*60*24,2) wait_time,
round((fcr.actual_completion_date - fcr.actual_start_date)*60*24,2) run_time,
que.concurrent_queue_name conc_que,
-- Final Phase--DECODE(fcr.phase_code, 'C', 'Completed', 'I', 'Inactive', 'P', 'Pending', 'R', 'Running', Fcr.Phase_Code) Phase_Code,
-- Final Phase--DECODE (Fcr.Status_Code, 'A', 'Waiting', 'B', 'Resuming', 'C', 'Normal', 'D', 'Cancelled', 'E', 'Error', 'F', 'Scheduled', 'G', 'Warning', 'H', 'On Hold', 'I', 'Normal', 'M', 'No Manager', 'Q', 'Standby', 'R', 'Normal', 'S', 'Suspended', 'T', 'Terminating', 'U', 'Disabled', 'W', 'Paused', 'X', 'Terminated', 'Z', 'Waiting', Fcr.Status_Code) Status_Code,
fcr.completion_text,
fcr.argument_text args,
fcr.logfile_name,
fcr.outfile_name
from
fnd_concurrent_queues que,
fnd_user usr,
fnd_concurrent_programs fcp,
fnd_concurrent_requests fcr,
fnd_concurrent_processes fcps,
fnd_concurrent_programs_tl fcpt
where
--ALL PENDING at that specific moment
fcr.requested_start_date <= to_date(:specific_date_time, 'DD-MON-YYYY HH24:MI:SS')
and actual_start_date > to_date(:specific_date_time, 'DD-MON-YYYY HH24:MI:SS')
and To_Char(fcr.requested_start_date,'HH24:MI:SS') <> '00:00:00'
and round((fcr.actual_start_date - fcr.requested_start_date)*60*24,2) > 4
--and fcr.root_request_id is not null
--(actual_start_date between to_date('&start_date', 'DD-MON-YYYY HH24:MI:SS')and to_date('&end_date', 'DD-MON-YYYY HH24:MI:SS')
--    or actual_completion_date between to_date('&start_dte', 'DD-MON-YYYY HH24:MI:SS')
--and fcp.concurrent_program_name = 'WSHINTERFACES'
and que.application_id= fcps.queue_application_id
and que.concurrent_queue_id = fcps.concurrent_queue_id
and fcr.controlling_manager= fcps.concurrent_process_id
and usr.user_id = fcr.requested_by
and fcp.concurrent_program_id = fcr.concurrent_program_id
and fcp.application_id = fcr.program_application_id
and fcp.concurrent_program_name not in ('ACTIVATE','ABORT','DEACTIVATE','VERIFY')
AND fcr.concurrent_program_id = fcpt.concurrent_program_id
AND fcpt.language = USERENV ('LANG')
order by fcr.requested_start_date;


--==================
--======================
--=======================

WITH
--============================== 1 PENDING in time ===================================
PENDINGinTIME as
-- Pending requests at specific moment in time
(select fcr.CONCURRENT_PROGRAM_ID,fcr.request_date,
fcr.request_id,
usr.user_name FND_USER,
fcpt.user_concurrent_program_name,
fcr.description,
--fcp.concurrent_program_name pname,
fcr.root_request_id,
To_Char(fcr.requested_start_date, 'DD-MON-YYYY HH24:MI:SS') requested_start_date,
To_Char(fcr.actual_start_date, 'DD-MON-YYYY HH24:MI:SS') actual_start_date,
TO_CHAR(fcr.Actual_Completion_Date, 'DD-MON-YYYY HH24:MI:SS')Actual_Completion_Date,
round((fcr.actual_start_date - fcr.requested_start_date)*60*24,2) wait_time,
round((fcr.actual_completion_date - fcr.actual_start_date)*60*24,2) run_time,
que.concurrent_queue_name conc_que,
-- Final Phase--DECODE(fcr.phase_code, 'C', 'Completed', 'I', 'Inactive', 'P', 'Pending', 'R', 'Running', Fcr.Phase_Code) Phase_Code,
-- Final Phase--DECODE (Fcr.Status_Code, 'A', 'Waiting', 'B', 'Resuming', 'C', 'Normal', 'D', 'Cancelled', 'E', 'Error', 'F', 'Scheduled', 'G', 'Warning', 'H', 'On Hold', 'I', 'Normal', 'M', 'No Manager', 'Q', 'Standby', 'R', 'Normal', 'S', 'Suspended', 'T', 'Terminating', 'U', 'Disabled', 'W', 'Paused', 'X', 'Terminated', 'Z', 'Waiting', Fcr.Status_Code) Status_Code,
fcr.completion_text,
fcr.argument_text args,
fcr.logfile_name,
fcr.outfile_name
from
fnd_concurrent_queues que,
fnd_user usr,
fnd_concurrent_programs fcp,
fnd_concurrent_requests fcr,
fnd_concurrent_processes fcps,
fnd_concurrent_programs_tl fcpt
where
--ALL PENDING at that specific moment
fcr.requested_start_date <= to_date(:specific_date_time, 'DD-MON-YYYY HH24:MI:SS')
and actual_start_date > to_date(:specific_date_time, 'DD-MON-YYYY HH24:MI:SS')
and To_Char(fcr.requested_start_date,'HH24:MI:SS') <> '00:00:00'
and round((fcr.actual_start_date - fcr.requested_start_date)*60*24,2) > 4
--and fcr.root_request_id is not null
--(actual_start_date between to_date('&start_date', 'DD-MON-YYYY HH24:MI:SS')and to_date('&end_date', 'DD-MON-YYYY HH24:MI:SS')
--    or actual_completion_date between to_date('&start_dte', 'DD-MON-YYYY HH24:MI:SS')
--and fcp.concurrent_program_name = 'WSHINTERFACES'
and que.application_id= fcps.queue_application_id
and que.concurrent_queue_id = fcps.concurrent_queue_id
and fcr.controlling_manager= fcps.concurrent_process_id
and usr.user_id = fcr.requested_by
and fcp.concurrent_program_id = fcr.concurrent_program_id
and fcp.application_id = fcr.program_application_id
and fcp.concurrent_program_name not in ('ACTIVATE','ABORT','DEACTIVATE','VERIFY')
AND fcr.concurrent_program_id = fcpt.concurrent_program_id
AND fcpt.language = USERENV ('LANG')
order by fcr.requested_start_date),
--============================== 2 Running in time ===================================
RUNNINGinTIME as 
(-- Running requests at specific moment in time
select fcr.CONCURRENT_PROGRAM_ID,fcr.request_date,
fcr.request_id,
usr.user_name FND_USER,
fcpt.user_concurrent_program_name,
fcr.description,
--fcp.concurrent_program_name pname,
fcr.root_request_id,
To_Char(fcr.requested_start_date, 'DD-MON-YYYY HH24:MI:SS') requested_start_date,
To_Char(fcr.actual_start_date, 'DD-MON-YYYY HH24:MI:SS') actual_start_date,
TO_CHAR(fcr.Actual_Completion_Date, 'DD-MON-YYYY HH24:MI:SS')Actual_Completion_Date,
round((fcr.actual_start_date - fcr.requested_start_date)*60*24,2) wait_time,
round((fcr.actual_completion_date - fcr.actual_start_date)*60*24,2) run_time,
que.concurrent_queue_name conc_que,
-- Final Phase--DECODE(fcr.phase_code, 'C', 'Completed', 'I', 'Inactive', 'P', 'Pending', 'R', 'Running', Fcr.Phase_Code) Phase_Code,
-- Final Phase--DECODE (Fcr.Status_Code, 'A', 'Waiting', 'B', 'Resuming', 'C', 'Normal', 'D', 'Cancelled', 'E', 'Error', 'F', 'Scheduled', 'G', 'Warning', 'H', 'On Hold', 'I', 'Normal', 'M', 'No Manager', 'Q', 'Standby', 'R', 'Normal', 'S', 'Suspended', 'T', 'Terminating', 'U', 'Disabled', 'W', 'Paused', 'X', 'Terminated', 'Z', 'Waiting', Fcr.Status_Code) Status_Code,
fcr.completion_text,
fcr.argument_text args,
fcr.logfile_name,
fcr.outfile_name
from
fnd_concurrent_queues que,
fnd_user usr,
fnd_concurrent_programs fcp,
fnd_concurrent_requests fcr,
fnd_concurrent_processes fcps,
fnd_concurrent_programs_tl fcpt
where
--ALL PENDING at that specific moment
fcr.actual_start_date <= to_date(:specific_date_time, 'DD-MON-YYYY HH24:MI:SS')
and actual_completion_date > to_date(:specific_date_time, 'DD-MON-YYYY HH24:MI:SS')
and To_Char(fcr.requested_start_date,'HH24:MI:SS') <> '00:00:00'
and round((fcr.actual_start_date - fcr.requested_start_date)*60*24,2) > 4
--and fcr.root_request_id is not null
--(actual_start_date between to_date('&start_date', 'DD-MON-YYYY HH24:MI:SS')and to_date('&end_date', 'DD-MON-YYYY HH24:MI:SS')
--    or actual_completion_date between to_date('&start_dte', 'DD-MON-YYYY HH24:MI:SS')
--and fcp.concurrent_program_name = 'WSHINTERFACES'
and que.application_id= fcps.queue_application_id
and que.concurrent_queue_id = fcps.concurrent_queue_id
and fcr.controlling_manager= fcps.concurrent_process_id
and usr.user_id = fcr.requested_by
and fcp.concurrent_program_id = fcr.concurrent_program_id
and fcp.application_id = fcr.program_application_id
and fcp.concurrent_program_name not in ('ACTIVATE','ABORT','DEACTIVATE','VERIFY')
AND fcr.concurrent_program_id = fcpt.concurrent_program_id
AND fcpt.language = USERENV ('LANG')
order by fcr.requested_start_date)
--============================  Incompatible:  ==================================
--Pending Standby Blocked By Running
SELECT
  
 -- r.PARENT_REQUEST_ID "Parent Req_ID" ,
 -- inc.Incompatible_Prog "Running Prg that Blocks others",
 -- TO_CHAR(r.request_date,'DD-MON-YY HH24:MI:SS') request_date_r ,
 -- TO_CHAR(r.REQUESTED_START_DATE,'DD-MON-YY HH24:MI:SS') REQUESTED_START_DATE_r ,
-- TO_CHAR(r.ACTUAL_START_DATE,'DD-MON-YY HH24:MI:SS') actual_start_date ,
 -- TO_CHAR(r.ACTUAL_COMPLETION_DATE,'DD-MON-YY HH24:MI:SS') actual_completion_date ,
 -- Extract(DAY FROM (NUMTODSINTERVAL((r.ACTUAL_START_DATE - r.REQUESTED_START_DATE)*24*60,'minute'))) delayed_days_r ,
 -- TO_CHAR (TRUNC(sysdate)                                 + NUMTODSINTERVAL((r.ACTUAL_START_DATE - r.REQUESTED_START_DATE)*24*60,'minute'),'HH24:MI:SS') requested_start_date_delay ,
 -- DECODE(r.phase_code, 'C', 'Completed', 'I', 'Inactive', 'P', 'Pending', 'R', 'Running', r.Phase_Code) Phase_Code ,
 -- DECODE (r.Status_Code, 'A', 'Waiting', 'B', 'Resuming', 'C', 'Normal', 'D', 'Cancelled', 'E', 'Error', 'F', 'Scheduled', 'G', 'Warning', 'H', 'On Hold', 'I', 'Normal', 'M', 'No Manager', 'Q', 'Standby', 'R', 'Normal', 'S', 'Suspended', 'T', 'Terminating', 'U', 'Disabled', 'W', 'Paused', 'X', 'Terminated', 'Z', 'Waiting', r.Status_Code) Status_Code ,
 -- r.argument_text ,
  --r.RESUBMIT_INTERVAL_TYPE_CODE,
  --r.RESUBMIT_INTERVAL,
  --r.RESUBMIT_INTERVAL_UNIT_CODE ,
 -- inc.Incompatible_Short "Running_Short",
     w.request_id "Waiting_REQUEST_ID",
  inc.Short "Waiting_Short" ,
   inc.Prog Waiting_Program ,
--  DECODE(w.phase_code, 'C', 'Completed', 'I', 'Inactive', 'P', 'Pending', 'R', 'Running', w.Phase_Code) Phase_Waiting ,
 -- DECODE (w.Status_Code, 'A', 'Waiting', 'B', 'Resuming', 'C', 'Normal', 'D', 'Cancelled', 'E', 'Error', 'F', 'Scheduled', 'G', 'Warning', 'H', 'On Hold', 'I', 'Normal', 'M', 'No Manager', 'Q', 'Standby', 'R', 'Normal', 'S', 'Suspended', 'T', 'Terminating', 'U', 'Disabled', 'W', 'Paused', 'X', 'Terminated', 'Z', 'Waiting', w.Status_Code) Status_Waiting ,
 -- w.HOLD_FLAG ,
--  TO_CHAR(w.request_date,'DD-MON-YY HH24:MI:SS') request_date_w ,
 -- TO_CHAR(w.REQUESTED_START_DATE,'DD-MON-YY HH24:MI:SS') REQUESTED_START_DATE_w,
 -- Extract(DAY FROM (NUMTODSINTERVAL((sysdate - w.REQUESTED_START_DATE)*24*60,'minute'))) Waiting_Days ,
--  TO_CHAR(TRUNC(sysdate)+ NUMTODSINTERVAL((sysdate - w.REQUESTED_START_DATE)*1440,'minute'),'HH24:MI:SS') Waiting_time ,
  '-- at '||:specific_date_time||' Blocked By -->' "-- Blocked By -->" ,
  r.request_id "Running_REQUEST_ID",
  inc.Incompatible_Short "Running_Short",
  inc.Incompatible_Prog "Running Prg that Blocks others"
FROM 
  PENDINGinTIME w,
  RUNNINGinTIME r,
  --#Check Incompatibilities:
  (
  SELECT a1.user_concurrent_program_name Prog,
    a3.concurrent_program_name Short,
    a1.CONCURRENT_PROGRAM_ID CONC_PR_ID,
    b1.CONCURRENT_PROGRAM_ID Incomp_CONC_PR_ID,
    b1.user_concurrent_program_name Incompatible_Prog,
    b3.concurrent_program_name Incompatible_Short,
    DECODE (to_run_type, 'P', 'Program', 'S', 'Request set', 'UNKNOWN' ) incompatible_type,
    b2.application_name "Incompatible App"
  FROM apps.fnd_concurrent_program_serial cps ,
    apps.fnd_concurrent_programs_tl a1 ,
    apps.fnd_application_tl a2 ,
    apps.fnd_concurrent_programs a3 ,
    apps.fnd_concurrent_programs_tl b1 ,
    apps.fnd_application_tl b2 ,
    apps.fnd_concurrent_programs b3
  WHERE a1.application_id      = cps.running_application_id
  AND a1.concurrent_program_id = cps.running_concurrent_program_id
  AND a2.application_id        = cps.running_application_id
  AND a3.concurrent_program_id = a1.concurrent_program_id
  AND b1.application_id        = cps.to_run_application_id
  AND b1.concurrent_program_id = cps.to_run_concurrent_program_id
  AND b2.application_id        = cps.to_run_application_id
  AND b3.concurrent_program_id = b1.concurrent_program_id
  AND a1.language              = 'US'
  AND a2.language              = 'US'
  AND b1.language              = 'US'
  AND b2.language              = 'US'
  ) inc
WHERE 1=1
--and w.HOLD_FLAG                                   ='N'
AND w.CONCURRENT_PROGRAM_ID                         = inc.CONC_PR_ID --(Waiting)
AND r.CONCURRENT_PROGRAM_ID                        = inc.Incomp_CONC_PR_ID --(Running)
ORDER BY w.requested_start_date,
  w.ACTUAL_COMPLETION_DATE DESC;
  
  
  --- CHECK QUEUE in TIME how to ... ????
  
  
  
  
  
  
  
  
  
  
  
  
  
  
