Declare
v_sqlid VARCHAR2(13);
v_num number;
BEGIN
dbms_output.put_line('SQL_ID       '||' '|| 'PLAN_HASH_VALUE' || ' ' || 'SQL_HANDLE                    ' || ' ' || 'PLAN_NAME                       ' || ' ' || 'ENABLED' || ' ' || 'ACCEPTED' || ' ' || 'FIXED' || ' ' || 'CREATED           ' || ' ' || 'LAST_EXECUTED     ');
dbms_output.put_line('-------------'||' '|| '---------------' || ' ' || '------------------------------' || ' ' || '--------------------------------' || ' ' || '-------' || ' ' || '--------' || ' ' || '-----' || ' ' || '------------------' || ' ' || '------------------');
for a in 
(SELECT
    sql_handle,
    plan_name,
    TRIM(substr(g.plan_table_output,instr(g.plan_table_output,':') + 1) ) plan_hash_value,
    LAST_EXECUTED,CREATED, enabled,accepted,fixed,
    sql_text
FROM
    ( SELECT
            t.*,
            c.sql_handle,
            c.plan_name,
            To_Char(LAST_EXECUTED,'DD-MON-YY HH24:MI:SS') LAST_EXECUTED,
            To_Char(CREATED,'DD-MON-YY HH24:MI:SS') CREATED, enabled,accepted,fixed,
            c.sql_text
        FROM
            dba_sql_plan_baselines c,
            TABLE ( dbms_xplan.display_sql_plan_baseline(c.sql_handle,c.plan_name) ) t
/*where c.sql_handle = '&sql_handle'*/
--where FIXED='YES'
    ) g
WHERE    plan_table_output LIKE 'Plan hash value%')
loop
v_num := to_number(sys.UTL_RAW.reverse(sys.UTL_RAW.SUBSTR(sys.dbms_crypto.hash(src => UTL_I18N.string_to_raw(a.sql_text || chr(0),'AL32UTF8'), typ => 2),9,4)) || sys.UTL_RAW.reverse(sys.UTL_RAW.SUBSTR(sys.dbms_crypto.hash(src => UTL_I18N.string_to_raw(a.sql_text || chr(0),'AL32UTF8'), typ => 2),13,4)),RPAD('x', 16, 'x'));
v_sqlid :='';
FOR i IN 0 .. FLOOR(LN(v_num) / LN(32))
LOOP
v_sqlid := SUBSTR('0123456789abcdfghjkmnpqrstuvwxyz',FLOOR(MOD(v_num / POWER(32, i), 32)) + 1,1) || v_sqlid;
END LOOP;
dbms_output.put_line(v_sqlid ||' ' || rpad(a.plan_hash_value,15) || ' ' || rpad(a.sql_handle,30) ||  ' ' || rpad(a.plan_name,30)||'   ' || rpad(a.ENABLED,8)||' ' || rpad(a.ACCEPTED,7)||' ' || rpad(a.FIXED,5)||' ' || rpad(a.CREATED,18)||'  ' || rpad(a.LAST_EXECUTED,18));
end loop;
end;
/
