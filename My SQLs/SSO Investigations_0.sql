select * from dba_directories;-- where directory_name='AGR_INTERFACES_ARCH_OUTBOUND';
select count(*) from APPS.FND_USER_PREFERENCES
 where MODULE_NAME='LDAP_SYNCH';
 
select z.tablespace_name tspace_name, 
(select sum(b.bytes)/(1024*1024) from dba_free_space b where b.tablespace_name = z.tablespace_name) MB_Free,
(select sum(a.bytes)/(1024*1024) from dba_data_files a where a.tablespace_name = z.tablespace_name) MB_Total,
((select sum(b.bytes)/(1024*1024) from dba_free_space b where b.tablespace_name = z.tablespace_name)/(select sum(a.bytes)/(1024*1024) from dba_data_files a where a.tablespace_name = z.tablespace_name)*100) Pcent_Free
from dba_tablespaces z
order by z.tablespace_name;

select adop_session_id, apply_status, cleanup_status
from AD_ADOP_SESSIONS
where adop_session_id = (select max(adop_session_id) from AD_ADOP_SESSIONS);

select text from dba_source where name = 'FND_SSO_REGISTRATION' and line < 3;

SELECT * FROM AD_BUGS WHERE BUG_NUMBER IN ('24008856','21229697','24691100');

--============== http://www.oraworld.co.uk/ora20001-not-able-to-create-users-in-oracle-app-unable-to-call-fnd_ldap_wrapper-create_user-or-update_user/

SELECT * FROM fnd_user_preferences WHERE
user_name='#INTERNAL' AND module_name='OID_CONF';

--==============https://myoraclemadeeasy.wordpress.com/2015/06/03/users-not-syncing-from-oid-to-ebs-oid_conf-missing/
select module_name, count(*) from apps.fnd_user_preferences where user_name='#INTERNAL' group by module_name;

/*SQL> execute fnd_oid_plug.setPlugin;
BEGIN fnd_oid_plug.setPlugin; END;

*
ERROR at line 1:
ORA-31203: DBMS_LDAP: PL/SQL - Init Failed.
ORA-06512: at "APPS.FND_OID_PLUG", line 916
ORA-06512: at line 1*/

Select * from dba_source where name= 'FND_OID_PLUG' and line between 875 and 930;
--==============
--1. Determine the current AppsDN username and password stored in EBS
select fnd_preference.get('#INTERNAL', 'LDAP_SYNCH','USERNAME') Apps_Instance_OID_Account from dual;
select fnd_preference.eget('#INTERNAL', 'LDAP_SYNCH','EPWD','LDAP_PWD') Apps_Password from dual;

-- Marc Check Edition
SELECT property_value
FROM   database_properties
WHERE  property_name = 'DEFAULT_EDITION';

SELECT * FROM dba_editions;

SELECT SYS_CONTEXT('USERENV', 'SESSION_EDITION_NAME') AS edition FROM dual;

ALTER SESSION SET EDITION = ora$base; 


--------- Check the Profiles SSO --------------------
SELECT * FROM fnd_user_preferences WHERE
user_name='#INTERNAL' AND module_name='OID_CONF';

UPDATE FND_PROFILE_OPTION_VALUES FPOV					
SET FPOV.PROFILE_OPTION_VALUE = (select name from v$database) || ' ('||nvl('&CLONE_DATE',to_char(sysdate,'YYYY-MM-DD'))||')'					
WHERE EXISTS (SELECT NULL FROM FND_PROFILE_OPTIONS FPO 					
              WHERE FPOV.PROFILE_OPTION_ID = FPO.PROFILE_OPTION_ID					
              AND FPO.PROFILE_OPTION_NAME = 'APPS_SSO_LDAP_SYNC');	
              
SELECT a.*			
  FROM apps.fnd_profile_option_values a, apps.fnd_profile_options b					
  where b.profile_option_name = 'APPS_SSO_LDAP_SYNC'					
  and a.application_id = b.application_id					
  and a.profile_option_id = b.profile_option_id
  and a.LEVEL_ID='10001';					
-- APPS_SSO_LDAP_SYNC

SELECT NULL FROM FND_PROFILE_OPTIONS FPO 					
              WHERE FPOV.PROFILE_OPTION_ID = FPO.PROFILE_OPTION_ID					
              AND FPO.PROFILE_OPTION_NAME = 'APPS_SSO_LDAP_SYNC';

DECLARE
stat BOOLEAN;
BEGIN
stat := FND_PROFILE.SAVE('APPS_SSO_LDAP_SYNC','Y','SITE');
IF stat THEN
dbms_output.put_line( 'Profile APPS_SSO_LDAP_SYNC updated with Enabled' );
ELSE
dbms_output.put_line( 'Profile APPS_SSO_LDAP_SYNC could NOT be updated with Enabled' );
commit;
END IF;
END;
/ 			

DECLARE	
stat BOOLEAN;	
BEGIN	
stat := FND_PROFILE.SAVE('APPS_SSO_OID_IDENTITY','Y','SITE');	
IF stat THEN	
dbms_output.put_line( 'Profile APPS_SSO_OID_IDENTITY updated with Enabled ' );	
ELSE	
dbms_output.put_line( 'Profile APPS_SSO_OID_IDENTITY could NOT be updated with Enabled' );	
commit;	
END IF;	
END;	
/ 	
	

DECLARE	
stat BOOLEAN;	
BEGIN	
stat := FND_PROFILE.SAVE('APPS_SSO_LINK_SAME_NAMES','Y','SITE');	
IF stat THEN	
dbms_output.put_line( 'Profile APPS_SSO_LINK_SAME_NAMES updated with Enabled' );	
ELSE	
dbms_output.put_line( 'Profile APPS_SSO_LINK_SAME_NAMES could NOT be updated with Enabled' );	
commit;	
END IF;	
END;	
/ 	
	

DECLARE	
stat BOOLEAN;	
begin	
stat := FND_PROFILE.SAVE('APPS_SSO', 'SSWA_SSO', 'SITE');	
IF stat THEN	
dbms_output.put_line( 'Profile APPS_SSO updated with SSWA_SSO' );	
ELSE	
dbms_output.put_line( 'Profile APPS_SSO could NOT be updated with SSWA_SSO' );	
commit;	
END IF;	
end;	
/ 	
	

DECLARE	
stat BOOLEAN;	
BEGIN	
stat := FND_PROFILE.SAVE('APPS_SSO_AUTO_LINK_USER','Y','SITE');	
IF stat THEN	
dbms_output.put_line( 'Profile APPS_SSO_AUTO_LINK_USER updated with Enabled' );	
ELSE	
dbms_output.put_line( 'Profile APPS_SSO_AUTO_LINK_USER could NOT be updated with Enabled' );	
commit;	
END IF;	
END;	
/ 	

DECLARE	
stat BOOLEAN;	
BEGIN	
stat := FND_PROFILE.SAVE('APPS_SSO_LOCAL_LOGIN','BOTH','USER');	
IF stat THEN	
dbms_output.put_line( 'Profile APPS_SSO_LOCAL_LOGIN updated with Enabled' );	
ELSE	
dbms_output.put_line( 'Profile APPS_SSO_LOCAL_LOGIN could NOT be updated with Enabled' );	
commit;	
END IF;	
END;	
/
COMMIT;

DECLARE
stat boolean;
BEGIN
dbms_output.disable;
dbms_output.enable(100000);
stat := FND_PROFILE.SAVE('APPS_SSO_LOCAL_LOGIN','SSO' ,'USER');
IF stat THEN dbms_output.put_line( 'profile updated' );
ELSE         dbms_output.put_line( 'profile NOT updated' );
END IF;
END;
/
commit;



--APPS_SSO_LOCAL_LOGIN


select p.profile_option_name SHORT_NAME,
n.user_profile_option_name NAME,
decode(v.level_id,
10001, 'Site',
10002, 'Application',
10003, 'Responsibility',
10004, 'User',
10005, 'Server',
10006, 'Org',
10007, decode(to_char(v.level_value2), '-1', 'Responsibility',
decode(to_char(v.level_value), '-1', 'Server',
'Server+Resp')),
'UnDef') LEVEL_SET,
decode(to_char(v.level_id),
'10001', '',
'10002', app.application_short_name,
'10003', rsp.responsibility_key,
'10004', usr.user_name,
'10005', svr.node_name,
'10006', org.name,
'10007', decode(to_char(v.level_value2), '-1', rsp.responsibility_key,
decode(to_char(v.level_value), '-1',
(select node_name from fnd_nodes
where node_id = v.level_value2),
(select node_name from fnd_nodes
where node_id = v.level_value2)||'-'||rsp.responsibility_key)),
'UnDef') "CONTEXT",
v.profile_option_value VALUE,
p.ZD_EDITION_NAME
from fnd_profile_options p,
fnd_profile_option_values v,
fnd_profile_options_tl n,
fnd_user usr,
fnd_application app,
fnd_responsibility rsp,
fnd_nodes svr,
hr_operating_units org
where p.profile_option_id = v.profile_option_id (+)
and p.profile_option_name = n.profile_option_name
and upper(p.profile_option_name) 
    in ( select profile_option_name
    from fnd_profile_options_tl 
    where 1=1
    --upper(user_profile_option_name) like upper('%&user_profile_name%'))
    --upper(profile_option_name) like upper('%&Profile_SHORT_NAME%')) --APPS_AUTH_AGENT: https://ebsprod.agropur.com:443/accessgate/
    AND upper(profile_option_name) in ('APPS_AUTH_AGENT','APPS_SSO_AUTO_LINK_USER','APPS_SSO','APPS_SSO_LINK_SAME_NAMES','APPS_SSO_OID_IDENTITY','APPS_SSO_LDAP_SYNC')
    --and n.user_profile_option_name = 'Applications SSO Login Types')
    --AND upper(profile_option_name) in ('APPS_SSO_LOCAL_LOGIN')
    )
and usr.user_id (+) = v.level_value
and rsp.application_id (+) = v.level_value_application_id
and rsp.responsibility_id (+) = v.level_value
and app.application_id (+) = v.level_value
and svr.node_id (+) = v.level_value
and org.organization_id (+) = v.level_value
order by 4,short_name, user_profile_option_name, level_id, level_set;
--$FND_TOP/patch/115/sql/fndssouu.sql�


select * from fnd_user where user_name = 'VLCASAPC';
--19712
-- Application SSO LDAP Synchronization

--APPS_SSO_LDAP_SYNC
--APPS_SSO_AUTO_LINK_USER
--APPS_SSO_OID_IDENTITY
--APPS_SSO_LINK_SAME_NAMES
--APPS_SSO


--
select fnd_preference.get('#INTERNAL', 'LDAP_SYNCH','USERNAME') Apps_Instance_OID_Account from dual;
        --orclapplicationcommonname=ebsprod_dcpspebsapps,cn=ebusiness,cn=products,cn=oraclecontext,dc=agropur,dc=com
        --orclapplicationcommonname=ebsdev9_dcssdebsapps,cn=ebusiness,cn=products,cn=oraclecontext,dc=agropur,dc=com
        --orclapplicationcommonname=ebsdevc_dcssaebsapps,cn=ebusiness,cn=products,cn=oraclecontext,dc=agropur,dc=com

select fnd_preference.eget('#INTERNAL', 'LDAP_SYNCH','EPWD','LDAP_PWD') Apps_Password from dual;
           --<PASSWORD>
/*on apps Tier: as ebsprora:*/
/* 
ldapbind -h dcsssoidoam.agropur.com -p 3060 -D orclapplicationcommonname=ebsdev9_dcssdebsapps,cn=ebusiness,cn=products,cn=oraclecontext,dc=agropur,dc=com -w welc0me01d                                                           <
*/
/* 
RESULT:
ldap_bind: Invalid credentials
ldap_bind: additional info: Password Policy Error :9000: GSL_PWDEXPIRED_EXCP :Your Password has expired. Please contact the Administrator to change your password.
*/

/*
EBSPROD1_R12# ldapbind -h dcpspoidoam.agropur.com -p 3060 -D orclapplicationcommonname=ebsprod_dcpspebsapps,cn=ebusiness,cn=products,cn=oracleconte>
bind successful
*/

----------------------------------------------------------------------------------------------------------------------------------------
------------------------------Comment d�tacher un usager dont le compte EBS a �t� reli� � un compte AD
-----------------------------------------------------------------------------------------------------------
--Valider avec quel compte l'usager est reli� (obtenir la valeur de USER_GUID)	
	select user_name, user_guid from fnd_user where user_name like 'VLCASAPC%';
    select user_name, user_guid from fnd_user where user_name like 'BEGOSSEL%';
    select user_name, user_guid from fnd_user where user_guid is not null;
    
    -- ldapbind -h ADHOST -p 389 -D "CN=John.Doe,OU=employees,dc=domain,dc=com" -w password
    
--bind successful
	
--Sur le serveur OID, ex�cuter la commande suivate (remplacer la valeur de USER_GUID par celle obtenue pr�c�demment)	
--	ldapsearch -v -h dcpspoidoam -p 3060 -D "cn=orcladmin" -w password -b "" -s sub "orclguid=5E1E0B53D8AC0074E0530AD2A6280074" sn uid orclguid orclactivestartdate orclactiveenddate orclisenabled
--  ldapsearch -v -h dcsssoidoam -p 3060 -D "cn=orcladmin" -w password -b "" -s sub "orclguid=5E1E0B53D8AC0074E0530AD2A6280074" sn uid orclguid orclactivestartdate orclactiveenddate orclisenabled
	
--D�tacher le compte	
--	sqlplus apps @$FND_TOP/patch/115/sql/fndssouu.sql <EBS_USERNAME>
	
--La valeur de USER_GUID devrait maintenant �tre nulle	
	select user_guid from fnd_user where user_name = 'MAJALBER';
	
--On peut aussi faire l'inverse et chercher le compte AD	
--	ldapsearch -v -h dcpspoidoam -p 3060 -D "cn=orcladmin" -w password -b "" -s sub "uid=USERNAME" sn uid orclguid orclactivestartdate orclactiveenddate orclisenabled
-- ldapsearch -v -h dcsssoidoam -p 3060 -D "cn=orcladmin" -w password -b "welc0me01d" -s sub "uid=vlcasapc" sn uid orclguid orclactivestartdate orclactiveenddate orclisenabled
	
--Puis trouver le compte EBS Associ�	
	select user_name from fnd_user where user_guid = 'user_guid_from_ldapsearch';
    
--check user ldap:  A new SSO user is not able to login to Ebusiness Suite 12.2.6 using the SSO Login screen (Doc ID 2385236.1)
/*

EBSPROD1_R12# ldapsearch -v -h dcpspoidoam.agropur.com -p 3060 -D "cn=orcladmin" -w "welc0me01d" -b "" -s sub "uid=vlcasapc" uid orclguid;

ldapsearch -v -h dcsstoidoam.agropur.com -p 3060 -D "cn=orcladmin" -w "YbEhDi3z51yc" -b "" -s sub "uid=vlcasapc" uid orclguid;
ldapsearch -v -h dcpspoidoam.agropur.com -p 3060 -D "cn=orcladmin" -w "welc0me01d" -b "" -s sub "uid=vlcasapc" uid orclguid;
ldap_open( dcpspoidoam.agropur.com, 3060 )
filter pattern: uid=MAJALBER
returning: uid orclguid
filter is: (uid=MAJALBER)
cn=marie-soleil jalbert,ou=users,ou=hub,ou=can,ou=2-country,cn=users,dc=agropur,dc=com
uid=MAJALBER
orclguid=6C23BB27304D0A90E0530AD2A6280A90
1 matches
EBSPROD1_R12#

*/

--update apps.FND_USER set user_guid = '5E1E0B53D8AC0074E0530AD2A6280074' where USER_NAME = 'VLCASAPC';
--commit;

--check Profile  Applications SSO Login Types 


select fnd_preference.get('#INTERNAL', 'LDAP_SYNCH','USERNAME') Apps_Instance_OID_Account from dual;
--orclapplicationcommonname=ebsacce_dcsssebsapps,cn=ebusiness,cn=products,cn=oraclecontext,dc=agropur,dc=com
--orclapplicationcommonname=ebsprod,cn=ebusiness,cn=products,cn=oraclecontext,dc=agropur,dc=com
select fnd_preference.eget('#INTERNAL', 'LDAP_SYNCH','EPWD','LDAP_PWD') Apps_Password from dual;


select fnd_preference.get('#INTERNAL', 'LDAP_SYNCH','USERNAME') AppsDn from dual;
ldapbind -h dcpspoidoam.agropur.com -p 3060 -D orclapplicationcommonname=ebsprod,cn=ebusiness,cn=products,cn=oraclecontext,dc=agropur,dc=com -w welc0me01d
 select fnd_preference.eget('#INTERNAL', 'LDAP_SYNCH','EPWD','LDAP_PWD') AppsDN_Password from dual;
 
 
ldapsearch -h $OID_HOSTNAME           -p $OID_PORT -X -D "cn=orcladmin" -w $IAS_PASSWORD -b "cn=users,dc=agropur,dc=com" -s sub "objectclass=inetorgperson" > portal_users.xml
List des users dans AD:
ldapsearch -h dcpspoidoam -p 3060 -X -D "cn=orcladmin" -w "welc0me01d"  -s sub "objectclass=inetorgperson" > portal_users.xml
List all with orclisenabled
ldapsearch -h dcpspoidoam -p 3060 -X -D "cn=orcladmin" -w "welc0me01d"  -s sub "objectclass=inetorgperson" sn uid orclguid orclactivestartdate orclactiveenddate orclisenabled > portal_OID_users.xml

ldapsearch -v -h dcsssoidoam -p 3060 -D "cn=orcladmin" -w password -b "welc0me01d" -s sub "uid=vlcasapc" sn uid orclguid orclactivestartdate orclactiveenddate orclisenabled

-- Provisionning Profile:
ldapsearch -h dcpspoidoam -p 3060  -D "cn=orcladmin" -w "welc0me01d"  \
-b "cn=Provisioning Profiles, cn=Changelog Subscriber, cn=Oracle Internet Directory" \
-s sub "objectclass=*" "*" 

ldapsearch -h dcpspoidoam -p 3060 -D "cn=orcladmin" -w "welc0me01d"  \
-b "cn=EBusiness,cn=Products,cn=OracleContext,dc=agropur,dc=com" -s sub "objectclass=*" >Reg_to_OID.txt

ldapsearch -h dcpspoidoam -p 3060 -D "cn=orcladmin" -w "welc0me01d"  \
-b "cn=common, cn=products, cn=oracleContext" -s base "objectclass=*" orcldefaultSubscriber

===============================================
OID Quesries/ Scripts FAQ

February 20, 2008 / scripts / By Atul Kumar / 52 COMMENTS
Oracle Internet Directory (OID) is LDAP compliant directory server from Oracle. OID is part of Identity Management stack of Fusion Middleware (Infrastructure tier of Oracle Application Server) 

Here is list of commonly asked questions/scripts for OID

Q: How to find orasso (Single Sign-On) schema password.
A: orasso password is randomly generated and stored in OID. To find password run below query

$ORACLE_HOME/bin/ldapsearch -h $OIDHOST -p $OIDPORT-D "cn=orcladmin" -w "$OrcladminPASSWORD" -b "cn=IAS Infrastructure Databases,cn=IAS,cn=Products,cn=OracleContext" -s sub "orclResourceName=$DBUSERNAME" orclpasswordattribute

like

$ORACLE_HOME/bin/ldapsearch -h focusthread.com -p 389-D "cn=orcladmin" -w "welcome1" -b "cn=IAS Infrastructure Databases,cn=IAS,cn=Products,cn=OracleContext" -s sub "orclResourceName=orasso" orclpasswordattribute

Q: How to find port on which OID server is listening ?
A: on Infrastructure Tier $ORACLE_HOME/ldap/admin/ldap.ora  look for line
DIRECTORY_SERVERS= (focusthread.com:389:636)   �> This means OID is listening on port 389 NonSSL & 636 on SSL Port

or

In file $ORACLE_HOME/install/portlist.ini look for line
Oracle Internet Directory port = 389
Oracle Internet Directory (SSL) port = 636

Q: To find password policy in OID
A: $ORACLE_HOME/bin/ldapsearch -h $OIDHOST -p $OIDPORT -D cn=orcladmin -w $ORCLADMIN_PASSWD -b "cn=default,cn=pwdPolicies,cn=Common,cn=Products,cn=OracleContext" -s base "objectclass=*"

like

$ORACLE_HOME/bin/ldapsearch -h focusthread.com-p 389 -D cn=orcladmin -w welcome1 -b "cn=default,cn=pwdPolicies,cn=Common,cn=Products,cn=OracleContext,dc=com" 
-s base "objectclass=*" >> pwdPolicy.ldif

This will create file pwdPolicy.ldif in currently directory listing password policies

Q: How to query what all E-Business Suite Instance are registered in OID ? (useful for OID-Apps 11i/R12 Integration)

$ORACLE_HOME/bin/ldapsearch -h $OIDHOST -p $OIDPORT-D "cn=orcladmin" -w "$OIDPW" -b "cn=EBusiness,cn=Products,cn=OracleContext,dc=default_realm" -s sub "objectclass=*"

###
ldapsearch -h dcpspoidoam -p 3060 -D "cn=orcladmin" -w "welc0me01d"  \
-b "cn=EBusiness,cn=Products,cn=OracleContext,dc=agropur,dc=com" -s sub "objectclass=*" >Reg_to_OID.txt
####

like 
$ORACLE_HOME/bin/ldapsearch -h focusthread -p 389-D "cn=orcladmin" -w "welcome1" -b "cn=EBusiness,cn=Products,cn=OracleContext,dc=co,dc=uk" -v -s sub "objectclass=*" 

Q: Query for subscription list in OID ? (useful for OID-Apps 11i/R12 Integration)
A. $ORACLE_HOME/ldap/odi/bin/provsubtool.orc operation=LIST \
     ldap_host=host ldap_port=port \
     app_dn="ApplicationDN" realm_dn="SubscriberDN" \
     app_pwd=AppDN password

Q: How to find provisioning profiles (useful for OID-Apps 11i/R12 Integration)
A.ldapsearch -h host -p port-D cn=orcladmin -w password \
-b "cn=Provisioning Profiles, cn=Changelog Subscriber, cn=Oracle Internet Directory" \
-s sub "objectclass=*" "*" 
 
Q: How to search for various user attributes ?
A.ldapsearch -v -h "${Host}" -p ${Port} -D "cn=orcladmin" -w "${OIDManagerPasswd}" -b "" -s sub "uid=${AppsUser}*"  uid orclguid orclactivestartdate orclactiveenddate orclisenabled 

Q: How to find default OID realm
A. $ORACLE_HOME/bin/ldapsearch -h $OIDHOST -p $OIDPORT-D "cn=orcladmin" -w $ORCLADMIN_password-b "cn=common, cn=products, cn=oracleContext" -s base "objectclass=*" orcldefaultSubscriber

$ORACLE_HOME/bin/ldapsearch -h focusthread.com -p 389 -D "cn=orcladmin" -w welcome1 -b "cn=common, cn=products, cn=oracleContext" -s base "objectclass=*" orcldefaultSubscriber


select fnd_preference.get('#INTERNAL', 'LDAP_SYNCH','USERNAME') Apps_Instance_OID_Account from dual;
--orclapplicationcommonname=ebstest_dcsstdbapps,cn=ebusiness,cn=products,cn=oraclecontext,dc=agropur,dc=com
select fnd_preference.eget('#INTERNAL', 'LDAP_SYNCH','EPWD','LDAP_PWD') Apps_Password from dual;
--YbEhDi3z51yc



                C:\Users\vlcasapc>nslookup
                Serveur par d�faut :   vi-dns-dcs.agropur.com
                Address:  10.210.1.100
                
                > set type=all
                >  _ldap._tcp.dc._msdcs.agropur.com
                Serveur :   vi-dns-dcs.agropur.com
                Address:  10.210.1.100
                
                _ldap._tcp.dc._msdcs.agropur.com        SRV service location:
                          priority       = 0
                          weight         = 100
                          port           = 389
                          svr hostname   = hyaspdc.agropur.com



EBSTEST_R12# ldapsearch -v -h dcsstoidoam.agropur.com -p 3060 -D "cn=orcladmin" -w "YbEhDi3z51yc" -b "" -s sub "uid=vlcasapc" uid orclguid;
ldap_open( dcsstoidoam.agropur.com, 3060 )
filter pattern: uid=vlcasapc
returning: uid orclguid
filter is: (uid=vlcasapc)
cn=vladimir casapciuc,ou=externals,ou=hub,ou=can,ou=2-country,cn=users,dc=agropur,dc=com
uid=VLCASAPC
orclguid=5E1E0B53D8AC0074E0530AD2A6280074
1 matches
                        
OIDHOST:
ldapbind -h dcpspoidoam.agropur.com -p 3060 -D "CN=Vladimir Casapciuc,OU=Externals,OU=HUB,OU=CAN,OU=2-Country,DC=agropur,DC=com" -w MyADpassword
EBSTEST_R12# ldapbind -h dcsstoidoam.agropur.com -p 3060 -D "CN=Vladimir Casapciuc,OU=Externals,OU=HUB,OU=CAN,OU=2-Country,DC=agropur,DC=com" -w MyADpassword
ldap_bind: Invalid credentials

ldapbind -h dcsstoidoam.agropur.com -p 3060 -D "cn=vladimir casapciuc,ou=externals,ou=hub,ou=can,ou=2-country,cn=users,dc=agropur,dc=com" -w
bind successful

ADHOST :          
ldapbind -h dcsspdc01.agropur.com -p 389 -D "CN=Vladimir Casapciuc,OU=Externals,OU=HUB,OU=CAN,OU=2-Country,DC=agropur,DC=com" -w MyADpassword


