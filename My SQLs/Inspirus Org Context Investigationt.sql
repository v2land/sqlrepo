

--inventory organizations assigned to ones operating unit 
--run as apps
SELECT hou.NAME operating_unit_name,
hou.short_code,
hou.organization_id operating_unit_id,
hou.set_of_books_id,
hou.business_group_id,
ood.organization_name inventory_organization_name,
ood.organization_code Inv_organization_code,
ood.organization_id Inv_organization_id,
ood.chart_of_accounts_id
FROM apps.hr_operating_units hou,
apps.org_organization_definitions ood
WHERE 1 = 1
AND hou.organization_id = ood.operating_unit
ORDER BY hou.organization_id ASC;
--123
--121


begin
apps.mo_global.set_policy_context('S',121);
end;
Select * from apps.po_line_locations;


begin
apps.mo_global.set_policy_context('S', 121);
end; 

select * from apps.ap_invoices where invoice_num = '816253RI';



Select count(*)
--INTO l_invoice_count
--from apps.ap_invoices_all
from apps.ap_invoices;

select * from all_policies where object_name='PO_HEADERS';
--SELECT * FROM PO_HEADERS WHERE EXISTS (SELECT 1 FROM mo_glob_org_access_tmp oa WHERE oa.organization_id = org_id);

 
 --------------------------------------------------
      -- Initialization
  -- To retrieve org specific rows from a synonym (In 11i, we are using a different func: dbms_application_info.set_client_info)
  apps.mo_global.set_policy_context('S', p_invoice_rec.org_id);
  G_INV_SYSDATE := TRUNC(sysdate);
  g_source := p_invoice_rec.source;
  if (g_number is null) then
     SELECT AXF_VALIDATION_REJECTION_SEQ.NEXTVAL INTO G_NUMBER FROM DUAL;
  end if;
  IF (p_invoice_rec.invoice_date IS NULL) THEN
    p_invoice_rec.invoice_date := trunc(AP_IMPORT_INVOICES_PKG.g_inv_sysdate);
    -- bug 8497933
    g_is_inv_date_null := 'Y';
  ELSE
    g_is_inv_date_null := 'N';
    -- bug 8497933
  END IF;


--agr_xxinspyrus
SELECT AXF_VALIDATION_REJECTION_SEQ.NEXTVAL FROM DUAL;

-- === Vendor Search Problem Investiagation -------
SELECT 
    ( vendor_index ),
    vendor_id,
    vendor_name,
    vendor_number,
    vendor_site_id,
    vendor_site_currency,
    address_line1,
    address_line2,
    city,
    state,
    postal_code,
    country,
    vat_registration_num,
    vendor_type,
    invoice_type,
    vendor_site_currency AS currency_code,
    vendor_liability_account,
    default_charge_account,
    vendor_requester,
    org_id,
    org_name,
    org_id_name,
    site_name,
    no_line_vendor,
    distribution_set_id,
    pay_group_code,
    payment_method_code,
    vendor_pymt_terms,
    requestor_full_name,
    alternate_vendor_name,
    vendor_tax_id,
    vendor_contact_first_name,
    vendor_contact_last_name,
    vendor_contact_email,
    NULL AS vendor_bank_account_num,
    NULL AS vendor_bank_account_name,
    NULL AS vendor_iban_number
FROM
    apps.xx_ofr_suppliers_v sv
    LEFT JOIN (
        SELECT
            iao.account_owner_party_id,
            ieba.bank_account_num AS vendor_bank_account_num,
            ieba.bank_account_name AS vendor_bank_account_name,
            ieba.iban AS vendor_iban_number
        FROM
            apps.iby_account_owners iao,
            apps.iby_ext_bank_accounts ieba
        WHERE
            iao.ext_bank_account_id = ieba.ext_bank_account_id
    ) vba ON sv.party_id = vba.account_owner_party_id
WHERE
    1 = 1
    AND   lower(org_name) LIKE ( '%canada retail products%' )
    AND   lower(vendor_name) LIKE ( '%minute%' );
    
    
    --------- from Greg :select from the AXF_VALIDATION_IMPORT_PKG: using Context org ID-------------
SELECT
    COUNT(*) AS invoice_count
FROM
    apps.ap_invoices
WHERE
        1 = 1
   -- AND
      --  vendor_id = 1242152 --313
    AND
        invoice_num = '816253RI' --5164906
    AND
        org_id = 121
    AND
            CASE
                WHEN NULL IS NULL THEN 1
                ELSE invoice_amount
            END
        = nvl(
            NULL,
            1
        );
