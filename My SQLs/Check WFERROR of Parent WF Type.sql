select distinct 
     itm.item_type item_type, 
     itm.item_key  item_key,
     WF_ENGINE.GetItemAttrText('WFERROR',itm.item_key,'ERROR_ITEM_TYPE') errant_item_type,
     WF_ENGINE.GetItemAttrText('WFERROR',itm.item_key,'ERROR_ITEM_KEY') errant_item_key
from 
     applsys.wf_items                  itm,
     wf_item_activity_statuses sta,
     wf_item_activity_statuses errsta,
     wf_process_activities     pra
where itm.item_type = 'WFERROR'
  and itm.end_date is null
  and to_number(WF_ENGINE.GetItemAttrText('WFERROR',itm.item_key,'ERROR_ACTIVITY_ID')) 
        = sta.process_activity
  and WF_ENGINE.GetItemAttrText('WFERROR',itm.item_key,'ERROR_ITEM_TYPE') 
        = sta.item_type(+)
  and WF_ENGINE.GetItemAttrText('WFERROR',itm.item_key,'ERROR_ITEM_KEY') 
        = sta.item_key(+)
  and WF_ENGINE.GetItemAttrText('WFERROR',itm.item_key,'ERROR_ITEM_TYPE') 
        like  '&itemtype'||'%'
  and sta.activity_status(+) <> 'ERROR'
  and errsta.item_type   = itm.item_type
  and errsta.item_key    = itm.item_key
  and errsta.end_date    is null
  and errsta.activity_status = 'NOTIFIED'
  and errsta.notification_id is not null
  and errsta.process_activity = pra.instance_id
UNION
select distinct 
     itm.item_type item_type, 
     itm.item_key  item_key,
     WF_ENGINE.GetItemAttrText('WFERROR',itm.item_key,'ERROR_ITEM_TYPE') errant_item_type,
     WF_ENGINE.GetItemAttrText('WFERROR',itm.item_key,'ERROR_ITEM_KEY') errant_item_key
from 
     wf_items                  itm,
     wf_item_activity_statuses errsta,
     wf_process_activities     pra
where itm.item_type = 'WFERROR'
  and itm.end_date is null
  and errsta.item_type   = itm.item_type
  and errsta.item_key    = itm.item_key
  and errsta.end_date    is null
  and errsta.notification_id is not null
  and errsta.process_activity = pra.instance_id
  and errsta.activity_status = 'NOTIFIED'
  and WF_ENGINE.GetItemAttrText('WFERROR',itm.item_key,'ERROR_ITEM_TYPE') 
        like  '&itemtype'||'%'
  and not exists 
  (select 1 from wf_item_activity_statuses sta 
   where to_number(WF_ENGINE.GetItemAttrText('WFERROR',itm.item_key,'ERROR_ACTIVITY_ID')) 
        = sta.process_activity
   and WF_ENGINE.GetItemAttrText('WFERROR',itm.item_key,'ERROR_ITEM_TYPE') 
        = sta.item_type(+)
   and WF_ENGINE.GetItemAttrText('WFERROR',itm.item_key,'ERROR_ITEM_KEY') 
        = sta.item_key(+)
  );
  
  
  ---
  
WFERROR	WF2182057	APINVHDN	550271
WFERROR	WF2182685	APINVHDN	552265