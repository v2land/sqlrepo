--=============  System privileges for a user:
	SELECT PRIVILEGE
	  FROM sys.dba_sys_privs
	 WHERE grantee = 'AGR_XXINSPYRUS'
	UNION
	SELECT PRIVILEGE 
	  FROM dba_role_privs rp JOIN role_sys_privs rsp ON (rp.granted_role = rsp.role)
	 WHERE rp.grantee = 'AGR_XXINSPYRUS'
	 ORDER BY 1;
 
 -- Direct grants to tables/views:
	 SELECT owner, table_name, select_priv, insert_priv, delete_priv, update_priv, references_priv, alter_priv, index_priv 
	  FROM table_privileges
	 WHERE grantee = 'AGR_XXINSPYRUS'
	 ORDER BY owner, table_name;

--Indirect grants to tables/views:

	SELECT DISTINCT owner, table_name, PRIVILEGE 
	  FROM dba_role_privs rp JOIN role_tab_privs rtp ON (rp.granted_role = rtp.role)
	 WHERE rp.grantee = 'AGR_XXINSPYRUS'
	 ORDER BY owner, table_name;
	
-- 
select * from SYS.DBA_SYS_PRIVS where GRANTEE in ('AGRPKG','AGROPUR'); 
--EXEMPT ACCESS POLICY

SELECT * 
  FROM all_tab_privs 
 WHERE table_name = 'your_directory';  --> needs to be upper case


--Check rights on directory in Oracle
SELECT 
grantee,
table_name directory_name,
privilege
  FROM dba_tab_privs
WHERE table_name = 'AGR_CONC_OUT'; # your_directory name
