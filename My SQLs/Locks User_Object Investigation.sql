-- Lock Investigation
-- select an kill command 
select
p.spid db_spid,
	s.sid,
	s.serial#,
  'alter system kill session '''
  || s.sid
  || ','
  || s.serial#
  || ''';' kill,
  s.sql_address,
	s.status,
	s.module,
	s.action,
	to_char(s.logon_time,'DD-MON HH24:MI') logon_time,
	nvl( (select  max(substr(USER_NAME,1,15)) usager
        from apps.FND_SIGNON_AUDIT_VIEW user_vw where user_vw.PROCESS_SPID = p.spid group by PROCESS_SPID),0) USER_NAME
from v$database a, v$session s, v$process p
where-- s.username ='APPS'
  --and
  s.paddr=p.addr
  AND p.addr    = s.paddr
  AND s.module is not null
  AND s.sid in
  (select distinct a.sid
    from v$lock a
      where (a.id1,a.id2,a.type) in
        (select b.ID1,b.ID2,b.type
	   from v$lock b
             where b.lmode = 0)
		and a.lmode <> 0)
order by to_char(s.logon_time,'DD-MON HH24:MI') desc;

--alter system kill session '7,24675';

--=============check Objects Lock=========================
set serveroutput on
BEGIN
dbms_output.enable(1000000);
for do_loop in (select session_id, a.object_id, xidsqn, oracle_username, b.owner owner,
b.object_name object_name, b.object_type object_type
FROM v$locked_object a, dba_objects b
WHERE xidsqn != 0
and b.object_id = a.object_id)
loop
dbms_output.put_line('.');
dbms_output.put_line('Blocking Session : '||do_loop.session_id);
dbms_output.put_line('Object (Owner/Name): '||do_loop.owner||'.'||do_loop.object_name);
dbms_output.put_line('Object Type : '||do_loop.object_type);
for next_loop in (select sid from v$lock
where id2 = do_loop.xidsqn
and sid != do_loop.session_id)
LOOP
dbms_output.put_line('Sessions being blocked : '||next_loop.sid);
end loop;
end loop;
END;
/
--==============Check EBS Locks User============================================

SELECT s.inst_id, 
NVL (s.username, 'Internal') "Database User", 
m.SID,
s.serial#, 
p.spid "DB OS Process", 
m.TYPE,
DECODE (m.lmode,
0, 'None',
1, 'Null',
2, 'Row Share',
3, 'Row Excl.',
4, 'Share',
5, 'S/Row Excl.',
6, 'Exclusive',
lmode, LTRIM (TO_CHAR (lmode, '990'))
) "Lock Type",
DECODE (m.request,
0, 'None',
1, 'Null',
2, 'Row Share',
3, 'Row Excl.',
4, 'Share',
5, 'S/Row Excl.',
6, 'Exclusive',
request, LTRIM (TO_CHAR (m.request, '990'))
) "Lock Request",
DECODE (command,
0, 'None',
DECODE (m.id2,
0, dusr.username || '.' || SUBSTR (dobj.NAME, 1, 30),
'Rollback Segment'
)
) "Object",
s.machine "Application Server", 
s.process "Apps OS process", 
m.ctime,
NVL (NVL (usr.description, s.action),
'Database Session'
) "Online User,Concurrent",
NVL (fnd.responsibility_name, s.module) "Responsibility,Module",
fnd.user_form_name "Form Name", 
SQL.sql_text "Statement"
FROM gv$session s,
gv$lock m,
gv$process p,
apps.fnd_form_sessions_v fnd,
apps.fnd_user usr,
gv$sqlarea SQL,
dba_users dusr,
SYS.obj$ dobj
WHERE m.id1 IN (SELECT il.id1
FROM gv$lock il
WHERE il.request <> 0)
AND m.SID = s.SID
AND s.paddr = p.addr
AND s.inst_id = p.inst_id
AND SQL.inst_id(+) = s.inst_id
AND SQL.address(+) = s.sql_address
AND SQL.hash_value(+) = s.sql_hash_value
AND s.username != 'SYS'
AND m.lmode != 4
AND fnd.audsid(+) = s.audsid
AND m.inst_id = s.inst_id
AND fnd.user_name = usr.user_name(+)
AND fnd.user_id = usr.user_id(+)
AND dobj.obj#(+) = DECODE (m.id2, 0, m.id1, 1)
AND dusr.user_id(+) = dobj.owner#
ORDER BY m.id1, m.request ASC, m.SID;

--  EBS USER check Lock
SELECT
'alter system kill session '''
  || VS.sid
  || ','
  || VS.serial#
  || ''';' kill,
        VS.sid as "BLOCKER SID", VS.username as " DB User:",
          nvl(vs.client_identifier, user_name) as " Blocker EBS User:",
          vs.prev_sql_id as "Prev SQL ID:",
          vs.sql_id as "SQL ID:",
          vs.program as "Program :",  vs.module as "Module :", vs.action "Action :", 
         VS.status as "Statut: ", 
         VS2.sid as "WAITER SID", nvl(vs2.client_identifier, null) as " WAITER EBS User:",
         vs2.program as "Program :", vs2.module as "Module :", vs2.action as "Action :", 
         vs2.event as "Event:",
         vs2.final_blocking_session as "Final Blocking Session:",
         su.name as "OBJECT.",  so.NAME as "Object",
  rownum NUM,
  vl1.ctime TIME
FROM V$PROCESS VP,
  V$SESSION VS,
  APPLSYS.FND_LOGINS FL,
  APPLSYS.FND_USER FU,
  V$LOCK VL1,
  V$LOCK VL2,
  V$SESSION VS2,
  v$lock vl,
  sys.obj$ so,
  sys.user$ su
WHERE vp.ADDR     = vs.PADDR(+)
AND VP.SPID       = FL.PROCESS_SPID(+)
AND VP.PID        = FL.PID(+)
AND FL.USER_ID    = FU.USER_ID(+)
AND VS.sid        = VL1.sid
AND VL1.ID1       = VL2.ID1
AND VL1.ID2       = VL2.ID2
AND VL2.sid       = VS2.sid
AND VL1.block     = 1
and VL2.REQUEST   > 0
and vs.sid = vl.sid(+)
and vl.id1 = so.obj#(+)
and vl.type='TM'
and so.OWNER# = su.user#;