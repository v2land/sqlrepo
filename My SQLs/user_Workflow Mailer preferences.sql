--Resending Failed/Sent Notifications Again
--1> Check if Notification mailer/Agent listener is running
  
    select (case when (select count(1) 
                         from apps.fnd_concurrent_queues
                        where concurrent_queue_name = 'WFMLRSVC') > 0
                 then 'Notification Mailer Running'  
                 else 'Notification Mailer Not Running'
                end) "Status"
       from dual;

--2> Run the below query get the exact number based on dates

      
       select *--distinct trunc(begin_date)
        from wf_notifications
      where 1=1 
         and trunc(begin_date) <= trunc(sysdate) -- set date
         --and mail_status ='SENT' --set status FAILED or SENT
         and status = 'OPEN'
         and CONTEXT Like '%APEXP%'
        and TO_USER in ('Gendreau, Daniel') -- set USER
         order by trunc(begin_date) desc;
         
         
         
         
--49627035	49627035	APEXP	OIE_AME_EXPENSE_REPORT_APPRVL	GERLAMA	OPEN	19215618155397942936271402111326726780		50	19-APR-17		24-APR-17			WF_ENGINE.CB	APEXP:65525-2:258942	GERLAMA	Masse, Michel	Lamarche, Gerald	Expense WEB65525 for Masse, Michel (7.20 CAD)	US		MICMASS		WEB65525	65525-2																																																													19-APR-17
--3> Run the concurrent program 'Resend Failed/Error Workflow Notifications' from 'System  Administrator' responsibility.
  --   > All parameters are optional, please select the parameter values accordingly  so that only required notifications get re-processed.

     Mail Status                            : Error/Failed
     Message Type                       : Workflow Item Types
     Recipient Role                       : List of workflow roles
     Notifications sent on or after    : Date
     Notifications sent on or before : Date

--4> For resending the Sent notifications, please 'Rewind'

    >> Goto Workflow > Status Monitor
        >> Search with 'Type Internal Name' and 'Item Key'
             >> You can get it from wf_notifications table

        >> Click on 'Activity History' > Select the Activity with 'Notified' Status and 'Rewind'
            >> Again select the same activity and 'Apply'
             >> You will get a message for Rewind.

    Check the wf_notifications table for same 'message_type' and 'item_type', there should be two entry with previous one 'Closed'.

--5> If notification is getting failed again and again, check the user preferences for that recipient.

      Select *
        from fnd_user_preferences 
       where user_name in (
 select DISTINCT RECIPIENT_ROLE --distinct trunc(begin_date)
        from wf_notifications
      where 1=1 
         and trunc(begin_date) <= trunc(sysdate) -- set date
         and mail_status ='SENT' --set status FAILED or SENT
         and status = 'OPEN'
         --and module_name='MAILTYPE'
         and CONTEXT Like '%APEXP%'
        and TO_USER in ('Lamarche, Gerald') -- set USER
         --order by trunc(begin_date) desc
         );

--You can also debug the notification with the notification id

--Run the @$FND_TOP/sql/wfmlrdbg.sql  
-- > Parameter: Enter Value for 1: <Notification_ID >
 
 SELECT email_address, nvl(WF_PREF.get_pref(name, 'MAILTYPE'),notification_preference)
FROM wf_roles
WHERE name = upper('CDAHL');

SELECT name,email_address, nvl(WF_PREF.get_pref(name, 'MAILTYPE'),notification_preference)
FROM wf_roles
WHERE name in (
 select DISTINCT RECIPIENT_ROLE --distinct trunc(begin_date)
        from wf_notifications
      where 1=1 
         and trunc(begin_date) <= trunc(sysdate) -- set date
         and mail_status ='SENT' --set status FAILED or SENT
         and status = 'OPEN'
         and CONTEXT Like '%APEXP%'
        and TO_USER in ('Lamarche, Gerald') -- set USER
         --order by trunc(begin_date) desc
         );
--MATBASS
--PETDARC
----JM36330

select name, display_name from wf_roles 
where name in ('MAILHTML', 'MAILTEXT', 'MAILHTM2') 
and email_address is null 
and orig_system in 'PER';

select USR.USER_NAME NAME,
     USR.USER_NAME DISPLAY_NAME,
     USR.DESCRIPTION DESCRIPTION,
     'MAILHTML' NOTIFICATION_PREFERENCE,
     'AMERICAN' LANGUAGE,
     'AMERICA' TERRITORY,
     USR.EMAIL_ADDRESS EMAIL_ADDRESS,
     USR.FAX FAX,
     'FND_USR' ORIG_SYSTEM,
     USR.USER_ID ORIG_SYSTEM_ID,
     USR.START_DATE START_DATE,
     decode(substr(to_char(nvl(usr.end_date,sysdate+1)-sysdate),1,1),
           '-', 'INACTIVE',
           'ACTIVE') STATUS,
     USR.END_DATE EXPIRATION_DATE,
     NULL SECURITY_GROUP_ID,
     'Y' USER_FLAG,
     1 PARTITION_ID
from   FND_USER USR
where  
--USR.EMPLOYEE_ID is null
--and
USR.USER_NAME in ('GERLAMA','MATBASS')
--and USR.DESCRIPTION like '%Gerald%'
;

Select *
from   FND_USER
where  
--EMPLOYEE_ID is null
 USER_NAME in ('GERLAMA','MATBASS');

select NVL(substr(wfe.corrid,1,50),'NULL - No Value') corrid, decode(wfe.state,0,'0 = Ready',1,'1 = Delayed',2,'2 =  Retained', 
3,'3 =    
Exception',to_char(substr(wfe.state,1,12))) State,
count(*) COUNT 
from applsys.wf_deferred wfe group by wfe.corrid, wfe.state;

SELECT name
         FROM wf_roles 
         where nvl(WF_PREF.get_pref(name, 'MAILTYPE'), notification_preference) = 'DISABLED';
         
select NAME, DISPLAY_NAME,NOTIFICATION_PREFERENCE, EMAIL_ADDRESS, CREATION_DATE, LAST_UPDATE_DATE 
from Wf_Local_Roles t
 -- Set T.Notification_Preference = 'QUERY'
Where
--T.Notification_Preference In ('DISABLED')
--and
name in ('DANGEND','PIEDUMA','CM18006','GUYBEAU','ANNLALL','DB41147')
/*name in (
 select DISTINCT RECIPIENT_ROLE --distinct trunc(begin_date)
        from wf_notifications
      where 1=1 
         and trunc(begin_date) <= trunc(sysdate) -- set date
         and mail_status is NULL--'SENT' --set status FAILED or SENT
         and status = 'OPEN'
         --and CONTEXT Like '%APEXP%'
        and TO_USER in ('Gendreau, Daniel') -- set USER
         --order by trunc(begin_date) desc
         )*/
;


--JM36330	Mihalic, John	MAILHTM2	john.mihalic@rona.ca	29-SEP-16	15-FEB-17
--MATBASS	Basso, Matthew	DISABLED	Matthew.Basso@rona.ca	11-APR-17	11-APR-17
--PETDARC	Darcy, Peter	DISABLED	Peter.Darcy@rona.ca	08-DEC-16	11-APR-17

--update wf_local_roles set notification_preference ='MAILHTM2' where name='MATBASS';
--update wf_local_roles set notification_preference ='MAILHTM2' where name='PETDARC';

select *
from wf_local_roles where 
--lower(email_address) in lower('yan.laurin@rona.ca')--,'Gerald.Lamarche@rona.ca')
--description in('Laurin, Yan','Lamarche, Gerald')
name in ('DANGEND','PIEDUMA','CM18006','GUYBEAU','ANNLALL','DB41147')
;
--DANGEND: Gendreau, Daniel 0	22-APR-17 SYSADMIN
--PIEDUMA: Dumais, Pierre 0	27-APR-17 SYSADMIN
--CM18006: McGaughey, Christopher 0	29-APR-17 SYSADMIN
--GUYBEAU: Beaumier, Guy 0	29-APR-17 SYSADMIN
--ANNLALL: Lallemand, Anne  1172 21-APR-17  ANDBOUR
--DB41147: Belanger, Denis 0	30-APR-17 SYSADMIN
SELECT user_name FROM fnd_user WHERE user_id = '0';
--(SELECT user_name FROM fnd_user WHERE user_id = v.last_updated_by ) �Last UPDATE By�

--select ur.*, wr.*  --from   APPLSYS.wf_user_role_assignments ur, apps.WF_ROLES wr 
select distinct UR.USER_NAME, WR.DISPLAY_NAME, WR.description
--, WR.STATUS,ROLE_ORIG_SYSTEM
from   APPLSYS.wf_user_role_assignments ur, apps.WF_ROLES wr 
where  1=1
--AND ROLE_NAME = ASSIGNING_ROLE
--AND ROLE_ORIG_SYSTEM in ( 'FND_RESP', 'UMX')
--AND wr.name=ur.role_name
AND ur.user_name like 'ANDBOUR%';

SELECT t.user_profile_option_name,
  profile_option_value,
  v.creation_date,
  v.last_update_date,
  v.creation_date v. last_update_date �Change Date�,
  (SELECT UNIQUE user_name FROM fnd_user WHERE user_id = v.created_by
  ) �Created By�,
  (SELECT user_name FROM fnd_user WHERE user_id = v.last_updated_by
  ) �Last UPDATE By�
FROM fnd_profile_options o,
  fnd_profile_option_values v,
  fnd_profile_options_tl t
WHERE o.profile_option_id           = v.profile_option_id
AND o.application_id                = v.application_id
AND start_date_active              <= SYSDATE
AND NVL (end_date_active, SYSDATE) >= SYSDATE
AND o.profile_option_name           = t.profile_option_name
AND level_id                        = 10001
AND t.LANGUAGE                     IN
  (SELECT language_code FROM fnd_languages WHERE installed_flag = �B�
  UNION
  SELECT nls_language FROM fnd_languages WHERE installed_flag = �B�
  )
ORDER BY user_profile_option_name;



--grep WFMLRSND_FAILED_UNDELIVERABLE  $APPLCSF/$APPLLOG/FNDCPGSC*.txt

	SELECT   n1.notification_id
       ,n1.access_key
       ,n1.MESSAGE_TYPE
       ,n1.message_name
       ,n1.recipient_role
       ,n1.SUBJECT
       ,n1.status
       ,n1.mail_status
       ,t1.*
       ,n1.begin_date
       ,n1.end_date
       ,n1.due_date
       ,n1.responder
       ,n1.user_comment
       ,n1.callback
       ,n1.CONTEXT
       ,n1.original_recipient
       ,n1.from_user
       ,n1.to_user
       ,n1.LANGUAGE
       ,n1.more_info_role
       ,n1.from_role
       ,n1.security_group_id
FROM     (SELECT t.deq_time q_deq_time
               ,t.enq_time q_enq_time
               ,t.q_name
               ,TO_NUMBER ((SELECT str_value
                            FROM   TABLE (t.user_data.header.properties)
                            WHERE  NAME = 'NOTIFICATION_ID'))
                                                           q_notification_id
               , (SELECT str_value
                  FROM   TABLE (t.user_data.header.properties)
                  WHERE  NAME = 'BES_FROM_AGENT') q_bes_from_agent
               , (SELECT str_value
                  FROM   TABLE (t.user_data.header.properties)
                  WHERE  NAME = 'ROLE') q_role
               , (SELECT str_value
                  FROM   TABLE (t.user_data.header.properties)
                  WHERE  NAME = 'BES_RECEIVE_DATE') q_bes_receive_date
               , (SELECT str_value
                  FROM   TABLE (t.user_data.header.properties)
                  WHERE  NAME = 'BES_SEND_DATE') q_bes_send_date
               , (SELECT str_value
                  FROM   TABLE (t.user_data.header.properties)
                  WHERE  NAME = 'BES_EVENT_KEY') q_bes_event_key
               , (SELECT str_value
                  FROM   TABLE (t.user_data.header.properties)
                  WHERE  NAME = 'BES_EVENT_NAME') q_bes_event_name
               ,t.user_data
         FROM   wf_notification_out t) t1
       ,wf_notifications n1
WHERE    1 = 1
AND      n1.notification_id = t1.q_notification_id(+)
--  and n1.notification_id in ( 229583, 229501                                 )
--  and n1.message_name='SMBC_NOTIFY_NEW_ASSIGNEE'
 --and n1.recipient_role like upper('VJV_TEST')
--  and n1.status='OPEN'
--  and n1.mail_status in ('MAIL','SENT')
--  and n1.mail_status in ('MAIL')
--  and trunc(n1.begin_date) = trunc(sysdate)
-- and (n1.notification_id=231529 or n1.notification_id=229501)
--and n1.notification_id in (49628163,49627035)
and n1.SUBJECT like upper('WEB66798')
ORDER BY n1.begin_date DESC;

select notification_preference,email_address
      from applsys.wf_local_roles 
     where name = 'DANGEND';
     
select fl.meaning,fcp.process_status_code,
decode(fcq.concurrent_queue_name,'WFMLRSVC','maile r container','WFALSNRSVC','listener container',fcq.concurrent_queue_name),
fcp.concurrent_process_id,os_process_id, fcp.logfile_name
from fnd_concurrent_queues fcq, fnd_concurrent_processes fcp , fnd_lookups fl
where fcq.concurrent_queue_id=fcp.concurrent_queue_id and fcp.process_status_code='A'
and fl.lookup_type='CP_PROCESS_STATUS_CODE' and
fl.lookup_code=fcp.process_status_code
and concurrent_queue_name in('WFMLRSVC','WFALSNRSVC')
order by fcp.logfile_name;
