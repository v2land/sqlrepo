SELECT a1.user_concurrent_program_name Prog,
    a3.concurrent_program_name Short,
    a1.CONCURRENT_PROGRAM_ID CONC_PR_ID,
    b1.CONCURRENT_PROGRAM_ID Incomp_CONC_PR_ID,
    b3.concurrent_program_name Incompatible_Short,
    b1.user_concurrent_program_name Incompatible_Prog,
    DECODE (to_run_type, 'P', 'Program', 'S', 'Request set', 'UNKNOWN' ) incompatible_type,
    b2.application_name "Incompatible App"
  FROM apps.fnd_concurrent_program_serial cps ,
    apps.fnd_concurrent_programs_tl a1 ,
    apps.fnd_application_tl a2 ,
    apps.fnd_concurrent_programs a3 ,
    apps.fnd_concurrent_programs_tl b1 ,
    apps.fnd_application_tl b2 ,
    apps.fnd_concurrent_programs b3
  WHERE  1=1
  and UPPER(a1.user_concurrent_program_name) like UPPER('%'||:user_concurrent_program_name_LIKE||'%')
  and a1.application_id      = cps.running_application_id
  AND a1.concurrent_program_id = cps.running_concurrent_program_id
  AND a2.application_id        = cps.running_application_id
  AND a3.concurrent_program_id = a1.concurrent_program_id
  AND b1.application_id        = cps.to_run_application_id
  AND b1.concurrent_program_id = cps.to_run_concurrent_program_id
  AND b2.application_id        = cps.to_run_application_id
  AND b3.concurrent_program_id = b1.concurrent_program_id
  AND a1.language              = 'US'
  AND a2.language              = 'US'
  AND b1.language              = 'US'
  AND b2.language              = 'US'
  order by 1;
  
 