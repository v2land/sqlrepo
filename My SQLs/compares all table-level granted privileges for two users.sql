--This query compares all table-level granted privileges for two users named USER_A and USER_B:

select
owner, 
   table_name, 
   select_priv, 
   insert_priv, 
   delete_priv, 
   update_priv, 
   references_priv, 
   alter_priv, 
   index_priv 
from 
   table_privileges
where
   grantee = 'AGRPKG'-- 'USER_A'
MINUS
select
   owner, 
   table_name, 
   select_priv, 
   insert_priv, 
   delete_priv, 
   update_priv, 
   references_priv, 
   alter_priv, 
   index_priv 
from 
   table_privileges
where
   grantee = 'AGROPUR'--'USER_B'
order by 
   owner, 
   table_name;

select
   owner, 
   table_name, 
   select_priv, 
   insert_priv, 
   delete_priv, 
   update_priv, 
   references_priv, 
   alter_priv, 
   index_priv 
from 
   table_privileges
where
   grantee = 'AGROPUR'--'USER_B'
MINUS
select
   owner, 
   table_name, 
   select_priv, 
   insert_priv, 
   delete_priv, 
   update_priv, 
   references_priv, 
   alter_priv, 
   index_priv 
from 
   table_privileges
where
   grantee = 'AGRPKG'-- 'USER_A'
order by 
   owner, 
   table_name;

--This query compares all role privileges for a pair of users:

select distinct 
   owner, 
   table_name, 
   privilege 
from 
   dba_role_privs rp,
   role_tab_privs rtp 
where
   rp.granted_role = rtp.role
and
   rp.grantee = 'AGRPKG'-- 'USER_A'
MINUS
select distinct 
   owner, 
   table_name, 
   privilege 
from 
   dba_role_privs rp,
   role_tab_privs rtp 
where
   rp.granted_role = rtp.role
and
   rp.grantee = 'AGROPUR'--'USER_B' 
order by 
   owner, 
   table_name;

select distinct 
   owner, 
   table_name, 
   privilege 
from 
   dba_role_privs rp,
   role_tab_privs rtp 
where
   rp.granted_role = rtp.role
and
   rp.grantee = 'AGROPUR'--'USER_B'
MINUS
select distinct 
   owner, 
   table_name, 
   privilege 
from 
   dba_role_privs rp,
   role_tab_privs rtp 
where
   rp.granted_role = rtp.role
and
   rp.grantee = 'AGRPKG'-- 'USER_A'
order by 
   owner, 
   table_name;

 

The following example will compare all system and role privileges for two users named USER_A and USER_B:

select 
   * 
from 
   dba_sys_privs
where 
   grantee = 'USER_A'
MINUS 
select 
   * 
from 
   dba_sys_privs
where 
   grantee = 'USER_B';

select 
   * 
from 
   dba_role_privs
where 
   grantee = 'USER_A'
MINUS 
select 
   * 
from 
   dba_role_privs
where 
   grantee = 'USER_B';

select 
   * 
from 
   dba_sys_privs
where 
   grantee = 'USER_B'
MINUS 
select 
   * 
from 
   dba_sys_privs
where 
   grantee = 'USER_A';

select 
   * 
from 
   dba_role_privs
where 
   grantee = 'USER_B'
MINUS 
select 
   * 
from 
   dba_role_privs
where 
   grantee = 'USER_A';