-- NEW Logic:
SELECT distinct fu.user_name
    FROM fnd_user fu
    WHERE 1=1
    --VC NOTE!change email for users that have following ROLE --96 dont follow the rule -- to be reviewed (logique � utiliser where role_name in (case X then retourne 2 valuses CLONEDEV,CLONE_FUNC else CLONE FUNC)  )
    AND fu.user_name IN
      (SELECT user_name
      FROM wf_local_user_roles
      WHERE role_name in ((CASE WHEN '&p_database' in ('EBSDEV1','EBSDEV6','EBSDEVE','EBSDEV3') THEN 
                        'UMX|XXAGR_CLONE_DEV_ROLE'
                            ELSE
                        'UMX|XXAGR_CLONE_FUNCTIONAL_ROLE'
                          END)
                          ,
                          (CASE WHEN '&p_database' in ('EBSDEV1','EBSDEV6','EBSDEVE','EBSDEV3') THEN 
                        'UMX|XXAGR_CLONE_FUNCTIONAL_ROLE'
                          END))
      AND NVL(expiration_date, sysdate + 1) > SYSDATE
      )
    ORDER BY 1;
-- end of New Logic 



-- CLONE_DEV  164 PROD  12 avril 2019
-- CLONE_DEV et CLONE_FUNC  208 PROD
-- TOTAL 372 qui tombe dans nos regles d'exclusion, une au l'autre.. 
--96 CLONE_FUNC qui n'on pas le CLONE_DEV
-- les autres sont par defaut restraint (e-mail , and user Disable)

-- Check CLONE_FUNC, CLONE_DEV Roles
select distinct ROLE_NAME, UR.USER_NAME, WR.DISPLAY_NAME, WR.description
, WR.STATUS,ROLE_ORIG_SYSTEM
from   APPLSYS.wf_user_role_assignments ur, apps.WF_ROLES wr 
where  1=1
AND ROLE_NAME like '%CLONE_FUNC%'
AND ROLE_ORIG_SYSTEM in ( 'FND_RESP', 'UMX')
AND wr.name=ur.role_name
--AND ur.user_name like '%JUTREMBL%'-- FUNC but not DEV
;

--96 CLONE_FUNC qui n'on pas le CLONE_DEV
select distinct ROLE_NAME, UR.USER_NAME, WR.DISPLAY_NAME, WR.description
, WR.STATUS,ROLE_ORIG_SYSTEM
from   APPLSYS.wf_user_role_assignments ur, apps.WF_ROLES wr 
where  1=1
AND ROLE_NAME like '%CLONE_FUNC%'
AND ROLE_ORIG_SYSTEM in ( 'FND_RESP', 'UMX')
AND wr.name=ur.role_name
AND ur.user_name not in  (select distinct UR.USER_NAME
                        from   APPLSYS.wf_user_role_assignments ur, apps.WF_ROLES wr 
                        where  1=1
                        AND ROLE_NAME like '%CLONE_DEV%'
                        AND ROLE_ORIG_SYSTEM in ( 'FND_RESP', 'UMX')
                        AND wr.name=ur.role_name)
;

-- Concurrent Programs in ERROR
select
  fcr.request_id,
  fcr.parent_request_id,
  fu.user_name requestor,
  to_char(fcr.last_update_date, 'MON-DD-YYYY HH24:MM:SS') LAST_UPDATE,
  fr.responsibility_key responsibility,
  fcp.concurrent_program_name,
  fcpt.user_concurrent_program_name,
  fcr.argument_text,
  decode(fcr.status_code,
    'A', 'Waiting',
    'B', 'Resuming',
    'C', 'Normal',
    'D', 'Cancelled',
    'E', 'Error',
    'F', 'Scheduled',
    'G', 'Warning',
    'H', 'On Hold',
    'I', 'Normal',
    'M', 'No Manager',
    'Q', 'Standby',
    'R', 'Normal',
    'S', 'Suspended',
    'T', 'Terminating',
    'U', 'Disabled',
    'W', 'Paused',
    'X', 'Terminated',
    'Z', 'Waiting') status,
  decode(fcr.phase_code,
    'C', 'Completed',
    'I', 'Inactive',
    'P', 'Pending',
    'R', 'Running') phase,
  fcr.completion_text
from
  fnd_concurrent_requests fcr,
  fnd_concurrent_programs fcp,
  fnd_concurrent_programs_tl fcpt,
  fnd_user fu,
  fnd_responsibility fr
where
  fcr.status_code in ('D', 'E', 'S', 'T', 'X') and
  fcr.phase_code = 'C' and
  fcr.last_update_date > sysdate - 1 and
  fu.user_id = fcr.requested_by and
  fcr.concurrent_program_id = fcp.concurrent_program_id and
  fcr.concurrent_program_id = fcpt.concurrent_program_id and
  fcr.responsibility_id = fr.responsibility_id and
  UPPER(fcpt.user_concurrent_program_name) like UPPER('%AR Factoring Invoices Report%')
order by
  fcr.last_update_date,
  fcr.request_id;
  
  
  
-- Check Responsibility of a Concurrent Program to Run 
  
SELECT frt.responsibility_name,frg.request_group_name,
frgu.request_unit_type,frgu.request_unit_id,
fcpt.user_concurrent_program_name
FROM fnd_Responsibility fr, fnd_responsibility_tl frt,
    fnd_request_groups frg, fnd_request_group_units frgu,
    fnd_concurrent_programs_tl fcpt
    WHERE frt.responsibility_id = fr.responsibility_id
    AND frg.request_group_id = fr.request_group_id
    AND frgu.request_group_id = frg.request_group_id
    AND fcpt.concurrent_program_id = frgu.request_unit_id
    AND frt.LANGUAGE = USERENV('LANG')
    AND fcpt.LANGUAGE = USERENV('LANG')
    AND fcpt.user_concurrent_program_name = :conc_prg_name
    ORDER BY 1,2,3,4;
    
--AR Factoring AGR CA	XXAGR_AR_FACTORING_REPORT	P	228397	AR Factoring Invoices Report

-- Users to reactivate:
SELECT user_name
    FROM fnd_user 
    WHERE NVL(end_date, sysdate+1) <= sysdate
    AND user_name IN
      (SELECT user_name
      FROM wf_local_user_roles
      WHERE (CASE WHEN 'EBSDEV9' in ('EBSDEV1','EBSDEV6','EBSDEVE','EBSDEV3') THEN 
                'UMX|XXAGR_CLONE_DEV_ROLE'
            ELSE
                'UMX|XXAGR_CLONE_FUNCTIONAL_ROLE'
            END) = role_name
      AND NVL(expiration_date, sysdate + 1 ) > SYSDATE
      );

--c_RespToReactivate
 SELECT fu.user_id                    ,
      fu.user_name                       ,
      frtl.responsibility_name             ,
      furgd.responsibility_id            ,
      furgd.responsibility_application_id,
      furgd.start_date                   ,
      furgd.description
  --    fr.start_date, fr.end_date
    FROM fnd_user_resp_groups_direct furgd,
      fnd_user fu                         ,
      fnd_responsibility_tl frtl,
      wf_local_user_roles wur,
      fnd_responsibility fr
    WHERE furgd.user_id               = fu.user_id
    AND furgd.end_date               IS NOT NULL
    AND NVL(fu.end_date, sysdate      + 1) > sysdate
    AND fu.user_id                   IN
      (SELECT DECODE(wur.user_orig_system, 'PER', fu2.user_id,
        wur.USER_ORIG_SYSTEM_ID)
      FROM wf_local_user_roles wur,
        fnd_user fu2
      WHERE (CASE WHEN 'EBSDEV9' in ('EBSDEV1','EBSDEV6','EBSDEVE','EBSDEV3') THEN
                'UMX|XXAGR_CLONE_DEV_ROLE'
            ELSE
                'UMX|XXAGR_CLONE_FUNCTIONAL_ROLE'
            END) = wur.role_name
      AND fu2.employee_id(+) = DECODE(wur.user_orig_system, 'PER',
        wur.USER_ORIG_SYSTEM_ID)
      )
    AND frtl.responsibility_id = furgd.responsibility_id
    AND frtl.language          = 'US'
    AND wur.role_orig_system_id = fr.responsibility_id
    and wur.role_orig_system = 'FND_RESP'
    and wur.user_name = fu.user_name
    AND wur.assignment_type = 'D'
    and fr.responsibility_id  = frtl.responsibility_id 
    and NVL(fr.end_date,sysdate+1) > sysdate
    and frtl.responsibility_name not in (SELECT lvv.meaning FROM fnd_lookup_values_vl lvv WHERE lvv.lookup_type   = 'XXAGR_ASSIGNMENTS_TO_RESTRICT')  
   ORDER BY 1;

--c_RoleToReactivate 
SELECT *
      FROM wf_local_user_roles wlur
           , wf_local_roles wlro
      WHERE wlur.role_orig_system = 'UMX'
      and wlur.user_name
      IN
      (SELECT wlur2.user_name
      FROM wf_local_user_roles wlur2
      WHERE (CASE WHEN 'EBSDEV9' in ('EBSDEV1','EBSDEV6','EBSDEVE','EBSDEV3') THEN 
                'UMX|XXAGR_CLONE_DEV_ROLE'
            ELSE
                'UMX|XXAGR_CLONE_FUNCTIONAL_ROLE'
            END) = wlur2.role_name
      AND NVL(wlur2.expiration_date, sysdate + 1 ) > SYSDATE
      ) 
      AND NVL(wlur.expiration_date, sysdate + 1 ) <= SYSDATE
      and wlro.display_name not in (SELECT lvv.meaning FROM fnd_lookup_values_vl lvv WHERE lvv.lookup_type   = 'XXAGR_ASSIGNMENTS_TO_RESTRICT')   
      AND wlro.name  = wlur.role_name
      ;