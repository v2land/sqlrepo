-- La derniere date de STATS_DATE
select MAX(STATS_DATE)from AGR_TABLE_STATS_HIST;
-- La liste des tables actifs pendant le dernier STATS_DATE:
-- compar� �
-- la liste des TABLES actifs, pesentes dans la BD pendant juste la derniere prise de STAT; --24132
select * from AGR_TABLE_STATS_HIST where STATS_DATE = (select MAX(STATS_DATE)from AGR_TABLE_STATS_HIST);

--RANK TOP 100 Tables  on Last STAT_DATE
select * from 
(SELECT stats_date, owner, table_name, num_rows, --blocks,bytes,
      -- LAG(num_rows, 1, 0) OVER (PARTITION BY table_name ORDER BY STATS_DATE) AS num_rows_prev,
       --num_rows - LAG(num_rows, 1, 0) OVER (PARTITION BY table_name ORDER BY STATS_DATE) "num_rows_diff",
       round((bytes) / 1024 / 1024,0) "MBytes",
       --round((LAG(bytes, 1, 0) OVER (PARTITION BY table_name ORDER BY STATS_DATE)) / 1024 / 1024,0) "MBytes_prev",
       --round((bytes - LAG(bytes, 1, 0) OVER (PARTITION BY table_name ORDER BY STATS_DATE)) / 1024 / 1024,0) AS MBytes_diff,
       dense_rank() over (order by Bytes desc) as rnk -- TOP 100 rank
from (select * from AGR_TABLE_STATS_HIST where STATS_DATE = (select MAX(STATS_DATE)from AGR_TABLE_STATS_HIST))-- source filtr� pour tables actifs � la derniere date 
)
where rnk <= 100;

select * from AGR_TABLE_STATS_HIST where TABLE_NAME='RG_REPORT_REQUEST_LOBS' and STATS_DATE > sysdate-3 ;
select * from AGR_TABLE_STATS_HIST where TABLE_NAME='FND_USER' and STATS_DATE > sysdate-3 ;
select * from dba_tab_statistics where TABLE_NAME='RG_REPORT_REQUEST_LOBS';

select count(*) from RG_REPORT_REQUEST_LOBS;
select num_rows from all_tables where table_name = 'RG_REPORT_REQUEST_LOBS';

SELECT table_name, 
       num_rows 
  FROM dba_tables 
 WHERE TABLE_NAME='RG_REPORT_REQUEST_LOBS';
 
--To update the latest row count value in the DBA_TABLES view execute

exec dbms_stats.gather_schema_stats(ownname => 'NAME');

--GROUPBY:
select stats_date,owner,table_name,SUM(num_rows),SUM(bytes),MAX(last_analyzed)
from AGR_TABLE_STATS_HIST
where table_name = 'RG_REPORT_REQUEST_LOBS' and STATS_DATE > sysdate-3 
group by stats_date,owner,table_name;

select *
from AGR_TABLE_STATS_HIST
where table_name = 'RG_REPORT_REQUEST_LOBS' and STATS_DATE > sysdate-3 
--group by stats_date,owner,table_name;
;
--===========================================================================================================================================================================================
--AGROPUR AGR_TABLE_STATS_HIST: 
--==  using Analytic Function: LAG,
/*When you paste a new Analytic Function into a Calculation box, Discoverer Desktop provides the following�generic template:
OVER (PARTITION BY expr1 ORDER BY expr2)
The expressions are used as follows.
	� OVER - indicates that the function operates on a query result set, after the other query clauses have been applied, (such as FROM, WHERE, HAVING etc.
	� PARTITION BY - partition (or group) the query results set. E.g. PARTITION BY 'Region'.
	� ORDER BY - specify how the results set is logically ordered. E.g. ORDER BY 'Sales SUM'.
*/
--== Group By month, select Max date of Month
 /*       SELECT max(to_char(stats_date, 'YYYY-MM-DD'))
        from AGR_TABLE_STATS_HIST 
        where
        num_rows >0
        and stats_date between '01-MAR-2018' and '01-FEB-2019'
        group by to_char(stats_date, 'YYYY-MM') order by 1;
*/
---- source filtr� pour tables actifs � la derniere date
/*
select distinct table_name from AGR_TABLE_STATS_HIST where STATS_DATE = (select MAX(STATS_DATE)from AGR_TABLE_STATS_HIST)
*/
--GROUPBY:
/*select stats_date,owner,table_name,SUM(num_rows),SUM(bytes),MAX(last_analyzed)
from AGR_TABLE_STATS_HIST
where 1=1
--table_name = 'RG_REPORT_REQUEST_LOBS' 
--and STATS_DATE > sysdate-3 
group by stats_date,owner,table_name;
*/
SELECT to_char(a.stats_date, 'YYYY-MM-DD') stats_date, a.owner, a.table_name, a.num_rows,-- a.blocks,
       LAG(a.num_rows, 1, 0) OVER (PARTITION BY a.table_name ORDER BY a.STATS_DATE) AS num_rows_prev,                           
       a.num_rows - LAG(a.num_rows, 1, 0) OVER (PARTITION BY a.table_name ORDER BY a.STATS_DATE) "num_rows_diff",
       round((a.bytes) / 1024 / 1024,0) "MBytes",
       round((LAG(a.bytes, 1, 0) OVER (PARTITION BY a.table_name ORDER BY a.STATS_DATE)) / 1024 / 1024,0) "MBytes_prev",
       round((a.bytes - LAG(a.bytes, 1, 0) OVER (PARTITION BY a.table_name ORDER BY a.STATS_DATE)) / 1024 / 1024,0) AS MBytes_diff
       --,a.last_analyzed
from (select stats_date,owner,table_name,SUM(num_rows)as num_rows,SUM(bytes) as bytes,MAX(last_analyzed) as last_analyzed
            from AGR_TABLE_STATS_HIST
                --where 1=1
                --table_name = 'RG_REPORT_REQUEST_LOBS' 
                --and STATS_DATE > sysdate-3 
            group by stats_date,owner,table_name) a                                                                                 --GROUPBY the LOB Tables Segments
    ,(SELECT to_date(max(to_char(stats_date, 'YYYY-MM-DD')),'YYYY-MM-DD') as stats_date
                from AGR_TABLE_STATS_HIST 
                where 1=1
               and bytes >0
                and stats_date between '01-MAR-2015' and '01-APR-2020'
                group by to_char(stats_date, 'YYYY-MM') order by 1) b                                                               -- Group By month, select Max date of Month
    ,(select distinct table_name from AGR_TABLE_STATS_HIST where STATS_DATE = (select MAX(STATS_DATE)from AGR_TABLE_STATS_HIST)) af -- source filtr� pour tables actifs � la derniere date
where 1=1
and a.stats_date=b.stats_date
and a.table_name = af.table_name                  -- source filtr� pour tables actifs � la derniere date
and a.bytes >0
--and a.table_name = 'XLA_DISTRIBUTION_LINKS'
order by stats_date desc, bytes desc, table_name; -- By DATE DESC, and Bytes desc
--order by a.bytes desc;
--order by stats_date, table_name;
--======================================================================================================
select  * from AGR_TABLE_STATS_HIST;

--GROUPBY:
select stats_date,owner,table_name,SUM(num_rows),SUM(bytes),MAX(last_analyzed)
from AGR_TABLE_STATS_HIST
where 1=1
--table_name = 'RG_REPORT_REQUEST_LOBS' 
--and STATS_DATE > sysdate-3 
group by stats_date,owner,table_name;



--You can also see the growth of the whole database with this Oracle growth tracking script.

--Below is a great script to display table size changes between two periods. 

/*column "Percent of Total Disk Usage" justify right format 999.99
column "Space Used (MB)" justify right format 9,999,999.99
column "Total Object Size (MB)" justify right format 9,999,999.99
set linesize 150
set pages 80
set feedback off*/


 
select * from (select to_char(end_interval_time, 'MM/DD/YY') mydate, sum(space_used_delta) / 1024 / 1024 "Space used (MB)", avg(c.bytes) / 1024 / 1024 "Total Object Size (MB)",
round(sum(space_used_delta) / sum(c.bytes) * 100, 2) "Percent of Total Disk Usage"
from 
   dba_hist_snapshot sn, 
   dba_hist_seg_stat a, 
   dba_objects b, 
   dba_segments c
where 
sn.snap_id = a.snap_id
and b.object_id = a.obj#
and b.owner = c.owner
and b.object_name = c.segment_name
--and b.owner = 'OZF'
--and c.segment_name = '&segment_name'
--and begin_interval_time > trunc(sysdate) - &days_back
group by to_char(end_interval_time, 'MM/DD/YY'))
order by to_date(mydate, 'MM/DD/YY');
--see code depot download for full scripts 









--A sample of this report show the total database growth between the two snapshot periods.  Also see using v$datafile to track database growth

--Historical table & index growth reports
select * from 
(SELECT o.OWNER , o.OBJECT_NAME , o.SUBOBJECT_NAME , o.OBJECT_TYPE ,
    t.NAME "Tablespace", s.growth/(1024*1024) "Growth in MB",
    (SELECT sum(bytes)/(1024*1024*1024)
    FROM dba_segments
    WHERE segment_name=o.object_name) "Total Size(GB)"
FROM DBA_OBJECTS o,
    ( SELECT TS#,OBJ#,
        SUM(SPACE_USED_DELTA) growth
   FROM DBA_HIST_SEG_STAT
    GROUP BY TS#,OBJ#
    HAVING SUM(SPACE_USED_DELTA) > 0
    ORDER BY 2 DESC ) s,
    v$tablespace t
WHERE s.OBJ# = o.OBJECT_ID
AND s.TS#=t.TS# and o.owner='OZF' and o.object_type='TABLE'
ORDER BY 6 DESC) where rownum<20;


--============================Database Space growth using DBA_HIST tables:==================================
--Below is a small code snippet which can be used to view the database growth with 1hr intervals.
Declare
    v_BaselineSize  number(20);
    v_CurrentSize   number(20);
    v_TotalGrowth   number(20);
    v_Space     number(20);
    cursor usageHist is
            select a.snap_id,
            SNAP_TIME,
            sum(TOTAL_SPACE_ALLOCATED_DELTA) over ( order by a.SNAP_ID) ProgSum,
            b.END_INTERVAL_TIME
        from
            (select SNAP_ID,
                sum(SPACE_ALLOCATED_DELTA) TOTAL_SPACE_ALLOCATED_DELTA
            from DBA_HIST_SEG_STAT
            group by SNAP_ID
            having sum(SPACE_ALLOCATED_TOTAL) <> 0
            order by 1 ) a,
            (select distinct SNAP_ID,
                to_char(END_INTERVAL_TIME,'DD-MON-YYYY HH24:Mi') SNAP_TIME,
                END_INTERVAL_TIME
            from DBA_HIST_SNAPSHOT order by 3) b
        where a.snap_id=b.snap_id order by 4;
Begin
    select sum(SPACE_ALLOCATED_DELTA) into v_TotalGrowth from DBA_HIST_SEG_STAT;
    select sum(bytes) into v_CurrentSize from dba_segments;
    v_BaselineSize := v_CurrentSize - v_TotalGrowth ;
 
    dbms_output.put_line('SNAP_TIME           Database Size(MB)');
 
    for row in usageHist loop
            v_Space := (v_BaselineSize + row.ProgSum)/(1024*1024);
        dbms_output.put_line(row.SNAP_TIME || '           ' || to_char(v_Space) );
    end loop;
end;
--=====
select sum(bytes)/1024/1024 size_in_mb from dba_data_files;--4802357
--=============== Segments with highest growth (Top n): ===============
/*
Below is a query which can be used to query segments with highest growth. This will also report the present size of the segment which is very useful in identifying the growth percecntage.     
*/

SELECT o.OWNER , o.OBJECT_NAME , o.SUBOBJECT_NAME , o.OBJECT_TYPE ,
    t.NAME "Tablespace Name", round(s.growth/(1024*1024),2) "Growth in MB",
    round( (SELECT sum(bytes)/(1024*1024)
    FROM dba_segments
    WHERE segment_name=o.object_name),2) "Total Size(MB)"
FROM DBA_OBJECTS o,
    ( SELECT TS#,OBJ#,
        SUM(SPACE_USED_DELTA) growth
    FROM DBA_HIST_SEG_STAT
    GROUP BY TS#,OBJ#
    HAVING SUM(SPACE_USED_DELTA) > 0
    ORDER BY 2 DESC ) s,
    v$tablespace t
WHERE s.OBJ# = o.OBJECT_ID
AND s.TS#=t.TS#
--AND rownum < 51
and o.object_name = 'RG_REPORT_REQUEST_LOBS'
ORDER BY 6 DESC
;
select * from DBA_OBJECTS where object_name = 'RG_REPORT_REQUEST_LOBS';
select * from dba_segments where segment_name = 'RG_REPORT_REQUEST_LOBS';
select * from dba_tables where table_name = 'RG_REPORT_REQUEST_LOBS';

--=============== LOB TABLES SIZING Info  =======

--Get the real size of a table with blobs
/*SELECT sum( bytes)/1024/1024 size_in_MB
     FROM dba_segments
       WHERE (segment_name = 'RG_REPORT_REQUEST_LOBS'
               OR segment_name in (
                                   SELECT segment_name FROM dba_lobs
                                       WHERE table_name = 'RG_REPORT_REQUEST_LOBS'
                                   UNION
                                   SELECT index_name FROM dba_lobs
                                       WHERE table_name = 'RG_REPORT_REQUEST_LOBS'
                                  ) 
              );*/ -- not optimized
              
-- Detailed Segmets BLOB TABLES view  OPTIMIZED : 
SELECT segment_name,s.SEGMENT_TYPE, sum( bytes)/1024/1024 size_in_MB -- TabName = 'RG_REPORT_REQUEST_LOBS'
    FROM dba_segments s
     WHERE                                             s.segment_name = :TabName  -- LOB TABLE
       group by  segment_name,SEGMENT_TYPE
UNION
SELECT sl.table_name,SEGMENT_TYPE, sum( bytes)/1024/1024 size_in_MB
    FROM dba_segments s,
        (SELECT table_name, segment_name FROM dba_lobs WHERE table_name = :TabName) sl  --LOBSEGMENT
    WHERE s.segment_name = sl.segment_name
    group by  sl.table_name,s.SEGMENT_TYPE
UNION
SELECT si.table_name,SEGMENT_TYPE, sum( bytes)/1024/1024 size_in_MB
    FROM dba_segments s,
        (SELECT table_name, index_name FROM dba_lobs WHERE table_name = :TabName) si  --LOBINXED
    WHERE s.segment_name = si.index_name
    group by  si.table_name,s.SEGMENT_TYPE
    ;
    
--17 997 824
--131 072
--71 821 557 760
 
select stats_date,owner,table_name,round(sum(bytes)/1024/1024,2) size_in_MB
from AGR_TABLE_STATS_HIST
where table_name = 'RG_REPORT_REQUEST_LOBS' and STATS_DATE > sysdate-3 
group by stats_date,owner,table_name
;
    
    
    
select 
   timepoint,
   to_char(timepoint,'DD-MON-YYYY HH24:Mi') SNAP_TIME,
   round(space_usage/(1024*1024*1024)) Space_usage_GB,
   round(space_alloc/(1024*1024*1024)) Space_alloc_GB,
   --space_usage, 
  -- space_alloc, 
   quality 
from
   table(dbms_space.OBJECT_GROWTH_TREND
   ('OZF','OZF_FUNDS_UTILIZED_ALL_B','TABLE'));
   
   SELECT *
FROM TABLE(dbms_space.object_growth_trend('OZF', 'OZF_FUNDS_UTILIZED_ALL_B', 'TABLE'));

SELECT timepoint, delta_space_usage, delta_space_alloc, total_space_usage, total_space_alloc
FROM TABLE(dbms_space.object_growth_trend_swrf('OZF', 'OZF_FUNDS_UTILIZED_ALL_B', 'TABLE'));



---===========

COLUMN table_name FORMAT a32

COLUMN object_name FORMAT a32

COLUMN owner FORMAT a10

SELECT
    owner,
    table_name,
    trunc(SUM(bytes) / 1024 / 1024) meg
FROM
    (
        SELECT
            segment_name table_name,
            owner,
            bytes
        FROM
            dba_segments
        WHERE
            segment_type = 'TABLE'
        UNION ALL
        SELECT
            i.table_name,
            i.owner,
            s.bytes
        FROM
            dba_indexes    i,
            dba_segments   s
        WHERE
            s.segment_name = i.index_name
            AND s.owner = i.owner
            AND s.segment_type = 'INDEX'
        UNION ALL
        SELECT
            l.table_name,
            l.owner,
            s.bytes
        FROM
            dba_lobs       l,
            dba_segments   s
        WHERE
            s.segment_name = l.segment_name
            AND s.owner = l.owner
            AND s.segment_type = 'LOBSEGMENT'
        UNION ALL
        SELECT
            l.table_name,
            l.owner,
            s.bytes
        FROM
            dba_lobs       l,
            dba_segments   s
        WHERE
            s.segment_name = l.index_name
            AND s.owner = l.owner
            AND s.segment_type = 'LOBINDEX'
    )
WHERE
    owner IN upper('OCZ')
GROUP BY
    table_name,
    owner
HAVING
    SUM(bytes) / 1024 / 1024 > 10 /* Ignore really small tables */
ORDER BY
    SUM(bytes) DESC;
-- Source: https://prograide.com/pregunta/15704/comment-calculer-la-taille-des-tables-dans-oracle


--===================Sommainre of all Segment Type Sise usage=============================
Select segment_type, SUM(round(segment_Bytes/(1024*1024*1024))) segment_GBytes from (
    SELECT
      s.segment_name   AS segment_name,
      s.owner          AS segment_owner,
      s.partition_name AS partition_name,
      s.segment_type   AS segment_type,
      CASE WHEN s.segment_type IN ('TABLE', 'TABLE PARTITION', 'TABLE SUBPARTITION')
        THEN s.segment_name
      WHEN s.segment_type IN ('INDEX', 'INDEX PARTITION', 'INDEX SUBPARTITION')
        THEN (SELECT i.table_name
              FROM dba_indexes i
              WHERE s.segment_name = i.index_name AND s.owner = i.owner)
      WHEN s.segment_type IN ('LOBSEGMENT', 'LOB PARTITION')
        THEN (SELECT l.table_name
              FROM dba_lobs l
              WHERE s.segment_name = l.segment_name AND s.owner = l.owner)
      WHEN s.segment_type IN ('LOBINDEX')
        THEN (SELECT l.table_name
              FROM dba_lobs l
              WHERE s.segment_name = l.index_name AND s.owner = l.owner)
      ELSE 'Unknown'
      END              AS table_name,
      s.bytes          AS segment_bytes
    FROM dba_segments s
   -- WHERE owner = input_owner
    ORDER BY table_name, segment_type)
Group by SEGMENT_TYPE;
