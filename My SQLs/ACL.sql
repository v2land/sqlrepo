SELECT host, lower_port, upper_port, acl
FROM   dba_network_acls
/
 
SELECT acl,
principal,
privilege,
is_grant,
TO_CHAR(start_date, 'DD-MON-YYYY HH24:MI') AS start_date,
TO_CHAR(end_date, 'DD-MON-YYYY') AS end_date
FROM   dba_network_acl_privileges
/

-- for  dcssdebsapps 8004  add new entry 
 BEGIN
DBMS_NETWORK_ACL_ADMIN.ASSIGN_ACL (
acl => '/sys/acls/oracle-sysman-ocm-Resolve-Access.xml',
host => 'dcssdebsapps',
lower_port => 8003,
upper_port => null);
COMMIT;
END;
/