create or replace directory FND_DIAG_DIR as  '/u01/app/oracle/PROD/12.1.0/admin/PROD_ebs-stby/diag/rdbms/prod/PROD/trace';
create or replace directory APPS_DATA_FILE_DIR AS '/u01/app/oracle/PROD/12.1.0/appsutil/outbound/PROD_ebs-stby';
create or replace directory TRACE_DIR AS          '/u01/app/oracle/PROD/12.1.0/admin/PROD_ebs-stby/diag/rdbms/prod/PROD/trace';

select PROFILE_OPTION_VALUE from apps.fnd_profile_option_values 
where APPLICATION_ID = 0 
and PROFILE_OPTION_ID = 125;

update apps.fnd_profile_option_values 	
set PROFILE_OPTION_VALUE = 'PROD 12.2.3 ebs-prod:8005 Novatech (Releve24Mai2017 ebs-stby) ' 	
where APPLICATION_ID = 0 	
and PROFILE_OPTION_ID = 125;	
	
update fnd_form_functions_tl 	
set user_function_name = 'PROD 12.2.3 ebs-prod:8005 Novatech (Releve24Mai2017 ebs-stby) ' 	
where function_id in (select function_id from fnd_form_functions where function_name='FWK_HOMEPAGE_BRAND');	

commit;


select distinct
b.user_profile_option_name "Long Name"
, a.profile_option_name "Short Name"
, decode(to_char(c.level_id),'10001','Site'
,'10002','Application'
,'10003','Responsibility'
,'10004','User'
,'Unknown') "Level"
, decode(to_char(c.level_id),'10001','Site'
,'10002',nvl(h.application_short_name,to_char(c.level_value))
,'10003',nvl(g.responsibility_name,to_char(c.level_value))
,'10004',nvl(e.user_name,to_char(c.level_value))
,'Unknown') "Level Value"
, c.PROFILE_OPTION_VALUE "Profile Value"
, c.profile_option_id "Profile ID"
, to_char(c.LAST_UPDATE_DATE,'DD-MON-YYYY HH24:MI') "Updated Date"
, nvl(d.user_name,to_char(c.last_updated_by)) "Updated By"
from 
apps.fnd_profile_options a
, apps.FND_PROFILE_OPTIONS_VL b
, apps.FND_PROFILE_OPTION_VALUES c
, apps.FND_USER d
, apps.FND_USER e
, apps.FND_RESPONSIBILITY_VL g
, apps.FND_APPLICATION h
where 
--a.application_id = nvl(401, a.application_id)
--and a.profile_option_name = nvl('INV', a.profile_option_name)
b.user_profile_option_name IN (
'Novatech Order Confirmation Sender Email',
'Novatech Order Confirmation CC Email',
'Novatech Output Directory',
'Novatech PO Fax Directory',
'Novatech Work Instruction : Folder',
'Novatech: AR Invoice Email Sender',
'ICX: Oracle Payment Server URL',
'HZ: API Debug File Directory'
) -- 'AFLOG_ENABLED'
and a.profile_option_name = b.profile_option_name
and a.profile_option_id = c.profile_option_id
and a.application_id = c.application_id
and c.last_updated_by = d.user_id (+)
and c.level_value = e.user_id (+);


DECLARE
stat boolean;
BEGIN
dbms_output.disable;
dbms_output.enable(100000);
stat := FND_PROFILE.SAVE('HZ_API_DEBUG_FILE_PATH','/u01/app/oracle/PROD/fs_ne/inst/PROD_ebs-prod/applptmp' ,'SITE');
IF stat THEN dbms_output.put_line( 'profile updated' );
ELSE         dbms_output.put_line( 'profile NOT updated' );
END IF;
END;

commit;


DECLARE
stat boolean;
BEGIN
dbms_output.disable;
dbms_output.enable(100000);
stat := FND_PROFILE.SAVE('XXN_ORDER_CONF_CC_EMAIL','test.oracle@groupenovatech.com' ,'SITE');
IF stat THEN dbms_output.put_line( 'profile updated' );
ELSE         dbms_output.put_line( 'profile NOT updated' );
END IF;
END;

commit;


DECLARE
stat boolean;
BEGIN
dbms_output.disable;
dbms_output.enable(100000);
stat := FND_PROFILE.SAVE('XXN_ORDER_CONF_SENDER_EMAIL','test.oracle@groupenovatech.com' ,'SITE');
IF stat THEN dbms_output.put_line( 'profile updated' );
ELSE         dbms_output.put_line( 'profile NOT updated' );
END IF;
END;

commit;


DECLARE
stat boolean;
BEGIN
dbms_output.disable;
dbms_output.enable(100000);
stat := FND_PROFILE.SAVE('XXN_AR_INVOICE_EMAIL_SENDER','test.oracle@groupenovatech.com' ,'SITE');
IF stat THEN dbms_output.put_line( 'profile updated' );
ELSE         dbms_output.put_line( 'profile NOT updated' );
END IF;
END;

commit;

DECLARE
stat boolean;
BEGIN
dbms_output.disable;
dbms_output.enable(100000);
stat := FND_PROFILE.SAVE('XXN_OUTPUT_DIR','/u01/app/oracle/PROD/fs_ne/inst/PROD_ebs-prod/applptmp' ,'SITE');
IF stat THEN dbms_output.put_line( 'profile updated' );
ELSE         dbms_output.put_line( 'profile NOT updated' );
END IF;
END;

commit;

DECLARE
stat boolean;
BEGIN
dbms_output.disable;
dbms_output.enable(100000);
stat := FND_PROFILE.SAVE('XXN_PO_FAX_DIR','/u01/app/oracle/PROD/fs_ne/inst/PROD_ebs-prod/applptmp' ,'SITE');
IF stat THEN dbms_output.put_line( 'profile updated' );
ELSE         dbms_output.put_line( 'profile NOT updated' );
END IF;
END;

commit;

select PROFILE_OPTION_VALUE from apps.fnd_profile_option_values	
where  (APPLICATION_ID = 222 and PROFILE_OPTION_ID =3620 ) or	
       (APPLICATION_ID = 275 and PROFILE_OPTION_ID = 3504 ) or	
       (APPLICATION_ID = 690 and PROFILE_OPTION_ID  = 4555 ) or	
       (APPLICATION_ID = 20003 and PROFILE_OPTION_ID =7570 ) or	
       (APPLICATION_ID = 20003 and PROFILE_OPTION_ID =7511 ) or	
       (APPLICATION_ID = 20003 and PROFILE_OPTION_ID =7450 ) or	
       (APPLICATION_ID = 20003 and PROFILE_OPTION_ID =7310 ) or 	
       (APPLICATION_ID = 20003 and PROFILE_OPTION_ID =7311 );	


DECLARE
l_return boolean;
BEGIN
l_return := fnd_profile.save(x_name => 'XXN_TEST_EMAIL_ADDRESS', x_value => 'test.oracle@groupenovatech.com', x_level_name => 'SITE');
if l_return = true then
dbms_output.put_line('Value updated');
else
dbms_output.put_line('Error');
end if;
commit;
END;

commit;
select fnd_profile.value('XXN_TEST_EMAIL_ADDRESS') from dual;

DECLARE
l_return boolean;
BEGIN
l_return := fnd_profile.save(x_name => 'XXN_AR_INV_STATEMENT_EMAIL_CC', x_value => '', x_level_name => 'SITE');
if l_return = true then
dbms_output.put_line('Value updated');
else
dbms_output.put_line('Error');
end if;
commit;
END;

commit;

select fnd_profile.value('XXN_AR_INV_STATEMENT_EMAIL_CC') from dual;

update fnd_user set email_address ='test.oracle@groupenovatech.com';

select APPLICATION_ID, PROFILE_OPTION_ID, PROFILE_OPTION_VALUE from apps.fnd_profile_option_values			
where   (APPLICATION_ID = 178 and PROFILE_OPTION_ID = 3480 ) or			
       (APPLICATION_ID = 222 and PROFILE_OPTION_ID = 5393 ) or			
       (APPLICATION_ID = 222 and PROFILE_OPTION_ID =3620 ) or			
       (APPLICATION_ID = 275 and PROFILE_OPTION_ID = 3504 ) or			
       (APPLICATION_ID = 690 and PROFILE_OPTION_ID  = 4555 ) or			
       (APPLICATION_ID = 20003 and PROFILE_OPTION_ID =7570 ) or			
       (APPLICATION_ID = 20003 and PROFILE_OPTION_ID =7511 ) or			
       (APPLICATION_ID = 20003 and PROFILE_OPTION_ID =7450 ) or			
       (APPLICATION_ID = 20003 and PROFILE_OPTION_ID =7310 ) or 			
       (APPLICATION_ID = 20003 and PROFILE_OPTION_ID =7311 ) or			
       (APPLICATION_ID = 20003 and PROFILE_OPTION_ID =7770 ) or			
       (APPLICATION_ID = 20003 and PROFILE_OPTION_ID =7771 ) or			
       (APPLICATION_ID = 20003 and PROFILE_OPTION_ID =12553 ) or			
       (APPLICATION_ID = 20003 and PROFILE_OPTION_ID =12555 )			
       order by  APPLICATION_ID,	PROFILE_OPTION_ID asc;		










