-- Testing API Credentials   FND 
DECLARE
   p_username           VARCHAR2(500) := 'VLCASAPC';
   p_password           VARCHAR2(500) := '';
   p_return_status      VARCHAR2(10);
BEGIN
   applsyspub.xxapiw_test_validate_login (p_username, p_password, p_return_status);
   dbms_output.put_line (p_return_status);
END;


-- TESTING  FND SSO Credentials
DECLARE
 
   v_sso_login_status   BOOLEAN;
   v_login_status       VARCHAR2 (10);
   v_apps_sso           VARCHAR2 (30) := fnd_profile.VALUE ('APPS_SSO');
   v_username           VARCHAR2(500) := 'VLCASAPC';
   v_password           VARCHAR2(500) := 'XxxxxxxxX';

BEGIN

   IF (v_apps_sso LIKE '%\_SSO' ESCAPE '\') OR (v_apps_sso LIKE 'SSO\_%' ESCAPE '\')
      THEN
 
         dbms_output.put_line('Checking SSO login');
         v_sso_login_status := fnd_ldap_wrapper.validate_login (v_username, v_password);
 
         IF v_sso_login_status
            THEN
               dbms_output.put_line('SSO Valid');
            ELSE
               dbms_output.put_line('SSO Invalid');
         END IF;

      ELSE
 
         dbms_output.put_line('Checking local login');
         v_login_status := fnd_web_sec.validate_login (v_username, v_password);
 
         IF v_login_status = 'Y'
            THEN
               dbms_output.put_line('Local Valid');
            ELSE
               dbms_output.put_line('Local Invalid');
         END IF;
 
   END IF;
 
END;   




-- testing non-SSO FND Credentials
select fnd_web_sec.validate_login('VLCASAPC','Oracle123') from dual;

-- check if SSO linked 
select USER_GUID from FND_USER where USER_NAME='VLCASAPC';
