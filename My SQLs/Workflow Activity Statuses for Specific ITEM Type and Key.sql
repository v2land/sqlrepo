--prompt **** Find the Activity Statuses for all workflow activities of a given item type and item key
--Yet in #STUCK:           Item _Type:APINVHDN Item_Key:550303 - pas pay� Hold 2
SELECT execution_time,
       to_char(ias.begin_date,
               'DD-MON-RR HH24:MI:SS') begin_date,
       ap.display_name || '/' || ac.display_name activity,
       ias.activity_status status,
       ias.activity_result_code RESULT,
       ias.assigned_user ass_user
  FROM wf_item_activity_statuses ias,
       wf_process_activities     pa,
       wf_activities_vl          ac,
       wf_activities_vl          ap,
       wf_items                  i
 WHERE ias.item_type = 'APINVHDN'
   AND ias.item_key = '550303'
   AND ias.process_activity = pa.instance_id
   AND pa.activity_name = ac.name
   AND pa.activity_item_type = ac.item_type
   AND pa.process_name = ap.name
   AND pa.process_item_type = ap.item_type
   AND pa.process_version = ap.version
   AND i.item_type = 'APINVHDN'
   AND i.item_key = ias.item_key
   AND i.begin_date >= ac.begin_date
   AND i.begin_date < nvl(ac.end_date,
                          i.begin_date + 1)
UNION ALL
SELECT execution_time,
       to_char(ias.begin_date,
               'DD-MON-RR HH24:MI:SS') begin_date,
       ap.display_name || '/' || ac.display_name activity,
       ias.activity_status status,
       ias.activity_result_code RESULT,
       ias.assigned_user ass_user
  FROM wf_item_activity_statuses_h ias,
       wf_process_activities       pa,
       wf_activities_vl            ac,
       wf_activities_vl            ap,
       wf_items                    i
 WHERE ias.item_type = 'APINVHDN'
   AND ias.item_key = '550303'
   AND ias.process_activity = pa.instance_id
   AND pa.activity_name = ac.name
   AND pa.activity_item_type = ac.item_type
   AND pa.process_name = ap.name
   AND pa.process_item_type = ap.item_type
   AND pa.process_version = ap.version
   AND i.item_type = 'APINVHDN'
   AND i.item_key = ias.item_key
   AND i.begin_date >= ac.begin_date
   AND i.begin_date < nvl(ac.end_date,
                          i.begin_date + 1)
 ORDER BY 2,
          1
;


----Get the error messages of the list of all Errored Workflow Activities for a given item type/ item key------------
SELECT i.item_type,i.item_key,
       i.begin_date,
       ias.execution_time,
       ac.display_name          activity,
       ias.activity_result_code RESULT,
       ias.error_name           error_name,
       ias.error_message        error_message,
       ias.error_stack          error_stack,
       ias.activity_status,
       ias.ACTIVITY_RESULT_CODE
  FROM wf_item_activity_statuses ias,
       wf_process_activities     pa,
       wf_activities_vl          ac,
       wf_activities_vl          ap,
       wf_items                  i
 WHERE ias.item_type = 'APINVHDN'--'POAPPRV'
  AND ias.item_key = '550303'
   --AND ias.activity_status = 'ERROR'
   AND ias.process_activity = pa.instance_id
   AND pa.activity_name = ac.name
   AND pa.activity_item_type = ac.item_type
   AND pa.process_name = ap.name
   AND pa.process_item_type = ap.item_type
   AND pa.process_version = ap.version
   AND i.item_type = 'APINVHDN'-- 'APINVHDN'
   AND i.item_key = ias.item_key
   AND i.begin_date >= ac.begin_date
   AND i.begin_date < nvl(ac.end_date,
                          i.begin_date + 1)
 ORDER BY ias.BEGIN_DATE
;

--=======WFERROR related to Parent WF===========
SELECT i.item_type,i.item_key,
       i.PARENT_ITEM_Type, i.PARENT_ITEM_KEY,
       i.begin_date,
       ias.execution_time,
       ac.display_name          activity,
       ias.activity_result_code RESULT,
       ias.error_name           error_name,
       ias.error_message        error_message,
       ias.error_stack          error_stack,
       ias.activity_status,
       ias.ACTIVITY_RESULT_CODE
  FROM wf_item_activity_statuses ias,
       wf_process_activities     pa,
       wf_activities_vl          ac,
       wf_activities_vl          ap,
       wf_items                  i
 WHERE ias.item_type = 'WFERROR'--'POAPPRV'
   --AND ias.item_key = 'WF2182011'
   AND ias.activity_status not in ('COMPLETE','NOTIFIED')
   AND ias.process_activity = pa.instance_id
   AND pa.activity_name = ac.name
   AND pa.activity_item_type = ac.item_type
   AND pa.process_name = ap.name
   AND pa.process_item_type = ap.item_type
   AND pa.process_version = ap.version
   AND i.item_type = 'WFERROR'-- 'APINVHDN'
   AND i.item_key = ias.item_key
   AND i.begin_date >= ac.begin_date
   AND i.begin_date < nvl(ac.end_date,
                          i.begin_date + 1)
    AND PARENT_ITEM_TYPE = 'APINVHDN'
    --AND PARENT_ITEM_KEY = '550303'
 ORDER BY ias.BEGIN_DATE
;

  
WFERROR	WF2182057	APINVHDN	550271
WFERROR	WF2182685	APINVHDN	552265
--WFERROR	WF2264770	APINVHDN	503124	17-11-08	650	Traitement par d�faut des erreurs	#NULL				ACTIVE	#NULL
--WFERROR	WF2267612	APINVHDN	515034	17-11-08	65	Traitement par d�faut des erreurs	#NULL				ACTIVE	#NULL


--=============================================
 SELECT *-- wi.item_type, wi.item_key, wpa.instance_label, wi.parent_item_type,WIAS.ACTIVITY_STATUS
    FROM APPLSYS.WF_ITEMS wi,
      APPLSYS.WF_ITEM_ACTIVITY_STATUSES wias,
      APPLSYS.Wf_Process_Activities wpa
    WHERE wi.item_type       = wias.item_type
    AND wi.item_key          = wias.item_key
    AND wias.process_activity = wpa.instance_id
    AND wi.item_type         = 'WFERROR'
    AND wi.parent_item_type     in ('APINVHDN')
   AND WIAS.ACTIVITY_STATUS not in('COMPLETE','NOTIFIED') --'ERROR'
--    AND wias.ASSIGNED_USER = 'OLMANNA'
    --and wi.ITEM_KEY = 'WF2264770'
    ;
--593
-- check wf_notifications
select *
from wf_notifications
where item_key = '1165737';
--where notification_id = 4175820;