select 
f.concurrent_program_id, f.user_concurrent_program_name
,a.request_id, a.requested_by, u.user_name
,a.phase_code, a.status_code, a.HOLD_FLAG, a.argument_text,a.*
from    
APPLSYS.FND_CONCURRENT_REQUESTS a,
fnd_concurrent_programs_vl f,
fnd_user u 
where 
 f.CONCURRENT_PROGRAM_ID=a.CONCURRENT_PROGRAM_ID
and u.user_id = a.requested_by
and a.request_id in (select request_id from fnd_concurrent_requests where phase_code != 'C' and Hold_flag ='Y')
       and  (requested_by = 0 --SYSADMIN
                and a.concurrent_program_id in ('38121', --Gather Schema Statistics
                                              '32592', --Purge Signon Audit data
                                              '36888', --Workflow Background Process
                                              '38089', --Purge Obsolete Workflow Runtime Data
                                              '41993', --Purge Logs and Closed System Alerts
                                              '42852', --OAM Applications Dashboard Collection
                                              '43593', --Workflow Control Queue Cleanup
                                              '46792', --Resend Failed/Error Workflow Notifications
                                              '46797', --Workflow Work Items Statistics Concurrent Program
                                              '46798', --Workflow Agent Activity Statistics Concurrent Program
                                              '46799', --Workflow Mailer Statistics Concurrent Program
                                              '142393', --PURGE CONC REQUEST - G�n�rer T�ches
                                              '206395', --Agropur - Analyse CUSTOM Tables and Indexes - Apps
                                              '50769',  --Purge Inactive Sessions
                                              '32263',  --Purge Concurrent Request and/or Manager Data
                                             -- '46790',	--Workflow Directory Services User/Role Validation
                                              '66349'	--Audit: Dequeue Process
                                                       )
            )
;
            
            
/*Update APPLSYS.FND_CONCURRENT_REQUESTS
Set    Hold_flag ='N'
Where  Hold_flag ='Y'  
and phase_code != 'C'
       and  (requested_by = 0 --SYSADMIN
                and concurrent_program_id in ('38121', --Gather Schema Statistics
                                              '32592', --Purge Signon Audit data
                                              '36888', --Workflow Background Process
                                              '38089', --Purge Obsolete Workflow Runtime Data
                                              '41993', --Purge Logs and Closed System Alerts
                                              '42852', --OAM Applications Dashboard Collection
                                              '43593', --Workflow Control Queue Cleanup
                                              '46792', --Resend Failed/Error Workflow Notifications
                                              '46797', --Workflow Work Items Statistics Concurrent Program
                                              '46798', --Workflow Agent Activity Statistics Concurrent Program
                                              '46799', --Workflow Mailer Statistics Concurrent Program
                                              '142393', --PURGE CONC REQUEST - G�n�rer T�ches
                                              '206395', --Agropur - Analyse CUSTOM Tables and Indexes - Apps
                                              '50769',  --Purge Inactive Sessions
                                              '32263',  --Purge Concurrent Request and/or Manager Data
                                              '46790',	--Workflow Directory Services User/Role Validation
                                              '66349'	--Audit: Dequeue Process
                                                       )
                )
;
rollback;
*/


--CHECK Concurrent requests :

select 
f.concurrent_program_id, f.user_concurrent_program_name
,a.request_id, a.requested_by, u.user_name
,a.phase_code, a.status_code, a.HOLD_FLAG, a.*
from    
APPLSYS.FND_CONCURRENT_REQUESTS a,
fnd_concurrent_programs_vl f,
fnd_user u 
where 
 f.CONCURRENT_PROGRAM_ID=a.CONCURRENT_PROGRAM_ID
and u.user_id = a.requested_by
and a.request_id in (select request_id from fnd_concurrent_requests where phase_code != 'C' )
/*and a.concurrent_program_id in ('38121', --Gather Schema Statistics
                                              '32592', --Purge Signon Audit data
                                              '36888', --Workflow Background Process
                                              '38089', --Purge Obsolete Workflow Runtime Data
                                              '41993', --Purge Logs and Closed System Alerts
                                              '42852', --OAM Applications Dashboard Collection
                                              '43593', --Workflow Control Queue Cleanup
                                              '46792', --Resend Failed/Error Workflow Notifications
                                              '46797', --Workflow Work Items Statistics Concurrent Program
                                              '46798', --Workflow Agent Activity Statistics Concurrent Program
                                              '46799', --Workflow Mailer Statistics Concurrent Program
                                              '142393', --PURGE CONC REQUEST - G�n�rer T�ches
                                              '206395', --Agropur - Analyse CUSTOM Tables and Indexes - Apps
                                              '50769',  --Purge Inactive Sessions
                                              '32263',  --Purge Concurrent Request and/or Manager Data
                                              '46790',	--Workflow Directory Services User/Role Validation
                                              '66349'	--Audit: Dequeue Process
                                                       )*/
    and  requested_by = 0                 
--and not requested_by in(1161,9512)
;


--	Workflow Background Proces
--DEV1,,3,4,5,6,7,8,9,E,A,B,C,D,Te,ACCE,PROD:
--36888	Workflow Background Process	21021600	0	SYSADMIN	P	Q	N	, , , Y, N, N, 
--36888	Workflow Background Process	21020105	0	SYSADMIN	P	Q	N	, , , N, N, Y
--36888	Workflow Background Process	21021522	0	SYSADMIN	P	Q	N	, , , N, Y, N, 

select 
f.concurrent_program_id, f.user_concurrent_program_name
,a.request_id, a.requested_by, u.user_name
,a.phase_code, a.status_code, a.HOLD_FLAG, a.argument_text, a.RESUBMIT_INTERVAL, a.RESUBMIT_INTERVAL_TYPE_CODE, a.RESUBMIT_INTERVAL_UNIT_CODE, a.*
from    
APPLSYS.FND_CONCURRENT_REQUESTS a,
fnd_concurrent_programs_vl f,
fnd_user u 
where 
 f.CONCURRENT_PROGRAM_ID=a.CONCURRENT_PROGRAM_ID
and u.user_id = a.requested_by
and  phase_code != 'C' --and Hold_flag ='N'
--and a.ARGUMENT4='Y'
--and a.ARGUMENT5='N'
--and a.ARGUMENT6='N'
and a.concurrent_program_id = '36888'  --	Workflow Background Process
;

--CHECK On Hold Concurrent requests :

select 
f.concurrent_program_id, f.user_concurrent_program_name
,a.request_id, a.requested_by, u.user_name
,a.phase_code, a.status_code, a.HOLD_FLAG, a.argument_text, a.RESUBMIT_INTERVAL, a.RESUBMIT_INTERVAL_TYPE_CODE, a.RESUBMIT_INTERVAL_UNIT_CODE, a.*
from    
APPLSYS.FND_CONCURRENT_REQUESTS a,
fnd_concurrent_programs_vl f,
fnd_user u 
where 
 f.CONCURRENT_PROGRAM_ID=a.CONCURRENT_PROGRAM_ID
and u.user_id = a.requested_by
and  phase_code != 'C' and Hold_flag ='Y' -- 
;


--374 PROD Q, I
--CHECK Scheduled Concurrent requests :
select 
f.concurrent_program_id, f.user_concurrent_program_name
,a.request_id, a.requested_by, u.user_name
,a.phase_code, a.status_code, a.HOLD_FLAG, a.argument_text, a.RESUBMIT_INTERVAL, a.RESUBMIT_INTERVAL_TYPE_CODE, a.RESUBMIT_INTERVAL_UNIT_CODE, a.*
from    
APPLSYS.FND_CONCURRENT_REQUESTS a,
fnd_concurrent_programs_vl f,
fnd_user u 
where 
 f.CONCURRENT_PROGRAM_ID=a.CONCURRENT_PROGRAM_ID
and u.user_id = a.requested_by
and phase_code in ('P')
--and   STATUS_CODE in ('Q','I')
AND requested_start_date > SYSDATE
AND hold_flag = 'N'
order by 3 desc;

--CHECK Pending Standby Concurrent requests :
select 
f.concurrent_program_id, f.user_concurrent_program_name
,a.request_id, a.requested_by, u.user_name
,a.phase_code, a.status_code, a.HOLD_FLAG, a.argument_text, a.RESUBMIT_INTERVAL, a.RESUBMIT_INTERVAL_TYPE_CODE, a.RESUBMIT_INTERVAL_UNIT_CODE, a.*
from    
APPLSYS.FND_CONCURRENT_REQUESTS a,
fnd_concurrent_programs_vl f,
fnd_user u 
where 
 f.CONCURRENT_PROGRAM_ID=a.CONCURRENT_PROGRAM_ID
and u.user_id = a.requested_by
and phase_code in ('P')
--and   STATUS_CODE in ('Q','I')
AND requested_start_date > SYSDATE
AND hold_flag = 'N'
order by 3 desc;


SELECT user_concurrent_program_name,
         DECODE (phase_code,
                 'C', 'Completed',
                 'I', 'Inactive',
                 'P', 'Pending',
                 'R', 'Running') Phase,
         DECODE (status_code,
                 'A', 'Waiting''B',
                 'Resuming''C', 'Normal''D',
                 'Cancelled''E', 'Error''F',
                 'Scheduled''G', 'Warning''H',
                 'On Hold''I', 'Normal''M',
                 'No Manager''Q', 'Standby''R',
                 'Normal''S', 'Suspended''T',
                 'Terminating''U', 'Disabled''W',
                 'Paused''X', 'Terminated''Z',
                 'Waiting') status,
            actual_start_date,
         actual_completion_date,
         completion_text,
         argument_text,
         requestor--, cp.*
    FROM fnd_conc_req_summary_v cp;
    
--LIST OF PHASES AND CODES:    

    select * from fnd_lookups 
where 
LOOKUP_TYPE like 'CP_PHASE_CODE';

select * from fnd_lookups 
where 
LOOKUP_TYPE like 'CP_STA%'; 

select * from FND_CONC_REQ_SUMMARY_V;

--#######################################################################
--############# Selects of CMClean ##########################################################
--REM Select process status codes that are TERMINATED of Concurrent Managers 
--prompt -- Select invalid process status codes in FND_CONCURRENT_PROCESSES
-- A=Active - OK
SELECT concurrent_queue_name manager,
concurrent_process_id pid,
process_status_code pscode,fcp.PROCESS_START_DATE,OS_PROCESS_ID,fcp.LOGFILE_NAME,fcp.SERVICE_PARAMETERS
FROM fnd_concurrent_queues fcq, fnd_concurrent_processes fcp
WHERE process_status_code not in ('K', 'S')
AND fcq.concurrent_queue_id = fcp.concurrent_queue_id
AND fcq.application_id = fcp.queue_application_id
order by 2,1;

--REM List invalid control codes
--prompt -- Listing invalid control_codes in FND_CONCURRENT_QUEUES
SELECT concurrent_queue_name manager,
control_code ccode
FROM fnd_concurrent_queues
WHERE control_code not in ('E', 'R', 'X')
AND control_code IS NOT NULL;

--REM List Target Node for All Managers
--REM Identify the target_node for all managers
--prompt -- Identify the target_node for all managers
select target_node from fnd_concurrent_queues;

--REM List Running or Terminating requests
--prompt -- Select Running or Terminating requests
SELECT request_id request,
phase_code pcode,
status_code scode, a.*
FROM fnd_concurrent_requests a
WHERE status_code = 'T' OR phase_code = 'R'
ORDER BY request_id;


--###########################################################################################
--#  Start Differ Stop date or requests   (INVESTIGATIOn REQUESTED_DATE differ from STARTED)
--#########################################################################################
select 
f.concurrent_program_id "conc pr ID", f.concurrent_program_name,f.user_concurrent_program_name
,a.request_id, a.PARENT_REQUEST_ID, a.requested_by, u.user_name
,To_Char(a.request_date,'DD-MON-YY HH24:MI:SS') submitted
,To_Char(a.REQUESTED_START_DATE,'DD-MON-YY HH24:MI:SS') REQUESTED_START_DATE
,To_Char(a.ACTUAL_START_DATE,'DD-MON-YY HH24:MI:SS') started
,To_Char(a.ACTUAL_COMPLETION_DATE,'DD-MON-YY HH24:MI:SS') completed
,a.phase_code, a.status_code, a.HOLD_FLAG, a.argument_text, 
a.RESUBMIT_INTERVAL, a.RESUBMIT_INTERVAL_TYPE_CODE, a.RESUBMIT_INTERVAL_UNIT_CODE
from    
APPLSYS.FND_CONCURRENT_REQUESTS a,
fnd_concurrent_programs_vl f,
fnd_user u 
where 
f.CONCURRENT_PROGRAM_ID=a.CONCURRENT_PROGRAM_ID
and u.user_id = a.requested_by
--and a.status_code = 'C' and a.phase_code = 'C'
--and a.ACTUAL_COMPLETION_DATE> SYSDATE-30
and a.REQUESTED_START_DATE>SYSDATE-1
--and request_id in ('21654900','21654549','21647375','21646020','21645974')
--and f.concurrent_program_name LIKE 'XXAOL0240_AUTO%'     ---Request Set Archiving AUTO Documents Interface
--and u.user_name = 'XXAPPSADM'
--and a.ACTUAL_START_DATE > (a.REQUESTED_START_DATE + INTERVAL '59' MINUTE)  -- !!! CHECHK The BUG SR		Severity 4		SR 3-18426826121 : The requested_start_date in the fnd_concurrent_requests is some time one day late
--and f.user_concurrent_program_name like 'Archiving%'
--# Running at specific moment
--and a.ACTUAL_START_DATE < To_Date(:State_CHECK_DATE,'DD-MON-YY HH24:MI:SS')
--and a.ACTUAL_COMPLETION_DATE > To_Date(:State_CHECK_DATE,'DD-MON-YY HH24:MI:SS')
--#RUNNING between specific period:
and a.ACTUAL_COMPLETION_DATE > To_Date(:State_Begin_CHECK_DATE,'DD-MON-YY HH24:MI:SS')
and a.ACTUAL_START_DATE <= To_Date(:State_End_CHECK_DATE,'DD-MON-YY HH24:MI:SS')
order by a.ACTUAL_START_DATE desc;

--1) add a day
select sysdate + INTERVAL '1' DAY from dual;
--2) add 20 days
select sysdate + INTERVAL '20' DAY from dual;
--2) add some minutes
select sysdate + INTERVAL '1' DAY  from dual;






--240448	XXAOL0240_AUTO	Request Set Archiving AUTO Documents Interface	22058896	1429	XXAPPSADM	21-NOV-18 18:34:51	22-NOV-18 05:00:00	22-NOV-18 08:16:18	22-NOV-18 08:54:07	C	C	N	20005, 2038	1	START	DAYS
/*
240448	XXAOL0240_AUTO	Request Set Archiving AUTO Documents Interface	21710127	1429	XXAPPSADM	09-NOV-18 05:13:02	10-NOV-18 05:00:00			P	Q	Y	20005, 2038	1	START	DAYS
*/

--##############################################
--------       INCOMPATIBLES

--Query to find the Concurrent Requests which are incompatible with Request
--input:    Concurrent program name
SELECT distinct fcpt.user_concurrent_program_name Input_Program,
            (SELECT fcpt1.user_concurrent_program_name Incompatible_Programs
                FROM fnd_concurrent_programs_tl   fcpt1
             WHERE fcpt1.concurrent_program_id in (fcps.to_run_concurrent_program_id)
                  AND rownum = 1) Incompatible_Programs
   FROM fnd_concurrent_program_serial fcps
            ,fnd_concurrent_programs_tl       fcpt       
WHERE fcps.running_concurrent_program_id = fcpt.concurrent_program_id
     AND fcpt.user_concurrent_program_name like '%'||:Program_Name||'%';  --XXAOL0240_MANUAL	Request Set --###########################################################################################
      --and fcpt.concurrent_program_id in ('240449') ;
     /*
Request Set Archiving AUTO Documents Interface	Jeu demandes AGR: FACTURATION VALIDATION QUOT. (CRP)
Request Set Archiving AUTO Documents Interface	Request Set AR - Print new invoices (CRP)
Request Set Archiving AUTO Documents Interface	Request Set Archiving AUTO Documents Interface
Request Set Archiving AUTO Documents Interface	Request Set Archiving CLAIMS Documents Interface
Request Set Archiving AUTO Documents Interface	OUT: EDI 882 Sommaire Factures
Request Set Archiving AUTO Documents Interface	Request Set Archiving INVOICE Documents Interface
Request Set Archiving AUTO Documents Interface	Request Set Archiving MANUAL Documents Interface
*/
SELECT * FROM fnd_concurrent_program_serial fcps order by 2 desc;
SELECT * from fnd_concurrent_programs_tl fcpt;  
WHERE fcps.running_concurrent_program_id = fcpt.concurrent_program_id
      AND fcpt.language = USERENV('Lang')
     AND fcpt.user_concurrent_program_name like '%'||:Program_Name||'%';  --Request Set Archiving AUTO Documents Interface   #######################################################################
    --and fcpt.concurrent_program_id in ('240454');
    --and fcps.to_run_concurrent_program_id ='';


SELECT *
   FROM fnd_concurrent_program_serial fcps  ,fnd_concurrent_programs_tl fcpt  
WHERE fcps.running_concurrent_program_id = fcpt.concurrent_program_id
      AND fcpt.language = USERENV('Lang')
     AND fcpt.user_concurrent_program_name like '%'||:Program_Name||'%';  --Request Set Archiving AUTO Documents Interface   #######################################################################
    --and fcpt.concurrent_program_id in ('240454');
    --and fcps.to_run_concurrent_program_id ='';

-- SIMPLE:
SELECT  fcpt.CONCURRENT_PROGRAM_ID,fcpt.user_concurrent_program_name Input_Program,
            fcps.to_run_concurrent_program_id,
                        (SELECT fcpt1.user_concurrent_program_name Incompatible_Programs
                FROM fnd_concurrent_programs_tl   fcpt1
             WHERE fcpt1.concurrent_program_id in (fcps.to_run_concurrent_program_id)
                  AND rownum = 1) Incompatible_Programs
   FROM fnd_concurrent_program_serial fcps
            ,fnd_concurrent_programs_tl       fcpt  
WHERE fcps.running_concurrent_program_id = fcpt.concurrent_program_id
      AND fcpt.language = USERENV('Lang')
     AND fcpt.user_concurrent_program_name like '%'||:Program_Name||'%'  --XXAOL0240_MANUAL	Request Set --###########################################################################################
    --and fcpt.concurrent_program_id in ('240454')
    ;
    
    

-- DRILLED DOWN
SELECT  fcpt.CONCURRENT_PROGRAM_ID,fcpt.user_concurrent_program_name Input_Program,
            fcps.to_run_concurrent_program_id,
                        (SELECT fcpt1.user_concurrent_program_name Incompatible_Programs
                FROM fnd_concurrent_programs_tl   fcpt1
             WHERE fcpt1.concurrent_program_id in (fcps.to_run_concurrent_program_id)
                  AND rownum = 1) Incompatible_Programs
   FROM fnd_concurrent_program_serial fcps
            ,fnd_concurrent_programs_tl       fcpt  
WHERE fcps.running_concurrent_program_id = fcpt.concurrent_program_id
      AND fcpt.language = USERENV('Lang')
     AND fcpt.user_concurrent_program_name like '%'||:Program_Name||'%'  --XXAOL0240_MANUAL	Request Set --###########################################################################################
    --and fcpt.concurrent_program_id in ('240449') ;
--AND Sub request sets incompatibility
UNION

SELECT fcpt.CONCURRENT_PROGRAM_ID,fcpt.user_concurrent_program_name Input_Program,
            fcps.to_run_concurrent_program_id,            
            (SELECT fcpt1.user_concurrent_program_name Incompatible_Programs
                FROM fnd_concurrent_programs_tl   fcpt1
             WHERE fcpt1.concurrent_program_id in (fcps.to_run_concurrent_program_id)
                  AND rownum = 1) Incompatible_Programs
   FROM fnd_concurrent_program_serial fcps
            ,fnd_concurrent_programs_tl   fcpt  
WHERE  fcps.running_concurrent_program_id = fcpt.concurrent_program_id
    AND fcpt.language = USERENV('Lang')
    and fcpt.concurrent_program_id in 
                (SELECT  fcps.to_run_concurrent_program_id
                       FROM fnd_concurrent_program_serial fcps
                                ,fnd_concurrent_programs_tl       fcpt  
                    WHERE fcps.running_concurrent_program_id = fcpt.concurrent_program_id
                         AND fcpt.user_concurrent_program_name like '%'||:Program_Name||'%' 
                        AND fcpt.language = USERENV('Lang')
                
                ) 
order by 1,3;

     
/*
240448	Request Set Archiving AUTO Documents Interface	240408	OUT: EDI 882 Sommaire Factures
240448	Request Set Archiving AUTO Documents Interface	240448	Request Set Archiving AUTO Documents Interface
240448	Request Set Archiving AUTO Documents Interface	240449	Request Set Archiving CLAIMS Documents Interface
240448	Request Set Archiving AUTO Documents Interface	240450	Request Set Archiving INVOICE Documents Interface
240448	Request Set Archiving AUTO Documents Interface	240451	Request Set Archiving MANUAL Documents Interface
240448	Request Set Archiving AUTO Documents Interface	240456	Jeu demandes AGR: FACTURATION VALIDATION QUOT. (CRP)
240448	Request Set Archiving AUTO Documents Interface	240458	Request Set AR - Print new invoices (CRP)
*/
--###########################################################################################
-- ###############           ALL INCOMPATIBILITIES    ###################################--
--###########################################################################################
--How to check  Incompatibilities in concurrent requests

SELECT a2.application_name, a1.user_concurrent_program_name,
DECODE (running_type,
'P', 'Program',
'S', 'Request set',
'UNKNOWN'
) "Type",
a1.CONCURRENT_PROGRAM_ID CONC_PR_ID,
b1.CONCURRENT_PROGRAM_ID Incomp_CONC_PR_ID,
b2.application_name "Incompatible App", 
b1.user_concurrent_program_name "Incompatible_Prog",
DECODE (to_run_type,
'P', 'Program',
'S', 'Request set',
'UNKNOWN'
) incompatible_type
FROM apps.fnd_concurrent_program_serial cps,
apps.fnd_concurrent_programs_tl a1,
apps.fnd_concurrent_programs_tl b1,
apps.fnd_application_tl a2,
apps.fnd_application_tl b2
WHERE a1.application_id = cps.running_application_id
AND a1.concurrent_program_id = cps.running_concurrent_program_id
AND a2.application_id = cps.running_application_id
AND b1.application_id = cps.to_run_application_id
AND b1.concurrent_program_id = cps.to_run_concurrent_program_id
AND b2.application_id = cps.to_run_application_id
AND a1.language = 'US'
AND a2.language = 'US'
AND b1.language = 'US'
AND b2.language = 'US'
--and a1.user_concurrent_program_name like '%Request Set Archiving AUTO Documents Interface%'
;


SELECT request_id, status_code, phase_code
from fnd_concurrent_requests
WHERE request_id = '21710127';

----------------- connect_by syntax -----------------------------
SELECT cat.CONCURRENT_PROGRAM_ID, items.CONCURRENT_PROGRAM_ID
  FROM fnd_concurrent_programs_tl cat JOIN fnd_concurrent_programs_tl items ON cat.CONCURRENT_PROGRAM_ID = items.CONCURRENT_PROGRAM_ID
 START WITH cat.CONCURRENT_PROGRAM_ID = 1  -- ID of the starting category
 CONNECT BY PRIOR item.to_run_concurrent_program_id = cat.running_concurrent_program_id;
 
 
 SELECT to_run_concurrent_program_id , running_concurrent_program_id
FROM fnd_concurrent_program_serial 
  START WITH to_run_concurrent_program_id = 256397 
  CONNECT BY PRIOR to_run_concurrent_program_id = running_concurrent_program_id;

 SELECT 
       t1.running_concurrent_program_id parent_id,
       t1.to_run_concurrent_program_id id, 
       --t1.name,
       t2.to_run_concurrent_program_id AS parent_id
       --t2.name AS parent_name,
        FROM fnd_concurrent_program_serial t1 LEFT JOIN fnd_concurrent_program_serial t2 ON t1.running_concurrent_program_id = t2.to_run_concurrent_program_id 
START WITH t1.to_run_concurrent_program_id = '256397' 
CONNECT BY PRIOR t1.to_run_concurrent_program_id = t1.running_concurrent_program_id;

-- Request Set execution programs,
--  with connect_by syntax


SELECT /*+ index (fcr1 fnd_concurrent_requests_n3) */ fcr1.request_id,
        FROM   apps.fnd_concurrent_requests fcr1
        WHERE  1 = 1
        START WITH fcr1.request_id = 21710127  -- SET
        CONNECT BY PRIOR fcr1.request_id = fcr1.parent_request_id;


----------Details of a Concurrent program Request set:
--  using connect by prior

SELECT /*+ ORDERED USE_NL(x fcr fcp fcptl)*/
                                fcr.request_id
                                "Request ID",
                                             fcptl.user_concurrent_program_name
                                "Program Name"
                                ,
                                fcr.phase_code,
                                fcr.status_code,
                                --     to_char(fcr.request_date,'DD-MON-YYYY HH24:MI:SS') "Submitted", 
                                --     (fcr.actual_start_date - fcr.request_date)*1440 "Delay",
                                To_char(fcr.actual_start_date,
                                'DD-MON-YYYY HH24:MI:SS')      "Start Time",
                                To_char(fcr.actual_completion_date,
                                'DD-MON-YYYY HH24:MI:SS') "End Time",
                                ( fcr.actual_completion_date -
                                  fcr.actual_start_date ) * 1440 "Elapsed",
                                fcr.oracle_process_id
                                "Trace ID"
FROM   (SELECT /*+ index (fcr1 fnd_concurrent_requests_n3) */ fcr1.request_id
        FROM   apps.fnd_concurrent_requests fcr1
        WHERE  1 = 1
        START WITH fcr1.request_id = 21710127
        CONNECT BY PRIOR fcr1.request_id = fcr1.parent_request_id) x,
       apps.fnd_concurrent_requests fcr,
       apps.fnd_concurrent_programs fcp,
       apps.fnd_concurrent_programs_tl fcptl
WHERE  fcr.request_id = x.request_id
       AND fcr.concurrent_program_id = fcp.concurrent_program_id
       AND fcr.program_application_id = fcp.application_id
       AND fcp.application_id = fcptl.application_id
       AND fcp.concurrent_program_id = fcptl.concurrent_program_id
       AND fcptl.LANGUAGE = 'US'

ORDER  BY fcr.actual_start_date;




--# -- Qu'est-ce que a roull� entre  08-NOV-18 05:45:00	08-NOV-18 10:39:16   (INVESTIGATIOn REQUESTED_DATE differ from STARTED)
--#########################################################################################
select 
f.concurrent_program_id "conc pr ID", f.concurrent_program_name,f.user_concurrent_program_name
,a.request_id, a.requested_by, u.user_name
,To_Char(a.request_date,'DD-MON-YY HH24:MI:SS') submitted
,To_Char(a.REQUESTED_START_DATE,'DD-MON-YY HH24:MI:SS') REQUESTED_START_DATE
,To_Char(a.ACTUAL_START_DATE,'DD-MON-YY HH24:MI:SS') started
,To_Char(a.ACTUAL_COMPLETION_DATE,'DD-MON-YY HH24:MI:SS') completed
,a.phase_code, a.status_code, a.HOLD_FLAG, a.argument_text, 
a.RESUBMIT_INTERVAL, a.RESUBMIT_INTERVAL_TYPE_CODE, a.RESUBMIT_INTERVAL_UNIT_CODE
from    
APPLSYS.FND_CONCURRENT_REQUESTS a,
fnd_concurrent_programs_vl f,
fnd_user u 
where 
f.CONCURRENT_PROGRAM_ID=a.CONCURRENT_PROGRAM_ID
and u.user_id = a.requested_by
--and a.status_code = 'C' and a.phase_code = 'C'
--and a.ACTUAL_COMPLETION_DATE> SYSDATE-30
and a.REQUEST_DATE>SYSDATE-1
--and request_id in ('21654900','21654549','21647375','21646020','21645974')
--and f.concurrent_program_name LIKE 'XXAOL0240%'
--and u.user_name = 'XXAPPSADM'
--and a.REQUESTED_START_DATE > to_date('08-NOV-18 05:45:00','DD-MON-YY HH24:MI:SS')
--and a.ACTUAL_START_DATE < to_date('08-NOV-18 10:39:16','DD-MON-YY HH24:MI:SS')
and f.user_concurrent_program_name in
(SELECT  (SELECT fcpt1.user_concurrent_program_name Incompatible_Programs
                FROM fnd_concurrent_programs_tl   fcpt1
             WHERE fcpt1.concurrent_program_id in (fcps.to_run_concurrent_program_id)
                  AND rownum = 1) Incompatible_Programs
  FROM fnd_concurrent_program_serial fcps
      ,fnd_concurrent_programs_tl       fcpt       
  WHERE fcps.running_concurrent_program_id = fcpt.concurrent_program_id
    -- AND fcpt.user_concurrent_program_name like '%'||:Program_Name||'%'  --XXAOL0240_MANUAL	Request Set Archiving MANUAL Documents Interface --Archiving e-Commerce invoice file
        and fcpt.CONCURRENT_PROGRAM_ID in
        ( 
        SELECT (SELECT fcpt1.CONCURRENT_PROGRAM_ID Incompatible_Programs_ID
                FROM fnd_concurrent_programs_tl   fcpt1
             WHERE fcpt1.concurrent_program_id in (fcps.to_run_concurrent_program_id)
                  AND rownum = 1) Incompatible_Programs_ID
        FROM fnd_concurrent_program_serial fcps
            ,fnd_concurrent_programs_tl       fcpt       
        WHERE fcps.running_concurrent_program_id = fcpt.concurrent_program_id
              AND fcpt.user_concurrent_program_name like '%'||:Program_Name||'%'  --XXAOL0240_MANUAL	Request Set --###########################################################################################
         )   
     )
order by 4 desc; --MANUAL Documents Interface
     

--###########################################################################################
--# -- Qu'est-ce que a roull� entre  08-NOV-18 05:45:00	08-NOV-18 10:39:16   (INVESTIGATIOn REQUESTED_DATE differ from STARTED)
--#########################################################################################
SELECT
    f.concurrent_program_id "conc pr ID",
    f.concurrent_program_name,
    f.user_concurrent_program_name,
    a.request_id,
    a.requested_by,
    u.user_name,
    TO_CHAR(a.request_date,'DD-MON-YY HH24:MI:SS') submitted,
    TO_CHAR(a.requested_start_date,'DD-MON-YY HH24:MI:SS') requested_start_date,
    TO_CHAR(a.actual_start_date,'DD-MON-YY HH24:MI:SS') started,
    TO_CHAR(a.actual_completion_date,'DD-MON-YY HH24:MI:SS') completed,
    a.phase_code,
    a.status_code,
    a.hold_flag,
    a.argument_text,
    a.resubmit_interval,
    a.resubmit_interval_type_code,
    a.resubmit_interval_unit_code
FROM
    applsys.fnd_concurrent_requests a,
    fnd_concurrent_programs_vl f,
    fnd_user u
WHERE
    f.concurrent_program_id = a.concurrent_program_id
    AND   u.user_id = a.requested_by
--and a.status_code = 'C' and a.phase_code = 'C'
--and a.ACTUAL_COMPLETION_DATE> SYSDATE-30
    AND   a.request_date > SYSDATE - 1
--and request_id in ('21654900','21654549','21647375','21646020','21645974')
and f.concurrent_program_name LIKE 'XXAOL%'
--and u.user_name = 'XXAPPSADM'
 --   AND   a.requested_start_date > TO_DATE('08-NOV-18 05:00:00','DD-MON-YY HH24:MI:SS')
   -- AND   a.actual_start_date < TO_DATE('08-NOV-18 10:39:16','DD-MON-YY HH24:MI:SS')
    AND   f.user_concurrent_program_name IN (
        SELECT
            (
                SELECT
                    fcpt1.user_concurrent_program_name incompatible_programs
                FROM
                    fnd_concurrent_programs_tl fcpt1
                WHERE
                    fcpt1.concurrent_program_id IN (
                        fcps.to_run_concurrent_program_id
                    )
                    AND   ROWNUM = 1
            ) incompatible_programs
        FROM
            fnd_concurrent_program_serial fcps,
            fnd_concurrent_programs_tl fcpt
        WHERE
            fcps.running_concurrent_program_id = fcpt.concurrent_program_id
            AND   fcpt.user_concurrent_program_name LIKE '%'
            ||:program_name
            || '%'  --XXAOL0240_MANUAL	Request Set Archiving MANUAL Documents Interface
    )
ORDER BY
    8;



