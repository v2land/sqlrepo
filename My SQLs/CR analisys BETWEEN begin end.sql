select
fcr.request_id,
usr.user_name FND_USER,
fcpt.user_concurrent_program_name,
--fcp.concurrent_program_name pname,
To_Char(fcr.actual_start_date, 'DD-MON-YYYY HH24:MI:SS') actual_start_date,
TO_CHAR(fcr.Actual_Completion_Date, 'DD-MON-YYYY HH24:MI:SS')Actual_Completion_Date,
round((fcr.actual_start_date - fcr.requested_start_date)*60*24,2) wait_time,
round((fcr.actual_completion_date - fcr.actual_start_date)*60*24,2) run_time,
que.concurrent_queue_name conc_que,
DECODE(fcr.phase_code, 'C', 'Completed', 'I', 'Inactive', 'P', 'Pending', 'R', 'Running', Fcr.Phase_Code) Phase_Code,
DECODE (Fcr.Status_Code, 'A', 'Waiting', 'B', 'Resuming', 'C', 'Normal', 'D', 'Cancelled', 'E', 'Error', 'F', 'Scheduled', 'G', 'Warning', 'H', 'On Hold', 'I', 'Normal', 'M', 'No Manager', 'Q', 'Standby', 'R', 'Normal', 'S', 'Suspended', 'T', 'Terminating', 'U', 'Disabled', 'W', 'Paused', 'X', 'Terminated', 'Z', 'Waiting', Fcr.Status_Code) Status_Code,
fcr.completion_text,
fcr.argument_text args,
fcr.logfile_name,
fcr.outfile_name
from
fnd_concurrent_queues que,
fnd_user usr,
fnd_concurrent_programs fcp,
fnd_concurrent_requests fcr,
fnd_concurrent_processes fcps,
fnd_concurrent_programs_tl fcpt
where
fcr.actual_start_date < to_date(:date_end, 'DD-MON-YYYY HH24:MI:SS')
and actual_completion_date > to_date(:date_begin, 'DD-MON-YYYY HH24:MI:SS')
--(actual_start_date between to_date('&start_date', 'DD-MON-YYYY HH24:MI:SS')and to_date('&end_date', 'DD-MON-YYYY HH24:MI:SS')
--    or actual_completion_date between to_date('&start_dte', 'DD-MON-YYYY HH24:MI:SS')
--and fcp.concurrent_program_name = 'WSHINTERFACES'
and que.application_id= fcps.queue_application_id
and que.concurrent_queue_id = fcps.concurrent_queue_id
and fcr.controlling_manager= fcps.concurrent_process_id
and usr.user_id = fcr.requested_by
and fcp.concurrent_program_id = fcr.concurrent_program_id
and fcp.application_id = fcr.program_application_id
and fcp.concurrent_program_name not in ('ACTIVATE','ABORT','DEACTIVATE','VERIFY')
AND fcr.concurrent_program_id = fcpt.concurrent_program_id
AND fcpt.language = USERENV ('LANG')
order by fcr.actual_start_date;