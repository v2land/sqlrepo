 select * from ad_bugs where bug_number='18505048';
--================Update Site=========================

select a.rowid, a.* from axf.AXF_CONFIGS a;
/*
AACChxAA1AAAAA0AAA	1	AP_APXINWKB	http://WLSDEV5APPS:16000/axf-ws/AxfSolutionMediatorService	AP_INVOICES	YES	INV_SUM_FOLDER	0	29-MAY-14	29-MAY-14	0	0
AACChxAA1AAAAA0AAB	2	AP_APXINWKB_SUMMARY_VIEW	http://WLSDEV5APPS:16000/axf-ws/AxfSolutionMediatorService	AP_INVOICES	YES	INV_SUM_FOLDER	0	29-MAY-14	29-MAY-14	0	0
AACChxAA1AAAAA0AAC	3	AP_APXVDMVD	http://WLSDEV5APPS:16000/axf-ws/AxfSolutionMediatorService	AP_SUPPLIERS	YES	VNDR	0	29-MAY-14	29-MAY-14	0	0
AACChxAA1AAAAA0AAD	4	AP_APXVDMVD_VIEW	http://WLSDEV5APPS:16000/axf-ws/AxfSolutionMediatorService	AP_SUPPLIERS	1	VNDR	0	29-MAY-14	29-MAY-14	0	0
AACChxAA1AAAAA0AAE	5	AP_APXINWKB_BATCHES	http://WLSDEV5APPS:16000/axf-ws/AxfSolutionMediatorService	AP_INVOICES	YES	INV_SUM_FOLDER	0	29-MAY-14	29-MAY-14	0	0
*/
--correct APPS server for WCI is DEVE
select a.rowid, a.* from axf.AXF_CONFIGS a;
update axf.AXF_CONFIGS set SOLUTIONENDPOINT = 'http://WLSDEVAAPPS:16000/axf-ws/AxfSolutionMediatorService';
commit;

----------Update User----------------------------------------------------------------------------------------
select a.rowid, a.* from axf.AXF_PROPERTIES a;
-- AACCh+AA1AAAAAMAAF	AXF_SOAP_USER	GAAPrdWLSAdmin -- correct this user for the good one 
--GAADevaWLSAdmin
update axf.AXF_PROPERTIES set PROPVALUE='GAADevaWLSAdmin' where PROPNAME ='AXF_SOAP_USER';
commit;
---------------------------------------------------------------------------------------------
--AAHbS/AA1AAAABMAAA	SecureMode	OFF
--AAHbS/AA1AAAABMAAB	AXFWalletPath	file:<walletpath>
--AAHbS/AA1AAAABMAAC	AXFWalletKey	AXFWalletKey
--AAHbS/AA1AAAABMAAD	AXF_VERSION	2
--AAHbS/AA1AAAABMAAE	AXF_SOAP_POLICY	USER_NAME_TOKEN
--AAHbS/AA1AAAABMAAF	AXF_SOAP_USER	GAASTGWLSAdmin
--AAHbS/AA1AAAABMAAG	AXF_SOAP_SECURITY	TRUE
------------------------------------------------------------------------------
--Check Password -------
select fnd_vault.get ('AXF','GAADevWLSAdmin') from dual; --------------DEVE------------------------ 
--==----
select fnd_vault.get ('AXF','GAAPrdWLSAdmin') from dual;    -------------Production------------
------------------------------------------------------------------------------
select fnd_vault.get ('AXF','GAASTGWLSAdmin') from dual;     ---------------UAT------------------------
--------------------------------------------------------------------------------------------------------------
select fnd_vault.get ('AXF','GAADevWLSAdmin') from dual;      ----------------DEV------------------------
----------------------------------------------------------------------------------------------------------
select fnd_vault.get ('AXF','GAADevaWLSAdmin') from dual; ----------------DEVA------------------------ 
-- Set password:
execute apps.fnd_vault.put('AXF','GAADevaWLSAdmin', 'QW2dgH5xQCuTrCG');



--Webcenter AXFCustom error ora-29273
select * from dba_network_acls;-- (attached A.xls)
select * from dba_network_acl_privileges; --(attached B.xls)
select * from dba_tab_privs where table_name = 'UTL_HTTP';-- (attached C.xls)
--========================= Un Setting Dans la BD Manquant: Andre Lacombe

/*
smtp2.agropur.com	25	25	/sys/acls/agr_gels_pkg_smtp_acl.xml	5853C5AC486C0132E0530AD204840490
*			/sys/acls/network_services_webcenter.xml	585E99990AEA0340E0530AD20484D0F1
dcpspebsapps	8009	8009	/sys/acls/OracleEBS.xml	5C9F4495A425B044E0438BB91869B044
ebsprod	443	443	/sys/acls/OracleEBS.xml	5C9F4495A425B044E0438BB91869B044

/sys/acls/OracleEBS.xml	5C9F4495A425B044E0438BB91869B044	APPS	connect	true	false		
/sys/acls/OracleEBS.xml	5C9F4495A425B044E0438BB91869B044	APPS	resolve	true	false		
/sys/acls/agr_gels_pkg_smtp_acl.xml	5853C5AC486C0132E0530AD204840490	AGR_GELS_PKG	connect	true	false		
/sys/acls/agr_gels_pkg_smtp_acl.xml	5853C5AC486C0132E0530AD204840490	AGR_GELS_PKG	resolve	true	false		
/sys/acls/network_services_webcenter.xml	585E99990AEA0340E0530AD20484D0F1	APPS	connect	true	false		
/sys/acls/network_services_webcenter.xml	585E99990AEA0340E0530AD20484D0F1	APPS	resolve	true	false		
/sys/acls/network_services_webcenter.xml	585E99990AEA0340E0530AD20484D0F1	AXF	resolve	true	false		
*/
BEGIN
  -- Only uncomment the following line if ACL "network_services.xml" has already been created
  --DBMS_NETWORK_ACL_ADMIN.DROP_ACL('network_services.xml');

  DBMS_NETWORK_ACL_ADMIN.CREATE_ACL(
    acl => 'network_services_webcenter.xml',
description => 'NETWORK ACL for WEBCENTER',
    principal => 'APPS',
    is_grant => true,
    privilege => 'connect');

  DBMS_NETWORK_ACL_ADMIN.ADD_PRIVILEGE(
acl => 'network_services_webcenter.xml',
principal => 'APPS',
is_grant => true,
    privilege => 'resolve');

  DBMS_NETWORK_ACL_ADMIN.ADD_PRIVILEGE(
    acl => 'network_services_webcenter.xml',
principal => 'AXF',
    is_grant => true,
    privilege => 'resolve');


  DBMS_NETWORK_ACL_ADMIN.ASSIGN_ACL(
    acl => 'network_services_webcenter.xml',
host => '*');
  COMMIT;

END;
 /
 
 --PL/SQL procedure successfully completed.