select (select tablespace_name
from dba_tablespaces
where tablespace_name = b.tablespace_name
) name
,round(kbytes_alloc/1024, 2) mbytes
,round((kbytes_alloc-nvl(kbytes_free,0))/1024, 2) used
,round(nvl(kbytes_free,0)/1024, 2) free
,round(((kbytes_alloc-nvl(kbytes_free,0))/ kbytes_alloc)*100, 2) pct_used
,round(nvl(largest,0)/1024, 2) largest
,round(nvl(kbytes_max,kbytes_alloc)/1024, 2) max_size
,round(decode(kbytes_max,0,0,((kbytes_alloc-nvl(kbytes_free,0))/kbytes_max)*100),2) pct_max_used
,(select extent_management
from dba_tablespaces
where tablespace_name = b.tablespace_name) extent_management
,(select segment_space_management
from dba_tablespaces
where tablespace_name = b.tablespace_name) segment_space_management
from (select sum(bytes)/1024 Kbytes_free, max(bytes)/1024 largest, tablespace_name
from sys.dba_free_space
group by tablespace_name ) a
,(select sum(bytes)/1024 Kbytes_alloc, sum(maxbytes)/1024 Kbytes_max, tablespace_name
from sys.dba_data_files
group by tablespace_name
union all
select sum(bytes)/1024 Kbytes_alloc, sum(maxbytes)/1024 Kbytes_max, tablespace_name
from sys.dba_temp_files group by tablespace_name ) b
where a.tablespace_name (+) = b.tablespace_name
order by 2;
--APPS_TS_LARGE_MEDIA	259583.92	234703.92	24880	90.42	17464	294911.86	79.58	LOCAL	AUTO
--##### List Datafiles ###############
select file_name, bytes, autoextensible, maxbytes from dba_data_files ;

--ex: APPS_TS_TX_IDX  is 100%

SELECT * FROM   database_properties WHERE  property_name like '%TABLESPACE';

--##check how much is left
select tablespace_name, file_name, autoextensible, maxbytes - bytes from dba_data_files where tablespace_name='APPS_TS_LARGE_MEDIA';

select file_name, bytes, autoextensible, maxbytes from dba_data_files where tablespace_name='APPS_TS_LARGE_MEDIA';
select tablespace_name, file_name, autoextensible, maxbytes - bytes from dba_data_files where tablespace_name='APPS_TS_LARGE_MEDIA';
-- alter tablespace APPS_TS_LARGE_MEDIA add datafile '+DATA' size 10G;
select name from v$asm_diskgroup;
--DATA

--Check free space on ASM 
SELECT name, free_mb, total_mb, free_mb/total_mb*100 as percentage FROM v$asm_diskgroup;
DATA	1980328	6553472	30.2179974218246450125979023027793511592

+DATA/sebsprd/datafile/data_d-ebsprd_ts-apps_ts_large_media_fno_10100	20401094656	YES	34359721984
+DATA/sebsprd/datafile/data_d-ebsprd_ts-apps_ts_large_media_fno-98	34359721984	YES	34359721984
+DATA/sebsprd/datafile/data_d-ebsprd_ts-apps_ts_large_media_fno-127	34359721984	YES	34359721984
+DATA/sebsprd/datafile/data_d-ebsprd_ts-apps_ts_large_media_fno-161	22011707392	YES	34359721984
+DATA/sebsprd/datafile/data_d-ebsprd_ts-apps_ts_large_media_fno-116	34359721984	YES	34359721984
+DATA/sebsprd/datafile/data_d-ebsprd_ts-apps_ts_large_media_fno-151	32212254720	YES	34359721984
+DATA/SEBSPRD/DATAFILE/apps_ts_large_media.469.921687821	25769803776	YES	34359721984
+DATA/sebsprd/datafile/data_d-ebsprd_ts-apps_ts_large_media_fno-74	34359721984	YES	34359721984
+DATA/sebsprd/datafile/data_d-ebsprd_ts-apps_ts_large_media_fno-144	34359721984	YES	34359721984
