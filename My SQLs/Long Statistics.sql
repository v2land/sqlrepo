-- what was running BETWEEN begin and end period:
select
fcr.request_id,
usr.user_name FND_USER,
fcpt.user_concurrent_program_name,
--fcp.concurrent_program_name pname,
To_Char(fcr.actual_start_date, 'DD-MON-YYYY HH24:MI:SS') actual_start_date,
TO_CHAR(fcr.Actual_Completion_Date, 'DD-MON-YYYY HH24:MI:SS')Actual_Completion_Date,
round((fcr.actual_start_date - fcr.requested_start_date)*60*24,2) wait_time,
round((fcr.actual_completion_date - fcr.actual_start_date)*60*24,2) run_time,
que.concurrent_queue_name conc_que,
DECODE(fcr.phase_code, 'C', 'Completed', 'I', 'Inactive', 'P', 'Pending', 'R', 'Running', Fcr.Phase_Code) Phase_Code,
DECODE (Fcr.Status_Code, 'A', 'Waiting', 'B', 'Resuming', 'C', 'Normal', 'D', 'Cancelled', 'E', 'Error', 'F', 'Scheduled', 'G', 'Warning', 'H', 'On Hold', 'I', 'Normal', 'M', 'No Manager', 'Q', 'Standby', 'R', 'Normal', 'S', 'Suspended', 'T', 'Terminating', 'U', 'Disabled', 'W', 'Paused', 'X', 'Terminated', 'Z', 'Waiting', Fcr.Status_Code) Status_Code,
fcr.completion_text,
fcr.argument_text args,
fcr.logfile_name,
fcr.outfile_name
from
fnd_concurrent_queues que,
fnd_user usr,
fnd_concurrent_programs fcp,
fnd_concurrent_requests fcr,
fnd_concurrent_processes fcps,
fnd_concurrent_programs_tl fcpt
where
--ALL RUNNING in this period
fcr.actual_start_date < to_date(:date_end, 'DD-MON-YYYY HH24:MI:SS')
and actual_completion_date > to_date(:date_begin, 'DD-MON-YYYY HH24:MI:SS')
--(actual_start_date between to_date('&start_date', 'DD-MON-YYYY HH24:MI:SS')and to_date('&end_date', 'DD-MON-YYYY HH24:MI:SS')
--    or actual_completion_date between to_date('&start_dte', 'DD-MON-YYYY HH24:MI:SS')
--and fcp.concurrent_program_name = 'WSHINTERFACES'
and que.application_id= fcps.queue_application_id
and que.concurrent_queue_id = fcps.concurrent_queue_id
and fcr.controlling_manager= fcps.concurrent_process_id
and usr.user_id = fcr.requested_by
and fcp.concurrent_program_id = fcr.concurrent_program_id
and fcp.application_id = fcr.program_application_id
and fcp.concurrent_program_name not in ('ACTIVATE','ABORT','DEACTIVATE','VERIFY')
AND fcr.concurrent_program_id = fcpt.concurrent_program_id
AND fcpt.language = USERENV ('LANG')
order by fcr.actual_start_date;



-- What Request_IDs was running at date: YYYY-MM-DD HH24:MI:SS
SELECT distinct a.request_id,
       a.actual_start_date,
       a.actual_completion_date,
       b.concurrent_program_name,
       b.user_concurrent_program_name pgm_name,
       d.user_name,
       frv.responsibility_name scheduled_by_responsibility,
       to_char(to_number(A.ACTUAL_completion_DATE - a.actual_start_date)*60*24, '99990.99') Minutes,
       to_char(to_number(A.ACTUAL_completion_DATE - a.actual_start_date)*24, '99990.99')    Hours,
       to_char(a.actual_start_date, 'YYYY-MM-DD HH24:MI:SS') start_date ,
       a.completion_text completion,
       pc.meaning,
       sc.meaning,
       a.argument1       arg1,
       a.argument2       arg2,
       a.argument3       arg3,
       a.argument4       arg4,
       a.argument5       arg5,
       a.argument6       arg6,
       a.argument7       arg7,
       a.argument8       arg8,
       a.argument9       arg9,
       a.argument10      arg10,
       a.argument11      arg11,
       a.argument12      arg12,
       a.concurrent_program_id,
       a.logfile_name,
       a.outfile_name,
       a.printer,
       a.oracle_process_id,
       a.os_process_id,
       a.*
FROM   fnd_concurrent_requests a,
       (SELECT B.ROWID ROW_ID, B.RESOURCE_CONSUMER_GROUP, B.ROLLBACK_SEGMENT, B.OPTIMIZER_MODE, B.SECURITY_GROUP_ID, B.APPLICATION_ID, B.CONCURRENT_PROGRAM_ID, B.CONCURRENT_PROGRAM_NAME, B.LAST_UPDATE_DATE, B.LAST_UPDATED_BY, B.CREATION_DATE, B.CREATED_BY, B.LAST_UPDATE_LOGIN, B.EXECUTABLE_APPLICATION_ID, B.EXECUTABLE_ID, B.EXECUTION_METHOD_CODE, B.ARGUMENT_METHOD_CODE, B.QUEUE_CONTROL_FLAG, B.QUEUE_METHOD_CODE, B.REQUEST_SET_FLAG, B.ENABLED_FLAG, B.PRINT_FLAG, B.RUN_ALONE_FLAG, B.SRS_FLAG, B.CLASS_APPLICATION_ID, B.CONCURRENT_CLASS_ID, B.EXECUTION_OPTIONS, B.SAVE_OUTPUT_FLAG, B.REQUIRED_STYLE, B.OUTPUT_PRINT_STYLE, B.PRINTER_NAME, B.MINIMUM_WIDTH, B.MINIMUM_LENGTH, B.REQUEST_PRIORITY, B.ATTRIBUTE_CATEGORY, B.ATTRIBUTE1, B.ATTRIBUTE2, B.ATTRIBUTE3, B.ATTRIBUTE4, B.ATTRIBUTE5, B.ATTRIBUTE6, B.ATTRIBUTE7, B.ATTRIBUTE8, B.ATTRIBUTE9, B.ATTRIBUTE10, B.ATTRIBUTE11, B.ATTRIBUTE12, B.ATTRIBUTE13, B.ATTRIBUTE14, B.ATTRIBUTE15, B.IPROG_ID, B.OUTPUT_FILE_TYPE, B.ENABLE_TRACE, B.RESTART, B.NLS_COMPLIANT, B.ICON_NAME, B.CD_PARAMETER, B.INCREMENT_PROC, B.MLS_EXECUTABLE_APP_ID, B.MLS_EXECUTABLE_ID, B.ENABLE_TIME_STATISTICS, B.REFRESH_PORTLET, B.PROGRAM_TYPE, B.ACTIVITY_SUMMARIZER, B.REQUEST_SCH_INTERVAL, T.USER_CONCURRENT_PROGRAM_NAME, T.DESCRIPTION 
        FROM FND_CONCURRENT_PROGRAMS_VL T, 
             FND_CONCURRENT_PROGRAMS B 
        WHERE B.APPLICATION_ID = T.APPLICATION_ID 
              and B.CONCURRENT_PROGRAM_ID = T.CONCURRENT_PROGRAM_ID) b,
       fnd_user d,
       fnd_lookups pc,
       fnd_lookups sc,
       fnd_responsibility_vl frv
 where  a.concurrent_program_id  = b.concurrent_program_id
  AND  a.program_application_id = b.application_id
  AND  d.user_id                = a.requested_by
  and pc.lookup_type = 'CP_PHASE_CODE'
  and a.phase_code = pc.lookup_code
  and sc.lookup_type = 'CP_STATUS_CODE'
  and a.status_code = sc.lookup_code
  --and d.user_name like upper(nvl(:user_name,'%'))
  --and upper(b.user_concurrent_program_name) like upper(nvl(:Concurrent_program_name, '%'))
  --and b.concurrent_program_name like nvl(:Concurrent_program_short_name,'%')
  --and nvl(to_number(A.ACTUAL_completion_DATE - a.actual_start_date)*24,0) >= nvl(:min_execution_time,-1)
  and a.responsibility_application_id = frv.application_id(+)
  and a.responsibility_id = frv.responsibility_id(+)  
  --and a.request_id like nvl(:request_id, '%')
  --and a.status_code like nvl(:status_code, '%')
  --and to_char(:datex, 'YYYY-MM-DD HH24:MI:SS') between A.ACTUAL_completion_DATE and a.actual_start_date
  and to_date(:date_YYYY_MM_DD_HH24_MI_SS, 'YYYY-MM-DD HH24:MI:SS') between a.actual_start_date and A.ACTUAL_completion_DATE
order by a.request_id desc;

 ------------ Statistic of Selected Requests 
 SELECT DISTINCT
        fcp.concurrent_program_name conc_prog,
       fcpt.user_concurrent_program_name user_conc_prog,
       fcr.completion_text,
       count(*) no_times_ran,
       MAX(TRUNC(((fcr.actual_start_date - fcr.REQUEST_DATE)*24)*60)) max_pend_time,
       ROUND(AVG(TRUNC(((fcr.actual_start_date - fcr.REQUEST_DATE)/(1/24))*60) )) avg_pend_time,
       MIN(TRUNC(((fcr.actual_start_date - fcr.REQUEST_DATE)/(1/24))*60)) min_pend_time,
       MAX(TRUNC(((fcr.actual_completion_date - fcr.actual_start_date)/(1/24))*60)) max_exec_time,
       ROUND(AVG(TRUNC(((fcr.actual_completion_date - fcr.actual_start_date)/(1/24))*60) )) avg_exec_time,
       MIN(TRUNC(((fcr.actual_completion_date - fcr.actual_start_date)/(1/24))*60)) min_exec_time
   FROM
    APPLSYS.fnd_concurrent_programs fcp,
    APPLSYS.fnd_concurrent_programs_tl fcpt,
    APPLSYS.fnd_concurrent_requests fcr
   WHERE
   -- TRUNC(((fcr.actual_completion_date-fcr.actual_start_date)/(1/24))*60) > 30
   --TRUNC(((fcr.actual_start_date - fcr.REQUEST_DATE)/(1/24))*60) > 0 and
   fcr.concurrent_program_id = fcp.concurrent_program_id
   AND fcr.program_application_id = fcp.application_id
   AND fcr.concurrent_program_id = fcpt.concurrent_program_id
   AND fcr.program_application_id = fcpt.application_id
   --AND fcr.request_date >'01-JAN-2016'
   AND fcr.phase_code = 'C'
   AND fcr.status_code = 'C'
   AND fcpt.language = USERENV('Lang')
   --and fcpt.user_concurrent_program_name like ('%Agropur - Analyse CUSTOM Tables and Indexes - Apps%')
   andfcp.CONCURRENT_PROGRAM_NAME in ('XXAGR_SCHEMA_ANALYZE_CUSTOM')
   GROUP BY fcp.concurrent_program_name,fcpt.user_concurrent_program_name,fcr.completion_text
   ORDER BY max_pend_time desc,avg_pend_time desc, min_pend_time desc;

--Queues of Selected Request:
select cq.user_concurrent_queue_name queue
     ,cpr.user_concurrent_program_name program
     ,count(cpr.user_concurrent_program_name) nb
from   fnd_concurrent_queues_tl   cq
     ,fnd_concurrent_programs_tl cpr
     ,fnd_concurrent_processes   cp
     ,fnd_concurrent_requests    cr
where  cr.phase_code = 'C'
and    cr.controlling_manager    = cp.concurrent_process_id
and    cr.concurrent_program_id  = cpr.concurrent_program_id
and    cp.concurrent_queue_id    = cq.concurrent_queue_id
--and    cq.language               = 'FRC'
--and    cpr.language              = 'FRC'
AND cpr.language = USERENV('Lang')
AND cr.request_date >'01-JAN-2018'
group by cpr.user_concurrent_program_name, cq.user_concurrent_queue_name;
-----------------
-- Check Responsabiulity of a Concurrent Program to run  
 ----------------------------------
SELECT frt.responsibility_name,frg.request_group_name,
frgu.request_unit_type,frgu.request_unit_id,
fcpt.user_concurrent_program_name
FROM fnd_Responsibility fr, fnd_responsibility_tl frt,
    fnd_request_groups frg, fnd_request_group_units frgu,
    fnd_concurrent_programs_tl fcpt
    WHERE frt.responsibility_id = fr.responsibility_id
    AND frg.request_group_id = fr.request_group_id
    AND frgu.request_group_id = frg.request_group_id
    AND fcpt.concurrent_program_id = frgu.request_unit_id
    AND frt.LANGUAGE = USERENV('LANG')
    AND fcpt.LANGUAGE = USERENV('LANG')
    AND fcpt.user_concurrent_program_name like ('%archiving%')
    ORDER BY 1,2,3,4;


--=================================================================================================
--List Specific Completed Concurrent jobs with Start date and end date in last 24Hours and Details
--====================================================================================================
select
      f.request_id ,f.parent_request_id,
      p.concurrent_program_name,
      pt.user_concurrent_program_name,
      f.description,
      --DECODE (f.description,NULL, pt.user_concurrent_program_name,f.description || ' ( ' || pt.user_concurrent_program_name|| ' ) ') program,
      f.actual_start_date start_on,
      f.actual_completion_date end_on,
      to_char(f.actual_start_date,'hh24:mi:ss') s_time,
      to_char(f.actual_completion_date,'hh24:mi:ss') e_time,
      floor(((f.actual_completion_date-f.actual_start_date) *24*60*60)/3600) || ' h ' ||
        floor((((f.actual_completion_date-f.actual_start_date) *24*60*60) - floor(((f.actual_completion_date-f.actual_start_date)  *24*60*60)/3600)*3600)/60)  || ' m ' ||
        round((((f.actual_completion_date-f.actual_start_date) *24*60*60) - floor(((f.actual_completion_date-f.actual_start_date) *24*60*60)/3600)*3600 -
             (floor((((f.actual_completion_date-f.actual_start_date) *24*60*60) -  floor(((f.actual_completion_date-f.actual_start_date) *24*60*60)/3600)*3600)/60)*60) ))  || ' s ' time_difference,
      round((f.actual_completion_date - f.actual_start_date)*1440) minutes ,
      p.concurrent_program_name concurrent_program_name,
      decode(f.phase_code,'R','Running','C','Complete','P','Scheduled',f.phase_code) Phase,
      f.status_code,
      substr(f.completion_text,1,20) completion,
      fu.USER_NAME,
      substr(f.Argument_text,1,20) Arguments,
       substr(f.argument1,1,25)       arg1,
       substr(f.argument2,1,25)       arg2,
       substr(f.argument3,1,25)       arg3,
       substr(f.argument4,1,25)       arg4,
       substr(f.argument5,1,25)       arg5,
       substr(f.argument6,1,25)       arg6,
       substr(f.argument7,1,25)       arg7,
       substr(f.argument8,1,25)       arg8,
       substr(f.argument9,1,25)       arg9,
       substr(f.argument10,1,25)      arg10,
       substr(f.argument11,1,25)      arg11,
       substr(f.argument12,1,25)      arg12,
       f.logfile_name,
       f.outfile_name
from  apps.fnd_concurrent_programs p,
      apps.fnd_concurrent_programs_tl pt,
      apps.fnd_concurrent_requests f,
      apps.fnd_user fu
where --PHASE_CODE = 'C'
      --STATUS_CODE = 'C'
      --AND
      f.concurrent_program_id = p.concurrent_program_id
      and f.requested_by = fu.user_id (+)
      and f.program_application_id = p.application_id
      and f.concurrent_program_id = pt.concurrent_program_id
      and f.program_application_id = pt.application_id
      AND pt.language = USERENV('Lang')
      and f.actual_start_date is not null
      AND f.requested_start_date > sysdate-2
      --and p.CONCURRENT_PROGRAM_ID in (select distinct CONCURRENT_PROGRAM_ID from fnd_concurrent_requests where phase_code in ('P','R')) -- only from Scheduled requests
      --AND f.request_date > '06-OCT-2019'
      --AND f.request_date < '05-APR-2019'
    -- and p.concurrent_program_name in ('XXAGR_SCHEMA_ANALYZE_CUSTOM')
    --  and pt.user_concurrent_program_name like ('%INVOICING INTERFACE %') --Workflow Background Process --Send Receipt of Payment Notifications
      and pt.user_concurrent_program_name like ('%Request Set AP - Positive pay%')
      --and TRUNC(((f.actual_completion_date-f.actual_start_date)/(1/24))*60) > 20
order by
      --f.actual_start_date desc
      1 desc;
      
--====================================================================---= 
--=== for Request Sets                                                    
-- ======== How to find Scheduled REQUEST SET programs and their parameters in Oracle EBS?
--==============================================================================================       
SELECT fcr.request_id,
       DECODE(fcpt.user_concurrent_program_name,
              'Report Set',
              'Report Set:' || fcr.description,
              fcpt.user_concurrent_program_name) CONC_PROG_NAME,
       argument_text PARAMETERS,
       NVL2(fcr.resubmit_interval,
            'PERIODICALLY',
            NVL2(fcr.release_class_id, 'ON SPECIFIC DAYS', 'ONCE')) PROG_SCHEDULE_TYPE,
       DECODE(NVL2(fcr.resubmit_interval,
                   'PERIODICALLY',
                   NVL2(fcr.release_class_id, 'ON SPECIFIC DAYS', 'ONCE')),
              'PERIODICALLY',
              'EVERY ' || fcr.resubmit_interval || ' ' ||
              fcr.resubmit_interval_unit_code || ' FROM ' ||
              fcr.resubmit_interval_type_code || ' OF PREV RUN',
              'ONCE',
              'AT :' ||
              TO_CHAR(fcr.requested_start_date, 'DD-MON-RR HH24:MI'),
              'EVERY: ' || fcrc.class_info) PROG_SCHEDULE,
       fu.user_name USER_NAME,
       to_char(fcr.request_date,'DD-MON-YY HH24:MI:SS') submitted,
       to_char(fcr.requested_start_date,'DD-MON-YY HH24:MI:SS') requested_start_date,
       To_Char(fcr.ACTUAL_START_DATE,'DD-MON-YY HH24:MI:SS') started,
       To_Char(fcr.ACTUAL_COMPLETION_DATE,'DD-MON-YY HH24:MI:SS') completed,
       floor(((fcr.actual_completion_date-fcr.actual_start_date) *24*60*60)/3600) || ' h ' ||
        floor((((fcr.actual_completion_date-fcr.actual_start_date) *24*60*60) - floor(((fcr.actual_completion_date-fcr.actual_start_date)  *24*60*60)/3600)*3600)/60)  || ' m ' ||
        round((((fcr.actual_completion_date-fcr.actual_start_date) *24*60*60) - floor(((fcr.actual_completion_date-fcr.actual_start_date) *24*60*60)/3600)*3600 -
             (floor((((fcr.actual_completion_date-fcr.actual_start_date) *24*60*60) -  floor(((fcr.actual_completion_date-fcr.actual_start_date) *24*60*60)/3600)*3600)/60)*60) ))  || ' s ' time_difference      
  FROM apps.fnd_concurrent_programs_tl fcpt,
       apps.fnd_concurrent_requests    fcr,
       apps.fnd_user                   fu,
       apps.fnd_conc_release_classes   fcrc
 WHERE fcpt.application_id = fcr.program_application_id
   AND fcpt.concurrent_program_id = fcr.concurrent_program_id
   AND fcr.requested_by = fu.user_id
   --AND fcr.phase_code = 'P' -- Pending (Scheduled)
   AND fcr.requested_start_date > SYSDATE-1
   AND fcpt.LANGUAGE = 'US'
   AND fcrc.release_class_id(+) = fcr.release_class_id
   AND fcrc.application_id(+) = fcr.release_class_app_id
   --and fcr.request_id = '24973406' request set id
   and  DECODE(fcpt.user_concurrent_program_name,
              'Report Set',
              'Report Set:' || fcr.description,
              fcpt.user_concurrent_program_name) like '%BuyBack%'
   order by  requested_start_date desc;
-- ========
      
--=====================================================
select * from STANDARD;
--Locate Tables used by specific Package
with TAB_REF as
(SELECT * FROM dba_dependencies WHERE name like 'XXONT0890_VELO_TRXS_OIT_LOAD')
select dd.*, tr.ref_table from   DBA_dependencies dd, TAB_REF tr where 
type like 'PACKAGE%'
and tr.name=dd.referenced_name;
--APPS	XXONT0890_VELO_TRXS_OIT_LOAD	PACKAGE BODY	APPS	XXONT0890_VELO_INV_LINES_STG	SYNONYM		HARD	XXONT0890_VELO_INV_LINES_STG



--======================================
--Locate Package Name used by a Concurrent Program
SELECT a.user_concurrent_program_name,a.concurrent_program_name, b.executable_name,
      b.execution_file_name package_name, b.user_executable_name,EXECUTION_FILE_PATH
 FROM fnd_concurrent_programs_vl a, fnd_executables_vl b
WHERE a.executable_id = b.executable_id
  AND concurrent_program_name like ('%XXONT0890%')
  --AND user_concurrent_program_name like ('%XXOZF0060%')
  ;
  
-- Locate Packages that are using specific TABLE:  
select * from   DBA_dependencies
where 
referenced_name = (SELECT name FROM dba_dependencies WHERE referenced_TYPE = 'TABLE' and referenced_name = 'XXONT0890_VELO_INV_LINES_STG')
and type like 'PACKAGE%'
;

with TAB_REF as
(SELECT name, referenced_name ref_table FROM dba_dependencies WHERE referenced_TYPE = 'TABLE' and referenced_name like 'XXONT0890_VELO_INV_LINES_STG')
select dd.*, tr.ref_table from   DBA_dependencies dd, TAB_REF tr where 
type like 'PACKAGE%'
and tr.name=dd.referenced_name;
--APPS	XXONT0890_VELO_TRXS_OIT_LOAD	PACKAGE BODY	APPS	XXONT0890_VELO_INV_LINES_STG	SYNONYM		HARD	XXONT0890_VELO_INV_LINES_STG

SELECT * FROM fnd_executables where execution_file_name like 'XXONT0850%'; 
SELECT *  FROM fnd_concurrent_programs where executable_id ='15948' ;

select * from dba_tables where table_name like '%XXONT0850%';


--LOCATE Concurrent Programm that is using specific Package
SELECT USER_concurrent_program_name  FROM fnd_concurrent_programs_tl 
where 
concurrent_program_id in(SELECT concurrent_program_id  
                        FROM fnd_concurrent_programs 
                        where executable_id in(SELECT executable_id --Check Executables
                                                FROM fnd_executables 
                                                where UPPER(substr(execution_file_name,1,instr(execution_file_name,'.')-1)) in 
                                                                                (with TAB_REF as   -- Check Packages                            
                                                                                (SELECT name, referenced_name ref_table
                                                                                FROM dba_dependencies
                                                                                WHERE referenced_TYPE = 'TABLE' and referenced_name like 'XXONT0890_VELO_INV_LINES_STG%')
                                                                            select dd.name from   DBA_dependencies dd, TAB_REF tr where 
                                                                            type like 'PACKAGE%'
                                                                            and tr.name=dd.referenced_name)
                                              )
                        )
AND language = userenv('LANG')
; 
 


select * from dba_identifiers
where  object_type like 'PACKAGE%'
and    usage in ('DECLARATION', 'DEFINITION')
and    type in ('FUNCTION', 'PROCEDURE');

select * from dba_objects where object_name like 'JCP4%';
select * from user_identifiers where OBJECT_TYPE like 'PACKAGE%' and object_name = 'JCP4XDODataEngine';
select * from fnd_executables_vl where execution_file_name like '%JCP4XDODataEngine%';
WITH TESTING AS
(
select 
DISTINCT 
name, 
type,
decode(usage,'DECLARATION', 'body only', 'DEFINITION', 'spec and body', usage) defined_on,
line body_line,
object_name
from user_identifiers ui
where --type = 'PROCEDURE'
usage_context_id = (select usage_id
from dba_identifiers
where object_name = ui.object_name
and object_type = ui.object_type
and usage_context_id = 0)
and object_name = 'JCP4XDODataEngine'
--and object_type = 'PACKAGE BODY'
order by line
)
SELECT DISTINCT X.*, Y.REFERENCED_NAME, Y.REFERENCED_TYPE FROM TESTING X, dba_dependencies Y
WHERE X.OBJECT_NAME = Y.NAME
AND X.OBJECT_NAME = 'JCP4XDODataEngine'
AND Y.REFERENCED_TYPE IN ('TABLE')
--AND Y.OWNER = 'OWNER_NAME'
--AND X.NAME = 'MY_PROCEDURE_NAME'
ORDER BY X.NAME, Y.REFERENCED_TYPE;
  

--*** LIST  REQUEST executed Queues Manager and Time execution  starting from specific Request ID****
select fcr.request_id req_id,
substr(fcq.concurrent_queue_name, 1, 22) queue,
to_char(fcr.actual_start_date,'hh24:mi') s_time,
substr(fcr.user_concurrent_program_name, 1, 60) name,
substr(fcr.requestor, 1, 9 ) u_name,
round((ACTUAL_completion_DATE -actual_start_date) *24, 2) elap,
decode(fcr.phase_code,'R','Running','P','Inactive','C','Completed', fcr.phase_code) Phase,
substr(decode( fcr.status_code, 'A', 'WAITING', 'B', 'RESUMING',
'C', 'NORMAL', 'D', 'CANCELLED', 'E', 'ERROR', 'F', 'SCHEDULED',
'G', 'WARNING', 'H', 'ON HOLD', 'I', 'NORMAL', 'M', 'NO MANAGER',
'Q', 'STANDBY', 'R', 'NORMAL', 'S', 'SUSPENDED', 'T', 'TERMINATED',
'U', 'DISABLED', 'W', 'PAUSED', 'X', 'TERMINATED', 'Z', 'WAITING',
'UNKNOWN'), 1, 10)
from
apps.fnd_concurrent_queues fcq,
apps.fnd_concurrent_processes fcp,
apps.fnd_conc_req_summary_v fcr
where fcp.concurrent_queue_id = fcq.concurrent_queue_id
and fcp.queue_application_id = fcq.application_id
and fcr.controlling_manager = fcp.concurrent_process_id
and fcr.request_id = '20769160'--'&RequestID'
order by request_id desc;

--==================================================================
--Combien de temps les Programmes  roule
--investigation sur le Temps d'execution, conflits et execution en paralele 
--===========================================================
select fcr.concurrent_program_id,
fcr.request_id req_id,
substr(fcq.concurrent_queue_name, 1, 22) queue,
fcr.actual_start_date s_date,
to_char(fcr.actual_start_date,'hh24:mi:ss') s_time,
to_char(fcr.actual_completion_date,'hh24:mi:ss') e_time,
substr(fcr.user_concurrent_program_name, 1, 60) name,
PROGRAM_SHORT_NAME,
substr(fcr.requestor, 1, 9 ) u_name,
round((actual_completion_date - actual_start_date) *24*60, 2) elap_in_MIN,
round((actual_completion_date - actual_start_date) *24*60*60, 0) elap_in_SEC,
decode(fcr.phase_code,'R','Running','P','Inactive','C','Completed', fcr.phase_code) Phase,
substr(decode( fcr.status_code, 'A', 'WAITING', 'B', 'RESUMING',
'C', 'NORMAL', 'D', 'CANCELLED', 'E', 'ERROR', 'F', 'SCHEDULED',
'G', 'WARNING', 'H', 'ON HOLD', 'I', 'NORMAL', 'M', 'NO MANAGER',
'Q', 'STANDBY', 'R', 'NORMAL', 'S', 'SUSPENDED', 'T', 'TERMINATED',
'U', 'DISABLED', 'W', 'PAUSED', 'X', 'TERMINATED', 'Z', 'WAITING',
'UNKNOWN'), 1, 10) as Status
from
apps.fnd_concurrent_queues fcq,
apps.fnd_concurrent_processes fcp,
apps.fnd_conc_req_summary_v fcr
where fcp.concurrent_queue_id = fcq.concurrent_queue_id
and fcp.queue_application_id = fcq.application_id
and fcr.controlling_manager = fcp.concurrent_process_id
--and fcr.request_id = '&RequstID'
--and fcr.USER_CONCURRENT_PROGRAM_NAME in  ('Invoice Approval Workflow')
and to_char(fcr.actual_start_date,'hh24:mi:ss') between ('00:00:00') and ('04:00:00')
and round((actual_completion_date - actual_start_date) *24*60, 2) >('5.00')
order by s_date desc,s_time ;
--=========================================================================================

---------------------------------SQL to get list of Scheduled Concurrent Programs

SELECT FCR.request_id REQUEST_ID
,FCP.concurrent_program_name PROGRAM_SHORT_NAME
,FCP.user_concurrent_program_name PROGRAM_NAME
,FNU.user_name SUBMITTED_BY 
,TO_CHAR(FCR.requested_start_date
,'DD-MON-YYYY HH24:MM:SS'
) REQUEST_START_DATE
,'Every '|| DECODE(LENGTH(FCL.class_info)
,39,FCL.class_info
,SUBSTR(FCL.class_info,1,INSTR(FCL.class_info,':',1)-1)||' '
|| DECODE(SUBSTR(FCL.class_info,INSTR(FCL.class_info,':',1)+1,1)
,'N','Minute(s) '
,'D','Day(s) '
,'H','Hour(s) '
,'M','Month(s) '
)
|| 'after '
|| DECODE(SUBSTR(FCL.class_info,INSTR(FCL.class_info,':',1,2)+1,1)
,'S','Start '
,'C','Completion '
)
|| 'of prior request'
) SCHEDULED_INTERVAL 
,NVL(TO_CHAR(FCL.end_date_active
,'DD-MON-YYYY'),'forever'
) ENDING_ON
FROM APPS.fnd_concurrent_requests FCR
,APPS.fnd_concurrent_programs_vl FCP
,APPS.fnd_user FNU
,APPS.fnd_conc_release_classes FCL
WHERE FCR.phase_code = 'P' 
AND FCR.status_code IN ('I','Q') 
AND FCR.program_application_id = FCP.application_id 
AND FCR.concurrent_program_id = FCP.concurrent_program_id 
AND FCR.requested_by = FNU.user_id 
AND FCR.release_class_app_id = FCL.application_id 
AND FCR.release_class_id = FCL.release_class_id 
and fcr.concurrent_program_id in 
(
'44245',
'44046',
'44686',
'44965',
'44526',
'44985'
)
ORDER BY FCP.concurrent_program_name
, FCR.requested_start_date;


SELECT 
r.REQUEST_ID,
t.user_concurrent_program_name,
to_char(r.ACTUAL_START_DATE,'dd-mm-yy hh24:mi:ss') "Started at",
to_char(r.ACTUAL_COMPLETION_DATE,'dd-mm-yy hh24:mi:ss') "Completed at",
r.PHASE_CODE,
decode(r.PHASE_CODE,'C','Completed','I','Inactive','P','Pending','R','Running','NA') Phase,
r.STATUS_CODE,
r.hold_flag,
decode(r.STATUS_CODE, 'A','Waiting', 'B','Resuming', 'C','Normal', 'D','Cancelled', 'E','Error', 'F','Scheduled', 'G','Warning', 'H','On Hold', 'I','Normal', 'M',
'No Manager', 'Q','Standby', 'R','Normal', 'S','Suspended', 'T','Terminating', 'U','Disabled', 'W','Paused', 'X','Terminated', 'Z','Waiting') "Status",
r.argument_text "Parameters",substr(u.description,1,25) "Who submitted",
round(((nvl(v.actual_completion_date,sysdate)-v.actual_start_date)*24*60)) Etime
FROM
apps.fnd_concurrent_requests r ,
apps.fnd_concurrent_programs p ,
apps.fnd_concurrent_programs_tl t,
apps.fnd_user u, apps.fnd_conc_req_summary_v v
WHERE r.CONCURRENT_PROGRAM_ID = p.CONCURRENT_PROGRAM_ID
--AND r.actual_start_date >= (sysdate-7)
--AND r.requested_by=22378
AND r.PROGRAM_APPLICATION_ID = p.APPLICATION_ID
AND t.concurrent_program_id=r.concurrent_program_id
AND r.REQUESTED_BY=u.user_id
AND v.request_id=r.request_id
--AND (r.PHASE_CODE not in ('C') and r.STATUS_CODE not in ('R'))
--AND r.request_id ='20767251'-- in ('13829387','13850423')
--and t.user_concurrent_program_name like '%%'
AND t.language = userenv('LANG')
order by to_char(r.ACTUAL_COMPLETION_DATE,'dd-mm-yy hh24:mi:ss');



-- ------------------------  DETAILS Request_ID-------------------------------
SELECT /*+   */
    concurrent_program_id,
    program_application_id,
    printer,
    program_short_name,
    argument_text,
    print_style,
    user_print_style,
    save_output_flag,
    row_id,
    actual_completion_date,
    completion_text,
    parent_request_id,
    request_type,
    fcp_printer,
    fcp_print_style,
    fcp_required_style,
    last_update_date,
    last_updated_by,
    requested_by,
    has_sub_request,
    is_sub_request,
    update_protected,
    queue_method_code,
    responsibility_application_id,
    responsibility_id,
    controlling_manager,
    last_update_login,
    priority_request_id,
    enabled,
    requested_start_date,
    phase_code,
    hold_flag,
    status_code,
    request_id,
    program,
    requestor,
    priority
FROM
    fnd_conc_req_summary_v
WHERE
    (
        DECODE(implicit_code,'N',status_code,'E','E','W','G') = status_code
        OR    DECODE(implicit_code,'W','E') = status_code
        OR    DECODE(implicit_code,'Y',status_code) = status_code
    )
    AND   (
        nvl(request_type,'X') != 'S'
        AND   ( request_id = '21675855' )
    )
ORDER BY
    request_id DESC;
    
    
SELECT
    r.rowid row_id,
    r.request_id,
    r.phase_code,
    r.status_code,
    r.priority_request_id,
    r.priority,
    r.request_date,
    r.requested_by,
    r.requested_start_date,
    r.hold_flag,
    r.has_sub_request,
    r.is_sub_request,
    r.update_protected,
    r.queue_method_code,
    r.responsibility_application_id,
    r.responsibility_id,
    r.save_output_flag,
    r.last_update_date,
    r.last_updated_by,
    r.last_update_login,
    r.printer,
    r.print_style,
    r.parent_request_id,
    r.controlling_manager,
    r.actual_start_date,
    r.actual_completion_date,
    r.completion_text,
    r.argument_text,
    r.implicit_code,
    r.request_type,
    r.program_application_id,
    r.concurrent_program_id,
    pb.concurrent_program_name program_short_name,
    pb.execution_method_code,
    pb.enabled_flag enabled,
    DECODE(r.description,NULL,pt.user_concurrent_program_name,r.description
    || ' ('
    || pt.user_concurrent_program_name
    || ')') program,
    pb.printer_name fcp_printer,
    pb.output_print_style fcp_print_style,
    pb.required_style fcp_required_style,
    u.user_name requestor,
    s.user_printer_style_name user_print_style,
    r.recalc_parameters,
    r.description description,
    pt.user_concurrent_program_name user_concurrent_program_name,
    DECODE(r.request_type,'M',r.argument1,'B',r.argument1,r.program_application_id) request_type_application_id,
    DECODE(r.request_type,'M',r.argument2,'B',r.argument2,r.concurrent_program_id) request_type_id,
    DECODE(r.request_type,'M',r.description,'S',r.description,'B',r.description,pt.user_concurrent_program_name) request_type_display_name
,
    r.nls_sort
FROM
    fnd_concurrent_programs_tl pt,
    fnd_concurrent_programs pb,
    fnd_user u,
    fnd_printer_styles_tl s,
    fnd_concurrent_requests r
WHERE
    pb.application_id = r.program_application_id
    AND   pb.concurrent_program_id = r.concurrent_program_id
    AND   pb.application_id = pt.application_id
    AND   pb.concurrent_program_id = pt.concurrent_program_id
    AND   pt.language = userenv('LANG')
    AND   u.user_id = r.requested_by
    AND   s.printer_style_name (+) = r.print_style
    AND   s.language (+) = userenv('LANG');