/*
--�	 set_global_prefs sets the global preferences. It replaces the set_param procedure.
--�	 set_database_prefs sets the database preferences. The difference between global and database preferences is that the latter aren�t used for the data dictionary objects. 
                 In other words, database preferences are used only for user-defined objects.
--�	 set_schema_prefs sets the preferences for a specific schema.
--�	 set_table_prefs sets the preferences for a specific table.

Unfortunately, Oracle doesn�t externalize this information through a data dictionary view, meaning this query is based on an internal table. The system privilege select any dictionary provides access to that table.
Values Accepted by the autostats_target Parameter Value Meaning all All objects are processed. Up to and including version 11.2, fixed tables are excluded. 
However, from version 12.1 onward, fixed tables are included. auto The job determines which objects are processed. oracle Only objects belonging to the data dictionary, except fixed tables, are processed.

Note that the parameters autostats_target and concurrent can only be modified with the set_global_prefs procedure.
? Caution The procedures set_database_prefs and set_schema_prefs don�t directly store preferences in the data dictionary. Instead, they�re converted into table preferences for all objects presently available in the database or
schema at the time the procedure is called. 
In other words, only global and table preferences exist. The procedures set_database_prefs and set_schema_prefs are simple wrappers around the set_table_prefs procedure. This means
that global preferences will be used for new tables created after these two procedures have been called.
*/
exec DBMS_STATS.SET_TABLE_PREFS ('OZF', 'OZF_OBJECT_FUND_SUMMARY_TEST_H','STALE_PERCENT', '100000000');
exec DBMS_STATS.SET_SCHEMA_PREFS ('OZF','STALE_PERCENT','100000000');
exec DBMS_STATS.SET_DATABASE_PREFS ('STALE_PERCENT','100000000'); -- Excluding oracle tables
exec DBMS_STATS.SET_DATABASE_PREFS ('STALE_PERCENT','100000000',TRUE); --DEFAULT FALSE); -- TRUE to include Oracle SYSTEM tables
exec DBMS_STATS.SET_GLOBAL_PREFS ('STALE_PERCENT','100000000'); 

select * from DBA_TAB_STAT_PREFS;
select * from OPTSTAT_HIST_CONTROL$; -- run as SYS for GLOBAL check

exec dbms_stats.delete_table_prefs(ownname=>'OZF',tabname=>'OZF_OBJECT_FUND_SUMMARY_TEST_H',pname=>'STALE_PERCENT');
exec dbms_stats.delete_schema_prefs(ownname => 'OZF', pname => 'STALE_PERCENT');
exec dbms_stats.delete_database_prefs(pname => 'STALE_PERCENT');
exec dbms_stats.delete_database_prefs('STALE_PERCENT',TRUE); ----DEFAULT FALSE -- TRUE to include Oracle SYSTEM tables
exec dbms_stats.reset_global_pref_defaults;
exec DBMS_STATS.FLUSH_DATABASE_MONITORING_INFO;
--===========================================================================================================


--Notes for how to set and get preferences from dbms_stats.
select owner,table_name,last_analyzed,NUM_ROWS  From dba_Tables where table_name = 'XXONT_STD_PRICE_CALC_1';--
select owner,table_name,last_analyzed From dba_Tables where table_name = 'OZF_OBJECT_FUND_SUMMARY_TEST';
select * From dba_objects where object_name = 'OZF_OBJECT_FUND_SUMMARY_TEST';

--=========================
--TABLE Level:
--===========================
exec DBMS_STATS.SET_TABLE_PREFS('OZF','OZF_OBJECT_FUND_SUMMARY_TEST_H','INCREMENTAL','FALSE');
exec dbms_stats.set_table_prefs('OZF', 'OZF_OBJECT_FUND_SUMMARY_TEST_H','STALE_PERCENT', '20');

select owner, table_name from DBA_TAB_STAT_PREFS  
where owner not in ('SYS','SYSTEM','MDSYS','ORDSYS','OUTLN','CTXSYS','DBSNMP','GSMADMIN_INTERNAL','MGDSYS','ORDDATA','XDB')
and owner not like  ('APPS_NE%') 
--and owner  like  ('CSR%') 
and table_name not like  ('SYS%')
and table_name not in  ('CSR_RULE_WINDOWS_B')
ORDER BY owner, table_name
;


SELECT --count(*) count
  --user,
  owner, table_name--, index_name
 ,
  DBMS_STATS.get_prefs(ownname=>owner,tabname=>table_name,pname=>'INCREMENTAL') incremental,
  DBMS_STATS.get_prefs(ownname=>owner,tabname=>table_name,pname=>'GRANULARITY') granularity,
  DBMS_STATS.get_prefs(ownname=>owner,tabname=>table_name,pname=>'STALE_PERCENT') stale_percent,
  DBMS_STATS.get_prefs(ownname=>owner,tabname=>table_name,pname=>'ESTIMATE_PERCENT') as estimate_percent,
  DBMS_STATS.get_prefs(ownname=>owner,tabname=>table_name,pname=>'CASCADE') cascade,
  DBMS_STATS.get_prefs(pname=>'METHOD_OPT') method_opt
FROM dba_tables --dba_indexes --
WHERE --table_name like 'XXONT_STD_PRICE_CALC_%' and
owner not in ('SYS','SYSTEM','MDSYS','ORDSYS','OUTLN','CTXSYS','DBSNMP','GSMADMIN_INTERNAL','MGDSYS','ORDDATA','XDB')
and table_name not like ('SYS%')
--and owner like  ('CSR%') -- ne sont pas presentes dans dba_tables
ORDER BY owner, table_name
;

--Note: Use the DBA view versus the above for better performance

select * from DBA_TAB_STAT_PREFS;

--=========================
--SCHEMA Level:
--===========================
exec DBMS_STATS.SET_SCHEMA_PREFS ('OZF','STALE_PERCENT','0.1');
select * from DBA_TAB_STAT_PREFS;
exec DBMS_STATS.FLUSH_DATABASE_MONITORING_INFO;
SELECT 
  username,
  DBMS_STATS.get_prefs(ownname=>username,pname=>'INCREMENTAL') incremental,
  DBMS_STATS.get_prefs(ownname=>username,pname=>'GRANULARITY') granularity,
  DBMS_STATS.get_prefs(ownname=>username,pname=>'STALE_PERCENT') stale_percent,
  DBMS_STATS.get_prefs(ownname=>username,pname=>'ESTIMATE_PERCENT') estimate_percent,
  DBMS_STATS.get_prefs(ownname=>username,pname=>'CASCADE') cascade,
  DBMS_STATS.get_prefs(pname=>'METHOD_OPT') method_opt
FROM dba_users
WHERE username like '%OZF'
ORDER BY username;


--Schemas:

SELECT 
  username,
  DBMS_STATS.get_prefs(ownname=>USERNAME,pname=>'INCREMENTAL') incremental,
  DBMS_STATS.get_prefs(ownname=>USERNAME,pname=>'GRANULARITY') granularity,
  DBMS_STATS.get_prefs(ownname=>USERNAME,pname=>'STALE_PERCENT') stale_percent,
  DBMS_STATS.get_prefs(ownname=>USERNAME,pname=>'NO_INVALIDATE') no_invalidate,
  DBMS_STATS.get_prefs(ownname=>USERNAME,pname=>'ESTIMATE_PERCENT') estimate_percent,
  DBMS_STATS.get_prefs(ownname=>USERNAME,pname=>'CASCADE') cascade,
  DBMS_STATS.get_prefs(ownname=>USERNAME,pname=>'METHOD_OPT') method_opt
FROM dba_users
ORDER BY username;
--===========================================
--DATABASE Level:
--===============================================
--SET_GLOBAL_PREFS Procedure: set_global_prefs sets the global preferences. It replaces the set_param procedure.
--SET_DATABASE_PREFS sets the database preferences.
--  The difference between global and database preferences is that the latter aren�t used for the data dictionary objects. 
--  In other words, database preferences are used only for user-defined objects.

--Ex: exec DBMS_STATS.SET_GLOBAL_PREFS('METHOD_OPT','FOR ALL COLUMNS SIZE REPEAT');
 -- Database Level for existing Tables 
exec DBMS_STATS.SET_DATABASE_PREFS ('STALE_PERCENT','100000000');
exec DBMS_STATS.FLUSH_DATABASE_MONITORING_INFO;
-- Database level for Future Tables
exec DBMS_STATS.SET_GLOBAL_PREFS ('STALE_PERCENT','100000000'); 

select dbms_stats.get_param('STALE_PERCENT') from dual; 

SELECT 
  DBMS_STATS.get_prefs(pname=>'INCREMENTAL') incremental,
  DBMS_STATS.get_prefs(pname=>'GRANULARITY') granularity,
  DBMS_STATS.get_prefs(pname=>'STALE_PERCENT') STALE_PERCENT,
  DBMS_STATS.get_prefs(pname=>'ESTIMATE_PERCENT') estimate_percent,
  DBMS_STATS.get_prefs(pname=>'CASCADE') cascade,
  DBMS_STATS.get_prefs(pname=>'METHOD_OPT') method_opt
FROM dual;

--list 
SELECT table_name, preference_name, preference_value
FROM dba_tab_stat_prefs
WHERE owner = 'JTF'
--AND table_name IN ('EMP', 'DEPT')
ORDER BY table_name, preference_name;
/*
To get rid of preferences, the dbms_stats package provides the following procedures:
�	 reset_global_pref_defaults resets the global preferences to the default values.
�	 delete_database_prefs deletes preferences at the database level.
�	 delete_schema_prefs deletes preferences at the schema level.
�	 delete_table_prefs deletes preferences at the table level
*/
exec dbms_stats.delete_table_prefs(ownname=>'OZF',tabname=>'OZF_FUNDS_UTILIZED_ALL_B',pname=>'STALE_PERCENT');
exec dbms_stats.delete_schema_prefs(ownname => 'OZF', pname => 'STALE_PERCENT');
exec dbms_stats.delete_schema_prefs(ownname => 'AP', pname => 'INCREMENTAL');
exec dbms_stats.delete_database_prefs(pname => 'STALE_PERCENT');
exec dbms_stats.reset_global_pref_defaults;
exec DBMS_STATS.FLUSH_DATABASE_MONITORING_INFO;

/*
To execute the procedures at the global and database levels, you need to have both the system privileges analyze
any dictionary and analyze any. To execute the procedures at a schema or table level, you need to be connected as
owner or have the system privilege analyze any.
*/
select * from DBA_TAB_STAT_PREFS;
select * from dba_tables where table_name like '%STAT_PREFS%';