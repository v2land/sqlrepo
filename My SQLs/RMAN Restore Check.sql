 
 --percentage completion, time taken and estimated time for completion.  The ( RMAN: aggregate input ) is the sum of total  and the other lines are of partials ( RMAN: full datafile restore )  
   select OPNAME,SOFAR/TOTALWORK*100 PCT, trunc(TIME_REMAINING/60) MIN_RESTANTES,
trunc(ELAPSED_SECONDS/60) MIN_ATEAGORA from v$session_longops where TOTALWORK>0 and OPNAME like '%RMAN%' and trunc(TIME_REMAINING/60) >0;
-- Average per file of 40G = 20 min with 3 Channels   231 min restant. (3 files of 40 G in 20 min)
--  

select s.sid, p.spid, s.program, s.client_info,s.action, seq#, event,
seconds_in_wait AS sec_wait  
from v$session s, v$process p  
WHERE S.PADDR = P.ADDR AND S.PROGRAM LIKE '%rman%';

SELECT SID, SERIAL#, CONTEXT, SOFAR, TOTALWORK,
       ROUND(SOFAR/TOTALWORK*100,2) "%_COMPLETE", opname, message
FROM   V$SESSION_LONGOPS
WHERE  OPNAME LIKE 'RMAN%'
AND    TOTALWORK != 0
AND    SOFAR <> TOTALWORK;

-- === Kill rman sessions ====
SELECT 'alter system kill session ''' || s.sid || ',' || s.SERIAL# || ''';' a,
'ps -ef |grep LOCAL=NO|grep ' || p.SPID SPID,
'kill -9 ' || p.SPID
FROM gv$session s, gv$process p
WHERE ( (p.addr(+) = s.paddr) AND (p.inst_id(+) = s.inst_id))
AND s.sid in (select s.sid 
from v$session s, v$process p  
WHERE S.PADDR = P.ADDR AND S.PROGRAM LIKE '%rman%');

------ Files Done ----------------------
SELECT r.FILE# AS df#, d.NAME AS df_name, t.NAME AS tbsp_name, 
       d.STATUS, r.ERROR, r.CHANGE#, r.TIME
FROM V$RECOVER_FILE r, V$DATAFILE d, V$TABLESPACE t
WHERE t.TS# = d.TS#
AND d.FILE# = r.FILE#;
-----------------------------------------------
select * from v$recover_file where error is not null;
--count files rest to restore:
select count(*) from v$recover_file where error is not null;
--67
--64
--46
--22


select * from v$recover_file order by time asc;

--211-167
-- ============  Start, Elapsed, Remaining SID =================
select sl.start_time,
       sl.elapsed_seconds "ELA(s)", sl.time_remaining "REM(s)",
       s.sid, s.username, s.osuser,
       sl.opname,
       sl.sql_hash_value hash_val,
       s.program,
       sl.message
from gv$session_longops sl, gv$session s
where ( sl.time_remaining is not null
or trunc(sl.start_time) >= trunc(sysdate)  )
and sl.sid = s.sid
and sl.serial# = s.serial#
order by sl.start_time, s.sid;

select session_recid,session_stamp from v$rman_status where start_time > trunc(sysdate-7);

--session_recid = 15712  session_stamp = 945247011

select output
from GV$RMAN_OUTPUT
where session_recid = 15630
and session_stamp = 944785202
order by recid;

select SID, START_TIME,TOTALWORK, sofar, (sofar/totalwork) * 100 done,
sysdate + TIME_REMAINING/3600/24 end_at
from v$session_longops
where totalwork > sofar
AND opname NOT LIKE '%aggregate%'
AND opname like 'RMAN%';

--= Channels Client Info ==
select client_info
     , event 
  from v$session 
 where event like 'Backup%'
 order by client_info;

--============= Estimation Complette ALL: ==============
select recid
     , output_device_type
     , dbsize_mbytes
     , input_bytes/1024/1024 input_mbytes
     , output_bytes/1024/1024 output_mbytes
     , (output_bytes/input_bytes*100) compression
     , (mbytes_processed/dbsize_mbytes*100) complete
     , to_char(start_time + (sysdate-start_time)/(mbytes_processed/dbsize_mbytes),'DD-MON-YYYY HH24:MI:SS') est_complete
  from v$rman_status rs
     , (select sum(bytes)/1024/1024 dbsize_mbytes from v$datafile) 
 where status='RUNNING'
   and output_device_type is not null;
 
-- =============  percentage completion, time taken and estimated time for completion.  The ( RMAN: aggregate input ) is the sum of total  and the other lines are of partials ( RMAN: full datafile restore )  
   select OPNAME,SOFAR/TOTALWORK*100 PCT, trunc(TIME_REMAINING/60) MIN_RESTANTES,
trunc(ELAPSED_SECONDS/60) MIN_ATEAGORA from v$session_longops where TOTALWORK>0 and OPNAME like '%RMAN%';
-- ===================================================================================================
SELECT SID, SERIAL#, MESSAGE, CONTEXT, SOFAR, TOTALWORK, ROUND(SOFAR/TOTALWORK*100,2) "%_COMPLETE" FROM V$SESSION_LONGOPS
 WHERE OPNAME LIKE 'RMAN%' AND OPNAME NOT LIKE '%aggregate%' AND TOTALWORK != 0 AND SOFAR <> TOTALWORK;

SELECT SERIAL# FROM V$SESSION WHERE SID=3763;

--This query will show a the progress size in MB and history of backup/restore operations. By changing the where start_time > sysdate -1 clause you may view for more then 1 day.

select to_char(start_time, 'dd-mon-yyyy@hh24:mi:ss') "Date", 
status, 
operation,
mbytes_processed
from v$rman_status vs
where start_time >  sysdate -1
order by start_time;

--To terminate an Oracle process that is hung in the media manager:
SELECT *
FROM V$SESSION_WAIT sw, V$SESSION s, V$PROCESS p
WHERE sw.EVENT LIKE 'Backup%'
       AND s.SID=sw.SID
       AND s.PADDR=p.ADDR;
       
       
SELECT SID, SERIAL#, CONTEXT, SOFAR, TOTALWORK,
ROUND(SOFAR/TOTALWORK*100,2) "%_COMPLETE"
FROM V$SESSION_LONGOPS
WHERE OPNAME LIKE 'RMAN%'
AND OPNAME NOT LIKE '%aggregate%'
AND TOTALWORK != 0
AND SOFAR <> TOTALWORK;


SELECT *
FROM V$SESSION_WAIT sw, V$SESSION s, V$PROCESS p
WHERE sw.EVENT LIKE 'Backup%'
       AND s.SID=sw.SID
       AND s.PADDR=p.ADDR;


