--XXR_GL_SLA_RPT: RONA - GL Analysis Retail SQL_ID: fj2vuxgbxgx2b
--XXR_GL_SLA_RPT3: RONA - GL Analysis Retail SQL_ID: 3tkf473cqucw2
--e:SQLGL:cp:xxr/XXR_GL_SLA_RPT2: RONA GL Logistic SQL_ID:2ca2hxx7590f3     bad plan : 2999942917   selected good plan : 1137602925
--2ca2hxx7590f3 --BAD Plan hash value: 1217733101
--884dqj5g9t363 e:PER:cp:xxr/XXRHR_OHR_CER_INT_TC_FILE
--4b6sqy5jz4s5v  15479026	XXRHR_OHR_CER_INT_TC_FILE	RONA - OHR Ceridian Interface - Time Card Information	00:06:54	02:11:53	00:00:02	00:12:42	00:00:07	00:20:32
--36m7jxdm01rga
--db file sequential read	3z77dzp83h5kw
--4b6sqy5jz4s5v coe_4b6sqy5jz4s5v_3666385270
--Request: 15479386
--d6tdsqjhqjmvb XXRHR_HOURS_REC RONA - Hours Reconciliation Report
--If any Concurrent request is slow or takes more than normal time to complete,follow the below steps first to verify ..
--HighLevel Steps..
--select * from v$session where process in(select os_process_id from fnd_concurrent_requests where request_id='');
--Get SID from above sql.
--Take that sid and find object_id's from below command
select * from v$locked_objects where session_id='SID';
--Now take all the object_id's and check any other session is locking those object_ids apart from the above SID...
select * from v$locked_objects where object_id ='';
--if any session is locking check the status of that session from v$session.
--if that is inactive kill that session ... to free up the resource
alter system kill session 'SID,SERIAL#';

-- FIND Session info and SQL_ID OF REQUEST
select sid,SERIAL#,status,sql_id,TO_CHAR(LOGON_TIME,'DD/MM/YYYY HH24:MI:SS') logon_time,module,action,machine,username
from v$session where paddr in (select addr from v$process
where spid=(select oracle_process_id from fnd_concurrent_requests where request_id='22088012')); 
--2584	49235	INACTIVE		09/01/2019 11:37:55	e:IBY:cp:iby/IBY_FC_PAYER_NOTIF_FORMAT	IBY/IBY_FC_PROCESS_MANAGER	dcssdebsapps.agropur.com	APPS
--194dm4z2yp21c
--4m8u4nmjyq8zj
--b0r1k3kcmp8sr
--5xrtgvsmvha9d
--0fjtyz6kf7p6y
--6r8cu9hgh3t0g
--194dm4z2yp21c
--3538	14381	ACTIVE	ccw1u1brf9wgk	13/04/2018 12:12:55	e:ZX:cp:zx/RXZXPTEX	ZX/XXZX_CLERK_1_R	dcssdebsapps	APPS
--ccw1u1brf9wgk

select /*+ ordered */
fcr.request_id
,sess.sid
,sess.serial#
,inst.inst_name
,sa.SQL_ID
,fcp.user_concurrent_program_name
--,round(24*60*( sysdate - actual_start_date ),'HH24:MI:SS') elapsed
,TO_CHAR ( TRUNC(sysdate)
               + NUMTODSINTERVAL ((sysdate - actual_start_date)*24*60,
                    'minute'),
               'HH24:MI:SS') "Process_TIME"
,fcr.oracle_process_id
,fu.USER_NAME
from apps.fnd_concurrent_requests fcr
,apps.fnd_concurrent_programs_tl fcp
,apps.fnd_concurrent_processes cp
,apps.fnd_user fu
,gv$process pro
,gv$session sess
,gv$sqlarea sa
,sys.v_$active_instances inst
where fcp.concurrent_program_id = fcr.concurrent_program_id
and fcp.application_id = fcr.program_application_id
and fcr.controlling_manager = cp.concurrent_process_id
and fcr.requested_by = fu.user_id (+)
and fcr.oracle_process_id = pro.spid (+)
and pro.addr = sess.paddr (+)
and sess.sql_address = sa.address (+)
and sess.sql_hash_value = sa.hash_value (+)
and sess.inst_id = inst.inst_number (+)
AND fcp.language = userenv('LANG')
and request_id='23704865';

--23352245	2584	49235		b0r1k3kcmp8sr	Send Receipt of Payment Notifications	00:04:19	19661968	CLSENEY
--194dm4z2yp21c
--4zwnutvbw6kry
--4mzxvp4k2jdqk
--4m8u4nmjyq8zj
--b0r1k3kcmp8sr
--5xrtgvsmvha9d


--## --  Last/Latest Running SQLs of the specific Request ID -- ## -- 
-----------------------
select
distinct
ash.inst_id
--,ash.sample_time
,ash.session_id,ash.session_serial#,ash.sql_id
--,ash.WAIT_TIME
from gv$active_session_history ash,
    (select sid,SERIAL#
    from v$session where paddr in (select addr from v$process
    where spid=(select oracle_process_id from fnd_concurrent_requests where request_id=:request_id))
    ) vs
where sql_id is not null
and ash.session_id=vs.sid
and ash.session_serial#=vs.SERIAL#
order by 2 desc;

--5frtq948kz5gr
--6r8cu9hgh3t0g

-- --- Query 11 wait events details related with Concurrent programs
SELECT s.saddr, s.SID, s.serial#, s.audsid, s.paddr, s.user#, s.username,
s.command, s.ownerid, s.taddr, s.lockwait, s.status, s.server,
s.schema#, s.schemaname, s.osuser, s.process, s.machine, s.terminal,
UPPER (s.program) program, s.TYPE, s.sql_address, s.sql_hash_value,
s.sql_id, s.sql_child_number, s.sql_exec_start, s.sql_exec_id,
s.prev_sql_addr, s.prev_hash_value, s.prev_sql_id,
s.prev_child_number, s.prev_exec_start, s.prev_exec_id,
s.plsql_entry_object_id, s.plsql_entry_subprogram_id,
s.plsql_object_id, s.plsql_subprogram_id, s.module, s.module_hash,
s.action, s.action_hash, s.client_info, s.fixed_table_sequence,
s.row_wait_obj#, s.row_wait_file#, s.row_wait_block#,
s.row_wait_row#, s.logon_time, s.last_call_et, s.pdml_enabled,
s.failover_type, s.failover_method, s.failed_over,
s.resource_consumer_group, s.pdml_status, s.pddl_status, s.pq_status,
s.current_queue_duration, s.client_identifier,
s.blocking_session_status, s.blocking_instance, s.blocking_session,
s.seq#, s.event#, s.event, s.p1text, s.p1, s.p1raw, s.p2text, s.p2,
s.p2raw, s.p3text, s.p3, s.p3raw, s.wait_class_id, s.wait_class#,
s.wait_class, s.wait_time, s.seconds_in_wait, s.state,
s.wait_time_micro, s.time_remaining_micro,
s.time_since_last_wait_micro, s.service_name, s.sql_trace,
s.sql_trace_waits, s.sql_trace_binds, s.sql_trace_plan_stats,
s.session_edition_id, s.creator_addr, s.creator_serial#
FROM v$session s
WHERE ( (s.username IS NOT NULL)
AND (NVL (s.osuser, 'x') <> 'SYSTEM')
AND (s.TYPE <> 'BACKGROUND') AND STATUS='ACTIVE'
)
and UPPER(s.program) like UPPER('%adf%')
--and MODULE='e:IBY:cp:iby/IBY_FC_PAYER_NOTIF_FORMAT'  ---MODULE -- 7xu16mtua5xkj
ORDER BY "PROGRAM";

-- Find Session info on sqlid
SELECT 'alter system kill session '''  || sid  || ','  || serial#  || ''';' kill, OSUSER, SID, SERIAL#, executions,
sql.SQL_ID ,sql.child_number, SQL_FULLTEXT 
FROM V$SESSION sess JOIN V$SQL sql 
on  (sess.SQL_ID = sql.SQL_ID) 
where 
--sess.STATUS = 'ACTIVE' and
sql.sql_id = '31spkjmduyvrq'; --('2b7jsh1b1acfd','344ts62mfmrb1');--'8m1m8x201j0st'; --fbajpvqg1fux6
--wlsdbadf	42562	1779	1	8m1m8x201j0st	0	SELECT * FROM (SELECT DISTINCT e.org_id                ,e.customer_trx_id                ,e.transaction_number                ,e.transaction_date                ,e.actual_shipment_date                ,e.scan_date                ,e.seq_num                ,e.document_type                ,e.document_description                ,e.document_link                ,(SELECT SUM(rctlas.extended_amount + rctlas.tax_recoverable)                  FROM   ra_customer_trx_all       rcta --AR Transaction header                        ,ra_customer_trx_lines_all rctlas --AR Transaction line Unit Standard Price                        ,oe_order_headers_all      ooha --Sales Order header                        ,oe_order_lines_all        oola --Sales Order line                        ,mtl_system_items_b        item --Item                  WHERE  1 = 1                  AND    rcta.customer_trx_id = rctlas.customer_trx_id                  AND    rctlas.interface_line_attribute6 = oola.line_id                  AN
--coe_8m1m8x201j0st_3882586307
--PL SQL DEV:
--1	plsqldev.exe	PL/SQL Developer	SQL*Net message from client	a2qup152vy2r5	AGROPUR\DCSSDDEVTOOLS	   0:01:40

-- search select in Active users 
   SELECT   b.status, substr(a.spid,1,9) pid,    substr(b.sid,1,5) sid,    substr(b.serial#,1,5) ser#,    substr(b.machine,1,6) box,    substr(b.username,1,10) username, b.server, b.program,
    substr(b.osuser,1,8) os_user,    substr(b.program,1,30) program,    s.sql_id,    b.SQL_EXEC_START,    b.MACHINE, b.CLIENT_IDENTIFIER,   SQL_FULLTEXT
    FROM    v$session b,    v$process a,    V$SQLAREA s 
    WHERE 
    b.paddr = a.addr    AND b.sql_hash_value = s.hash_value    AND    type = 'USER'   -- AND   b.username in ('APPS')
  and 
  -- and OSUSER LIKE '%wls%'
  -- and b.program like '%adfuser%'
  and SQL_TEXT LIKE '%%'
--  and s.sql_id = '0fjtyz6kf7p6y'
    ORDER BY    spid;
--    alter system kill session '2784,59370';
 
 select m.status
     , m.sql_text
     , dbms_sqltune.report_sql_monitor
       ( sql_id => m.sql_id
       , sql_exec_id => m.sql_exec_id
       , type => 'ACTIVE'
       , report_level => 'ALL' ) as report
from   v$sql_monitor m
where  m.sid = :sid
and    m.session_serial# = :serial#
order by m.sql_exec_start desc;
    
    select * from v$session where program like '%adfuser%';
----in History
select * from dba_hist_sqltext where SQL_TEXT LIKE '%SELECT * FROM (SELECT mrpsalesforecasteo.fiscal_period%';

select t.sql_id,
    s.snap_id,
    t.sql_text,
    s.OPTIMIZER_COST,
    s.PLAN_HASH_VALUE,
    s.executions_delta,
    s.executions_total,
    s.elapsed_time_total, module
from DBA_HIST_SQLSTAT s, DBA_HIST_SQLTEXT t
where s.snap_id between (Select snap_id from DBA_HIST_SNAPSHOT where begin_interval_time like '%19-JAN-19 04% PM%')
                    and (Select snap_id from DBA_HIST_SNAPSHOT where begin_interval_time like '%19-JAN-19 04% PM%')  -- 
and SQL_TEXT like '%OOL.LINE_ID, (SELECT TRIP_ID%'
and module like '%e:XXEBS:cp:xxebs/XXONT0850%'
order by s.OPTIMIZER_COST desc
;

SELECT
   h.sample_time,
   u.username,
   h.program,
   h.module,
   s.sql_text
FROM
   DBA_HIST_ACTIVE_SESS_HISTORY h,
   DBA_USERS u,
   DBA_HIST_SQLTEXT s
WHERE  sample_time >= SYSDATE - 1
   AND h.user_id=u.user_id
   AND h.sql_id = s.sql_iD
   and h.sql_id='5f93f78fkyz6a'
ORDER BY h.sample_time desc;

---------Check Bind Variables-------
SELECT * FROM v$sql_bind_capture WHERE sql_id='5f93f78fkyz6a';
--or
SELECT NAME,POSITION,DATATYPE_STRING,VALUE_STRING 
FROM v$sql_bind_capture WHERE sql_id='7zxzk5qrz7s0q';

select ADDRESS, HASH_VALUE, PLAN_HASH_VALUE, MODULE,sql_id, ELAPSED_TIME, DISK_READS, USER_IO_WAIT_TIME, sql_profile, OBJECT_STATUS, SQL_FULLTEXT  from v$sql where sql_id in
('0n06j7pngq39h'
,'194dm4z2yp21c'
,'3h1z4juqjpvnd'
,'4m8u4nmjyq8zj'
,'4zwnutvbw6kry'
,'5xrtgvsmvha9d'
,'6r8cu9hgh3t0g'
,'6s2x8dvp411tm'
,'6vn5c04yyw0p5'
,'ath34wjhmxshr'
,'b0r1k3kcmp8sr'
,'bgpdr7qd0yuzp'
,'gpanz2xt8mv87')
 order by ELAPSED_TIME desc;
 
 --2 in 1 
 select v_sql.ADDRESS, v_sql.HASH_VALUE, v_sql.PLAN_HASH_VALUE, v_sql.MODULE,v_sql.sql_id, v_sql.ELAPSED_TIME, v_sql.DISK_READS, v_sql.USER_IO_WAIT_TIME, v_sql.sql_profile, v_sql.OBJECT_STATUS, v_sql.SQL_FULLTEXT
from v$sql v_sql
,(select
    distinct
    --ash.inst_id
    --,ash.sample_time
    --,ash.session_id,ash.session_serial#,
    ash.sql_id
    --,ash.WAIT_TIME
    from gv$active_session_history ash,
        (select sid,SERIAL#
        from v$session where paddr in (select addr from v$process
        where spid=(select oracle_process_id from fnd_concurrent_requests where request_id=:request_id))
        ) vs
    where sql_id is not null
    and ash.session_id=vs.sid
    and ash.session_serial#=vs.SERIAL#
    )r_id_sql_id
where v_sql.sql_id = r_id_sql_id.sql_id
order by ELAPSED_TIME desc;

--3790	12822	ACTIVE	32agdmaqbbjmv	20/03/2017 15:52:50	e:AR:cp:ar/RXARUNAR	AR/XXRAR_FACTURE_AR_FIN_PERIODE	l90990prdebsmt.rona.ca	APPS coe_5f93f78fkyz6a_99053727
select ADDRESS, HASH_VALUE, PLAN_HASH_VALUE, MODULE, ELAPSED_TIME, DISK_READS, USER_IO_WAIT_TIME, sql_profile,EXECUTIONS, OBJECT_STATUS, SQL_FULLTEXT  from v$sql where sql_id = 'gfzm6ghrr2d3k' order by ELAPSED_TIME desc;
--070001060AF5E838	795948146	2304027116  : gfzm6ghrr2d3k
--agzxzrz8dc065 old
--bmwwb8rcc6jfm new
--7zxzk5qrz7s0q: TEST STALE en TEST: 
--07000101479EEFE0	2951995414	2462133398	e:OZF:fwk:ozf.oa.claim.server.ClaimUpdateAM	106141381	4869	105553908		VALID	SELECT 'F' selectFlag, utiz.actId actId, lk.meaning actTypeName, utiz.actType actType, utiz.actOfferType actOfferType, utiz.offerCustomSetupId offerCustomSetupId,(CASE (utiz.actType) WHEN  'OFFR' THEN (select offer.offer_code || ' (' || qp.description || ')' from ozf_offers offer, qp_list_headers_vl qp where qp.list_header_id = offer.qp_list_header_id and offer.qp_list_header_id = utiz.actId)  WHEN 'PRIC' THEN (select qp.description from qp_list_headers_vl qp where qp.list_header_id = utiz.actId)  WHEN  'CAMP' THEN (select campaign_name from ams_campaigns_v where campaign_id = utiz.actId)  ELSE NULL END )  AS actCode,  NULL docTypeName, NULL docType, NULL documentId, NULL docNum, NULL docDate, NULL itemTypeName, NULL itemType, NULL itemId, NULL itemName, NULL period, curr.name planCurrency, SUM(utiz.plan_curr_amount) planCurAmount, SUM(utiz.plan_curr_amount_remaining) planCurAmRem, SUM(utiz.acctd_amount) acctdAmount, SUM(utiz.acctd_amount_remaining) acctdAmRem,'Canadian Dollar' acctdCurrency, utiz.custAcctId custAcctId, (SELECT party_name FROM hz_parties hp, hz_cust_accounts hca WHERE hp.party_id = hca.party_id AND hca.cust_account_id = utiz.custAcctId) custName, utiz.plan_currency_code currencyCode, sum(asso_amt) asso_amt FROM(SELECT 'OFFR' actType , fu.plan_id actId , offer.offer_type actOfferType , offer.custom_setup_id offerCustomSetupId , fu.cust_account_id custAcctId , fu.product_level_type , fu.product_id , fu.object_type object_type , fu.object_id object_id , fu.plan_curr_amount plan_curr_amount , fu.plan_curr_amount_remaining plan_curr_amount_remaining , fu.acctd_amount acctd_amount , fu.acctd_amount_remaining acctd_amount_remaining , fu.plan_currency_code plan_currency_code , fu.gl_date ,(select sum(clu.amount) from ozf_claim_lines_util_all clu, ozf_claim_lines_all cl where clu.utilization_id = fu.utilization_id and clu.claim_line_id = cl.claim_line_id and cl.claim_id = :1) asso_amt FROM ozf_funds_utilized_all_b fu , ozf_offers offer , qp_list_headers_b qp WHERE fu.plan_type = 'OFFR'  AND fu.plan_id = offer.qp_list_header_id  AND fu.org_id = :2 AND offer.qp_list_header_id = qp.list_header_id  AND offer.offer_type <> 'SCAN_DATA'  AND (fu.reference_type IS NULL OR fu.reference_type NOT IN ('SOFT_FUND', 'SPECIAL_PRICE'))  AND  ((FU.PLAN_ID ,FU.CUST_ACCOUNT_ID)  in ( SELECT /*+ unnest no_merge  */ fun.plan_id , fun.CUST_ACCOUNT_ID FROM ozf_claim_lines_util clu,   ozf_claim_lines_all cln, ozf_funds_utilized_all_b fun WHERE cln.claim_line_id = clu.claim_line_id AND fun.utilization_id = clu.utilization_id AND cln.claim_id  = :3)) AND fu.utilization_type IN ('ACCRUAL', 'ADJUSTMENT')  AND fu.gl_posted_flag = 'Y'  UNION ALL SELECT fu.plan_type actType , fu.plan_id actId , null actOfferType , to_number(null) offerCustomSetupId , fu.cust_account_id custAcctId , fu.product_level_type , fu.product_id , fu.object_type object_type , fu.object_id object_id , fu.plan_curr_amount plan_curr_amount , fu.plan_curr_amount_remaining plan_curr_amount_remaining , fu.acctd_amount acctd_amount , fu.acctd_amount_remaining acctd_amount_remaining , fu.plan_currency_code plan_currency_code , fu.gl_date ,(select sum(clu.amount) from ozf_claim_lines_util_all clu, ozf_claim_lines_all cl where clu.utilization_id = fu.utilization_id and clu.claim_line_id = cl.claim_line_id and cl.claim_id = :4) asso_amt FROM ozf_funds_utilized_all_b fu WHERE fu.plan_type = 'PRIC'  AND fu.org_id = :5 AND  ((FU.PLAN_ID ,FU.CUST_ACCOUNT_ID)  in ( SELECT /*+ unnest no_merge  */ fun.plan_id , fun.CUST_ACCOUNT_ID FROM ozf_claim_lines_util clu,   ozf_claim_lines_all cln, ozf_funds_utilized_all_b fun WHERE cln.claim_line_id = clu.claim_line_id AND fun.utilization_id = clu.utilization_id AND cln.claim_id  = :6 ) )  AND fu.utilization_type IN ('ACCRUAL', 'ADJUSTMENT')  AND fu.gl_posted_flag = 'Y' ) utiz , fnd_currencies_vl curr , ozf_lookups lk  WHERE (utiz.plan_currency_code = curr.currency_code AND  utiz.actType = lk.lookup_code AND  lk.lookup_type = 'OZF_CLAIM_ASSO_ACT_TYPE') GROUP BY utiz.actType, utiz.actId, utiz.actOfferType, utiz.offerCustomSetupId, lk.meaning, curr.name, utiz.custAcctId , utiz.plan_currency_code, utiz.custAcctId
--7zxzk5qrz7s0q	e:OZF:fwk:ozf.oa.claim.server.ClaimUpdateAM	APPS	2019-01-25/14:30:18	106141381	2434.5	4869	9832	1	0	2	0	50.48	323841	193720	189488	563804	SELECT 'F' selectFlag, utiz.actId actId, lk.meaning actTypeName, utiz.actType actType, utiz.actOfferType actOfferType, utiz.offerCustomSetupId offerCustomSetupId,(CASE (utiz.actType) WHEN  'OFFR' THEN (select offer.offer_code || ' (' || qp.description || ')' from ozf_offers offer, qp_list_headers_vl qp where qp.list_header_id = offer.qp_list_header_id and offer.qp_list_header_id = utiz.actId)  WHEN 'PRIC' THEN (select qp.description from qp_list_headers_vl qp where qp.list_header_id = utiz.actId)  WHEN  'CAMP' THEN (select campaign_name from ams_campaigns_v where campaign_id = utiz.actId)  ELSE NULL END )  AS actCode,  NULL docTypeName, NULL docType, NULL documentId, NULL docNum, NULL docDate, NULL itemTypeName, NULL itemType, NULL itemId, NULL itemName, NULL period, curr.name planCurrency, SUM(utiz.plan_curr_amount) planCurAmount, SUM(utiz.plan_curr_amount_remaining) planCurAmRem, SUM(utiz.acctd_amount) acctdAmount, SUM(utiz.acctd_amount_remaining) acctdAmRem,'Canadian Dollar' acctdCu
--07000101479EEFE0	2951995414	2462133398	e:OZF:fwk:ozf.oa.claim.server.ClaimUpdateAM	115683302	4909	114932688		VALID	SELECT 'F' selectFlag, utiz.actId actId, lk.meaning actTypeName, utiz.actType actType, utiz.actOfferType actOfferType, utiz.offerCustomSetupId offerCustomSetupId,(CASE (utiz.actType) WHEN  'OFFR' THEN (select offer.offer_code || ' (' || qp.description || ')' from ozf_offers offer, qp_list_headers_vl qp where qp.list_header_id = offer.qp_list_header_id and offer.qp_list_header_id = utiz.actId)  WHEN 'PRIC' THEN (select qp.description from qp_list_headers_vl qp where qp.list_header_id = utiz.actId)  WHEN  'CAMP' THEN (select campaign_name from ams_campaigns_v where campaign_id = utiz.actId)  ELSE NULL END )  AS actCode,  NULL docTypeName, NULL docType, NULL documentId, NULL docNum, NULL docDate, NULL itemTypeName, NULL itemType, NULL itemId, NULL itemName, NULL period, curr.name planCurrency, SUM(utiz.plan_curr_amount) planCurAmount, SUM(utiz.plan_curr_amount_remaining) planCurAmRem, SUM(utiz.acctd_amount) acctdAmount, SUM(utiz.acctd_amount_remaining) acctdAmRem,'Canadian Dollar' acctdCurrency, utiz.custAcctId custAcctId, (SELECT party_name FROM hz_parties hp, hz_cust_accounts hca WHERE hp.party_id = hca.party_id AND hca.cust_account_id = utiz.custAcctId) custName, utiz.plan_currency_code currencyCode, sum(asso_amt) asso_amt FROM(SELECT 'OFFR' actType , fu.plan_id actId , offer.offer_type actOfferType , offer.custom_setup_id offerCustomSetupId , fu.cust_account_id custAcctId , fu.product_level_type , fu.product_id , fu.object_type object_type , fu.object_id object_id , fu.plan_curr_amount plan_curr_amount , fu.plan_curr_amount_remaining plan_curr_amount_remaining , fu.acctd_amount acctd_amount , fu.acctd_amount_remaining acctd_amount_remaining , fu.plan_currency_code plan_currency_code , fu.gl_date ,(select sum(clu.amount) from ozf_claim_lines_util_all clu, ozf_claim_lines_all cl where clu.utilization_id = fu.utilization_id and clu.claim_line_id = cl.claim_line_id and cl.claim_id = :1) asso_amt FROM ozf_funds_utilized_all_b fu , ozf_offers offer , qp_list_headers_b qp WHERE fu.plan_type = 'OFFR'  AND fu.plan_id = offer.qp_list_header_id  AND fu.org_id = :2 AND offer.qp_list_header_id = qp.list_header_id  AND offer.offer_type <> 'SCAN_DATA'  AND (fu.reference_type IS NULL OR fu.reference_type NOT IN ('SOFT_FUND', 'SPECIAL_PRICE'))  AND  ((FU.PLAN_ID ,FU.CUST_ACCOUNT_ID)  in ( SELECT /*+ unnest no_merge  */ fun.plan_id , fun.CUST_ACCOUNT_ID FROM ozf_claim_lines_util clu,   ozf_claim_lines_all cln, ozf_funds_utilized_all_b fun WHERE cln.claim_line_id = clu.claim_line_id AND fun.utilization_id = clu.utilization_id AND cln.claim_id  = :3)) AND fu.utilization_type IN ('ACCRUAL', 'ADJUSTMENT')  AND fu.gl_posted_flag = 'Y'  UNION ALL SELECT fu.plan_type actType , fu.plan_id actId , null actOfferType , to_number(null) offerCustomSetupId , fu.cust_account_id custAcctId , fu.product_level_type , fu.product_id , fu.object_type object_type , fu.object_id object_id , fu.plan_curr_amount plan_curr_amount , fu.plan_curr_amount_remaining plan_curr_amount_remaining , fu.acctd_amount acctd_amount , fu.acctd_amount_remaining acctd_amount_remaining , fu.plan_currency_code plan_currency_code , fu.gl_date ,(select sum(clu.amount) from ozf_claim_lines_util_all clu, ozf_claim_lines_all cl where clu.utilization_id = fu.utilization_id and clu.claim_line_id = cl.claim_line_id and cl.claim_id = :4) asso_amt FROM ozf_funds_utilized_all_b fu WHERE fu.plan_type = 'PRIC'  AND fu.org_id = :5 AND  ((FU.PLAN_ID ,FU.CUST_ACCOUNT_ID)  in ( SELECT /*+ unnest no_merge  */ fun.plan_id , fun.CUST_ACCOUNT_ID FROM ozf_claim_lines_util clu,   ozf_claim_lines_all cln, ozf_funds_utilized_all_b fun WHERE cln.claim_line_id = clu.claim_line_id AND fun.utilization_id = clu.utilization_id AND cln.claim_id  = :6 ) )  AND fu.utilization_type IN ('ACCRUAL', 'ADJUSTMENT')  AND fu.gl_posted_flag = 'Y' ) utiz , fnd_currencies_vl curr , ozf_lookups lk  WHERE (utiz.plan_currency_code = curr.currency_code AND  utiz.actType = lk.lookup_code AND  lk.lookup_type = 'OZF_CLAIM_ASSO_ACT_TYPE') GROUP BY utiz.actType, utiz.actId, utiz.actOfferType, utiz.offerCustomSetupId, lk.meaning, curr.name, utiz.custAcctId , utiz.plan_currency_code, utiz.custAcctId
--2462133398	53.071
--0700010088328C30	520217615	4059195173  227176562 DEV9
select ADDRESS, HASH_VALUE, PLAN_HASH_VALUE, MODULE, ELAPSED_TIME, DISK_READS, USER_IO_WAIT_TIME, sql_profile, OBJECT_STATUS, SQL_FULLTEXT  from v$sql where sql_id = '29p1jxrajz5g4' order by ELAPSED_TIME desc;
--29p1jxrajz5g4 en PROD: 07000106AEEB3A08	3575616996	2998707547	e:OZF:cp:xxebs/XXSQLAP0140	3979492472	467637	57158732		VALID	SELECT UTILIZATION_ID, NVL(GL_POSTED_FLAG,'N') FROM OZF_FUNDS_UTILIZED_ALL_B WHERE PLAN_ID = :B5 AND PLAN_TYPE = :B4 AND FUND_ID = :B3 AND NVL(PRODUCT_ID,0) = NVL(:B2 ,0) AND NVL(CUST_ACCOUNT_ID,0) = NVL(:B1 ,0) AND UTILIZATION_TYPE NOT IN ('REQUEST', 'TRANSFER')
select ADDRESS, HASH_VALUE, PLAN_HASH_VALUE, MODULE, ELAPSED_TIME, DISK_READS, USER_IO_WAIT_TIME, sql_profile, OBJECT_STATUS, SQL_FULLTEXT  from v$sql where sql_id = 'b5ds90337ynyq' order by ELAPSED_TIME desc;
--97tz3991us031
select ADDRESS, HASH_VALUE, PLAN_HASH_VALUE, MODULE, ELAPSED_TIME, DISK_READS, USER_IO_WAIT_TIME, sql_profile, OBJECT_STATUS, SQL_FULLTEXT  from v$sql where sql_id = '4zwnutvbw6kry' order by ELAPSED_TIME desc;
--2hngbw174ar52
select ADDRESS, HASH_VALUE, PLAN_HASH_VALUE, MODULE, ELAPSED_TIME, DISK_READS, USER_IO_WAIT_TIME, sql_profile, OBJECT_STATUS, SQL_TEXT  from v$sql where sql_id = '194dm4z2yp21c' order by ELAPSED_TIME desc;

--bms2srys6w8ub
select ADDRESS, HASH_VALUE, PLAN_HASH_VALUE, MODULE, ELAPSED_TIME, DISK_READS, USER_IO_WAIT_TIME, sql_profile, OBJECT_STATUS, SQL_FULLTEXT  from v$sql where sql_id = '16ygd3xb4mjqy' order by ELAPSED_TIME desc;

-- 0000001128536E90	1313168546	3521863213	e:PER:frm:PAYWSACV	207195196	57407	198654660		INVALID_UNAUTH	SELECT ROUND(SUM(FND_NUMBER.CANONICAL_TO_NUMBER(PEEV.SCREEN_ENTRY_VALUE)), 2) FROM PAY_ELEMENT_ENTRIES_F PEE, PAY_ELEMENT_ENTRY_VALUES_F PEEV WHERE PEEV.ELEMENT_ENTRY_ID = PEE.ELEMENT_ENTRY_ID AND EXISTS (SELECT BF.INPUT_VALUE_ID FROM PAY_BALANCE_FEEDS_F BF WHERE NVL(PEE.DATE_EARNED, PEE.EFFECTIVE_START_DATE) BETWEEN BF.EFFECTIVE_START_DATE AND BF.EFFECTIVE_END_DATE AND BF.INPUT_VALUE_ID = PEEV.INPUT_VALUE_ID AND BF.BALANCE_TYPE_ID = :B4 ) AND NVL(PEE.DATE_EARNED, PEE.EFFECTIVE_START_DATE) BETWEEN :B3 AND :B2 AND PEE.ASSIGNMENT_ID = :B1 

select ADDRESS, HASH_VALUE, PLAN_HASH_VALUE, MODULE, ELAPSED_TIME, DISK_READS, USER_IO_WAIT_TIME, sql_profile, OBJECT_STATUS, SQL_TEXT  from v$sql where sql_profile LIKE  'coe%';


-- Show the Plan Hash Values for a Given SQLID Over a Given Period
--
 
SELECT DISTINCT sql_id, plan_hash_value
FROM dba_hist_sqlstat q,
    (
    SELECT /*+ NO_MERGE */ MIN(snap_id) min_snap, MAX(snap_id) max_snap
    FROM dba_hist_snapshot ss
    WHERE ss.begin_interval_time BETWEEN (SYSDATE - &No_Days) AND SYSDATE
    ) s
WHERE q.snap_id BETWEEN s.min_snap AND s.max_snap
  AND q.sql_id IN ( 'gfzm6ghrr2d3k')
/

--OBJECT_STATUS
--Plan_HASH_VALUE BAD!!: 612266679
-- 00000011273FFAD0	1413892842	1097484155	e:AR:cp:xla/XLABAPUB	68924723744	3763153	3697992070		VALID	INSERT INTO xla_ctrl_bal_interim_gt (                application_id                                              , ledger_id                                              , code_combination_id                                              , party_type_code                                              , party_id                                              , party_site_id                                              , period_name                                              , effective_period_num                                              , period_balance_dr                                              , period_balance_cr                                              , period_year                                             )                                    SELECT   /*+  use_nl(aeh) use_nl(ael) */                                                ael.application_id                                              , ael.ledger_id                                

select * from V$SQL_PLAN where sql_id='gf23k4qh7tj6q';
--Plan hash value: 1137602925  HASH Value : 1314161091
--000000033CFEE538	768276254	c06hcmnqwpxsy	612266679
select * from V$SQL_PLAN where sql_id='4b6sqy5jz4s5v';
--Plan hash value: 74079347  HASH Value : 3647812482
select * from V$SQL_PLAN where sql_id='4tqnw651a6b4q';
select * from V$SQL_PLAN where sql_id='ckksbs3dvdgxm';

--Generate new Plan:
--cd /staging/scripts/oracle/sqlt/utl	
--sqlplus / as sysdba	
--	@coe_xfr_sql_profile.sql
--coe_xfr_sql_profile_2ca2hxx7590f3_1137602925.sql generated
-- pwd: /staging/scripts/oracle/sqlt/utl
-- /TEMP_DBA/vlcasapc/sqlt/utl
--/AGRGESTION_UTL/DBATools/sqlt/utl

--Check the best existing plan by cost on awr
select * from TABLE(dbms_xplan.display_awr('gfzm6ghrr2d3k'));

select ADDRESS, HASH_VALUE, PLAN_HASH_VALUE from V$SQLAREA where SQL_ID = '194dm4z2yp21c';
-- Dans SQL Area on a le BAD! :
-- ADDRESS            HASH_VALUE   PLAN_HASH_VALUE
--070001014D7475D0	3320481836	216763253

--00000010C97F3E50	1676828859	3666385270 

--check ADDRESS and HASH_VALUE for PURGE
select ADDRESS, HASH_VALUE, PLAN_HASH_VALUE from V$SQLAREA where SQL_ID = '3tkf473cqucw2';
-- Dans SQL Area on a le BAD! :
-- ADDRESS            HASH_VALUE   PLAN_HASH_VALUE
--000000118B5BE480	3647812482	612266679
select ADDRESS, HASH_VALUE, PLAN_HASH_VALUE from V$SQLAREA where SQL_ID = '4tqnw651a6b4q';
--0000000FFC2FCF10	3621254219	2175235308
--0000000FFC2FCF10	3621254219	2761763819
--0000000FFC2FCF10	3621254219	2761763819
--0000000FFC2FCF10	3621254219	2761763819
--0000000FFC2FCF10	3621254219	2761763819
--0000000FFC2FCF10	3621254219	2458404945
--00000001DCDEB0B8	1487942796	3840325703
--070001025378C638	2149090073	192217757  ---coe_xfr_sql_profile_8m1m8x201j0st_3882586307.sql

--@coe_xfr_sql_profile_fj2vuxgbxgx2b_2761763819.sql
-- @coe_xfr_sql_profile_3tkf473cqucw2_74079347.sql
--@coe_xfr_sql_profile_2ca2hxx7590f3_1137602925.sql
--@coe_xfr_sql_profile_7sf1p7wt4t9nt_3550130030.sql --
--00000010CA7BC800	1634258795
--000000033CFEE538	768276254	612266679
--070001027F9FCE48	843884185
--0700010043973B80	2200635992	3882586307 pour avsbyn21kq2ks  --coe_xfr_sql_profile_avsbyn21kq2ks_3882586307.sql
--070001025378C638	2149090073	632985112


set serveroutput on
begin
 for c in (select address,hash_value,users_executing,sql_text from v$sqlarea where sql_id='&sql_id') 
 loop 
  dbms_output.put_line(c.users_executing||' users executing '||c.sql_text);
  sys.dbms_shared_pool.purge(c.address||','||c.hash_value,'...'); 
  dbms_output.put_line('flushed.');
 end loop;
end;
/
--Enlever le mauvais plan du shared pool  pour SQL_ID:2ca2hxx7590f3  use ADDRESS and HASH_VALUE :
--07000101AB679378	3320481836	1242669342
--21-JAN-19 14:10:58	070001073268F2E8	489651402	3690151373
exec DBMS_SHARED_POOL.PURGE ('070001073268F2E8, 489651402', 'C');

--Enlever le mauvais plan du shared pool de SEBSPRD pour SQL_ID:fj2vuxgbxgx2b  use ADDRESS and HASH_VALUE :
exec DBMS_SHARED_POOL.PURGE ('070001060AF5E838, 795948146', 'C');
070001060AF5E838	795948146

	-- Check againt it should be clean :
select ADDRESS, HASH_VALUE from GV$SQLAREA where SQL_ID = '194dm4z2yp21c';
select ADDRESS, HASH_VALUE, PLAN_HASH_VALUE, MODULE, ELAPSED_TIME, DISK_READS, USER_IO_WAIT_TIME, SQL_TEXT, sql_profile  from v$sql where sql_id = '2ca2hxx7590f3' order by ELAPSED_TIME desc;

--transporter le plan:
--cd /staging/scripts/oracle/sqlt/utl
--sqlplus / as sysdba

select * from dba_sql_profiles;
--@coe_xfr_sql_profile_fj2vuxgbxgx2b_2761763819.sql
--@coe_xfr_sql_profile_3tkf473cqucw2_74079347.sql
--@coe_xfr_sql_profile_2ca2hxx7590f3_1137602925.sql
--@coe_xfr_sql_profile_bwm53fxa4ckra_3124606464.sql

-- IF ERROR at line 1:
--ORA-13841: SQL profile named coe_fj2vuxgbxgx2b_2761763819 already exists for a different signature/category pair
--ORA-06512: at "SYS.DBMS_SQLTUNE_INTERNAL", line 17518
--ORA-06512: at "SYS.DBMS_SQLTUNE", line 7858
--ORA-06512: at "SYS.DBMS_SQLTUNE", line 7828
--ORA-06512: at line 670

--Query to show sql_ids related to SQL Profiles
	select 
    --distinct
    --p.*, s.*
    p.name sql_profile_name,
    s.sql_id,
	p.created, 
    p.LAST_MODIFIED,
    p.STATUS,p.TYPE,
    p.SIGNATURE,
    s.FORCE_MATCHING_SIGNATURE,p.FORCE_MATCHING,
    s.MODULE,
    s.ACTION,
    s.OPTIMIZER_MODE,
    p.SQL_TEXT
    
from
	dba_sql_profiles p,
	DBA_HIST_SQLSTAT s
	where
p.name=s.sql_profile
--and p.name LIKE 'coe_%'  -- remove that to see all SYS also
   order by LAST_MODIFIED desc;
   
 --Profile Attribute Detail  
   SELECT *
  FROM DBA_SQL_PROFILES prof, 
       DBMSHSXP_SQL_PROFILE_ATTR attr
  WHERE prof.NAME=attr.PROFILE_NAME
  --and prof.name LIKE 'coe_%'  -- remove that to see all SYS also
  ORDER BY prof.name; -- FORCE_MATCHING NO

--Enable/Disable SQL Profile
--Locate the name of the SQL Profile you would like to disable and plug it in to the following statement:?1
EXEC DBMS_SQLTUNE.ALTER_SQL_PROFILE('PROFILE_NAME','STATUS','DISABLED');
EXEC DBMS_SQLTUNE.ALTER_SQL_PROFILE('coe_5f93f78fkyz6a_99053727','STATUS','DISABLED');



--drop SQL profile named coe_fj2vuxgbxgx2b_2761763819
    --EXEC DBMS_SQLTUNE.DROP_SQL_PROFILE('coe_fj2vuxgbxgx2b_2761763819');
    EXEC DBMS_SQLTUNE.DROP_SQL_PROFILE('coe_5f93f78fkyz6a_99053727');
    -- re run @coe_xfr_sql_profile_fj2vuxgbxgx2b_2761763819.sql
    -- OK  ... manual custom SQL Profile has been created


--5. Demander de relancer le rapport afin de s'assurer qu'il prends le nouveau plan.
select * from table(dbms_xplan.display_cursor('13wz9tamy7zzy')); -- le Bon Plan_hash_value fixe etait: 1137602925 - OK


select * from table(dbms_xplan.display_cursor('4tqnw651a6b4q'));
select * from table(dbms_xplan.display_cursor('ckksbs3dvdgxm'));
--Plan hash value: 74079347
select * from V$SQL_PLAN where sql_id='4tqnw651a6b4q';
select * from V$SQL_PLAN where sql_id='ckksbs3dvdgxm';



50308, PERIOD, Y, P12-16, P12-16, , , , 001-000000-0000-21625-000-0000, 001-000000-0000-21625-999-0000, 001-000000-0000-21635-000-0000, 001-000000-0000-21635-999-0000, Y, Y, N, Finance, N
--	6.load_plans_from_cursor to dba_sql_plan_baselines
	
		declare v_sql_plan_id pls_integer;
		begin
		v_sql_plan_id := dbms_spm.load_plans_from_cursor_cache(
		sql_id => '7sf1p7wt4t9nt');
		end;
		/
    
    declare v_sql_plan_id pls_integer;
		begin
		v_sql_plan_id :=dbms_spm.load_plans_from_cursor_cache(
 sql_id => 'bwm53fxa4ckra',
 plan_hash_value => 3124606464);
 end;
 /
 --sql_handle => '*****************�);
 
 --DROP BASELINE
 SET SERVEROUTPUT ON
DECLARE
  l_plans_dropped  PLS_INTEGER;
BEGIN
  l_plans_dropped := DBMS_SPM.drop_sql_plan_baseline (
    sql_handle => NULL,
    plan_name  => 'SQL_PLAN_9sw5j7n773ptjdc283a0b');
    
  DBMS_OUTPUT.put_line(l_plans_dropped);
END;
/

--An sql_handle can have multiple sql baselines tagged, So if you want to 
--drop all the sql baselines of that handle, then drop the sql handle itself.
�
declare
drop_result pls_integer;
begin
drop_result := DBMS_SPM.DROP_SQL_PLAN_BASELINE(
sql_handle => 'SQL_10505542c1f798dd');
dbms_output.put_line(drop_result);
end;
/

  
-- CHECK c06hcmnqwpxsy
	select 
	sql_handle, 
	plan_name, 
	enabled, 
	accepted,
  fixed,
  origin
  from 
	dba_sql_plan_baselines
	where LOWER(sql_text) like LOWER('%SELECT party_num%party_name%invoice_num%invoice_date%description%entered_dr%entered_cr%accounted_dr%accounted_cr%');

 -- cehck if sql_id is in baseline 
 SELECT sql_handle, 
	plan_name, 
	enabled, 
	accepted,
  fixed,
  origin
FROM dba_sql_plan_baselines
WHERE signature IN  ( SELECT exact_matching_signature FROM v$sql WHERE sql_id='194dm4z2yp21c') ;
--SQL_a8a072e26b0e2176	SQL_PLAN_aj83kw9phw8bq5ff05655	YES	YES	NO	MANUAL-LOAD
 
  
-- Plan name desired to fix: SQL_PLAN_8z366fum04qmu7bad8939   and SQL_HANDLE: SQL_8f8cc676a6025a7a
-- FIX PLAN
--SQL_8f8cc676a6025a7a	SQL_PLAN_8z366fum04qmu7bad8939	YES	YES	NO	MANUAL-LOAD
--
		SET SERVEROUTPUT ON
		
		DECLARE
		l_plans_altered PLS_INTEGER;
		BEGIN
		l_plans_altered := DBMS_SPM.alter_sql_plan_baseline(
		sql_handle => 'SQL_a8a072e26b0e2176',
		plan_name => 'SQL_PLAN_aj83kw9phw8bq5ff05655',
		attribute_name =>'fixed',
		attribute_value => 'YES');
		DBMS_OUTPUT.put_line('Plans Altered: ' || l_plans_altered);
		END;
		/


--FORCE DISABLE PLAN
DECLARE
		l_plans_altered NUMBER;
		BEGIN
		l_plans_altered := DBMS_SPM.ALTER_SQL_PLAN_BASELINE(
     SQL_HANDLE => 'SQL_8f8cc676a6025a7a',
     PLAN_NAME
      => 'SQL_PLAN_8z366fum04qmu96f0bb07',
    ATTRIBUTE_NAME => 'enabled',
       ATTRIBUTE_VALUE => 'NO'); 
    DBMS_OUTPUT.put_line('Plans Altered: ' || l_plans_altered);
		END;
		/
 -- UNACCEPT PLAN   
DECLARE
		l_plans_altered NUMBER;
		BEGIN
		l_plans_altered := DBMS_SPM.ALTER_SQL_PLAN_BASELINE(
     SQL_HANDLE => 'SQL_8f8cc676a6025a7a',
     PLAN_NAME
      => 'SQL_PLAN_8z366fum04qmu96f0bb07',
    ATTRIBUTE_NAME => 'accepted',
       ATTRIBUTE_VALUE => 'NO'); 
    DBMS_OUTPUT.put_line('Plans Altered: ' || l_plans_altered);
		END;
		/

--########################
select 
signature,
sql_handle, 
plan_name, 
enabled, 
accepted,
fixed
from 
dba_sql_plan_baselines
where fixed ='YES';
-- SQL_cc58509cf2fa1b1c	SQL_PLAN_csq2hmmtgn6sw3dda46d5	YES	YES	YES	MANUAL-LOAD  Pour fj2vuxgbxgx2b
-- SQL_86ebb8c4c6fa9f8b	SQL_PLAN_8duxssm3gp7wb7c7bb961	YES	YES	YES	MANUAL-LOAD  pour 3tkf473cqucw2
-- SQL_8f8cc676a6025a7a	SQL_PLAN_8z366fum04qmu7c45e77e	YES	YES	YES MANUAL-LOAD  pour 2ca2hxx7590f3

--Accept a PLAN
SET LONG 10000
SELECT DBMS_SPM.evolve_sql_plan_baseline(sql_handle => 'SQL_cc58509cf2fa1b1c') FROM   dual;

--FIX The Plan
		SET SERVEROUTPUT ON
		DECLARE
		l_plans_altered PLS_INTEGER;
		BEGIN
		l_plans_altered := DBMS_SPM.alter_sql_plan_baseline(
		sql_handle => 'SQL_9c70b13d0e71d731',
		plan_name => 'SQL_PLAN_9sw5j7n773ptjdc283a0b',
		attribute_name =>'fixed',
		attribute_value => 'YES');
		DBMS_OUTPUT.put_line('Plans Altered: ' || l_plans_altered);
		END;
		/


--Query to show sql_ids related to SQL Profiles
select
*
from
dba_sql_profiles
--where
--status = 'ENABLED'
;

select distinct 
p.name sql_profile_name,
s.sql_id,
p.FORCE_MATCHING
from 
dba_sql_profiles p,
DBA_HIST_SQLSTAT s
where
p.name=s.sql_profile
--and s.SQL_PROFILE Like '%coe_%'
;
--check FORCE_MATCH
--6205922102956930212 signature coe_8m1m8x201j0st_3882586307

--
select ADDRESS, HASH_VALUE, PLAN_HASH_VALUE from V$SQLAREA where SQL_ID = 'fj2vuxgbxgx2b'; 
-- Dans SQL Area on a le Good Plan! :
-- ADDRESS            HASH_VALUE   PLAN_HASH_VALUE
--0000000A75FC6D10	1314161091	1137602925


--Plan Details:
select * from DBA_SQL_PLAN_BASELINES where sql_handle='SQL_9ea1d7f8ce29f8cc'; 
--signature:  14975815506442855371
select plan_table_output
from table(dbms_xplan.display_cursor('194dm4z2yp21c',null,'basic'));


--select t.* from
--table(dbms_xplan.display_sql_plan_baseline('SQL_PLAN_8duxssm3gp7wb7c7bb961',
 --                                          format => 'basic')) t;


SELECT parameter_name, parameter_value
FROM   dba_sql_management_config;

--Check PLAN by Sql_ID
SELECT
sql_handle, plan_name,LAST_EXECUTED,CREATED, enabled,accepted,fixed,SQL_TEXT
FROM dba_sql_plan_baselines p
;
select SQL_PLAN_BASELINE from v$sql where SQL_PLAN_BASELINE is not null;

--CHECK ALL PLANS AND SQL_IDs in v$sql
SELECT 
     v.sql_id, p.sql_handle, p.plan_name,p.LAST_EXECUTED,CREATED, p.enabled,p.accepted,p.fixed--,SQL_TEXT
FROM dba_sql_plan_baselines p,
 v$sql v
WHERE 
p.signature = v.exact_matching_signature
--and p.PLAN_NAME = v.SQL_PLAN_BASELINE
;

--calculate HASH_VALUE from sql_id
select
    lower(trim(:sql_id)) sql_id
  , trunc(mod(sum((instr('0123456789abcdfghjkmnpqrstuvwxyz',substr(lower(trim(:sql_id)),level,1))-1)
                       *power(32,length(trim(:sql_id))-level)),power(2,32))) hash_value
from
    dual
connect by
    level <= length(trim(:sql_id));

--List all Baselines with plan_hash_value
SELECT
    sql_handle,
    plan_name,
    TRIM(substr(g.plan_table_output,instr(g.plan_table_output,':') + 1) ) plan_hash_value,
    LAST_EXECUTED,CREATED, enabled,accepted,fixed,
    sql_text
FROM
    ( SELECT
            t.*,
            c.sql_handle,
            c.plan_name,
            To_Char(LAST_EXECUTED,'DD-MON-YY HH24:MI:SS') LAST_EXECUTED,
            To_Char(CREATED,'DD-MON-YY HH24:MI:SS') CREATED, enabled,accepted,fixed,
            c.sql_text
        FROM
            dba_sql_plan_baselines c,
            TABLE ( dbms_xplan.display_sql_plan_baseline(c.sql_handle,c.plan_name) ) t
/*where c.sql_handle = '&sql_handle'*/
    ) g
WHERE    plan_table_output LIKE 'Plan hash value%';

-- find SQL_ID matched to SQL baseline
--SQL_3b4975a4007b2116
--SQL_1af9a485da4f947b
--SQL_9ea1d7f8ce29f8cc
--------------------------------------------------------------------------------------------------------------------------------
--------------------------List all Baselines SQL_IDs with plan_hash_value -----------------------------------------------------------------------------------

Declare
v_sqlid VARCHAR2(13);
v_num number;
BEGIN
dbms_output.put_line('SQL_ID������ '||' '|| 'PLAN_HASH_VALUE' || ' ' || 'SQL_HANDLE������������������� ' || ' ' || 'PLAN_NAME                       ' || ' ' || 'ENABLED' || ' ' || 'ACCEPTED' || ' ' || 'FIXED' || ' ' || 'CREATED           ' || ' ' || 'LAST_EXECUTED     ');
dbms_output.put_line('-------------'||' '|| '---------------' || ' ' || '------------------------------' || ' ' || '--------------------------------' || ' ' || '-------' || ' ' || '--------' || ' ' || '-----' || ' ' || '------------------' || ' ' || '------------------');
for a in 
(SELECT
    sql_handle,
    plan_name,
    TRIM(substr(g.plan_table_output,instr(g.plan_table_output,':') + 1) ) plan_hash_value,
    LAST_EXECUTED,CREATED, enabled,accepted,fixed,
    sql_text
FROM
    ( SELECT
            t.*,
            c.sql_handle,
            c.plan_name,
            To_Char(LAST_EXECUTED,'DD-MON-YY HH24:MI:SS') LAST_EXECUTED,
            To_Char(CREATED,'DD-MON-YY HH24:MI:SS') CREATED, enabled,accepted,fixed,
            c.sql_text
        FROM
            dba_sql_plan_baselines c,
            TABLE ( dbms_xplan.display_sql_plan_baseline(c.sql_handle,c.plan_name) ) t
/*where c.sql_handle = '&sql_handle'*/
    ) g
WHERE    plan_table_output LIKE 'Plan hash value%')
loop
v_num := to_number(sys.UTL_RAW.reverse(sys.UTL_RAW.SUBSTR(sys.dbms_crypto.hash(src => UTL_I18N.string_to_raw(a.sql_text || chr(0),'AL32UTF8'), typ => 2),9,4)) || sys.UTL_RAW.reverse(sys.UTL_RAW.SUBSTR(sys.dbms_crypto.hash(src => UTL_I18N.string_to_raw(a.sql_text || chr(0),'AL32UTF8'), typ => 2),13,4)),RPAD('x', 16, 'x'));
v_sqlid :='';
FOR i IN 0 .. FLOOR(LN(v_num) / LN(32))
LOOP
v_sqlid := SUBSTR('0123456789abcdfghjkmnpqrstuvwxyz',FLOOR(MOD(v_num / POWER(32, i), 32)) + 1,1) || v_sqlid;
END LOOP;
dbms_output.put_line(v_sqlid ||' ' || rpad(a.plan_hash_value,15) || ' ' || rpad(a.sql_handle,30) ||  ' ' || rpad(a.plan_name,30)||'   ' || rpad(a.ENABLED,8)||' ' || rpad(a.ACCEPTED,7)||' ' || rpad(a.FIXED,5)||' ' || rpad(a.CREATED,18)||'  ' || rpad(a.LAST_EXECUTED,18));
end loop;
end;
/
--Details:
select * from table(dbms_xplan.display_sql_plan_baseline(sql_handle=>'SQL_3b4975a4007b2116',  format=>'basic'));
select * from TABLE(dbms_xplan.display_awr('194dm4z2yp21c'));








--=================   MARC  NOTES    ================== take the plan from cursor enable and fix

--070001025378C638	2149090073	632985112
DECLARE
  n NUMBER;
BEGIN
  n:=DBMS_SPM.LOAD_PLANS_FROM_CURSOR_CACHE ( sql_id => '194dm4z2yp21c', plan_hash_value => '1826518928', FIXED => 'YES', ENABLED => 'YES');
  dbms_output.put_line(n);
END;
/ 

SELECT *
FROM dba_sql_plan_baselines
WHERE fixed='YES'; --SQL_cbf3827f28adc63a

 --DROP BASELINE
 SET SERVEROUTPUT ON
DECLARE
  l_plans_dropped  PLS_INTEGER;
BEGIN
  l_plans_dropped := DBMS_SPM.drop_sql_plan_baseline (
    sql_handle => NULL,
    plan_name  => 'SQL_PLAN_10n2p8b0zg66x');
    
  DBMS_OUTPUT.put_line(l_plans_dropped);
END;
/

==============
Baseline

-- Transfert du baseline vers la table de staging
DECLARE
  l_plans_packed  PLS_INTEGER;
BEGIN
  l_plans_packed := DBMS_SPM.pack_stgtab_baseline(
    table_name      => 'mb_spm_stageing_tab',
    table_owner     => 'APPS',
    sql_handle      => 'SQL_cfd4c8db9b5023cb');

END;
/

-- export datapump de la table de staging baseline
expdp system tables=APPS.mb_spm_stageing_tab directory=DATAPUMP_1 dumpfile=mb_spm.dmp logfile=EXP_mb_spm.log 

-- import datapump de la table de staging baseline vers bd cible
impdp system tables=APPS.mb_spm_stageing_tab directory=DATAPUMP_1 dumpfile=mb_spm.dmp logfile=IMP_mb_spm.log table_exists_action=append


-- valider que les donn�es ont bie �t� export�
select * 
from mb_spm_stageing_tab;


-- Unpack du baseline pour un sql_handle sp�cifique
DECLARE
  l_plans_unpacked  PLS_INTEGER;
BEGIN
  l_plans_unpacked := DBMS_SPM.unpack_stgtab_baseline(
    table_name      => 'mb_spm_stageing_tab',
    table_owner     => 'APPS',
    sql_handle      => 'SQL_cfd4c8db9b5023cb');

END;
/

--Meme principe pour les profiles.


--Profiles

EXEC DBMS_SQLTUNE.CREATE_STGTAB_SQLPROF (table_name=>'AGR_SQL_PROFILES',schema_name=>'APPS');  
EXEC DBMS_SQLTUNE.PACK_STGTAB_SQLPROF (staging_table_name => 'AGR_SQL_PROFILES',profile_name=>'SYS_SQLPROF_0161fc32c37c0000'); 

expdp system tables=APPS.AGR_SQL_PROFILES directory=DATAPUMP_1 dumpfile=AGR_SQL_PROFILES.dmp logfile=EXP_AGR_SQL_PROFILES.log
impdp system tables=APPS.AGR_SQL_PROFILES directory=DATAPUMP_1 dumpfile=AGR_SQL_PROFILES.dmp logfile=IMP_AGR_SQL_PROFILES.log table_exists_action=replace

SELECT *
FROM AGR_SQL_PROFILES;

EXEC DBMS_SQLTUNE.UNPACK_STGTAB_SQLPROF(REPLACE => TRUE,staging_table_name => 'AGR_SQL_PROFILES',profile_name=>'SYS_SQLPROF_016262ce116c0001');

SELECT * 
FROM dba_sql_profiles;

SHOW PARAMETER OPTIMIZER_CAPTURE_SQL_PLAN_BASELINES
/*
NAME                                 TYPE    VALUE 
------------------------------------ ------- ----- 
optimizer_capture_sql_plan_baselines boolean FALSE
*/

SHOW PARAMETER OPTIMIZER_USE_SQL_PLAN_BASELINES
/*
NAME                             TYPE    VALUE 
-------------------------------- ------- ----- 
optimizer_use_sql_plan_baselines boolean TRUE  
*/

select * from TABLE(dbms_xplan.display_cursor('gfzm6ghrr2d3k'));
--Plan hash value: 834975123
--Plan hash value: 1084500059

--2358777653
