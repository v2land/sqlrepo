
--=============================================================================
--1. Est-ce que l'usager a le ROLE CLONE ?
select * from apps.wf_local_user_roles where role_name in ('UMX|XXAGR_CLONE_DEV_ROLE','UMX|XXAGR_CLONE_FUNCTIONAL_ROLE') and user_name='MIROY' order by 1;
/*UMX|XXAGR_CLONE_DEV_ROLE
UMX|XXAGR_CLONE_FUNCTIONAL_ROLE*/
-- Oui UMX|XXAGR_CLONE_DEV_ROLE  et UMX|XXAGR_CLONE_FUNCTIONAL_ROLE donc on reactive ces Roles DEV et FUNC en dependance du type de l'environement 
--2. Qelles sont les Roles qu'il a?
select * from apps.wf_user_role_assignments where user_name  ='MIROY' and role_name in ('UMX|XXFND_DEVELOPER_2_ROLE','UMX|XXFND_DEVELOPER_3_ROLE','UMX|XXFND_DEVELOPER_5_ROLE','UMX|XXFND_FUNCT_ANALYST_1_ROLE','UMX|XXFND_FUNCT_ANALYST_2_ROLE','UMX|XXFND_GAC_ANALYST_1_ROLE','UMX|XXFND_GAC_ANALYST_2_ROLE')  order by 1; --'role_name like 'UMX%EBS_WF%' order by 1;
/*UMX|XXFND_DEVELOPER_2_ROLE
UMX|XXFND_DEVELOPER_3_ROLE
UMX|XXFND_DEVELOPER_5_ROLE
UMX|XXFND_GAC_ANALYST_1_ROLE
UMX|XXFND_GAC_ANALYST_2_ROLE*/
-- 3. Quelle Environement on Clone ? DEV6 donc juste les Roles de DEV 2,3,5 sont activ�s
-- 4. Quelles sont ces Descriptions des Roles?:
select * from apps.wf_local_roles  where name like 'UMX%' and name in ('UMX|XXFND_DEVELOPER_2_ROLE','UMX|XXFND_DEVELOPER_3_ROLE','UMX|XXFND_DEVELOPER_5_ROLE');
AGR Developer UAT Role
AGR Developer DEV Role
AGR Developer GELS DEV Role
--=======================================================================
--VC NOTE! reactivate Responsabilities for users that have following ROLE UMX|XXAGR_CLONE_DEV_ROLE or UMX|XXAGR_CLONE_FUNCTIONAL_ROLE (case X then retourne 2 valuses CLONEDEV,CLONE_FUNC else CLONE FUNC)
	 --fu.user_id IN -- list uf users that have the role UMX|XXAGR_CLONE_DEV_ROLE or UMX|XXAGR_CLONE_FUNCTIONAL_ROLE
    
     SELECT wur.user_name,
     --DISTINCT
     wur.role_name,
     DECODE(wur.user_orig_system, 'PER', fu2.user_id, wur.USER_ORIG_SYSTEM_ID)
      FROM wf_local_user_roles wur, fnd_user fu2
    WHERE 1=1 
    /*and  wur.role_name in (CASE WHEN 'EBSDEV1' in ('EBSDEV1','EBSDEV6','EBSDEVE','EBSDEV3') THEN
                'UMX|XXAGR_CLONE_DEV_ROLE,UMX'||','||'XXAGR_CLONE_DEV_ROLE' --168
            ELSE
                'UMX|XXAGR_CLONE_FUNCTIONAL_ROLE' --211
            END) 
	*/
      and wur.role_name in ('UMX|XXAGR_CLONE_DEV_ROLE','UMX|XXAGR_CLONE_FUNCTIONAL_ROLE') --379
      --and wur.role_name in ('UMX|XXAGR_CLONE_FUNCTIONAL_ROLE') --211
      --and wur.role_name in ('UMX|XXAGR_CLONE_DEV_ROLE') --211
      --and wur.role_name LIKE '%UAT%'
      AND fu2.employee_id(+) = DECODE(wur.user_orig_system, 'PER', wur.USER_ORIG_SYSTEM_ID)
      ;
 
 --==================
 --============
 ---- RESPONSABILITY NAMES:

SELECT fu.user_id                    ,
      fu.user_name                       ,
      frtl.responsibility_name             ,
      furgd.responsibility_id            ,
      furgd.responsibility_application_id,
      furgd.start_date                   ,
      furgd.description
  --    fr.start_date, fr.end_date
    FROM fnd_user_resp_groups_direct furgd,
      fnd_user fu                         ,
      fnd_responsibility_tl frtl,
      wf_local_user_roles wur,
      fnd_responsibility fr
    WHERE furgd.user_id               = fu.user_id
    AND furgd.end_date               IS NOT NULL
    AND NVL(fu.end_date, sysdate      + 1) > sysdate
    --VC NOTE! reactivate Responsabilities for users that don't have following ROLE UMX|XXAGR_CLONE_DEV_ROLE or UMX|XXAGR_CLONE_FUNCTIONAL_ROLE (case X then retourne 2 valuses CLONEDEV,CLONE_FUNC else CLONE FUNC)
	AND fu.user_id                   IN
      (SELECT DECODE(wur.user_orig_system, 'PER', fu2.user_id,
        wur.USER_ORIG_SYSTEM_ID)
      FROM wf_local_user_roles wur,
        fnd_user fu2
      /*WHERE (CASE WHEN 'EBSDEV5' in ('EBSDEV1','EBSDEV6','EBSDEVE','EBSDEV3') THEN
                'UMX|XXAGR_CLONE_DEV_ROLE'
            ELSE
                'UMX|XXAGR_CLONE_FUNCTIONAL_ROLE'
            END) = wur.role_name*/
		WHERE wur.role_name in ((CASE WHEN 'EBSDEV6' in ('EBSDEV1','EBSDEV6','EBSDEVE','EBSDEV3','UEBSDEV1','UEBSDEV2','UEBSDEV3','UEBSDEVE') THEN 
                        'UMX|XXAGR_CLONE_DEV_ROLE'
                            ELSE
                        'UMX|XXAGR_CLONE_FUNCTIONAL_ROLE'
                          END)
                          ,
                          (CASE WHEN 'EBSDEV6' in ('EBSDEV1','EBSDEV6','EBSDEVE','EBSDEV3','UEBSDEV1','UEBSDEV2','UEBSDEV3','UEBSDEVE') THEN 
                        'UMX|XXAGR_CLONE_FUNCTIONAL_ROLE'
                          END))
      AND fu2.employee_id(+) = DECODE(wur.user_orig_system, 'PER',
        wur.USER_ORIG_SYSTEM_ID)
      )
    AND frtl.responsibility_id = furgd.responsibility_id
    AND frtl.language          = 'US'
    AND wur.role_orig_system_id = fr.responsibility_id
    and wur.role_orig_system = 'FND_RESP'
    and wur.user_name = fu.user_name
    AND wur.assignment_type = 'D'
    and fr.responsibility_id  = frtl.responsibility_id 
    and NVL(fr.end_date,sysdate+1) > sysdate
    -- VC :Exception DEV9 and frtl.responsibility_name not in (SELECT lvv.meaning FROM fnd_lookup_values_vl lvv WHERE lvv.lookup_type   = 'XXAGR_ASSIGNMENTS_TO_RESTRICT')  
	/*and frtl.responsibility_name not in (CASE WHEN 'EBSDEV6' in ('EBSDEV9','UEBSDEV9') THEN
										'1' -- of DEV9 : then not executed
										ELSE
										(SELECT lvv.meaning FROM fnd_lookup_values_vl lvv WHERE lvv.lookup_type   = 'XXAGR_ASSIGNMENTS_TO_RESTRICT') 
										END) */
   -- and frtl.responsibility_name not in (SELECT lvv.meaning FROM fnd_lookup_values_vl lvv 
   --                                           where lvv.lookup_type in 
     --                                         (CASE WHEN 'EBSDEV6' in ('EBSDEV9','UEBSDEV9') THEN
       --                                             'DO_NOT_EXECUTE_IN_DEV9'
         --                                       ELSE
           --                                         'XXAGR_ASSIGNMENTS_TO_RESTRICT'
             --                                   END)
               --                               ) --sauf DEV9
   --and frtl.responsibility_name not in ('1')  --235
   ORDER BY 1;
   
   --== ROLES:  ===============================
   SELECT wlur.*
      FROM wf_local_user_roles wlur
           , wf_local_roles wlro
      WHERE wlur.role_orig_system = 'UMX'
      --VC NOTE! reactivate ROLES for users that have following ROLE --96 dont follow the rule -- to be reviewed
	  and wlur.user_name
      IN
      (SELECT wlur2.user_name
      FROM wf_local_user_roles wlur2
      /*WHERE (CASE WHEN p_database in ('EBSDEV1','EBSDEV6','EBSDEVE','EBSDEV3') THEN 
                'UMX|XXAGR_CLONE_DEV_ROLE'
            ELSE
                'UMX|XXAGR_CLONE_FUNCTIONAL_ROLE'
            END) = wlur2.role_name*/
		WHERE wlur2.role_name in ((CASE WHEN 'EBSDEV6' in ('EBSDEV1','EBSDEV6','EBSDEVE','EBSDEV3','UEBSDEV1','UEBSDEV2','UEBSDEV3','UEBSDEVE') THEN 
                        'UMX|XXAGR_CLONE_DEV_ROLE'
                            ELSE
                        'UMX|XXAGR_CLONE_FUNCTIONAL_ROLE'
                          END)
                          ,
                          (CASE WHEN 'EBSDEV6' in ('EBSDEV1','EBSDEV6','EBSDEVE','EBSDEV3','UEBSDEV1','UEBSDEV2','UEBSDEV3','UEBSDEVE') THEN 
                        'UMX|XXAGR_CLONE_FUNCTIONAL_ROLE'
                          END))
      AND NVL(wlur2.expiration_date, sysdate + 1 ) > SYSDATE
      ) 
      AND NVL(wlur.expiration_date, sysdate + 1 ) <= SYSDATE
      --VC: Exception DEV9  and wlro.display_name not in (SELECT lvv.meaning FROM fnd_lookup_values_vl lvv WHERE lvv.lookup_type   = 'XXAGR_ASSIGNMENTS_TO_RESTRICT')   
	 and wlro.display_name not in (SELECT lvv.meaning FROM fnd_lookup_values_vl lvv 
                                              where lvv.lookup_type in 
                                              (CASE WHEN 'EBSDEV6' in ('EBSDEV9','UEBSDEV9') THEN
                                                    'DO_NOT_EXECUTE_IN_DEV9'
                                                ELSE
                                                    'XXAGR_ASSIGNMENTS_TO_RESTRICT'
                                                END)
                                        ) --sauf DEV9 										
      AND wlro.name  = wlur.role_name
      order by 1
    ;
    
    --==================
    --======================
 
---------------------
--select ur.*, wr.*  --from   APPLSYS.wf_user_role_assignments ur, apps.WF_ROLES wr 
select distinct UR.USER_NAME, WR.DISPLAY_NAME, WR.description
--, WR.STATUS,ROLE_ORIG_SYSTEM
from   APPLSYS.wf_user_role_assignments ur, apps.WF_ROLES wr 
where  1=1
--AND ROLE_NAME = ASSIGNING_ROLE
AND ROLE_ORIG_SYSTEM in ( 'FND_RESP', 'UMX')
AND wr.name=ur.role_name
--AND ur.user_name like 'USER%'
;

------------------------------------

---------------------------------------------------------------
--R12 Role, Responsibilities and Assignments Queries
select pu.username, prdt.role_name, prd.role_common_name, pur.active_flag
from apps.per_users pu, apps.per_user_roles pur, apps.per_roles_dn prd, apps.per_roles_dn_tl prdt
where pu.user_id = pur.user_id
and pur.role_id = prd.role_id
and prd.role_id = prdt.role_id
and prdt.language = 'US'
and pur.role_guid = prd.role_guid
and pu.username =  'VLCASAPC'
group by pu.username, pur.active_flag, prd.role_common_name, prdt.role_name;

select * from apps.wf_local_roles  where name like 'UMX%' and name in ('UMX|XXEBS_AGR_CUSTOM_DEV_1_ROLE') order by 1;
select * from apps.wf_local_roles  where name like 'UMX%' and display_name in ('Agropur Custom Dev AGR CA Role','AGR GAC Analyst UAT Role','AGR GAC Analyst DEV Role','AGR Funct. Analyst DEV Role','AGR Funct. Analyst UAT Role','AGR Developer UAT Role','AGR Developer DEV Role','AGR Developer GELS DEV Role') order by 1;
select * from apps.wf_local_user_roles where role_name like'UMX|XXEBS_AGR_CUSTOM_DEV_1_ROLE'order by 1;

select * from apps.wf_local_user_roles where role_name in ('UMX|XXAGR_CLONE_DEV_ROLE','UMX|XXAGR_CLONE_FUNCTIONAL_ROLE') and user_name='MIROY' order by 1;
select * from apps.wf_local_user_roles where role_name in ('UMX|XXFND_DEVELOPER_3_ROLE') order by 1;--AGR Developer DEV Role = UMX|XXFND_DEVELOPER_3_ROLE


select * from apps.wf_user_role_assignments where user_name  ='DESIN' and role_name in ('UMX|XXEBS_AGR_CUSTOM_DEV_1_ROLE','UMX|XXFND_DEVELOPER_2_ROLE','UMX|XXFND_DEVELOPER_3_ROLE','UMX|XXFND_DEVELOPER_5_ROLE','UMX|XXFND_FUNCT_ANALYST_1_ROLE','UMX|XXFND_FUNCT_ANALYST_2_ROLE')  order by 1; --'role_name like 'UMX%EBS_WF%' order by 1;
select * from apps.wf_local_roles  where name like 'UMX%' and name in ('UMX|XXFND_DEVELOPER_2_ROLE','UMX|XXFND_DEVELOPER_3_ROLE');


--and display_name in ('AGR Funct. Analyst DEV Role','AGR Funct. Analyst UAT Role','AGR Developer UAT Role','AGR Developer DEV Role','AGR Developer GELS DEV Role') order by 1;
select * from apps.wf_user_role_assignments where assigning_role  like 'UMX%' order by 1;



--The following query I wrote in an effort to list all the responsibilities that are assigned to the Role I create under UMX. I could not find any simpler query that would define relationship directly
select distinct role_name, assigning_role from apps.wf_user_role_assignments where assigning_role  like 'UMX|XXAGR_CLONE%' order by 1;

--The following Query also works better
select WRH.SUPER_NAME, WRH.SUB_NAME,
--apps.FND_LOAD_UTIL.Owner_Name(WRH.LAST_UPDATED_BY) OWNER,
to_char(WRH.LAST_UPDATE_DATE, 'YYYY/MM/DD') LAST_UPDATE_DATE,
WRH.ENABLED_FLAG
from apps.WF_ROLE_HIERARCHIES WRH, apps.WF_ROLES WR
where WRH.SUB_NAME = WR.NAME
--and SUB_NAME like 'UMX%WF%'
and WR.ORIG_SYSTEM in ('FND_RESP', 'UMX')
connect by wrh.super_name = wrh.sub_name
--start with WRH.SUB_NAME = 'UMX|XXX AP INVOICE ENTRY';
start with WRH.SUB_NAME LIKE 'UMX|XXAGR%';


select * from  apps.FND_RESPONSIBILITY_VL where responsibility_key like '%WF%';
select *  from  apps.FND_USER_RESP_GROUPS;
select * from apps.WF_ROLES where name like '%' order by orig_system_id;
select * from apps.WF_ROLES where name like 'UMX%XXX_WF%' order by orig_system_id;
select * from  apps.FND_RESPONSIBILITY;

select * from apps.umx_role_assignments_v where role_name like 'UMX%';
select * from apps.umx_current_roles_v;
-------
--Important tables :
SELECT * FROM WF_ROLES;
SELECT * FROM WF_USER_ROLES;
SELECT * FROM WF_LOCAL_ROLES;
SELECT * FROM WF_USER_ROLE_ASSIGNMENTS;
