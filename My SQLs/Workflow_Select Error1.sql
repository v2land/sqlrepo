-------------------------------
Select Item_key from wf_items where STATUS ='ERROR';
--POAPPRV  Get the error messages of the list of all Errored Workflow Activities for a given item type/ item key
SELECT distinct i.item_type,i.item_key,user_key,
       i.begin_date,
       i.end_date,
       ias.execution_time,
       ac.display_name          activity,
       ias.activity_status  STATUS,
       ias.activity_result_code RESULT,
       ias.error_name           error_name,
       ias.error_message        error_message,
       ias.error_stack          error_stack
  FROM wf_item_activity_statuses ias,
       wf_process_activities     pa,
       wf_activities_vl          ac,
       wf_activities_vl          ap,
       wf_items                  i
 WHERE ias.item_type = 'POAPPRV'--'POAPPRV'
  --AND ias.item_key in ('1216705-468541','1192305-518536')
   AND ias.activity_status = 'ERROR'
   AND ias.process_activity = pa.instance_id
   AND pa.activity_name = ac.name
   AND pa.activity_item_type = ac.item_type
   AND pa.process_name = ap.name
   AND pa.process_item_type = ap.item_type
   AND pa.process_version = ap.version
   AND i.item_type = 'POAPPRV'-- 'APINVHDN'
   AND i.item_key = ias.item_key
   AND i.begin_date >= ac.begin_date
   AND i.begin_date < nvl(ac.end_date,
                          i.begin_date + 1)
  --and ias.error_message like '%1152950%' --From Worklist errors Local Error
  and  i.end_date is not NULL
-- ORDER BY ias.BEGIN_DATE
-- Item_Key 1216705-468541
 -- Notif ID: 1152950
;

/*--POAPPRV	1216705-468541	336831	ERROR	#MAIL	ZOJOVANO	1181587	26-SEP-17	26-SEP-17	15	WF_ERROR	"[WF_ERROR] ERROR_MESSAGE=3835: Error '100 - ORA-01403: no data found' encountered during execution of Generate function 'WF_XML.Generate' for event 'oracle.apps.wf.notification.send'. ERROR_STACK=
WF_XML.GetAttachments(1181587, https://ebsdev9.agropur.com:443/, 4966)
WF_XML.GenerateDoc(oracle.apps.wf.notification.send, 1181587)
WF_XML.Generate(oracle.apps.wf.notification.send, 1181587)
WF_XML.Generate(oracle.apps.wf.notification.send, 1181587)
Wf_Event.setMessage(oracle.apps.wf.notification.send, 1181587, WF_XML.Generate)
Wf_Event.dispatch_internal()"							1	POAPPRV	NOTIFY_THE_BUYER	6	POAPPRV	AME_PO_HAS_BEEN_APPROVED	336831	AME_PO_HAS_BEEN_APPROVED	ITEMATTR	1000	0			160,16	PREPARER_USER_NAME			SET1	INSERTED	AAAhEbAATAAARN9AAD	POAPPRV	AME_PO_HAS_BEEN_APPROVED	6	NOTICE	RESET	N	1000	0	03-SEP-17			*	0				MAIL.ICO	AME_PO_HAS_BEEN_APPROVED		N	WFERROR				AME PO Has Been Approved	AME PO Has Been Approved	AAAhEbAATAAARN6AAm	POAPPRV	NOTIFY_THE_BUYER	6	PROCESS	RESET	N	1000	0	03-SEP-17			*	0				PROCESS.ICO			N	WFERROR				Notify the Buyer		POAPPRV	1216705-468541	PO_AME_APPRV_TOP	6	ZOJOVANO				26-SEP-17	26-SEP-17	2500049503			1
*/
select *
from wf_notifications
where item_key = '1152950';
--To see error message for a workflow notification
select ERROR_MESSAGE from wf_item_activity_statuses_v WHERE NOTIFICATION_ID = 1152950;

--======================================
--17-10-16	192	Avis de blocage qt� rec de BC rapproch�	#EXCEPTION	WFNTF_ROLE	3205: 'JESASSEV' is not a valid role or user name. 	"
---Wf_Notification.Send(JESASSEV, APINVHDN, POMATCHED_QTYREC_HLD_NOTIF_MSG, 25-NOV-17, WF_ENGINE.CB)
--Wf_Engine_Util.Notification_Send(APINVHDN, 570010, 344030, APINVHDN:POMATCHED_QTYREC_HLD_NOTIF_MSG)
--Wf_Engine_Util.Notification(APINVHDN, 570010, 344030, RUN)"

--17-11-14	1463	Obtenir l'approbateur	#EXCEPTION	-20215	ORA-20215: An invalid approver has been encountered in ame_engine.fetchRuntimeGroup:XXAGR_AP_HOLDS_RESOLUTION_APPROVER_GROUP. Please remove the invalid approver. Invalid approver originating system person_id and originating system ID 1777.	"
--AME_APPROVER_TYPE_PKG.getWfRolesName(1094854)
--AME_ENGINE.fetchRuntimeGroup(1094855)
--AME_ENGINE.getRuntimeGroupMembers(1094856)
--AME_AG_CHAIN_HANDLER.handler(1094857)
--AME_ENGINE.processActionType(1094858)
--AME_ENGINE.processRules(1094859)
---AME_ENGINE.updateTransactionState(1094860)
--AME_ENGINE.getNextApprovers(1094861)
--AME_API2.getNextApprovers4(1094862)
--AME_API.getNextApprover(1094863)
--APINVHDN.GET_APPROVER(APINVHDN, 593147, 344012, RUN)
--Wf_Engine_Util.Function_Call(XXSQLAP0090_AP_WORKFLOW_PKG.get_approver, APINVHDN, 593147, 344012, RUN)"


select name  from wf_item_attribute_values;--

-- Details of Mailer Notification of one specific item_key and item_type
 select  wn.notification_id nid, 
        wn.context, 
        wn.group_id, 
        wn.status, 
        wn.mail_status, 
        wn.message_type, 
        wn.message_name, 
        wn.access_key, 
        wn.priority, 
        wn.begin_date, 
        wn.end_date, 
        wn.due_date, 
        wn.callback, 
        wn.recipient_role, 
        wn.responder, 
        wn.original_recipient, 
        wn.from_user, 
        wn.to_user, 
        wn.subject 
from    wf_notifications wn, wf_item_activity_statuses wias 
where  wn.group_id = wias.notification_id 
and  wias.item_type = 'POAPPRV'
--and  wias.item_key = '1216705-468541'
and wn.MAIL_STATUS not in ('SENT','MAIL')
and wn.STATUS='OPEN'
;

  select item_type, item_key, root_activity, owner_role, begin_date from wf_items where item_type='POAPPRV' and item_key in (
select item_key from wf_notifications
where message_name='AME_PO_HAS_BEEN_APPROVED' and status='OPEN')
order by begin_date;

--69
--Ex: POAPPRV	1201577-418874	PO_AME_APPRV_TOP	CAHAMEL	17-09-10
--======================================================================================
--Check for non closed sub WF where Parents Item_type is Closed WF
-- This script will find all worklist items that are open but are tied
-- to errant activities that are no longer in an ERROR state.  It will
-- retry them to close them out.
--then use: bde_wf_clean_worklist.sql script to close this child Items too and to become them purgeble

select distinct 
     itm.item_type item_type, 
     itm.item_key  item_key,
     WF_ENGINE.GetItemAttrText('WFERROR',itm.item_key,'ERROR_ITEM_TYPE') errant_item_type,
     WF_ENGINE.GetItemAttrText('WFERROR',itm.item_key,'ERROR_ITEM_KEY') errant_item_key
from 
     wf_items                  itm,
     wf_item_activity_statuses sta,
     wf_item_activity_statuses errsta,
     wf_process_activities     pra
where itm.item_type = 'WFERROR'
  and itm.end_date is null
  and to_number(WF_ENGINE.GetItemAttrText('WFERROR',itm.item_key,'ERROR_ACTIVITY_ID')) 
        = sta.process_activity
  and WF_ENGINE.GetItemAttrText('WFERROR',itm.item_key,'ERROR_ITEM_TYPE') 
        = sta.item_type(+)
  and WF_ENGINE.GetItemAttrText('WFERROR',itm.item_key,'ERROR_ITEM_KEY') 
        = sta.item_key(+)
  and WF_ENGINE.GetItemAttrText('WFERROR',itm.item_key,'ERROR_ITEM_TYPE') 
        like  '&itemtype'||'%'
  and sta.activity_status(+) <> 'ERROR'
  and errsta.item_type   = itm.item_type
  and errsta.item_key    = itm.item_key
  and errsta.end_date    is null
  and errsta.activity_status = 'NOTIFIED'
  and errsta.notification_id is not null
  and errsta.process_activity = pra.instance_id
UNION
select distinct 
     itm.item_type item_type, 
     itm.item_key  item_key,
     WF_ENGINE.GetItemAttrText('WFERROR',itm.item_key,'ERROR_ITEM_TYPE') errant_item_type,
     WF_ENGINE.GetItemAttrText('WFERROR',itm.item_key,'ERROR_ITEM_KEY') errant_item_key
from 
     wf_items                  itm,
     wf_item_activity_statuses errsta,
     wf_process_activities     pra
where itm.item_type = 'WFERROR'
  and itm.end_date is null
  and errsta.item_type   = itm.item_type
  and errsta.item_key    = itm.item_key
  and errsta.end_date    is null
  and errsta.notification_id is not null
  and errsta.process_activity = pra.instance_id
  and errsta.activity_status = 'NOTIFIED'
  and WF_ENGINE.GetItemAttrText('WFERROR',itm.item_key,'ERROR_ITEM_TYPE') 
        like  '&itemtype'||'%'
  and not exists 
  (select 1 from wf_item_activity_statuses sta 
   where to_number(WF_ENGINE.GetItemAttrText('WFERROR',itm.item_key,'ERROR_ACTIVITY_ID')) 
        = sta.process_activity
   and WF_ENGINE.GetItemAttrText('WFERROR',itm.item_key,'ERROR_ITEM_TYPE') 
        = sta.item_type(+)
   and WF_ENGINE.GetItemAttrText('WFERROR',itm.item_key,'ERROR_ITEM_KEY') 
        = sta.item_key(+)
  );
  
 