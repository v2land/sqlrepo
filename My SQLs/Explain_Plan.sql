--- Explain Plan:

EXPLAIN PLAN 
  SET statement_id = 'ex_plan_VC1' FOR
SELECT /*+ ORDERED INDEX(IL) USE_NL(TL,T) 
           DYNAMIC_SAMPLING(IL,5) DYNAMIC_SAMPLING(TL,5) */
INTERFACE_LINE_ID,
'Bla',
'trx_number='||T.TRX_NUMBER||','||'customer_trx_id='||TL.CUSTOMER_TRX_ID,
IL.ORG_ID
FROM RA_INTERFACE_LINES_GT IL, RA_CUSTOMER_TRX_LINES TL, RA_CUSTOMER_TRX T
WHERE 
--IL.REQUEST_ID = :request_id
--AND
IL.INTERFACE_LINE_CONTEXT = 'CLAIM'
AND    T.CUSTOMER_TRX_ID =TL.CUSTOMER_TRX_ID
AND  IL.INTERFACE_LINE_CONTEXT = TL.INTERFACE_LINE_CONTEXT 
AND IL.INTERFACE_LINE_ATTRIBUTE1 = TL.INTERFACE_LINE_ATTRIBUTE1
AND IL.INTERFACE_LINE_ATTRIBUTE2 = TL.INTERFACE_LINE_ATTRIBUTE2
AND IL.INTERFACE_LINE_ATTRIBUTE3 = TL.INTERFACE_LINE_ATTRIBUTE3
AND IL.INTERFACE_LINE_ATTRIBUTE4 = TL.INTERFACE_LINE_ATTRIBUTE4
AND IL.INTERFACE_LINE_ATTRIBUTE5 = TL.INTERFACE_LINE_ATTRIBUTE5
AND IL.INTERFACE_LINE_ATTRIBUTE6 = TL.INTERFACE_LINE_ATTRIBUTE6
AND IL.INTERFACE_LINE_ATTRIBUTE7 = TL.INTERFACE_LINE_ATTRIBUTE7;

SELECT PLAN_TABLE_OUTPUT 
  FROM TABLE(DBMS_XPLAN.DISPLAY(NULL, 'ex_plan_VC1','BASIC'));


SELECT *
  FROM TABLE(DBMS_XPLAN.DISPLAY_CURSOR());

--Documentation states that with NULL SQL_ID as parameter, it will run the explain plan on the most recent query ran. I was hoping that this would take care of earlier issues. However ... I got the plan for the exact same insert into the SYS.AUD$ table.

--You might say, ok, then just simply put a comment in your query that allows to easily capture the SQL_ID, like in following:
--SELECT /* SQL: 1234-18' */ * FROM DUAL;
--Then I can try to find the SQL_ID as follows:
SELECT * FROM V$SQLAREA WHERE sql_text like '%SELECT * FROM (SELECT DISTINCT e.org_id%';
--2rnkjadhjv01r

select plan_table_output
from table(dbms_xplan.display_cursor('2rnkjadhjv01r',null,'basic'));