select *
 from V$RMAN_BACKUP_JOB_DETAILS
 order by session_key desc;
 
 select to_char(START_TIME,'mm/dd/yy hh24:mi') start_time,
 backup_type, INCREMENTAL_LEVEL from v$backup_set where start_time>sysdate -7
order by start_time;
 
select 
*
        from v$rman_status rs
        where end_time > sysdate - &NUMBER_OF_DAYS
        and (status like ('%WARNING%') OR status  like ('%ERROR%') OR status  like ('%FAILED%') OR status  like ('%RUNNING%') OR status  like ('%COMPLETE%'))
        order by recid desc;
--0	5102	942703202			5102	942703202	0	SESSION	2017-04-30T22:00:01	RMAN	COMPLETED WITH ERRORS	8850924	30-APR-17	01-MAY-17	9280864387072	8864703447040	NO			NO	0
--14607	942703202
--Backup jobs� status and metadata=================================== 
--CF: Number of controlfile backups included in the backup set
--DF: Number of datafile full backups included in the backup set
--I0: Number of datafile incremental level-0 backups included in the backup set
--I1: Number of datafile incremental level-1 backups included in the backup set
--L: Number of archived log backups included in the backup set
--OUT INST: Instance where the job was executed and the output is available (see below)
select
  j.session_recid, j.session_stamp,
  to_char(j.start_time, 'yyyy-mm-dd hh24:mi:ss') start_time,
  to_char(j.end_time, 'yyyy-mm-dd hh24:mi:ss') end_time,
  (j.output_bytes/1024/1024) output_mbytes, j.status, j.input_type,
  decode(to_char(j.start_time, 'd'), 1, 'Sunday', 2, 'Monday',
                                     3, 'Tuesday', 4, 'Wednesday',
                                     5, 'Thursday', 6, 'Friday',
                                     7, 'Saturday') dow,
  round(j.elapsed_seconds,0) Elapsed_Sec, j.time_taken_display,
  x.cf, x.df, x.i0, x.i1, x.l,
  ro.inst_id output_instance
from V$RMAN_BACKUP_JOB_DETAILS j
  left outer join (select
                     d.session_recid, d.session_stamp,
                     sum(case when d.controlfile_included = 'YES' then d.pieces else 0 end) CF,
                     sum(case when d.controlfile_included = 'NO'
                               and d.backup_type||d.incremental_level = 'D' then d.pieces else 0 end) DF,
                     sum(case when d.backup_type||d.incremental_level = 'D0' then d.pieces else 0 end) I0,
                     sum(case when d.backup_type||d.incremental_level = 'I1' then d.pieces else 0 end) I1,
                     sum(case when d.backup_type = 'L' then d.pieces else 0 end) L
                   from
                     V$BACKUP_SET_DETAILS d
                     join V$BACKUP_SET s on s.set_stamp = d.set_stamp and s.set_count = d.set_count
                   where s.input_file_scan_only = 'NO'
                   group by d.session_recid, d.session_stamp) x
    on x.session_recid = j.session_recid and x.session_stamp = j.session_stamp
  left outer join (select o.session_recid, o.session_stamp, min(inst_id) inst_id
                   from GV$RMAN_OUTPUT o
                   group by o.session_recid, o.session_stamp)
    ro on ro.session_recid = j.session_recid and ro.session_stamp = j.session_stamp
where j.start_time > trunc(sysdate)-&NUMBER_OF_DAYS
and j.SESSION_STAMP='&SessionStamp'
order by j.start_time desc; 

--5102	942703202	2017-04-30 22:00:05	2017-05-01 09:23:24	4227208	COMPLETED	DB INCR	Sunday	40999	11:23:19	3	2	0	155	47	1
--14607	942703202
--Backup set details   --15749	945342470
select
  d.bs_key, d.backup_type, d.controlfile_included, d.incremental_level, d.pieces,
  to_char(d.start_time, 'yyyy-mm-dd hh24:mi:ss') start_time,
  to_char(d.completion_time, 'yyyy-mm-dd hh24:mi:ss') completion_time,
  d.elapsed_seconds, d.device_type, d.compressed, (d.output_bytes/1024/1024) output_mbytes, s.input_file_scan_only
from V$BACKUP_SET_DETAILS d
  join V$BACKUP_SET s on s.set_stamp = d.set_stamp and s.set_count = d.set_count
where session_recid = &SESSION_RECID
  and session_stamp = &SESSION_STAMP
order by d.start_time;

--Backup job output
--15749	945342470
select output
from GV$RMAN_OUTPUT
where session_recid = &SESSION_RECID
  and session_stamp = &SESSION_STAMP
order by recid;
