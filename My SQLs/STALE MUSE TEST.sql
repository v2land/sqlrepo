select * from OZF_OBJECT_FUND_SUMMARY_TEST_H;
select count(*) from OZF_OBJECT_FUND_SUMMARY_TEST_H;


create table OZF_OBJECT_FUND_SUMMARY_TEST as (select * from OZF_OBJECT_FUND_SUMMARY);
create synonym OZF_OBJECT_FUND_SUMMARY_TEST for OZF.OZF_OBJECT_FUND_SUMMARY_TEST_H;

create table OZF.muse (id number, muse_date date, name varchar2(10));
create synonym muse for OZF.muse;

--Table created.
  
declare
  v_count  number;
  begin
  v_count:=0;
  for i in 1..1830 loop
     for j in 1..1000 loop
     v_count:= v_count+1;
     insert into muse values (v_count, sysdate-i, 'MUSE');
     end loop;
  end loop;
  commit;
  end;
 /
  
PL/SQL procedure successfully completed.
  
SQL> create index muse_i on muse(muse_date);
  
Index created.
  
SQL> exec dbms_stats.gather_table_stats(ownname=>'BOWIE', tabname=>'MUSE', casca
de=>true, estimate_percent=>null, method_opt=>'FOR ALL COLUMNS SIZE 1');
  
PL/SQL procedure successfully completed.



--#############-------TEST SYMULATION------------       
create table OZF_OBJECT_FUND_SUMMARY_TEST as (select * from OZF_OBJECT_FUND_SUMMARY);
create synonym OZF_OBJECT_FUND_SUMMARY_TEST for OZF.OZF_OBJECT_FUND_SUMMARY_TEST_H;
select count(*) from OZF_OBJECT_FUND_SUMMARY_TEST;

--1
--drop table OZF_OBJECT_FUND_SUMMARY_TEST_H;
create table OZF.OZF_OBJECT_FUND_SUMMARY_TEST_H as select * from OZF_OBJECT_FUND_SUMMARY_TEST where rownum<1;
create synonym OZF_OBJECT_FUND_SUMMARY_TEST_H for OZF.OZF_OBJECT_FUND_SUMMARY_TEST_H;
alter table OZF.OZF_OBJECT_FUND_SUMMARY_TEST nologging;
alter table OZF.OZF_OBJECT_FUND_SUMMARY_TEST_H nologging;
select count(*) from OZF_OBJECT_FUND_SUMMARY_TEST_H;
--2
select
    insert /*+ append parallel (xyzhold,12) */ 
        into OZF_OBJECT_FUND_SUMMARY_TEST_H -- xyzhold (f1, f2, f3,f4,f5,f6,f7,f8,f9,f10,f11,f12,f13,f14,f15,f16,f17,f18,f19,f20,f21,f22,f23,f24,f25,f26,f27,f28,f29,f30,f31,f32,f33,f34,f35,f36,f37,f37,f38,f39,f40,f41,f42,f43,f44,f45,f46,f47,f48,f49) 
            select /*+ parallel (x,12) */ * from OZF_OBJECT_FUND_SUMMARY_TEST;
from OZF_OBJECT_FUND_SUMMARY_TEST;
--commit;

select * from OZF_OBJECT_FUND_SUMMARY_TEST_H;
select count(*) from OZF_OBJECT_FUND_SUMMARY_TEST_H;
--8678

--Check Statistics and -- Get Last DML on table --
Exec DBMS_STATS.FLUSH_DATABASE_MONITORING_INFO;

select m.TABLE_OWNER,
 m.TABLE_NAME,
 m.INSERTS,
 m.UPDATES,
 m.DELETES,
 m.TRUNCATED,
 to_char(m.TIMESTAMP,'yyyy/mm/dd hh24:mi:ss') as LAST_MODIFIED, 
 round((m.inserts+m.updates+m.deletes)*100/NULLIF(t.num_rows,0),2) as EST_PCT_MODIFIED,
 t.num_rows as last_known_rows_number,
 to_char(t.last_analyzed,'yyyy/mm/dd hh24:mi:ss') as last_analyszed
From dba_tab_modifications m,
 dba_tables t
where m.table_owner=t.owner
and m.table_name=t.table_name
and table_owner in ('OZF')
and m.TABLE_NAME ='OZF_OBJECT_FUND_SUMMARY_TEST_H'
--and table_owner not in ('SYS','SYSTEM')
--and ((m.inserts+m.updates+m.deletes)*100/NULLIF(t.num_rows,0) > 10 or t.last_analyzed is null)
--order by timestamp desc;
order by 10 desc;

--Check dba_tab_statistics for STALE stats
select table_name, stale_stats, last_analyzed 
from dba_tab_statistics 
where stale_stats='YES'
and table_name='OZF_OBJECT_FUND_SUMMARY_TEST_H';
--
select * from DBA_TAB_COL_STATISTICS where owner ='OZF' and table_name='OZF_OBJECT_FUND_SUMMARY_TEST_H';

exec dbms_stats.gather_schema_stats('OZF', options=>'LIST STALE');

--gather statistics 
EXEC DBMS_STATS.gather_table_stats('OZF', 'OZF_OBJECT_FUND_SUMMARY_TEST_H');
--quand tu rouleras les statistiques pour OZF, fait le de la facons suivante :
EXEC FND_STATS.GATHER_SCHEMA_STATS('OZF',DBMS_STATS.AUTO_SAMPLE_SIZE); 

-- Check last_analyszed
select
owner
,table_name
,to_char(last_analyzed,'yyyy/mm/dd hh24:mi:ss') as last_analyszed
From dba_Tables
where table_name = 'OZF_OBJECT_FUND_SUMMARY_TEST_H'
;
--OZF	OZF_OBJECT_FUND_SUMMARY_TEST_H	2019/01/07 13:50:41

-- symulate UPDATEs--
select ATTRIBUTE2 from OZF_OBJECT_FUND_SUMMARY_TEST_H;
select count(*) from OZF_OBJECT_FUND_SUMMARY_TEST_H;

update OZF_OBJECT_FUND_SUMMARY_TEST_H set ATTRIBUTE2 = '-updated' where rownum <=867; --ATTRIBUTE2 || 
--rollback;
--commit;

UPDATE OZF_OBJECT_FUND_SUMMARY_TEST_H t
    SET ATTRIBUTE2 = (SELECT ATTRIBUTE2
                FROM OZF_OBJECT_FUND_SUMMARY_TEST_H t2
                WHERE t.OBJFUNDSUM_ID = t2.OBJFUNDSUM_ID
               ) 
    WHERE  rownum <=1000;
commit;


-- search select in Active users 
   SELECT   b.status, substr(a.spid,1,9) pid,    substr(b.sid,1,5) sid,    substr(b.serial#,1,5) ser#,    substr(b.machine,1,6) box,    substr(b.username,1,10) username, b.server, b.program,
    substr(b.osuser,1,8) os_user,    substr(b.program,1,30) program,    s.sql_id,    b.SQL_EXEC_START,    b.MACHINE, b.CLIENT_IDENTIFIER,   SQL_FULLTEXT
    FROM    v$session b,    v$process a,    V$SQLAREA s 
    WHERE 
    b.paddr = a.addr    AND b.sql_hash_value = s.hash_value    AND    type = 'USER'   -- AND   b.username in ('APPS')
    -- and OSUSER LIKE '%wls%'
  -- and b.program like '%adfuser%'
  and SQL_TEXT LIKE '%OZF_OBJECT_FUND_SUMMARY_TEST_H%'
--  and s.sql_id = '0fjtyz6kf7p6y'
    ORDER BY    spid;