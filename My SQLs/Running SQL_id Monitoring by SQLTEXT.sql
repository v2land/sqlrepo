select to_char(to_date(v.FIRST_LOAD_TIME,'YYYY-MM-DD HH24:MI:SS'), 'YYYY-MM-DD HH24:MI:SS') stamp,
--           v.PARSING_SCHEMA_NAME,
           u.USERNAME,
--           v.PARSING_SCHEMA_ID,
           v.sql_id,
           v.MODULE,
--           v.ADDRESS,
--           v.CHILD_ADDRESS,
           v.ROWS_PROCESSED,
           v.SQL_TEXT,
           v.SQL_FULLTEXT,
--           v.USER_IO_WAIT_TIME,
           v.DISK_READS,
           v.ELAPSED_TIME,
           v.service
--           v.USERS_EXECUTING,
--           v.OBJECT_STATUS,
--           v.REMOTE
from       dba_users u,v$sql v
--where to_date(v.FIRST_LOAD_TIME,'YYYY-MM-DD hh24:mi:ss')>ADD_MONTHS(trunc(sysdate,'MM'),-1)
--where to_date(v.FIRST_LOAD_TIME,'YYYY-MM-DD hh24:mi:ss')>sysdate-1
where to_date(v.FIRST_LOAD_TIME,'YYYY-MM-DD hh24:mi:ss')
      between to_date('2018-03-21 08:00','YYYY-MM-DD HH24:MI')
          and to_date('2018-03-21 17:00','YYYY-MM-DD HH24:MI')
and SQL_FULLTEXT LIKE '%xxinv0560_item_cat_desc_v%'
--and  v.PARSING_SCHEMA_NAME = 'AGR_XXINSPYRUS'
--and u.USERNAME =  'AGR_XXINSPYRUS'
and  u.user_id = v.PARSING_USER_ID
--and  v.module <> 'SQL Developer' and u.username <> 'SYS'
order by 1 desc;