select * from v$database@ETL_EBS_PROFITABILITY;

select last_login from dba_users where username like '%AGRTEMP%';

--check last password change date
Select name, ptime from sys.USER$ where TYPE# = 1 order by ptime, name ; -- TYPE# 1 is USER, TYPE 0 is a ROLE
--Select name, ptime from sys.USER$ where TYPE# = 1 ;--and name = 'SYSMAN' order by ptime, name ; -- all USERS
Select name, ptime, ASTATUS from sys.USER$ where TYPE# = 1 and ASTATUS > 4 order by ptime, name ; -- Those are all Locked and not used
Select name, ptime, ASTATUS from sys.USER$ where TYPE# = 1 and ASTATUS =0 order by ptime, name ; -- Only Actifs, check PTIME last changed Password.

                        --Account TYPE# 1 - USERS; 0 - ROLES
                        
                        --Account Status 
                        /*0 OPEN
                        1 EXPIRED
                        2 EXPIRED(GRACE)
                        4 LOCKED(TIMED)
                        5 EXPIRED & LOCKED(TIMED)
                        6 EXPIRED(GRACE) & LOCKED(TIMED)
                        8 LOCKED
                        9 EXPIRED & LOCKED
                        10 EXPIRED(GRACE) & LOCKED
                        �
                        The astatus column gets is value derived as
                        25 = 16 + 8 + 1
                        16 - user is having default password (this bit from user$.astatus is what gets checked for DBA_USERS_WITH_DEFPWD view)
                        8 - account is locked
                        1 - password is expired
                        */

Select name, ptime,astatus from sys.USER$ where name in (select username from dba_users_with_defpwd) and TYPE# = 1 and ASTATUS =16 order by ptime, name ; -- check users with default password
SELECT name, ptime FROM sys.USER$ WHERE TYPE#=1 AND name IN( select username from dba_users where username in (select username from dba_users_with_defpwd) and ACCOUNT_STATUS != 'EXPIRED &' || ' LOCKED');
--1. operating system users
/*passwd ebsacora*/

--2. database users, such as SYS,SYSTEM
SELECT name, ptime FROM sys.USER$ WHERE TYPE#=1 AND name IN('SYS', 'SYSTEM'); --2
/*
alter user sys identified by pass1234;
alter user system identified by pass1234;
*/

--You can identify Oracle E-Business Suite managed schemas by querying the table FND_ORACLE_USERID. 
--The managed schemas are all listed in FND_ORACLE_USERID. The READ_ONLY_FLAG identifies the category.
    --C - Category 5 - APPLSYSPUB
    --U - Category 5 - APPS
    --E - Category 5 - APPS's friends APPLSYS + APPS_NE
    --U - Category 5 - APPS variations for the obsolete Multiple Reporting Currencies feature
    --Z - Category 5 - Schema for Editioned objects to make NE
    --A - Category 6 - Oracle E-Business Suite Base Product schemas
    --X - Category 4 - Non-Oracle E-Business Suite schemas where Oracle E-Business Suite patching needs access

--3. and the application of ERP system linkage DB users, such as APPS, APPLSYS, AP, INV, GL etc. --FND_ORACLE_USERID Table

  --To backup before modification: 
  create table apps.fnd_user_20180717 as select * from apps.fnd_user;
  create table apps.fnd_oracle_userid_20180717 as select * from apps.fnd_oracle_userid; 

    /*
      3.1 APPS and APPLSYS , APPS_NE, and APPLSYSPUB(optional) users (count:4)
        
        AFPASSWD -s APPLSYS   -- this will change APPS APPLSYS and APPS_NE
        $ADMIN_SCRIPTS_HOME/adadminsrvctl.sh start
          --Update Apps password in WLS
            perl $FND_TOP/patch/115/bin/txkManageDBConnectionPool.pl -options=updateDSPassword -contextfile=$CONTEXT_FILE
          --test:
            $ADMIN_SCRIPTS_HOME/admanagedsrvctl.sh start oacore_server1
            $ADMIN_SCRIPTS_HOME/admanagedsrvctl.sh stop oacore_server1
            $ADMIN_SCRIPTS_HOME/adadminsrvctl.sh stop
            $ADMIN_SCRIPTS_HOME/adnodemgrctl.sh stop

        check APPS APPLSYS and APPS_NE LAST_UPDATE_DATE */
        select * from APPLSYS.FND_ORACLE_USERID where READ_ONLY_FLAG in ('U','E','Z');   /* 
                
        AFPASSWD -f APPLSYSPUB -- this will change APPLSYSPUB*/
        select * from APPLSYS.FND_ORACLE_USERID where READ_ONLY_FLAG in ('C');   /*
         
        Before running AutoConfig, update the�s_gwyuid_pass�variable in the AutoConfig context file:		
      	$ vi $CONTEXT_FILE
        grep s_gwyuid_pass $CONTEXT_FILE
        Run Autoconfig
        
       
      3.2 EBS Module users
        AFPASSWD -a -- change ALLORACLE
        check ALLORACLE LAST_UPDATE_DATE */
        select * from APPLSYS.FND_ORACLE_USERID where READ_ONLY_FLAG = 'A' order by oracle_username;  --166
        /*
          AGR_APPS: Agropur - APPS
          AGR_GELS: Agropur - GELS
          AGR_RPAS: Agropur - RPAS
          XXLAAPPS: Agropur - CCG-PCG
        
      3.3 this is the non EBS based user management module  READ_ONLY_FLAG = 'X'      */
        select * from APPLSYS.FND_ORACLE_USERID where READ_ONLY_FLAG = 'X';  --ODM: 1
 --So Far:          
-- All DB Users
select count (*) from dba_users; --293 
select count(*) from sys.USER$ where TYPE#=1;--293
select count(*) from dba_users where  ACCOUNT_STATUS = 'EXPIRED &' || ' LOCKED'; --22 locked
select count(*) from FND_ORACLE_USERID order by READ_ONLY_FLAG, ORACLE_USERNAME;--176    176-5-1-166-4 =0

 

  select * from dba_users where username in (select oracle_username from APPLSYS.FND_ORACLE_USERID where READ_ONLY_FLAG = 'B') and ACCOUNT_STATUS != 'EXPIRED &' || ' LOCKED'; --5 Agropur
/*
AXF
AGRPKG
AGR_GELS_PKG
AGRCONV
APPSDEV  -- Locked
*/

select name, ptime, astatus from sys.USER$ where TYPE#=1 and name in (select oracle_username from APPLSYS.FND_ORACLE_USERID where READ_ONLY_FLAG = 'B');-- and ASTATUS =0;--count:5  (status 8= LOCKED)
--4 a CHANGER encore en PROD
/*
AGRPKG
APPSDEV
AGRCONV
AGR_GELS_PKG
AXF
*/
--== LIST only READONLY accounts
select
'alter user '|| name || ' identified by '|| '"NEW_PASSWORD";' as "ALTER",ptime, ASTATUS
  from sys.USER$ where TYPE#=1 and name in (select oracle_username from APPLSYS.FND_ORACLE_USERID where READ_ONLY_FLAG = 'B');--and ASTATUS =0;



--=======================================================================
-- Jusqu'ici toutes les passwords presents dans FND_ORACLE_USERID table sont chang�.
--=============================================== Non Managed: ==========
--AGR custom Schemas
--293-2-176-22=92
select * from dba_users where  --293
username not in (select oracle_username from APPLSYS.FND_ORACLE_USERID ) --176
and ACCOUNT_STATUS != 'EXPIRED &' || ' LOCKED' --22
and username not in ('SYS','SYSTEM')
;--93

select name, ptime, astatus from sys.USER$ where TYPE#=1 and name not in (select oracle_username from APPLSYS.FND_ORACLE_USERID) --176
and ASTATUS <3  -- NOT EXPIRED
and name not in ('SYS','SYSTEM')
and ptime < '30-APR-19' --SYSDATE - 100  ; 02-SEP-18 -> LAST MEP
order by 3 desc, 2;

/*
-- CHANGE PASSWORD DBSNMP:
    NOTE : La commande emcli update_db_password" va mettre � jour le mot de passe dans OEM et aussi dans la base de donn�es (R�f�rence : Doc ID 1592390.1)	
    ssh oemprora@dcsspgestaix	
    sudo su - oemprora	
    Prendre l'option 3. du menu de login (OMS)	
     3) Oracle Management Services 12c - OMS	
	emcli login -username=sysman -password="<sysman_password>"	
	# IMPORTANT : Si <old_password> ou <new_password> contiennent le caract�re $ (signe de dollard) alors mettre le caract�re \ (backslash) avant (ex: ceci\$estlepwd au lieu de ceci$estlepwd)	
	
  emcli update_db_password -target_name="<SID>" -target_type="oracle_database" -user_name="dbsnmp" -change_at_target=yes  old_password="<old_password>" -new_password="<new_password>" -retype_new_password="<new_password>"	
  emcli update_db_password -target_name="EBSACCE" -target_type="oracle_database" -user_name="dbsnmp" -change_at_target=yes  old_password="kov27EBSACCE" -new_password="YNf@yDaFN63d" -retype_new_password="Ynf@yDaFN63d"	
  
  S'assurer que la job indiqu�� dans OEM (CHANGE_PWD_JOB*) termine avec un Status "Succeeded".	
	4de5- Test de bon fonctionnement apr�s le changement de password	
*/

select * from dba_users where username like 'DBSNMP';

--=========  AGR =========
select * from dba_users where 
username like '%AGR%' 
; --22


/*
AGR_OBI_READ
AGR_PROXY_DCPSPEKHOSQL
AGR_INTERFACE_WOOD
AGR_INTERFACE_PLES
AGR_UPK_PROD
AGR_INTERFACE_BONC
AGRTEMP
AGR_AGR
AGROPUR_LINK
AGR_PROXY_DCSSPEKHOSQL
AGR_COMPILE
AGR_APPS_WCI
AGR_INTERFACE_LUX
AGR_INTERFACE_DIAM
AGR_INTERFACE_BCVL
AGR_INTERFACE_GRBY
AGR_UPK_APP_PROD
AGR_OBI_READ_DBLINK
AGR_INTERFACE_DCPSP
AGR_INTERFACE_HYAC
AGROPUR
AGR_INTERFACE_VELO
*/

select name, ptime, astatus from sys.USER$ where TYPE#=1 and name not in (select oracle_username from APPLSYS.FND_ORACLE_USERID) --176
and ASTATUS <3  -- NOT EXPIRED
and name not in ('SYS','SYSTEM','DBSNMP')
and name not like ('%AGR%')
and ptime < '30-APR-19' --SYSDATE - 100  ; 02-SEP-18 -> LAST MEP
;
--PI_USER	03-DEC-14  - TO BE DISABLED 

--AGR custom Schemas
--293-176-22-22=73
select * from dba_users where  --293
username not in (select oracle_username from APPLSYS.FND_ORACLE_USERID) --176
and ACCOUNT_STATUS != 'EXPIRED &' || ' LOCKED' --22
and username not in ('SYS','SYSTEM','DBSNMP')
and username not like '%AGR%'; --70

select name, ptime, astatus from sys.USER$ where TYPE#=1 and name not in (select oracle_username from APPLSYS.FND_ORACLE_USERID) --176
and ASTATUS <3  -- NOT EXPIRED
and name not in ('SYS','SYSTEM','DBSNMP','PI_USER')
and name not like ('%AGR%')
and ptime < '30-APR-19' --SYSDATE - 100  ; 02-SEP-18 -> LAST MEP
--and name in (select username from dba_users_with_defpwd)
;

SELECT *
FROM sys.USER$
WHERE TYPE#=1
AND name  IN
  (SELECT username
  FROM dba_users where username NOT IN
    (SELECT oracle_username FROM APPLSYS.FND_ORACLE_USERID
    ) 
  AND ACCOUNT_STATUS != 'EXPIRED &'
    || ' LOCKED'
  AND username NOT IN ('SYS','SYSTEM','DBSNMP')
  AND username NOT LIKE '%AGR%'
  );
  
--70

-- CHECK if still default password 
 select username,account_status from dba_users where username in (select username from dba_users_with_defpwd) and ACCOUNT_STATUS != 'EXPIRED &' || ' LOCKED';
select distinct ACCOUNT_STATUS from dba_users;

--Check LOCKED
select username,account_status from dba_users where ACCOUNT_STATUS = 'LOCKED';

-- select all but: not in fnd_oracle_userid mot expired not systmems, not AGR
select
'alter user '|| username || ' identified by '|| 'AFPWD_EBSTEST_893567;'
  from dba_users where username in 
  (SELECT name
FROM sys.USER$
WHERE TYPE#=1
AND name  IN
  (SELECT username
  FROM dba_users where username not IN
    (select name from sys.USER$ where TYPE#=1 and name not in (select oracle_username from APPLSYS.FND_ORACLE_USERID) --176
and ASTATUS <3  -- NOT EXPIRED
and name not in ('SYS','SYSTEM','DBSNMP','PI_USER')
and name not like ('%AGR%')
and ptime < '30-APR-19'
)));

select name, ptime, astatus from sys.USER$ where TYPE#=1 and name not in (select oracle_username from APPLSYS.FND_ORACLE_USERID) --176
and ASTATUS <3  -- NOT EXPIRED
and name not in ('SYS','SYSTEM','DBSNMP','PI_USER')
and name not like ('%AGR%')
and ptime < '30-APR-19' --SYSDATE - 100  ; 02-SEP-18 -> LAST MEP
--and name in (select username from dba_users_with_defpwd)
;  
  
  /*
  Passwords and Referenced Files List
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 
Username    | Description             | Default Password | Created or Referenced         |Comments
            |                         |                  | in $ORACLE_HOME/ files        |
------------+-------------------------+------------------+-------------------------------+--------
            |                         |                  |                               |
CTXSYS      | Intermedia Text schema  | CTXSYS           |./ctx/admin/dr0csys.sql        |change
            |                         |                  |./bin/ctxsrv -user ctxsys/*    |                                                      
------------+-------------------------+------------------+-------------------------------+--------                   
DBSNMP      | Intelligent Agent user  | DBSNMP           |./rdbms/admin/catsnmp.sql      |change
            |                         |                  |./bin/dbsnmp                   |
            |                         |                  |./network/admin/snmp_rw.ora    |
------------+-------------------------+------------------+-------------------------------+--------
MGMT_VIEW   | DB Control Repository   | autogenerated    |./sysman/admin/emdrep/bin/RepManager|
------------+-------------------------+------------------+-------------------------------+--------
SYSMAN      | DB Control Repository   | asked            |                               |See Note 259379.1
------------+-------------------------+------------------+-------------------------------+--------
MDSYS       | Spatial Data Option user| MDSYS            |./md/admin/mdinst.sql          |change
            |                         |                  |./ord/admin/ordisysc.sql       |
MDDATA      | Spatial Data Option user| MDDATA           |./md/admin/catmd.sql           |lock
------------+-------------------------+------------------+-------------------------------+--------
WKSYS       | Ultra Search option user| WKSYS  (admin)   |./ultrasearch/admin/wk0csys.sql|change
WKUSER (9i) | Ultra Search option user| WKUSER (end-user)|./ultrasearch/admin/wk0csys.sql|
WK_TEST (10g)|                        | WK_TEST (same)   |./ultrasearch/admin/wk0csys.sql|
------------+-------------------------+------------------+-------------------------------+--------
ODM         | Oracle Data Mining      | ODM              |./dm/admin/dmcrt.sql           |change
ODM_MTR     | Oracle Data Mining      | MTRPW            |./dm/admin/dmcrt.sql           |change
DMSYS (10g) | Oracle Data Mining      | DMSYS            |./dm/admin/odmcrtm.sql         |change
any name    | Oracle Data Mining      | any pasword      |./dm/admin/odmcrt.sql          |
------------+-------------------------+------------------+-------------------------------+--------
ORDPLUGINS  | InterMedia Audio option | ORDPLUGINS       |./ord/admin/ordisysc.sql       |change
ORDSYS      | InterMedia Audio option | ORDSYS           |./ord/admin/ordisysc.sql       |change
SI_INFORMTN_SCHEMA |InterMedia option | SI_INFORMTN_SCHEMA | ./ord/admin/ordisysc.sql    |lock  
------------+-------------------------+------------------+-------------------------------+--------
OUTLN       | Stored Outlines         | OUTLN            |./rdbms/admin/c0703040.sql     |lock
            |                         |                  |./rdbms/admin/c0800050.sql     |
            |                         |                  |./rdbms/admin/sql.bsq          |
------------+-------------------------+------------------+-------------------------------+--------
PERFSTAT    | STATSPACK Repository    | PERFSTAT         |./rdbms/admin/spcreate.sql     |change
            |                         |                  |(./rdbms/admin/spcusr.sql)     |
------------+-------------------------+------------------+-------------------------------+--------
RMAN        | RMAN catalog Owner      | RMAN             | manually                              |change
------------+-------------------------+------------------+-------------------------------+--------
SCOTT       | Demo user               | TIGER            |./rdbms/admin/utlsampl.sql     |*drop 
------------+-------------------------+------------------+-------------------------------+--------
WKPROXY     | Ultraseach user         | change_on_install|./ultrasearch/admin/wk0csys.sql|change
WKSYS       | Ultraseach user         | change_on_install|./ultrasearch/admin/wk0install.sql| ""
------------+-------------------------+------------------+-------------------------------+--------
WMSYS       | Oracle Workspace Manager| wmsys            |./rdbms/admin/owmctab.plb      |lock
------------+-------------------------+------------------+-------------------------------+--------
XDB         | SQL XML management      | change_on_install|./rdbms/admin/catqm.sql        |lock
ANONYMOUS   | SQL XML management      | values anonymous |./rdbms/admin/catqm.sql        |lock
------------+-------------------------+------------------+-------------------------------+--------
TRACESVR    | Oracle Trace user       | trace            |./rdbms/admin/otrcsvr.sql      |change
------------+-------------------------+------------------+-------------------------------+--------
OAS_PUBLIC  | Web Toolkit/Content     | manager          | See Note:99088.1              |change
WEBSYS      | Web Toolkit/Content     | manager          | See Note:99088.1              |change
------------+-------------------------+------------------+-------------------------------+--------
REPADMIN    | Replication user        | managed by DBA   |created manually by CREATE USER|
            |                         |                  |./ldap/admin/oidrsrms.sql      |change
            |                         |                  |./ldap/admin/oidrsms.sql       |
------------+-------------------------+------------------+-------------------------------+--------
AURORA$ORB$UNAUTHENTICATED| OSE       | random value     |./javavm/install/jisorb.sql    |lock
AURORA$JIS$UTILITY$       | OSE       | random value     |./javavm/install/jisbgn.sql    |lock
OSE$HTTP$ADMIN            | OSE       | random value     |./javavm/install/jishausr.sql  |lock
------------+-------------------------+------------------+-------------------------------+--------
LBACSYS     | Label Security          | LBACSYS          |./rdbms/admin/catlbacs.sql     |change                                                       
------------+-------------------------+------------------+-------------------------------+--------
DVSYS       | Database Vault          | DVSYS            |./rdbms/admin/catmacs.sql      |change
------------+-------------------------+------------------+-------------------------------+--------
DVF         | Database Vault          | DVF              |./rdbms/admin/catmacs.sql      |change
------------+-------------------------+------------------+-------------------------------+--------
SYS         |  Administrative         | change_on_install|./rdbms/admin/sql.bsq          |change
SYSTEM      |  Administrative         | manager          |./rdbms/admin/sql.bsq          |change
------------+-------------------------+------------------+-------------------------------+---------
EXFSYS      | Expression Filter Feature repository |asked|./rdbms/admin/exfsys.sql       |locked
------------+-------------------------+------------------+-------------------------------+--------
DIP         | Provision event processing| DIP            |./rdbms/admin/catdip.sql       |locked
------------+-------------------------+------------------+-------------------------------+--------
TSMSYS      | Transparent Session Migration| TSMSYS      |./rdbms/admin/cattsm.sql       |locked
------------+-------------------------+------------------+-------------------------------+--------
*/

select
'alter user '|| username || ' identified by '|| 'AFPWDa_EBSACCE_4679912;'

  from dba_users where username in 
  (SELECT name
FROM sys.USER$
WHERE TYPE#=1
AND name  IN
  (SELECT username
  FROM dba_users where username NOT IN
    (SELECT oracle_username FROM APPLSYS.FND_ORACLE_USERID) 
  AND ACCOUNT_STATUS != 'EXPIRED &'
    || ' LOCKED'
  AND username NOT IN ('SYS','SYSTEM','DBSNMP')
  AND username NOT LIKE '%AGR%'
  AND username NOT in ('PI_USER') -- Exclusion , to not change
 -- AND username NOT in ('OUTLN','') --Predefined Administrative Accounts --Oracle Created Database Users: Password, Usage and Files References (Doc ID 160861.1)
  )) order by username;
  
 Select name, ptime,ASTATUS from sys.USER$ 
where TYPE# = 1
and ASTATUS <3 -- Account Status OPEN, and EXPIRED
--and name in (select USERNAME from dba_users where username like '%AGR%')
and ptime < '30-APR-19' --SYSDATE - 100
order by ptime, name ;

 
select username,account_status from dba_users where username in (select username from dba_users_with_defpwd) and ACCOUNT_STATUS != 'EXPIRED &' || ' LOCKED';
alter user ODM identified by "AFPWD_EBSTEST_893567";

