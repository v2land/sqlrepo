/*[?2018-?02-?26 09:54]  Janine Racicot:  
S.V.P. faire un backup de la table  agr_apps.XXONT_STD_PRICE_CALC_2 avec le nom XXONT_STD_PRICE_CALC_RPPY  dans DEV8
 */

create table 
agr_apps.XXONT_STD_PRICE_CALC_RPPY
nologging parallel 15  as select /*+parallel(source 15) */ * 
from 
   agr_apps.XXONT_STD_PRICE_CALC_2;

--Select * from agr_apps.XXONT_STD_PRICE_CALC_RPPY;
--drop table agr_apps.XXONT_STD_PRICE_CALC_RPPY;
--select * from agr_apps.XXONT_STD_PRICE_CALC_2;
select count(*) from agr_apps.XXONT_STD_PRICE_CALC_RPPY;

