 --CHECK PURGEBLE Items by Item Type
 --Verify Closed and Purgeable Items.       

SELECT COUNT(*) RECORDS,
  WF_PURGE.GETPURGEABLECOUNT(A.ITEM_TYPE) PURGEABLE,
  A.ITEM_TYPE, B.PERSISTENCE_TYPE PERSISTENCE,
  B.DISPLAY_NAME
FROM  WF_ITEMS A, WF_ITEM_TYPES_VL B
WHERE A.ITEM_TYPE = B.NAME
  AND A.ITEM_TYPE like nvl('&item_type_selected','%')
GROUP BY A.ITEM_TYPE, B.PERSISTENCE_TYPE, B.DISPLAY_NAME
order by 4, 1 desc;

--This query will help you to determine volume of wf process classified by Status, item Type, and permanency.
select wi.item_type ITEM_TYPE,  wit.persistence_type P_TYPE,  decode (wi.end_date, NULL, 'OPEN', 'CLOSED') Status,  count(*) COUNT
from wf_items wi, wf_item_types wit
where wit.name = wi.item_type
group by item_type, wit.persistence_type,  WIT.PERSISTENCE_DAYS,  decode (wi.end_date, NULL, 'OPEN', 'CLOSED') 
order by decode (wi.end_date, NULL, 'OPEN', 'CLOSED'), 4 desc
; 

--This query displays the parameter arguments in order for a Concurrent Program:

SELECT p.USER_CONCURRENT_PROGRAM_NAME "NAME", 
c.CONCURRENT_PROGRAM_NAME "INTERNAL",
f.END_USER_COLUMN_NAME "PARAMETER",
f.ENABLED_FLAG "ON_OFF",
f.DEFAULT_VALUE,
f.REQUIRED_FLAG, f.DESCRIPTION
FROM FND_DESCR_FLEX_COL_USAGE_VL f, FND_CONCURRENT_PROGRAMS_TL p, fnd_concurrent_programs c
WHERE substr(f.DESCRIPTIVE_FLEXFIELD_NAME,7,8)=c.CONCURRENT_PROGRAM_NAME
and c.CONCURRENT_PROGRAM_ID = p.CONCURRENT_PROGRAM_ID
--and p.USER_CONCURRENT_PROGRAM_NAME LIKE '%Workflow%' 
and (f.DESCRIPTIVE_FLEXFIELD_NAME like '$SRS$.FNDWFPR%') 
AND p.LANGUAGE = 'US'
order by f.DESCRIPTIVE_FLEXFIELD_NAME, f.COLUMN_SEQ_NUM;

--Records in these tables are purged only if the records in the workflow master table (WF_ITEMS) qualifies to be purged.

--These queries give the volume of data in the tables.
select count(*) from WF_ITEM_ATTRIBUTE_VALUES;
select count(*) from WF_ITEM_ACTIVITY_STATUSES;
select count(*) from WF_NOTIFICATION_ATTRIBUTES;
/*
The above Workflow tables contain data which is used for Workflow background processing.
If these tables grow too large the Workflow performance level becomes slower. 
You should be purging these tables should be purged on a regular basis.
*/


--The SQL statements below will help analyze their contents
--The main tables of concern when dealing with workflows are WF_ITEM_ACTIVITY_STATUS and WF_ITEM_ATTRIBUTE_VALUES..

select item_type,activity_status,count(*)
from wf_item_activity_statuses
group by item_type,activity_status
order by Item_type;

--An understanding of WFERROR will help define what needs to happen next:

--ACTIVE    Nobody has reviewed these yet  COMPLETE    Someone has responded to the error notification with abort/retry/ignore. These are complete and should be purged.

--ERROR    The error process is in error. This usually happens on new sites where the configuration is incomplete. For example, an error process notification was sent to non-existent person. Reviewing the error stack should tell you what the problem is.

--NOTIFIED    A workflow exception has been raised and the wferror process started. There will be an unactioned notification somewhere on the system.

--Go to $APPL_TOP/fnd and find  wfstat.sql


/*****************************Additional Investigation ******************/

--Check PARENTS for non closed sub WF Childs  where Parents Item_type is Closed WF. Enter the parent Item_type to check for nonclosed childs.
-- This script will find all worklist items that are open but are tied
-- to errant activities that are no longer in an ERROR state.  It will
-- retry them to close them out.
--then use: bde_wf_clean_worklist.sql script to close this child Items too and to become them purgeble
select distinct 
     itm.item_type item_type, 
     itm.item_key  item_key,
     WF_ENGINE.GetItemAttrText('WFERROR',itm.item_key,'ERROR_ITEM_TYPE') errant_item_type,
     WF_ENGINE.GetItemAttrText('WFERROR',itm.item_key,'ERROR_ITEM_KEY') errant_item_key
from 
     wf_items                  itm,
     wf_item_activity_statuses sta,
     wf_item_activity_statuses errsta,
     wf_process_activities     pra
where itm.item_type = 'WFERROR'
  and itm.end_date is null
  and to_number(WF_ENGINE.GetItemAttrText('WFERROR',itm.item_key,'ERROR_ACTIVITY_ID')) 
        = sta.process_activity
  and WF_ENGINE.GetItemAttrText('WFERROR',itm.item_key,'ERROR_ITEM_TYPE') 
        = sta.item_type(+)
  and WF_ENGINE.GetItemAttrText('WFERROR',itm.item_key,'ERROR_ITEM_KEY') 
        = sta.item_key(+)
  and WF_ENGINE.GetItemAttrText('WFERROR',itm.item_key,'ERROR_ITEM_TYPE') 
        like  '&itemtype'||'%'
  and sta.activity_status(+) <> 'ERROR'
  and errsta.item_type   = itm.item_type
  and errsta.item_key    = itm.item_key
  and errsta.end_date    is null
  and errsta.activity_status = 'NOTIFIED'
  and errsta.notification_id is not null
  and errsta.process_activity = pra.instance_id
UNION
select distinct 
     itm.item_type item_type, 
     itm.item_key  item_key,
     WF_ENGINE.GetItemAttrText('WFERROR',itm.item_key,'ERROR_ITEM_TYPE') errant_item_type,
     WF_ENGINE.GetItemAttrText('WFERROR',itm.item_key,'ERROR_ITEM_KEY') errant_item_key
from 
     wf_items                  itm,
     wf_item_activity_statuses errsta,
     wf_process_activities     pra
where itm.item_type = 'WFERROR'
  and itm.end_date is null
  and errsta.item_type   = itm.item_type
  and errsta.item_key    = itm.item_key
  and errsta.end_date    is null
  and errsta.notification_id is not null
  and errsta.process_activity = pra.instance_id
  and errsta.activity_status = 'NOTIFIED'
  and WF_ENGINE.GetItemAttrText('WFERROR',itm.item_key,'ERROR_ITEM_TYPE') 
        like  '&itemtype'||'%'
  and not exists 
  (select 1 from wf_item_activity_statuses sta 
   where to_number(WF_ENGINE.GetItemAttrText('WFERROR',itm.item_key,'ERROR_ACTIVITY_ID')) 
        = sta.process_activity
   and WF_ENGINE.GetItemAttrText('WFERROR',itm.item_key,'ERROR_ITEM_TYPE') 
        = sta.item_type(+)
   and WF_ENGINE.GetItemAttrText('WFERROR',itm.item_key,'ERROR_ITEM_KEY') 
        = sta.item_key(+)
  );

/*
=
=
-----bde_wf_data.sql - Query Workflow Runtime Data That Is Eligible For Purging
=
=
=
=*/
select wit.NAME                    ITEM_TYPE, 
       wit.persistence_type        P_TYPE,  
       WIT.PERSISTENCE_DAYS        P_DAYS,
       wtl.DISPLAY_NAME            DISPLAY_NAME
     from wf_item_types    wit,
          wf_item_types_tl wtl
     where wit.name like nvl('&item_type_selected','%')
       and wtl.name = wit.name
     order by 1;

/* BREAK ON REPORT; */
--COMPUTE SUM OF COUNT ON REPORT;


--prompt Closed Workflow Items

select wi.item_type                  ITEM_TYPE, 
       wit.persistence_type          P_TYPE,  
       WIT.PERSISTENCE_DAYS          P_DAYS,
       count(*)                      COUNT, 
       avg(end_date - begin_date)    AVG_LIFE,
       STDDEV(end_date - begin_date) DEV,
       MIN(end_date - begin_date)    MIN_LIFE,
       MAX(end_date - begin_date)    MAX_LIFE,
       ('exec WF_PURGE.ITEMS('''||WI.ITEM_TYPE||''',NULL,SYSDATE,FALSE);') STATEMENT
     from wf_items wi,
          wf_item_types wit
     where wi.end_date is not null
       and wit.name = wi.item_type
       and wi.item_type like nvl('&item_type_selected','%')
     group by item_type, 
              wit.persistence_type, 
              WIT.PERSISTENCE_DAYS
     order by 2,4,1;

--column STATEMENT format a75;

--prompt Open and Closed Workflow Items

select wi.item_type                               ITEM_TYPE, 
       wit.persistence_type                       P_TYPE,  
       WIT.PERSISTENCE_DAYS                       P_DAYS,
       count(*)                                   COUNT,
       avg(nvl(end_date,sysdate) - begin_date)    AVG_LIFE,
       STDDEV(nvl(end_date,sysdate) - begin_date) DEV,
       MIN(nvl(end_date,sysdate) - begin_date)    MIN_LIFE,
       MAX(nvl(end_date,sysdate) - begin_date)    MAX_LIFE
     from wf_items wi,
          wf_item_types wit,
          wf_item_types_tl wtl
     where wit.name = wi.item_type
       and wi.item_type like nvl('&item_type_selected','%')
       and wtl.name = wit.name
     group by wi.item_type, 
              wit.persistence_type, 
              WIT.PERSISTENCE_DAYS,
              wtl.DISPLAY_NAME            
     order by 2,4,1;

--prompt Verify Closed and Purgeable Items.       

SELECT COUNT(*) RECORDS,
  WF_PURGE.GETPURGEABLECOUNT(A.ITEM_TYPE) PURGEABLE,
  A.ITEM_TYPE, B.PERSISTENCE_TYPE PERSISTENCE,
  B.DISPLAY_NAME
FROM  WF_ITEMS A, WF_ITEM_TYPES_VL B
WHERE A.ITEM_TYPE = B.NAME
  AND A.ITEM_TYPE like nvl('&item_type_selected','%')
GROUP BY A.ITEM_TYPE, B.PERSISTENCE_TYPE, B.DISPLAY_NAME
order by 4, 1 desc;

--prompt Closed Activity Statuses 

select sta.item_type              ITEM_TYPE, 
       count(*)                   COUNT,
       ('exec WF_PURGE.ITEMS('''||STA.ITEM_TYPE||''',NULL,SYSDATE,FALSE);') STATEMENT
from wf_item_activity_statuses sta,
     wf_items wfi
where sta.item_type = wfi.item_type
  and sta.item_key = wfi.item_key
  and wfi.end_date is not null
  and wfi.item_type like nvl('&item_type_selected','%')
group by sta.item_type
order by 2,1;

--prompt Open and Closed Activity Statuses

select sta.item_type          ITEM_TYPE, 
       count(*)               COUNT
from wf_item_activity_statuses sta,
     wf_items wfi
where sta.item_type = wfi.item_type
  and sta.item_key  = wfi.item_key
  and wfi.item_type like nvl('&item_type_selected','%')
group by sta.item_type
order by 2,1;

--prompt Large Activity Status Item Keys
--column STATEMENT format a51;
select sta.item_type, 
       sta.item_key, 
       count(*),
       wfi.begin_date,
       wfi.end_date, 
       decode(wfi.end_date, NULL, 'Run $FND_TOP/sql/WFSTAT.SQL to pursue closing item',
          ('exec WF_PURGE.ITEMS('''||STA.ITEM_TYPE||''','''||STA.ITEM_KEY||''',SYSDATE,FALSE);')
             ) STATEMENT
from wf_item_activity_statuses sta,
     wf_items wfi
where sta.item_type = wfi.item_type
  and sta.item_key  = wfi.item_key
  and wfi.item_type like nvl('&item_type_selected','%')
group by sta.item_type, 
         sta.item_key, 
         wfi.begin_date,
         wfi.end_date
having count(*) > 100
order by 2,1,3;


--prompt Closed Activity History Statuses 
--column STATEMENT format a75;
select sta.item_type         ITEM_TYPE, 
       count(*)              COUNT,
       ('exec WF_PURGE.ITEMS('''||STA.ITEM_TYPE||''',NULL,SYSDATE,FALSE);') STATEMENT
from wf_item_activity_statuses_h sta,
     wf_items wfi
where sta.item_type = wfi.item_type
  and sta.item_key  = wfi.item_key
  and wfi.item_type like nvl('&item_type_selected','%')
  and wfi.end_date  is not null
group by sta.item_type
order by 2,1;

--prompt Large activity History Status Item Keys
--column STATEMENT format a51;
select sta.item_type       ITEM_TYPE, 
       sta.item_key        ITEM_KEY, 
       count(*)            COUNT,
       wfi.begin_date      BEGIN_DATE,
       wfi.end_date        END_DATE, 
       decode(wfi.end_date, NULL, 'Run $FND_TOP/sql/WFSTAT.SQL to pursue closing item',
          ('exec WF_PURGE.ITEMS('''||STA.ITEM_TYPE||''','''||STA.ITEM_KEY||''',SYSDATE,FALSE);')
             )             STATEMENT
from wf_item_activity_statuses_h sta,
     wf_items wfi
where sta.item_type = wfi.item_type
  and sta.item_key  = wfi.item_key
  and wfi.item_type like nvl('&item_type_selected','%')
group by sta.item_type, 
      sta.item_key,
      wfi.USER_KEY,
      wfi.begin_date, 
      wfi.end_date
having count(*) > 300
order by 3 desc;

--prompt Notification Totals 
--column STATEMENT format a75;
    select WN.MESSAGE_TYPE     ITEM_TYPE, 
           count(*)            COUNT
     from WF_NOTIFICATIONS WN
     where WN.MESSAGE_TYPE like nvl('&item_type_selected','%')
    group by WN.MESSAGE_TYPE
    order by 2;

--prompt Unreferenced Notifications - Purge using WF_PURGE.NOTIFICATIONS

    select WN.MESSAGE_TYPE     ITEM_TYPE, 
           count(*)            COUNT,
           'N'                 HISTORY,
          ('exec WF_PURGE.NOTIFICATIONS('''||WN.MESSAGE_TYPE||''',SYSDATE,FALSE);') STATEMENT
     from WF_NOTIFICATIONS WN
     where WN.MESSAGE_TYPE like nvl('&item_type_selected','%')
     and not exists
       (select NULL
       from WF_ITEM_ACTIVITY_STATUSES WIAS
       where WIAS.NOTIFICATION_ID = WN.GROUP_ID)
     group by WN.MESSAGE_TYPE
    union all
    select WN.MESSAGE_TYPE     ITEM_TYPE, 
           count(*)            COUNT,
           'Y'                 HISTORY,
          ('exec WF_PURGE.NOTIFICATIONS('''||WN.MESSAGE_TYPE||''',SYSDATE,FALSE);') STATEMENT
     from WF_NOTIFICATIONS WN
     where WN.MESSAGE_TYPE like nvl('&item_type_selected','%')
     and not exists
       (select NULL
       from WF_ITEM_ACTIVITY_STATUSES_H WIAS
       where WIAS.NOTIFICATION_ID = WN.GROUP_ID)
     and exists
     (select null
     from WF_ITEM_TYPES WIT
     where WN.END_DATE+nvl(WIT.PERSISTENCE_DAYS,0)<=sysdate
     and WN.MESSAGE_TYPE = WIT.NAME
     and WIT.PERSISTENCE_TYPE = 'TEMP')
    group by WN.MESSAGE_TYPE
    order by 2;


--PROMPT If you run any Purge API that deletes 10% of the data or more Gather Stats using:
--PROMPT
--PROMPT ITEMS
--PROMPT =====
--PROMPT
SELECT 'EXEC DBMS_STATS.DELETE_TABLE_STATS(ownname=>''APPLSYS'',tabname=>''WF_ITEM_ACTIVITY_STATUSES'');' GATHER_STATEMENT from dual
UNION ALL                                                                                                                      
SELECT 'EXEC FND_STATS.GATHER_TABLE_STATS(ownname=>'||'''APPLSYS'''||
         ',tabname=>'||'''WF_ITEM_ACTIVITY_STATUSES'''||
         ',percent=>10'||
         ',granularity=>'||DECODE((SELECT PARTITIONED FROM ALL_TABLES AT 
                                 WHERE  AT.OWNER      = 'APPLSYS' 
                                 AND    AT.TABLE_NAME = 'WF_ITEM_ACTIVITY_STATUSES'),
                                'YES','''PARTITION''','''DEFAULT''')||');' GATHER_STATEMENT
FROM DUAL
UNION ALL
SELECT 'EXEC DBMS_STATS.DELETE_TABLE_STATS(ownname=>''APPLSYS'',tabname=>''WF_ITEM_ACTIVITY_STATUSES_H'');' GATHER_STATEMENT from dual
UNION ALL                                                                                                                      
SELECT 'EXEC FND_STATS.GATHER_TABLE_STATS(ownname=>'||'''APPLSYS'''||
         ',tabname=>'||'''WF_ITEM_ACTIVITY_STATUSES_H'''||
         ',percent=>10'||
         ',granularity=>'||DECODE((SELECT PARTITIONED FROM ALL_TABLES AT 
                                 WHERE  AT.OWNER      = 'APPLSYS' 
                                 AND    AT.TABLE_NAME = 'WF_ITEM_ACTIVITY_STATUSES_H'),
                                'YES','''PARTITION''','''DEFAULT''')||');' GATHER_STATEMENT
FROM DUAL
UNION ALL
SELECT 'EXEC DBMS_STATS.DELETE_TABLE_STATS(ownname=>''APPLSYS'',tabname=>''WF_ITEM_ATTRIBUTE_VALUES'');' GATHER_STATEMENT from dual
UNION ALL                                                                                                                      
SELECT 'EXEC FND_STATS.GATHER_TABLE_STATS(ownname=>'||'''APPLSYS'''||
         ',tabname=>'||'''WF_ITEM_ATTRIBUTE_VALUES'''||
         ',percent=>10'||
         ',granularity=>'||DECODE((SELECT PARTITIONED FROM ALL_TABLES AT 
                                 WHERE  AT.OWNER      = 'APPLSYS' 
                                 AND    AT.TABLE_NAME = 'WF_ITEM_ATTRIBUTE_VALUES'),
                                'YES','''PARTITION''','''DEFAULT''')||');' GATHER_STATEMENT
FROM DUAL
UNION ALL
SELECT 'EXEC DBMS_STATS.DELETE_TABLE_STATS(ownname=>''APPLSYS'',tabname=>''WF_ITEMS'');' GATHER_STATEMENT from dual
UNION ALL                                                                                                                      
SELECT 'EXEC FND_STATS.GATHER_TABLE_STATS(ownname=>'||'''APPLSYS'''||
         ',tabname=>'||'''WF_ITEMS'''||
         ',percent=>10'||
         ',granularity=>'||DECODE((SELECT PARTITIONED FROM ALL_TABLES AT 
                                 WHERE  AT.OWNER      = 'APPLSYS' 
                                 AND    AT.TABLE_NAME = 'WF_ITEMS'),
                                'YES','''PARTITION''','''DEFAULT''')||');' GATHER_STATEMENT
FROM DUAL
UNION ALL
SELECT 'EXEC DBMS_STATS.DELETE_TABLE_STATS(ownname=>''APPLSYS'',tabname=>''WF_NOTIFICATIONS'');' GATHER_STATEMENT from dual
UNION ALL
SELECT 'EXEC FND_STATS.GATHER_TABLE_STATS(ownname=>'||'''APPLSYS'''||
         ',tabname=>'||'''WF_NOTIFICATIONS'''||
         ',percent=>10'||
         ',granularity=>'||DECODE((SELECT PARTITIONED FROM ALL_TABLES AT 
                                 WHERE  AT.OWNER      = 'APPLSYS' 
                                 AND    AT.TABLE_NAME = 'WF_NOTIFICATIONS'),
                                'YES','''PARTITION''','''DEFAULT''')||');' GATHER_STATEMENT
FROM DUAL
UNION ALL
SELECT 'EXEC DBMS_STATS.DELETE_TABLE_STATS(ownname=>''APPLSYS'',tabname=>''WF_NOTIFICATION_ATTRIBUTES'');' GATHER_STATEMENT from dual
UNION ALL
SELECT 'EXEC FND_STATS.GATHER_TABLE_STATS(ownname=>'||'''APPLSYS'''||
         ',tabname=>'||'''WF_NOTIFICATION_ATTRIBUTES'''||
         ',percent=>10'||
         ',granularity=>'||DECODE((SELECT PARTITIONED FROM ALL_TABLES AT 
                                 WHERE  AT.OWNER      = 'APPLSYS' 
                                 AND    AT.TABLE_NAME = 'WF_NOTIFICATION_ATTRIBUTES'),
                                'YES','''PARTITION''','''DEFAULT''')||');' GATHER_STATEMENT
FROM DUAL;

--PROMPT NOTIFICATIONS 
--PROMPT ===============
SELECT 'EXEC DBMS_STATS.DELETE_TABLE_STATS(ownname=>''APPLSYS'',tabname=>''WF_NOTIFICATIONS'');' GATHER_STATEMENT from dual
UNION ALL
SELECT 'EXEC FND_STATS.GATHER_TABLE_STATS(ownname=>'||'''APPLSYS'''||
         ',tabname=>'||'''WF_NOTIFICATIONS'''||
         ',percent=>10'||
         ',granularity=>'||DECODE((SELECT PARTITIONED FROM ALL_TABLES AT 
                                 WHERE  AT.OWNER      = 'APPLSYS' 
                                 AND    AT.TABLE_NAME = 'WF_NOTIFICATIONS'),
                                'YES','''PARTITION''','''DEFAULT''')||');' GATHER_STATEMENT
FROM DUAL
UNION ALL
SELECT 'EXEC DBMS_STATS.DELETE_TABLE_STATS(ownname=>''APPLSYS'',tabname=>''WF_NOTIFICATION_ATTRIBUTES'');' GATHER_STATEMENT from dual
UNION ALL
SELECT 'EXEC FND_STATS.GATHER_TABLE_STATS(ownname=>'||'''APPLSYS'''||
         ',tabname=>'||'''WF_NOTIFICATION_ATTRIBUTES'''||
         ',percent=>10'||
         ',granularity=>'||DECODE((SELECT PARTITIONED FROM ALL_TABLES AT 
                                 WHERE  AT.OWNER      = 'APPLSYS' 
                                 AND    AT.TABLE_NAME = 'WF_NOTIFICATION_ATTRIBUTES'),
                                'YES','''PARTITION''','''DEFAULT''')||');' GATHER_STATEMENT
FROM DUAL;

--spool off;

--CLEAR BREAKS;
--CLEAR COMPUTES;
--CLEAR COLUMNS;

-------------------------------------------------------------------------------------------
