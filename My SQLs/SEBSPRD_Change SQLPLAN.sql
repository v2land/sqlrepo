--XXR_GL_SLA_RPT: RONA - GL Analysis Retail SQL_ID: fj2vuxgbxgx2b
--XXR_GL_SLA_RPT3: RONA - GL Analysis Retail SQL_ID: 3tkf473cqucw2
--e:SQLGL:cp:xxr/XXR_GL_SLA_RPT2: RONA GL Logistic SQL_ID:2ca2hxx7590f3     bad plan : 2999942917   selected good plan : 1137602925
--2ca2hxx7590f3 --BAD Plan hash value: 1217733101

select ADDRESS, HASH_VALUE, PLAN_HASH_VALUE, MODULE, ELAPSED_TIME, DISK_READS, USER_IO_WAIT_TIME, sql_profile, OBJECT_STATUS, SQL_TEXT  from v$sql where sql_id = 'bwm53fxa4ckra' order by ELAPSED_TIME desc;
select ADDRESS, HASH_VALUE, PLAN_HASH_VALUE, MODULE, ELAPSED_TIME, DISK_READS, USER_IO_WAIT_TIME, sql_profile, OBJECT_STATUS, SQL_TEXT  from v$sql where sql_id = 'fj2vuxgbxgx2b' order by ELAPSED_TIME desc;
select ADDRESS, HASH_VALUE, PLAN_HASH_VALUE, MODULE, ELAPSED_TIME, DISK_READS, USER_IO_WAIT_TIME, sql_profile, OBJECT_STATUS, SQL_TEXT  from v$sql where sql_id = '3tkf473cqucw2';
--OBJECT_STATUS
--Plan_HASH_VALUE BAD!!: 612266679
-- 00000011273FFAD0	1413892842	1097484155	e:AR:cp:xla/XLABAPUB	68924723744	3763153	3697992070		VALID	INSERT INTO xla_ctrl_bal_interim_gt (                                                application_id                                              , ledger_id                                              , code_combination_id                                              , party_type_code                                              , party_id                                              , party_site_id                                              , period_name                                              , effective_period_num                                              , period_balance_dr                                              , period_balance_cr                                              , period_year                                             )                                    SELECT   /*+  use_nl(aeh) use_nl(ael) */                                                ael.application_id                                              , ael.ledger_id                                

select * from V$SQL_PLAN where sql_id='bwm53fxa4ckra';
--Plan hash value: 1137602925  HASH Value : 1314161091
select * from V$SQL_PLAN where sql_id='3tkf473cqucw2';
--Plan hash value: 74079347  HASH Value : 3647812482
select * from V$SQL_PLAN where sql_id='fj2vuxgbxgx2b';

--Generate new Plan:
--cd /staging/scripts/oracle/sqlt/utl	
--sqlplus / as sysdba	
--	@coe_xfr_sql_profile.sql
--coe_xfr_sql_profile_2ca2hxx7590f3_1137602925.sql generated
-- pwd: /staging/scripts/oracle/sqlt/utl


select ADDRESS, HASH_VALUE, PLAN_HASH_VALUE from V$SQLAREA where SQL_ID = 'bwm53fxa4ckra';
-- Dans SQL Area on a le BAD! :
-- ADDRESS            HASH_VALUE   PLAN_HASH_VALUE

--00000011273FFAD0	1413892842	1097484155
--check ADDRESS and HASH_VALUE for PURGE
select ADDRESS, HASH_VALUE, PLAN_HASH_VALUE from V$SQLAREA where SQL_ID = '3tkf473cqucw2';
-- Dans SQL Area on a le BAD! :
-- ADDRESS            HASH_VALUE   PLAN_HASH_VALUE
--000000118B5BE480	3647812482	612266679
select ADDRESS, HASH_VALUE, PLAN_HASH_VALUE from V$SQLAREA where SQL_ID = 'fj2vuxgbxgx2b';
0000000FFC2FCF10	3621254219	2175235308
0000000FFC2FCF10	3621254219	2761763819
0000000FFC2FCF10	3621254219	2761763819
0000000FFC2FCF10	3621254219	2761763819
0000000FFC2FCF10	3621254219	2761763819
0000000FFC2FCF10	3621254219	2458404945

--@coe_xfr_sql_profile_fj2vuxgbxgx2b_2761763819.sql
-- @coe_xfr_sql_profile_3tkf473cqucw2_74079347.sql
--@coe_xfr_sql_profile_2ca2hxx7590f3_1137602925.sql



--Enlever le mauvais plan du shared pool de SEBSPRD pour SQL_ID:2ca2hxx7590f3  use ADDRESS and HASH_VALUE :
exec DBMS_SHARED_POOL.PURGE ('00000011273FFAD0, 1413892842', 'C');

--Enlever le mauvais plan du shared pool de SEBSPRD pour SQL_ID:fj2vuxgbxgx2b  use ADDRESS and HASH_VALUE :
exec DBMS_SHARED_POOL.PURGE ('0000000FFC2FCF10, 3621254219', 'C');
	-- Check againt it should be clean :
  select ADDRESS, HASH_VALUE, PLAN_HASH_VALUE, MODULE, ELAPSED_TIME, DISK_READS, USER_IO_WAIT_TIME, SQL_TEXT, sql_profile  from v$sql where sql_id = '2ca2hxx7590f3' order by ELAPSED_TIME desc;

--transporter le plan:
--cd /staging/scripts/oracle/sqlt/utl
--sqlplus / as sysdba


--@coe_xfr_sql_profile_fj2vuxgbxgx2b_2761763819.sql
-- @coe_xfr_sql_profile_3tkf473cqucw2_74079347.sql
--@coe_xfr_sql_profile_2ca2hxx7590f3_1137602925.sql
--@coe_xfr_sql_profile_bwm53fxa4ckra_3124606464.sql

-- IF ERROR at line 1:
--ORA-13841: SQL profile named coe_fj2vuxgbxgx2b_2761763819 already exists for a different signature/category pair
--ORA-06512: at "SYS.DBMS_SQLTUNE_INTERNAL", line 17518
--ORA-06512: at "SYS.DBMS_SQLTUNE", line 7858
--ORA-06512: at "SYS.DBMS_SQLTUNE", line 7828
--ORA-06512: at line 670

--Query to show sql_ids related to SQL Profiles
	select distinct 
	p.name sql_profile_name,
	s.sql_id
	from 
	dba_sql_profiles p,
	DBA_HIST_SQLSTAT s
	where
p.name=s.sql_profile;

    --drop SQL profile named coe_fj2vuxgbxgx2b_2761763819
    --EXEC DBMS_SQLTUNE.DROP_SQL_PROFILE('coe_fj2vuxgbxgx2b_2761763819');
    
    -- re run @coe_xfr_sql_profile_fj2vuxgbxgx2b_2761763819.sql
    -- OK  ... manual custom SQL Profile has been created


--5. Demander de relancer le rapport afin de s'assurer qu'il prends le nouveau plan.
select * from table(dbms_xplan.display_cursor('bwm53fxa4ckra')); -- le Bon Plan_hash_value fixe etait: 1137602925 - OK


select * from table(dbms_xplan.display_cursor('fj2vuxgbxgx2b'));
select * from table(dbms_xplan.display_cursor('3tkf473cqucw2'));
--Plan hash value: 74079347
select * from V$SQL_PLAN where sql_id='bwm53fxa4ckra';
select * from V$SQL_PLAN where sql_id='3tkf473cqucw2';
select * from V$SQL_PLAN where sql_id='2ca2hxx7590f3';


50308, PERIOD, Y, P12-16, P12-16, , , , 001-000000-0000-21625-000-0000, 001-000000-0000-21625-999-0000, 001-000000-0000-21635-000-0000, 001-000000-0000-21635-999-0000, Y, Y, N, Finance, N
--	6.load_plans_from_cursor to dba_sql_plan_baselines
	
		declare v_sql_plan_id pls_integer;
		begin
		v_sql_plan_id := dbms_spm.load_plans_from_cursor_cache(
		sql_id => 'bwm53fxa4ckra');
		end;
		/
    
    declare v_sql_plan_id pls_integer;
		begin
		v_sql_plan_id :=dbms_spm.load_plans_from_cursor_cache(
 sql_id => 'bwm53fxa4ckra',
 plan_hash_value => 3124606464);
 end;
 /
 --sql_handle => '*****************�);
 
 --DROP BASELINE
 SET SERVEROUTPUT ON
DECLARE
  l_plans_dropped  PLS_INTEGER;
BEGIN
  l_plans_dropped := DBMS_SPM.drop_sql_plan_baseline (
    sql_handle => NULL,
    plan_name  => 'SQL_PLAN_8z366fum04qmu96f0bb07');
    
  DBMS_OUTPUT.put_line(l_plans_dropped);
END;
/
    
-- CHECK
	select 
	sql_handle, 
	plan_name, 
	enabled, 
	accepted,
  fixed,
  origin
  from 
	dba_sql_plan_baselines
	where LOWER(sql_text) like LOWER('%INSERT into xla_ctrl_bal_interim_gt%');
  --SQL_867e032e25690150	SQL_PLAN_8czh35skqk0ahed62ba3a	YES	YES	NO	MANUAL-LOAD
  SQL_867e032e25690150	SQL_PLAN_8czh35skqk0ahed62ba3a	YES	YES	NO	MANUAL-LOAD
  -- BAD PLAN TO DISABLE
  --SQL_8f8cc676a6025a7a	SQL_PLAN_8z366fum04qmu96f0bb07	YES	YES	NO	MANUAL-LOAD
  
-- Plan name desired to fix: SQL_PLAN_8z366fum04qmu7bad8939   and SQL_HANDLE: SQL_8f8cc676a6025a7a
-- FIX PLAN
--SQL_8f8cc676a6025a7a	SQL_PLAN_8z366fum04qmu7bad8939	YES	YES	NO	MANUAL-LOAD
--
		SET SERVEROUTPUT ON
		
		DECLARE
		l_plans_altered PLS_INTEGER;
		BEGIN
		l_plans_altered := DBMS_SPM.alter_sql_plan_baseline(
		sql_handle => 'SQL_867e032e25690150',
		plan_name => 'SQL_PLAN_8czh35skqk0ahed62ba3a',
		attribute_name =>'fixed',
		attribute_value => 'YES');
		DBMS_OUTPUT.put_line('Plans Altered: ' || l_plans_altered);
		END;
		/


--FORCE DISABLE PLAN
DECLARE
		l_plans_altered NUMBER;
		BEGIN
		l_plans_altered := DBMS_SPM.ALTER_SQL_PLAN_BASELINE(
     SQL_HANDLE => 'SQL_8f8cc676a6025a7a',
     PLAN_NAME
      => 'SQL_PLAN_8z366fum04qmu96f0bb07',
    ATTRIBUTE_NAME => 'enabled',
       ATTRIBUTE_VALUE => 'NO'); 
    DBMS_OUTPUT.put_line('Plans Altered: ' || l_plans_altered);
		END;
		/
 -- UNACCEPT PLAN   
DECLARE
		l_plans_altered NUMBER;
		BEGIN
		l_plans_altered := DBMS_SPM.ALTER_SQL_PLAN_BASELINE(
     SQL_HANDLE => 'SQL_8f8cc676a6025a7a',
     PLAN_NAME
      => 'SQL_PLAN_8z366fum04qmu96f0bb07',
    ATTRIBUTE_NAME => 'accepted',
       ATTRIBUTE_VALUE => 'NO'); 
    DBMS_OUTPUT.put_line('Plans Altered: ' || l_plans_altered);
		END;
		/

--########################
select 
signature,
sql_handle, 
plan_name, 
enabled, 
accepted,
fixed
from 
dba_sql_plan_baselines
where fixed ='YES';
-- SQL_cc58509cf2fa1b1c	SQL_PLAN_csq2hmmtgn6sw3dda46d5	YES	YES	YES	MANUAL-LOAD  Pour fj2vuxgbxgx2b
-- SQL_86ebb8c4c6fa9f8b	SQL_PLAN_8duxssm3gp7wb7c7bb961	YES	YES	YES	MANUAL-LOAD  pour 3tkf473cqucw2
-- SQL_8f8cc676a6025a7a	SQL_PLAN_8z366fum04qmu7c45e77e	YES	YES	YES MANUAL-LOAD  pour 2ca2hxx7590f3

--Accept a PLAN
SET LONG 10000
SELECT DBMS_SPM.evolve_sql_plan_baseline(sql_handle => 'SQL_cc58509cf2fa1b1c') FROM   dual;

--FIX The Plan
		SET SERVEROUTPUT ON
		DECLARE
		l_plans_altered PLS_INTEGER;
		BEGIN
		l_plans_altered := DBMS_SPM.alter_sql_plan_baseline(
		sql_handle => 'SQL_86ebb8c4c6fa9f8b',
		plan_name => 'SQL_PLAN_8duxssm3gp7wb7c7bb961',
		attribute_name =>'fixed',
		attribute_value => 'YES');
		DBMS_OUTPUT.put_line('Plans Altered: ' || l_plans_altered);
		END;
		/


--Query to show sql_ids related to SQL Profiles
select distinct 
p.name sql_profile_name,
s.sql_id
from 
dba_sql_profiles p,
DBA_HIST_SQLSTAT s
where
p.name=s.sql_profile;

--
select ADDRESS, HASH_VALUE, PLAN_HASH_VALUE from V$SQLAREA where SQL_ID = 'fj2vuxgbxgx2b'; 
-- Dans SQL Area on a le Good Plan! :
-- ADDRESS            HASH_VALUE   PLAN_HASH_VALUE
--0000000A75FC6D10	1314161091	1137602925


Plan Details:
select * from DBA_SQL_PLAN_BASELINES where sql_handle='SQL_8f8cc676a6025a7a';

select plan_table_output
from table(dbms_xplan.display_cursor('fj2vuxgbxgx2b',null,'basic'));


--select t.* from
--table(dbms_xplan.display_sql_plan_baseline('SQL_PLAN_8duxssm3gp7wb7c7bb961',
                                           format => 'basic')) t;


SELECT parameter_name, parameter_value
FROM   dba_sql_management_config;

SELECT sql_handle, plan_name,LAST_EXECUTED,CREATED, enabled,accepted,fixed
FROM dba_sql_plan_baselines
WHERE signature IN ( SELECT exact_matching_signature FROM v$sql WHERE sql_id in ('fj2vuxgbxgx2b','3tkf473cqucw2','2ca2hxx7590f3'));



