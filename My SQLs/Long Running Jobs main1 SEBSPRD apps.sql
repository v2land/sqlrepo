--Select statistics of specified Concurrent Request Programs Count MIN MAX AVG  FORMAT Days +
SELECT P.CONCURRENT_PROGRAM_NAME,
         PT.USER_CONCURRENT_PROGRAM_NAME,
         COUNT (*),
            TRUNC (MAX (F.ACTUAL_COMPLETION_DATE - F.ACTUAL_START_DATE))
         || ' Days'
         || ' + '
         || TO_CHAR (
               TRUNC (SYSDATE)
               + NUMTODSINTERVAL (
                    MAX ( (F.ACTUAL_COMPLETION_DATE - F.ACTUAL_START_DATE))
                    * 86400,
                    'second'),
               'HH24:MI:SS')
            "MAXIMUM",
            TRUNC (MIN (F.ACTUAL_COMPLETION_DATE - F.ACTUAL_START_DATE))
         || ' Days'
         || ' + '
         || TO_CHAR (
               TRUNC (SYSDATE)
               + NUMTODSINTERVAL (
                    MIN ( (F.ACTUAL_COMPLETION_DATE - F.ACTUAL_START_DATE))
                    * 86400,
                    'second'),
               'HH24:MI:SS')
            "MINIMUM",
            TRUNC (AVG (F.ACTUAL_COMPLETION_DATE - F.ACTUAL_START_DATE))
         || ' Days'
         || ' + '
         || TO_CHAR (
               TRUNC (SYSDATE)
               + NUMTODSINTERVAL (
                    AVG ( (F.ACTUAL_COMPLETION_DATE - F.ACTUAL_START_DATE))
                    * 86400,
                    'second'),
               'HH24:MI:SS')
            "AVERAGE",
           TO_CHAR(
               TRUNC (SYSDATE)
               + NUMTODSINTERVAL (
               --mean, max, min, median, mode and standard deviation 
                    STATS_MODE( (F.ACTUAL_COMPLETION_DATE - F.ACTUAL_START_DATE))
                    * 24*60,
                    'minute'),
               'MI:SS')
            "STATS_MODE"  
            ,
           TO_CHAR(
               TRUNC (SYSDATE)
               + NUMTODSINTERVAL (
               --mean, max, min, median, mode and standard deviation 
                    STDDEV( (F.ACTUAL_COMPLETION_DATE - F.ACTUAL_START_DATE))
                    * 24*60,
                    'minute'),
               'MI:SS')
            "STDDEV"
    FROM APPLSYS.FND_CONCURRENT_REQUESTS F,
         APPLSYS.FND_CONCURRENT_PROGRAMS P,
         APPLSYS.FND_USER A,
         APPLSYS.FND_CONCURRENT_PROGRAMS_TL PT
   WHERE     PHASE_CODE = 'C'
         AND STATUS_CODE = 'C'
         AND F.CONCURRENT_PROGRAM_ID = P.CONCURRENT_PROGRAM_ID
         AND P.CONCURRENT_PROGRAM_ID = PT.CONCURRENT_PROGRAM_ID
         AND A.USER_ID(+) = F.REQUESTED_BY
         AND PT.language = USERENV('Lang')
         and (P.CONCURRENT_PROGRAM_NAME in ('XXRFND_SUBMIT_REQ_SET', 'APXPBASL','POXPOPDF','XXR_GL_SLA_RPT','XXR_GL_SLA_RPT2','XLAACCPB','XXRHR_EMPL_EXTRACT_CURRENT','XXRHR_OHR_CER_INT_TC_FILE','FNDWFBG')
         or PT.USER_CONCURRENT_PROGRAM_NAME in ('Invoice Approval Workflow'))
       /*  and P.CONCURRENT_PROGRAM_NAME in(SELECT DISTINCT 
                                        b.CONCURRENT_PROGRAM_NAME
                                        FROM
                                        apps.fnd_concurrent_requests a
                                        ,apps.fnd_concurrent_programs b
                                        --,apps.FND_CONCURRENT_PROGRAMS_TL c
                                        WHERE
                                        a.concurrent_program_id=b.concurrent_program_id
                                        --AND b.concurrent_program_id=c.concurrent_program_id
                                        and a.status_code='R'
                                        --AND c.language = 'US')--USERENV('Lang')
                                        )     */                       
GROUP BY P.CONCURRENT_PROGRAM_NAME, PT.USER_CONCURRENT_PROGRAM_NAME;

-- Select AVG of specific request in Minutes
SELECT 
         --P.CONCURRENT_PROGRAM_NAME,
         --PT.USER_CONCURRENT_PROGRAM_NAME,
      --     TRUNC (AVG (F.ACTUAL_COMPLETION_DATE - F.ACTUAL_START_DATE))
        -- || ' Days'
        -- || ' + '
         --||
         TO_CHAR (
               TRUNC (SYSDATE)
               + NUMTODSINTERVAL (
                    AVG ( (F.ACTUAL_COMPLETION_DATE - F.ACTUAL_START_DATE))
                    * 1440,
                    'minute'),
               'MI')
            "AVERAGE"
    FROM APPLSYS.FND_CONCURRENT_REQUESTS F,
         APPLSYS.FND_CONCURRENT_PROGRAMS P
       --  ,APPLSYS.FND_USER A,
         --APPLSYS.FND_CONCURRENT_PROGRAMS_TL PT
   WHERE     PHASE_CODE = 'C'
         AND STATUS_CODE = 'C'
         AND F.CONCURRENT_PROGRAM_ID = P.CONCURRENT_PROGRAM_ID
         --AND P.CONCURRENT_PROGRAM_ID = PT.CONCURRENT_PROGRAM_ID
         --AND A.USER_ID(+) = F.REQUESTED_BY
         --AND PT.language = USERENV('Lang')
         and P.CONCURRENT_PROGRAM_NAME in ('XXR_GL_SLA_RPT2')                            
GROUP BY P.CONCURRENT_PROGRAM_NAME
--, PT.USER_CONCURRENT_PROGRAM_NAME
;