--1. Verify if you have any existing system generated notifications that may need to be closed and purged.
select item_type, root_activity, count(item_key)
from wf_items
where item_type='WFERROR'
and root_activity like 'DEFAULT_EVENT%'
and parent_item_type is null
--and begin_date < sysdate-30
and end_date is null
group by item_type, root_activity;
--3170

--2. Verify if any of the System Generated WFERROR notifications are recent or still occurring
select n.message_name, substr(n.subject,0,instr(n.subject,'/')) "SUBJECT", 
       to_char(n.begin_date,'MON-DD') "BEGAN", count(n.notification_id) COUNT
from wf_notifications n
where n.message_type = 'WFERROR'
and n.message_name like 'DEFAULT_EVENT%'
and n.begin_date > sysdate-14
and n.end_date is null
group by n.message_name, substr(n.subject,0,instr(n.subject,'/')), 
         to_char(n.begin_date,'MON-DD')
order by COUNT desc;

--3. Verify what the Mailer Parameter "SEND_CANCELED_EMAIL" is set to :
SELECT sc.COMPONENT_ID, SC.COMPONENT_NAME, v.COMPONENT_PARAMETER_ID ID, v.parameter_name, v.PARAMETER_VALUE, v.PARAMETER_DISPLAY_NAME 
FROM FND_SVC_COMP_PARAM_VALS_V v, FND_SVC_COMPONENTS SC 
WHERE v.COMPONENT_ID=sc.COMPONENT_ID AND sc.COMPONENT_TYPE = 'WF_MAILER' 
--AND v.parameter_name IN ('OUTBOUND_PROTOCOL','OUTBOUND_SERVER','PROCESSOR_OUT_THREAD_COUNT','AUTOCLOSE_FYI','SEND_CANCELED_EMAIL') 
AND v.parameter_name = 'SEND_CANCELED_EMAIL' ORDER BY sc.COMPONENT_ID, v.parameter_name;

4-- sqlplus apps/<apps_pwd> @/TEMP_DBA/vlcasapc/WFERROR/bde_end_date_wf_ntfs_event_errors.sql

--5. Verify the counts again by re-running the WF Analyzer or use this query :

select item_type, root_activity, count(item_key)
from wf_items
where item_type='WFERROR'
and root_activity like 'DEFAULT_EVENT%'
and parent_item_type is null
--and begin_date < sysdate-30
and end_date is null
group by item_type, root_activity;

--6. additional
select h.header_id, h.order_number, h.org_id, h.rowid
from oe_order_headers_all h
where h.open_flag = 'Y'
and not exists (
select 1 from wf_items i
where i.item_type = 'OEOH'
and i.item_key = to_char(h.header_id)
and i.end_date is null)
and not exists (
select 1 from oe_order_lines_all l
where l.open_flag = 'Y'
and l.header_id = h.header_id)
and not exists (
select 1 from wf_items i
where i.item_type = 'OEOL'
and i.parent_item_key = to_char(h.header_id)
and i.parent_item_type = 'OEOH'
and i.end_date is null);