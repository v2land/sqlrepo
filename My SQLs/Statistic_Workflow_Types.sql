--By Hour :
SELECT item_type, to_char(begin_date,'DD-MON-YYYY HH24') begin_date, count(item_type) count
--item_type,item_key, to_char(begin_date,'DD-MON-RR HH24:MI:SS') begin_date, to_char(end_date,'DD-MON-RR HH24:MI:SS') end_date, root_activity activity
FROM apps.wf_items
WHERE item_type in ('OEOL')-- :item_type
and begin_date > to_date(:s_date_YYYY_MM_DD_HH24_MI_SS, 'DD-MON-YYYY HH24:MI:SS')
and begin_date <= to_date(:e_date_YYYY_MM_DD_HH24_MI_SS, 'DD-MON-YYYY HH24:MI:SS')
AND end_date IS NULL
GROUP BY item_type, to_char(begin_date,'DD-MON-YYYY HH24')
--HAVING Count(item_type) >= 1
ORDER BY to_date(begin_date,'DD-MON-YYYY hh24') DESC;

--Daily :
SELECT item_type, to_char(begin_date,'DD-MON-YYYY') begin_date, count(item_type) count
--item_type,item_key, to_char(begin_date,'DD-MON-RR HH24:MI:SS') begin_date, to_char(end_date,'DD-MON-RR HH24:MI:SS') end_date, root_activity activity
FROM apps.wf_items
WHERE item_type in ('OEOL')-- :item_type
and begin_date > to_date(:s_date_YYYY_MM_DD_HH24_MI_SS, 'DD-MON-YYYY HH24:MI:SS')
and begin_date <= to_date(:e_date_YYYY_MM_DD_HH24_MI_SS, 'DD-MON-YYYY HH24:MI:SS')
AND end_date IS NULL
GROUP BY item_type, to_char(begin_date,'DD-MON-YYYY')
--HAVING Count(item_type) >= 1
ORDER BY to_date(begin_date,'DD-MON-YYYY hh24:mi:ss') DESC;

SELECT item_type, trunc(begin_date), count(*)
FROM apps.wf_items
WHERE item_type in ('OEOL')-- :item_type
and begin_date > to_date(:s_date_YYYY_MM_DD_HH24_MI_SS, 'DD-MON-YYYY HH24:MI:SS')
and begin_date <= to_date(:e_date_YYYY_MM_DD_HH24_MI_SS, 'DD-MON-YYYY HH24:MI:SS')
AND end_date IS NULL
GROUP BY item_type, trunc(begin_date)
ORDER BY trunc(begin_date) DESC;

--Monthly
SELECT item_type, to_char(begin_date,'MON-YYYY') begin_date, count(item_type) count
--item_type,item_key, to_char(begin_date,'DD-MON-RR HH24:MI:SS') begin_date, to_char(end_date,'DD-MON-RR HH24:MI:SS') end_date, root_activity activity
FROM apps.wf_items
WHERE item_type = :item_type
and begin_date > to_date(:s_date_YYYY_MM_DD_HH24_MI_SS, 'DD-MON-YYYY HH24:MI:SS')
and begin_date <= to_date(:e_date_YYYY_MM_DD_HH24_MI_SS, 'DD-MON-YYYY HH24:MI:SS')
AND end_date IS NULL
GROUP BY item_type, to_char(begin_date,'MON-YYYY')
--HAVING Count(item_type) >= 1
ORDER BY to_date(begin_date,'MON-YYYY') DESC;