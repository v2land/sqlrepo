select * from wf_item_types where num_active >0 order by 1 ;
--5492
--75460

select * from wf_item_types_tl where name in ('AMSGAPP');

select * from wf_items where begin_date >sysdate - 200 and  item_type in ('AMSGAPP');-- and user_key like '%2200036914%' order by begin_date desc; -- ITEM_KEY:56757994

select * from wf_item_attributes where item_type in ('OEOL','OEOH');

select * from wf_item_attribute_values where item_type in ('OEOL','OEOH');

select * from wf_activities  where  item_type in ('AMSGAPP') order by begin_date desc;

select * from wf_activity_attributes where activity_item_type in ('OEOL','OEOH');

SELECT * FROM WF_NOTIFICATIONS where begin_date >sysdate - 10 and CONTEXT like '%OEOH%' ;

SELECT * FROM WF_NOTIFICATIONS_out;

select * from WF_NOTIFICATION_attributes;

select * from wf_messages where type in ('OEOL','OEOH');

select * from wf_process_Activities where process_item_type in ('OEOL','OEOH') order by 3 desc,6 desc;

select * from wf_items where begin_date >sysdate - 10 and  item_type in ('OEOL','OEOH') and user_key like '%2200036362%' order by begin_date desc; -- ITEM_KEY:56757994
select * from wf_item_activity_statuses where item_type in ('OEOL','OEOH') and begin_date >sysdate - 1 and ACTIVITY_RESULT_CODE = 'ON_HOLD';


select item_type, activity_status, count(*)	from wf_item_activity_statuses where item_type in ('OEOL','OEOH') group by item_type,activity_status;
/*
APIMP	COMPLETE	32720
ARCMGTAP	COMPLETE	86429
ARCMGTAP	NOTIFIED	12347
OENH	COMPLETE	13720
  OEOH	COMPLETE	1576955
GMDQMSMC	COMPLETE	1838763
OZF_CSTL	COMPLETE	2342355
REQAPPRV	COMPLETE	198001
AMSGAPP	COMPLETE	844125
APWRECPT	COMPLETE	851
WFERROR	ERROR	1460
WFTESTS	NOTIFIED	14
POERROR	ERROR	1
POAPPRV	WAITING	6
OENH	NOTIFIED	8
APWHOLDS	COMPLETE	28
UMXNTWF2	ACTIVE	4
UMXNTWF2	COMPLETE	12
APWEXPRT	COMPLETE	80
RCVDMEMO	COMPLETE	4
UMXPXYNF	COMPLETE	996
APINVHDN	COMPLETE	17293
  OEOH	DEFERRED	2412
GMDQMSMC	ERROR	243
OEOL	NOTIFIED	31402
AMSGAPP	ACTIVE	614
AMSGAPP	NOTIFIED	262
OENH	ACTIVE	12
UMXREGWF	COMPLETE	696
POAPPRV	DEFERRED	4
OEOL	ACTIVE	63538
UMXLHELP	ERROR	2
APCCARD	ERROR	1460
AMSGAPP	ERROR	178
APINVHDN	DEFERRED	183
UMXUPWD	COMPLETE	24
OEOL	COMPLETE	50462116
OECHGORD	COMPLETE	4576
WFERROR	NOTIFIED	91
POWFPOAG	NOTIFIED	55
UMXREGWF	ACTIVE	8
OEBH	ACTIVE	1370
APEXP	COMPLETE	93296
  OEOH	ACTIVE	9443
APINVHDN	NOTIFIED	1168
APINVHDN	ACTIVE	677
WFERROR	COMPLETE	7141
POWFPOAG	ACTIVE	55
GMDQMSMC	ACTIVE	243
APCCARD	ACTIVE	1460
APCCARD	COMPLETE	1460
APVRMDER	COMPLETE	3171
WFTESTS	COMPLETE	18
UMXNTWF2	ERROR	4
UMXREGWF	NOTIFIED	4
AMEUPDUN	COMPLETE	28
  OEOL	DEFERRED	721
APVRMDER	NOTIFIED	845
POAPPRV	ACTIVE	465
APEXP	NOTIFIED	190
CREATEPO	COMPLETE	65304
UMXLHELP	COMPLETE	152
POAPPRV	ERROR	10
POERROR	COMPLETE	76
APINVHDN	ERROR	2
POAPPRV	COMPLETE	1805936
OEBH	COMPLETE	5320
ARCMGTAP	ACTIVE	12347
APVRMDER	ACTIVE	845
OEOH	NOTIFIED	2312
POAPPRV	NOTIFIED	207
WFERROR	ACTIVE	1462
WFTESTS	ACTIVE	8
OMERROR	COMPLETE	44
OEBH	NOTIFIED	685
POWFPOAG	COMPLETE	3304
APEXP	ACTIVE	330
UMXLHELP	ACTIVE	2
WFTESTS	WAITING	2
UMXNTWF1	COMPLETE	154
*/
select * from WF_LOOKUP_TYPES_TL;


SELECT A.*, b.*
FROM APPS.WF_ITEM_ACTIVITY_STATUSES A,
apps.WF_PROCESS_ACTIVITIES B
WHERE A.PROCESS_ACTIVITY = B.INSTANCE_ID(+)
AND B.PROCESS_ITEM_TYPE = A.ITEM_TYPE
AND A.item_type in ('OEOL','OEOH')
AND A.ITEM_KEY = '56757994' 
order by instance_id desc;

SELECT A.*, b.*
FROM APPS.WF_ITEM_ACTIVITY_STATUSES A,
apps.WF_PROCESS_ACTIVITIES B
WHERE A.PROCESS_ACTIVITY = B.INSTANCE_ID(+)
And B.Process_Item_Type = A.Item_Type
AND A.ITEM_TYPE = 'POAPPRV'
And A.Item_Key = &p_item_key
AND INSTANCE_LABEL=&p_label_name
order by instance_id desc;

--MONITORING of FNDWFBG;
select w.user_data.itemtype "Item Type", w.user_data.itemkey "Item Key", 
decode(w.state, 0, '0 = Ready',
1, '1 = Delayed',
2, '2 = Retained',
3, '3 = Exception',
to_char(w.state)) State,
w.priority, w.ENQ_TIME, w.DEQ_TIME, w.msgid
from wf_deferred_table_m w
where w.user_data.itemtype = '&item_type';

--
SELECT wfdtm.corrid, wfdtm.user_data.ITEMTYPE ITEM_TYPE, 
wfdtm.user_data.ITEMKEY ITEM_KEY, wfdtm.enq_time, 
DECODE(wfdtm.state, 
0, '0 = Ready', 
1, '1 = Delayed', 
2, '2 = Retained', 
3, '3 = Exception', 
TO_CHAR(SUBSTR(wfdtm.state,1,12))) State 
FROM wf_deferred_table_m wfdtm 
WHERE wfdtm.state = 0 
ORDER BY wfdtm.priority, wfdtm.enq_time; 
/*
WF TABLES

WF_ITEM_TYPES
The WF_ITEM_TYPES table defines an item that is transitioning through a workflow process. NAME (PK), PROTECT_LEVEL, CUSTOM_LEVEL, PERSISTENCE_TYPE

WF_ITEM_ATTRIBUTES
The WF_ITEM_ATTRIBUTES table stores definitions of attributes associated with a process. Each row includes the sequence in which the attribute is used as well as the format of the attribute data. ITEM_TYPE (PK), NAME (PK), SEQUENCE, TYPE, PROTECT_LEVEL, CUSTOM_LEVEL

WF_ACTIVITIES
WF_ACTIVITIES table stores the definition of an activity. Activities can be processes, notifications, functions or folders.ITEM_TYPE (PK), NAME (PK), VERSION(PK), TYPE, RERUN, EXPAND_ROLE, PROTECT_LEVEL, CUSTOM_LEVEL, BEGIN_DATE, RROR_ITEM_TYPE, RUNNABLE_FLAG

WF_ACTIVITY_ATTRIBUTES
The WF_ACTIVITY_ATTRIBUTES table defines attributes which behave as parameters for an activity. Activity attributes are only used by function activities.Examples of valid attribute types are DATE, DOCUMENT, FORM, ITEMATTR, LOOKUP, and VARCHAR2.
ACTIVITY_ITEM_TYPE (PK), ACTIVITY_NAME (PK), ACTIVITY_VERSION (PK), NAME (PK), SEQUENCE, TYPE, VALUE_TYPE, PROTECT_LEVEL, CUSTOM_LEVEL

WF_MESSAGES
WF_MESSAGES contains the definitions of messages which may be sent out as notifications. TYPE (PK), NAME (PK), PROTECT_LEVEL, CUSTOM_LEVEL

WF_MESSAGE_ATTRIBUTES
WF_MESSAGE_ATTRIBUTES contains message attribute definitions.

WF_NOTIFICATIONS
WF_NOTIFICATIONS holds the runtime information about a specific instance of a sent message. A new row is created in the table each time a message is sent.

WF_NOTIFICATION_ATTRIBUTES
WF_NOTIFICATION_ATTRIBUTES holds rows created for attributes of a notification. When each new notification is created, a notification attribute row is created for each message attribute in the message definition. Initially, the values of the notification attributes are set to the default values specified in the message attribute definition.

WF_ITEMS
WF_ITEMS is the runtime table for workflow processes. Each row defines one work item within the system. ITEM_TYPE (PK), ITEM_KEY (PK), ROOT_ACTIVITY, ROOT_ACTIVITY_VERSION, BEGIN_DATE

WF_ITEM_ACTIVITY_STATUSES
The WF_ITEM_ACTIVITY_STATUSES TABLE is the runtime table for a work item. Each row includes the start and end date, result code, and any error information an activity generates. ITEM_TYPE (PK), ITEM_KEY (PK), PROCESS_ACTIVITY (PK)

WF_ITEM_ACTIVITY_STATUSES_H
The WF_ITEM_ACTIVITY_STATUSES_H table stores the history of the WF_ITEM_ACTIVITY_STATUSES table. ITEM_TYPE, ITEM_KEY, PROCESS_ACTIVITY

WF_PROCESS_ACTIVITIES
WF_PROCESS_ACTIVITIES stores the data for an activity within a specific process. PROCESS_ITEM_TYPE, PROCESS_NAME, PROCESS_VERSION, ACTIVITY_ITEM_TYPE, ACTIVITY_NAME, INSTANCE_ID (PK), INSTANCE_LABEL, PERFORM_ROLE_TYPE, PROTECT_LEVEL, CUSTOM_LEVEL

WF_ACTIVITY_TRANSITIONS
The WF_ACTIVITY_TRANSITIONS table defines the transitions from one activity to another in a process. Each row includes the activities at the beginning and end of the transition, as well as the result code and physical location of the transition in the process window. FROM_PROCESS_ACTIVITY (PK), RESULT_CODE (PK), TO_PROCESS_ACTIVITY (PK), PROTECT_LEVEL,CUSTOM_LEVEL

WF_ACTIVITY_ATTR_VALUES
The WF_ACTIVITY_ATTR_VALUES table contains the data for the activity attributes. Each row includes the process activity id and the associated value for the attribute. PROCESS_ACTIVITY_ID (PK), NAME (PK), VALUE_TYPE, PROTECT_LEVEL, CUSTOM_LEVEL

select * from apps.wf_activities where name ='AP_EXPENSE_REPORT_PROCESS';

SELECT * FROM APPS.WF_ACTIVITY_ATTRIBUTES WHERE ACTIVITY_ITEM_TYPE='APEXP';



most commonly used api's in workflow,


To initiate workflow (creating and starting a process)



WF_ENGINE.CREATEPROCESS (ITEMTYPE IN VARCHAR2,ITEMKEY IN VARCHAR2,PROCESS IN VARCHAR2 DEFAULT );

WF_ENGINE.STARTPROCESS (ITEMTYPE IN VARCHAR2,ITEMKEY IN VARCHAR2);

To set values

WF_ENGINE.SETITEMATTRTEXT(ITEMTYPE IN VARCHAR2,ITEMKEY IN VARCHAR2,ANAME IN VARCHAR2,AVALUE IN VARCHAR2);

WF_ENGINE.SETITEMATTRNUMBER(ITEMTYPE IN VARCHAR2,ITEMKEY IN VARCHAR2,ANAME IN VARCHAR2,AVALUE IN NUMBER);

WF_ENGINE.SETITEMATTRDATE (ITEMTYPE IN VARCHAR2,ITEMKEY IN VARCHAR2,ANAME IN VARCHAR2,AVALUE IN DATE);
 
To get values


WF_ENGINE.GETITEMATTRTEXT(ITEMTYPE IN VARCHAR2,ITEMKEY IN VARCHAR2,ANAME IN VARCHAR2) RETURN VARCHAR2;

WF_ENGINE.GETITEMATTRNUMBER(ITEMTYPE IN VARCHAR2,ITEMKEY IN VARCHAR2,ANAME IN VARCHAR2) RETURN NUMBER;

WF_ENGINE.GETITEMATTRDATE(ITEMTYPE IN VARCHAR2,ITEMKEY IN VARCHAR2,ANAME IN VARCHAR2) RETURN DATE;

To synchronize workflow tables - below program needs to run

Synchronize Workflow LOCAL tables

Meaning of status -


FUNCMODE : For functions, it can be RUN or CANCEL
For notifications, it can be either of RESPOND, FORWARD, TRANSFER or TIMEOUT

RESULTOUT : COMPLETE  - Means Activity is completed successfully.
WAITING : Pending for some other activity
DEFERRED: : Activity deferred till
NOTIFIED::: Activity notified to with a . Externally, must be completed using WF_ENGINE.CompleteActivity.
ERROR: :ACTIVITY ENCOUNTERS AN ERROR
*/